package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;
import fr.etu.m1.miage.festibed.model.HebergementMazen;

public interface HebergementMazenDAO {
	
	public Iterator<HebergementMazen> getHebergement(String ville) throws SQLException;

	public void ajouterHebergement(HebergementMazen hebergement) throws SQLException;
	
	public HebergementMazen getHebergementbyId(String numHebergement) throws SQLException;
	
}
