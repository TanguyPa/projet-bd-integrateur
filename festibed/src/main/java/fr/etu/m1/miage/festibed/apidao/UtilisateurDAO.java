package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;
import fr.etu.m1.miage.festibed.model.Utilisateur;

public interface UtilisateurDAO {

	Iterator<Utilisateur> getUtilisateurs();

	String ajouterUtilisateur(int numPersonne, String nom, String prenom, String telephone, String mail)
			throws SQLException;

	String ajouterUtilisateur(String numPersonne) throws SQLException;

	String getNumUtilisateurById(String numPersonne) throws SQLException;

	String ajouterPersonne(String numPersonne, String nom, String prenom, String telephone, String mail)
			throws SQLException;

	String getNumPersonneById(String numPersonne) throws SQLException;

}
