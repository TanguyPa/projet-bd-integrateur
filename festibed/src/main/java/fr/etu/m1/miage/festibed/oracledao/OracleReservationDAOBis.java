package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.ReservationDAO;
import fr.etu.m1.miage.festibed.apidao.ReservationDAOBis;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.ReservationBis;

public class OracleReservationDAOBis implements ReservationDAOBis {

	@Override
	public Iterator<ReservationBis> recuperationReservationsEnCours(int numUtilisateur) {
		
		String query = "SELECT * " +
					   "FROM Reservation " +
					   "WHERE numUtilisateur = " + numUtilisateur +
					   "	AND etatReservation = 'En cours'";
		
		ResultSet rsReservationEnCours = null;
		try {
			rsReservationEnCours = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		//Reservation reservation = new Reservation(int numReservation, String dateReservation, String dateValidation, String etatReservation, float montantTotal, int nbEnfant, int nbAdulte, int numUtilisateur);
		ArrayList<ReservationBis> reservations = new ArrayList<ReservationBis>();
		try {
			while(rsReservationEnCours.next()) {
				ReservationBis reservation = new ReservationBis(rsReservationEnCours.getInt("numReservation"),			
														  rsReservationEnCours.getString("dateReservation"),
														  rsReservationEnCours.getString("dateValidation"),
														  rsReservationEnCours.getString("etatReservation"),
														  rsReservationEnCours.getFloat("montanTotal"),
														  rsReservationEnCours.getInt("nbEnfant"),
														  rsReservationEnCours.getInt("nbAdulte"),
														  rsReservationEnCours.getInt("numUtilisateur"));
				reservations.add(reservation);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return reservations.iterator();
	}

	@Override
	public boolean verifNbJoursEgauxReservationsEnCours(int numReservation) {
		
		System.out.println("numResa =" + numReservation);
		
		boolean egaux;
		
//		-- On regarde si le nombre de jours de festival est égal au nombre de jours de logement pris --
//		-- Comptage du nombre de jours de réservation des festivals
//		SELECT COUNT(*)
//		FROM FESTIVALRESERVATION f 
//		WHERE NUMRESERVATION = 2 ;
//		-- Comptage du nombre de jours de réservation des logements 
//		SELECT (SUM(rl.DATEFIN - rl.DATEDEBUT)) AS nbJoursReservationLogement
//		FROM RESERVATIONLOGEMENT rl 
//		WHERE rl.NUMRESERVATION = 2 ;

		int nbJoursFestival = 999;
		String query = "SELECT COUNT(*) FROM FESTIVALRESERVATION f WHERE NUMRESERVATION = " + numReservation;
		
		ResultSet rsReservationFestivalEnCours = null;
		try {
			rsReservationFestivalEnCours = OracleBdAccess.select(query);
			rsReservationFestivalEnCours.next();
			nbJoursFestival = rsReservationFestivalEnCours.getInt(1);
			
			System.out.println("nbPlacesFestival = "+nbJoursFestival);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		int nbJoursLogement = 999;
		String query2 = "SELECT (SUM(rl.DATEFIN - rl.DATEDEBUT)+1) AS nbJoursReservationLogement FROM RESERVATIONLOGEMENT rl WHERE rl.NUMRESERVATION = " + numReservation;
		
		ResultSet rsReservationLogementEnCours = null;
		try {
			rsReservationLogementEnCours = OracleBdAccess.select(query2);
			rsReservationLogementEnCours.next();
			nbJoursLogement = rsReservationLogementEnCours.getInt(1);
			
			System.out.println("nbPlacesLogement = "+nbJoursLogement);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(nbJoursFestival - nbJoursLogement == 0) {
			return true;
		}
		
		return false;
		
		
		// Pour les jours
		/*int nbPlacesLogement = 999;
		String query1 = "SELECT SUM(f.NBCATEGORIE0 + f.NBCATEGORIE1 + f.NBCATEGORIE2) FROM FESTIVALRESERVATION f WHERE NUMRESERVATION = " + numUtilisateur;
		System.out.println(query);
		
		ResultSet rsReservationEnCours = null;
		try {
			rsReservationEnCours = OracleBdAccess.select(query);
			nbPlacesFestival = rsReservationEnCours.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		
	}
	

}
