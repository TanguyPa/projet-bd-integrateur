package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.ReservationLogement;

public interface ReservationLogementDAO {
	
	int ajouterReservationLogement(String numReservation, String numLogement, String dateDeb, String dateFin) throws SQLException;
	
	Iterator<ReservationLogement> getReservationLogement(String Reservation);

	ReservationLogement getReservationLogementById(String numFestival,String numReservation, String dateReservation);
}
