package fr.etu.m1.miage.festibed.model;

public class Hebergeur {
	private String numHebergeur;

	public Hebergeur(String numHebergeur) {
		super();
		this.numHebergeur = numHebergeur;
	}

	public String getNumHebergeur() {
		return numHebergeur;
	}

	public void setNumHebergeur(String numHebergeur) {
		this.numHebergeur = numHebergeur;
	}
	
}
