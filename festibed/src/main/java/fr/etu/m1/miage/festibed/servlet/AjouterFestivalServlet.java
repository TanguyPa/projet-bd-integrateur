	package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.FestivalService;

public class AjouterFestivalServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		FestivalService festivalService = new FestivalService();
		try {
			festivalService.ajouterFestival(req.getParameter("nomF"), req.getParameter("dateDeb"),
					req.getParameter("dateFin"), req.getParameter("typeF"), req.getParameter("siteWeb"),
					req.getParameter("codePostal"), req.getParameter("ville"), req.getParameter("pays"),
					req.getParameter("capaciteCateg0"), req.getParameter("capaciteCateg1"),
					req.getParameter("capaciteCateg2"), req.getParameter("noteMoyenne"),
					req.getParameter("tarifCateg0"), req.getParameter("tarifCateg1"), req.getParameter("tarifCateg2"),
					req.getParameter("etat"), req.getParameter("longitude"), req.getParameter("latitude"),
					req.getParameter("numOrganisateur"));
		} catch (SQLException sqlException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;	
		} catch (ParameterErrorException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		resp.setStatus(HttpServletResponse.SC_OK);
	}
}