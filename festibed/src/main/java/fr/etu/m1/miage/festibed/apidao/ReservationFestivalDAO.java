package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.ReservationFestival;

public interface ReservationFestivalDAO {
	
	int ajouterReservationFestival(String numFestival, String numReservation, String nbCateg0, String nbCateg1, String nbCateg2, String dateRes) throws SQLException;
	
	Iterator<ReservationFestival> getReservationFestival(String numReservation);

	ReservationFestival getReservationFestivalById(String numFestival,String numReservation, String dateReservation);
}
