package fr.etu.m1.miage.festibed.apidao;

import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.LogementMazen;

public interface LogementMazenDAO {

	Iterator<LogementMazen> getLogementDisponible(String dateDeb , String dateFin,String numHebergement);
	
}
