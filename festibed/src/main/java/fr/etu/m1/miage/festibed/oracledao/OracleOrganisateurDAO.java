package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.OrganisateurDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.Organisateur;

public class OracleOrganisateurDAO implements OrganisateurDAO {

	@Override
	public Iterator<Organisateur> getOrganisateurs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumOrganisateurById(String numPersonne) {
		
		int resultat = 0;
		ResultSet rs;
		String quest = "select numOrganisateur from Organisateur where numPersonne = '" + numPersonne +"'";
		try {
			rs = OracleBdAccess.select(quest);
			resultat = rs.getInt("numOrganisateur");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultat;
	}


	@Override
	public int ajouterOrganisateur(String numPersonne) throws SQLException {
		String insert = "insert into Organisateur values ( (select max(numOrganisateur) from Organisateur) + 1 ,'" + numPersonne + "')";
		return OracleBdAccess.update(insert);
	}

}
