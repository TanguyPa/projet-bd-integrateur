package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.FestivalService;

public class RechercherFestivalsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp){
	    HttpFormater.formatHeader(resp);
	    resp.setContentType("application/json");
		
		FestivalService festivalService = new FestivalService();
		String jsonResponse;
		try {
			jsonResponse = festivalService.rechercherDesFestivals(req.getParameter("motsCles"), req.getParameter("ville"), req.getParameter("categorie"), req.getParameter("dateDebut"), req.getParameter("dateFin"), req.getParameter("numPage"), req.getParameter("taillePage"));
		} catch (ParameterErrorException parameterErrorException) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		} catch (SQLException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			e.printStackTrace();
			return;
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
        try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
}