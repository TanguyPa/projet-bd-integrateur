package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.HebergementMazenService;

public class RechercherHebergementMazen extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");

		HebergementMazenService reservationService = new HebergementMazenService();
		String jsonResponse = "";
		try {
			String dateD, dateF, ville;
			dateD = (req.getParameter("dateDeb") == null || req.getParameter("dateFin") == null ) ? "" : req.getParameter("dateDeb");
			dateF = (req.getParameter("dateDeb") == null || req.getParameter("dateFin") == null ) ? "" : req.getParameter("dateFin");
			ville = (req.getParameter("ville") == null) ? "" :req.getParameter("ville");
			jsonResponse = reservationService.rechercherHebergementLogement(dateD, dateF, ville);

		} catch (ParameterErrorException parameterErrorException) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		resp.setStatus(HttpServletResponse.SC_OK);
		try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}

}
