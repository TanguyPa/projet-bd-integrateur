package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.JoursLogementFestivalNonEquivalentException;
import fr.etu.m1.miage.festibed.exception.MultipleReservationEnCoursException;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.service.ReservationServiceBis;

public class ValiderPanierServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		String jsonResponse = "";

		// Récupération des paramètres
		int numUtilisateur = Integer.parseInt(req.getParameter("numUtilisateur"));
		
		// Récupération et vérification de la réservation de l'utilisateur (etat "en cours") -> Si pas de résultat alors la resa a expirée
		ReservationServiceBis reservationService = new ReservationServiceBis();
		try {
			reservationService.checkReservationByUser(numUtilisateur);
		} catch (RessourceNotFoundException e) {
			e.printStackTrace();
			jsonResponse += "Aucune réservation n'est disponible pour cet utilisateur";
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
			return ;
		} catch (MultipleReservationEnCoursException e) {
			e.printStackTrace();
			jsonResponse += "Plusieurs réservations sont en cours pour cet utilisateur !";
			resp.setStatus(HttpServletResponse.SC_MULTIPLE_CHOICES);
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
			return;
		} catch (JoursLogementFestivalNonEquivalentException e) {
			e.printStackTrace();
			jsonResponse += "{ \"message\": \"Pas le même nombre de jours réservé pour les logements et les festivals\" }";
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
			return;
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.getWriter().write(jsonResponse);
		resp.getWriter().flush();
		resp.getWriter().close();
	}

}
