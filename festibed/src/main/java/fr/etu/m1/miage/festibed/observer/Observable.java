package fr.etu.m1.miage.festibed.observer;

public interface Observable<T> {
	void addObserver(final Observer<? super T> observer);
	void removeObserver(final Observer<? super T> observer);
	void notifyObservers();
}
