package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.HebergementMazenDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;

import fr.etu.m1.miage.festibed.model.HebergementMazen;

public class OracleHebergementMazenDAO implements HebergementMazenDAO {

	@Override
	public Iterator<HebergementMazen> getHebergement(String ville) throws SQLException {
		String query = "";
		query = "select * from Hebergement where ROWNUM <= 10 and upper(ville) like '%"+ville.toUpperCase()+"%'";
		
		ResultSet rsRes = null;
		rsRes = OracleBdAccess.select(query);
		ArrayList<HebergementMazen> hebergements = new ArrayList<HebergementMazen>();

			while (rsRes.next()) {
				HebergementMazen res = new HebergementMazen(rsRes.getString("numHebergement"),
						rsRes.getString("nomHebergement"), rsRes.getString("noteMoyenne"),
						rsRes.getString("classement"), rsRes.getString("telephone"), rsRes.getString("siteWeb"),
						rsRes.getString("nom_rue"), rsRes.getString("codePostal"), rsRes.getString("ville"),
						rsRes.getString("pays"), rsRes.getString("description"), rsRes.getString("longitude"),
						rsRes.getString("latitude"), rsRes.getString("numHebergeur"));
				hebergements.add(res);
			}
		rsRes.close();
		return hebergements.iterator();
	}

	@Override
	public void ajouterHebergement(HebergementMazen hebergement) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public HebergementMazen getHebergementbyId(String numHebergement) throws SQLException {
		String query ="select * from Hebergement where numHebergement = " + numHebergement; 
		
		ResultSet rsRes = null;
		HebergementMazen res = null;
		
     	try {
			rsRes = OracleBdAccess.select(query);
			rsRes.next();
		    res = new HebergementMazen(
					rsRes.getString("numHebergement"),			
					rsRes.getString("nomHebergement"),
					rsRes.getString("noteMoyenne"),
					rsRes.getString("classement"),
					rsRes.getString("telephone"),
					rsRes.getString("siteWeb"),
					rsRes.getString("nom_rue"),
					rsRes.getString("codePostal"),
					rsRes.getString("ville"),
					rsRes.getString("pays"),
					rsRes.getString("description"),
					rsRes.getString("longitude"),
					rsRes.getString("latitude"),
					rsRes.getString("numHebergeur"));
		} finally {
			rsRes.close();
		}
			
		return res;
	}
	
}
