package fr.etu.m1.miage.festibed.observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PanierObservable implements Observable<Integer> {

	private int idResa;
	
	private Timer timer;
	
    private List<Observer<?super Integer>> observers = Collections.synchronizedList(new ArrayList<Observer<?super Integer>>());
    
	public PanierObservable(int idResa) {
		this.timer = new Timer();
		this.timer.schedule( new TimerTask() {
			
			@Override
			public void run() {
				System.out.println("saLUTTTjherjezh");
				notifyObservers();
				timer.cancel();
			}
		},1*60*1000);
		this.idResa = idResa;
	}

	@Override
	public void addObserver(Observer<?super Integer> observer) {
		this.observers.add(observer);
		
	}

	@Override
	public void removeObserver(Observer<?super Integer> observer) {
		this.observers.remove(observer);		
	}

	@Override
	public void notifyObservers() {
		for(Observer<?super Integer> o: this.observers) {
			o.update(this, this.idResa);
			// Une fois qu'on a appelé la méthode on supprime l'observer (plus de lien avec l'objet)
			// Car la résa a été vérifié pour son expiration
			
			
			//this.removeObserver(o);
		}
	}


}
