package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.HebergementMazenService;

public class RechercherUnHebergementServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
	    HttpFormater.formatHeader(resp);
	    resp.setContentType("application/json");
		HebergementMazenService hebergementService = new HebergementMazenService();
		
		String jsonResponse;
		try {
			jsonResponse = hebergementService.rechercherUnHebergementLogement(req.getParameter("numHebergement"));
		} catch(ParameterErrorException parameterErrorException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} catch (SQLException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
        try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}

}
