package fr.etu.m1.miage.festibed.database;

import java.sql.*;

public class OracleBdAccess {
    private static Connection con;
    
    public static void connect() throws SQLException {
    	if(con == null) {
            // chargement du pilote
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch(ClassNotFoundException e) {
                System.err.println("Impossible de charger le pilote mysql");
            }

            // connection a la base de données
            System.out.println("connexion a la base de données");
            try {
            	DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            	String userId = "waelm";
                String DBurl =  "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
                String passwd = "M1A5z4E9n";
                con = DriverManager.getConnection(DBurl, userId, passwd);
                con.setAutoCommit(true);
                System.out.println("connexion reussie");
            } catch (SQLException e) {
                System.err.println(e.getMessage() + "   \nConnection à la base de données impossible");
                throw e;
            }

    	} 
    }   
    
    public static void disconnect() throws SQLException {
    	con.close();
    }
    
    
    
//    public static String authentifyUser(String userId) throws SQLException {
//        
//        // chargement du pilote
//        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//        } catch(ClassNotFoundException e) {
//            System.err.println("Impossible de charger le pilote mysql");
//        }
//
//        // connection a la base de données
//        System.out.println("connexion a la base de données");
//        try {
//            String DBurl =  "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
//            String passwd = "M1A5z4E9n";
//            con = DriverManager.getConnection(DBurl, userId, passwd);
//            con.setAutoCommit(true);
//            System.out.println("connexion reussie");
//        } catch (SQLException e) {
//            System.err.println(e.getMessage() + "   \nConnection à la base de données impossible");
//            throw e;
//        }
//
//        return "Connexion réussi";
//    }
    
//    public static String disconnectUser(String userId) throws SQLException {
//        
//        System.out.println("Déconnexion en cours");
//        // Ajouter vérification de si l'utilisateur est connecté si besoin
//        if(con != null){
//            try {
//                con.close();
//                /** vérifier si conn est NULL après un close */
//            } catch(SQLException e) {
//                System.out.println("La connexion ne peut être fermée");
//                throw e;
//            }
//        }
//
//        return "Déconnexion réussi";
//    }
    
    
    
    // Select
    public static ResultSet select(String request) throws SQLException {
    	Statement stmt = con.createStatement();
    	System.out.println(request);
        return stmt.executeQuery(request);        
    }
    
    // Insert/Update/Update
    public static int update(String request) throws SQLException {
    	Statement stmt = con.createStatement();
    	System.out.println(request);
        return stmt.executeUpdate(request);
    }
    
    public static boolean callStoredProcedure(String request) throws SQLException {
    	CallableStatement callableStatement = con.prepareCall(request);
    	System.out.println(request);
    	return callableStatement.execute();
    }
}
