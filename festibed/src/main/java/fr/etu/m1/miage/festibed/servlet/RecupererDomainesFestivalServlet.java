package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.service.FestivalService;

public class RecupererDomainesFestivalServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		FestivalService festivalService = new FestivalService();
		String jsonResponse;
		try {
			jsonResponse = festivalService.getDomaines();
		} catch (SQLException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.getWriter().write(jsonResponse);
		resp.getWriter().flush();
		resp.getWriter().close();
	}
}