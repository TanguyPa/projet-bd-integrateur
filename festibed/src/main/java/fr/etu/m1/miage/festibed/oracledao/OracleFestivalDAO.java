package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.time.temporal.ChronoUnit;

import fr.etu.m1.miage.festibed.apidao.FestivalDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.model.Disponibilite;
import fr.etu.m1.miage.festibed.model.Festival;
import fr.etu.m1.miage.festibed.model.FormatDate;

public class OracleFestivalDAO implements FestivalDAO {

	@Override
	public Iterator<Festival> getFestivals(String motsCles, String ville, String categorie, String dateDebut,
			String dateFin, int numPage, int taillePage) throws SQLException {
		int rowNumMin = numPage * taillePage;
		int rowNumMax = rowNumMin + taillePage;
		String query = "";

		query += "SELECT f.*";
		query += " FROM (SELECT fe.*, rownum AS rnumb";
		query += " FROM Festival fe";
		query += " WHERE upper(fe.ville) LIKE '%" + ville + "%'";
		query += " AND upper(fe.typeF) LIKE '%" + categorie + "%'";
		if (dateDebut != null && !dateDebut.isEmpty()) {
			if (dateFin != null && !dateFin.isEmpty()) {
				query += " AND fe.dateDeb <= to_date('" + dateDebut + "','YYYY-MM-DD')";
				query += " AND fe.dateFin > to_date('" + dateFin + "','YYYY-MM-DD')";
			} else {
				query += " AND fe.dateDeb <= to_date('" + dateDebut + "','YYYY-MM-DD')";
			}
		}
		query += " AND (";
		boolean firstMotCle = true;
		if (motsCles != null) {
			Iterator<String> mCles = Arrays.asList(motsCles.split(" ")).iterator();
			while (mCles.hasNext()) {
				String mCle = mCles.next();
				System.out.println(mCle);
				if (firstMotCle) {
					query += " upper(fe.nomF) LIKE '%" + mCle + "%'";
					firstMotCle = false;
				} else {
					query += " OR upper(fe.nomF) LIKE '%" + mCle + "%'";
				}
			}
		}
		query += ")";
		query += " AND (fe.capaciteCateg0 > 0 OR fe.capaciteCateg1 > 0 OR fe.capaciteCateg2 >0)";
		query += ") f";
		query += " WHERE f.rnumb BETWEEN " + rowNumMin + " AND " + rowNumMax;

		ArrayList<Festival> festivals = new ArrayList<Festival>();
		ResultSet rsFestivals = null;
		rsFestivals = OracleBdAccess.select(query);
		while (rsFestivals.next()) {
			Festival fest = new Festival(rsFestivals.getInt("numFestival"), rsFestivals.getString("nomF"),
					rsFestivals.getDate("dateDeb"), rsFestivals.getDate("dateFin"), rsFestivals.getString("siteWeb"),
					rsFestivals.getString("typeF"), rsFestivals.getString("codePostal"), rsFestivals.getString("ville"),
					rsFestivals.getString("pays"), rsFestivals.getInt("capaciteCateg1"),
					rsFestivals.getInt("capaciteCateg2"), rsFestivals.getInt("capaciteCateg0"),
					rsFestivals.getInt("noteMoyenne"), rsFestivals.getInt("tarifCateg1"),
					rsFestivals.getInt("tarifCateg2"), rsFestivals.getInt("tarifCateg0"), rsFestivals.getString("etat"),
					rsFestivals.getLong("longitude"), rsFestivals.getLong("latitude"),
					rsFestivals.getInt("numOrganisateur"));
			festivals.add(fest);
		}
		rsFestivals.close();

		return festivals.iterator();
	}

	@Override
	public Iterator<String> getDomaines() throws SQLException {
		String query = "SELECT DISTINCT typeF";
		query += " FROM Festival";
		ResultSet rsFestivals = null;
		ArrayList<String> domaines = new ArrayList<String>();
		rsFestivals = OracleBdAccess.select(query);

		while (rsFestivals.next()) {
			domaines.add(rsFestivals.getString("typeF"));
		}
		rsFestivals.close();
		return domaines.iterator();
	}

	@Override
	public Festival getFestivalById(int idFestival) throws SQLException {
		String query = "SELECT *";
		query += " FROM Festival";
		query += " WHERE numFestival =" + idFestival;
		ResultSet rsFestival = null;
		Festival fest = null;
		rsFestival = OracleBdAccess.select(query);
		if (rsFestival.next()) {
			fest = new Festival(rsFestival.getInt("numFestival"), rsFestival.getString("nomF"),
					rsFestival.getDate("dateDeb"), rsFestival.getDate("dateFin"), rsFestival.getString("siteWeb"),
					rsFestival.getString("typeF"), rsFestival.getString("codePostal"), rsFestival.getString("ville"),
					rsFestival.getString("pays"), rsFestival.getInt("capaciteCateg1"),
					rsFestival.getInt("capaciteCateg2"), rsFestival.getInt("capaciteCateg0"),
					rsFestival.getInt("noteMoyenne"), rsFestival.getInt("tarifCateg1"),
					rsFestival.getInt("tarifCateg2"), rsFestival.getInt("tarifCateg0"), rsFestival.getString("etat"),
					rsFestival.getLong("longitude"), rsFestival.getLong("latitude"),
					rsFestival.getInt("numOrganisateur"));
		}
		rsFestival.close();
		return fest;
	}

	@Override
	public void ajouterFestival(Festival festival) throws SQLException {
		String query = "";
		query += "INSERT INTO Festival(NUMFESTIVAL, NOMF, NOTEMOYENNE, DATEDEB, DATEFIN, "
				+ "TYPEF, SITEWEB, CODEPOSTAL, VILLE, PAYS, CAPACITECATEG0, CAPACITECATEG1,"
				+ " CAPACITECATEG2, TARIFCATEG0, TARIFCATEG1, TARIFCATEG2, ETAT, LONGITUDE, LATITUDE, NUMORGANISATEUR)";
		query += " VALUES (";
		query += " (SELECT MAX(NUMFESTIVAL) FROM FESTIVAL)+1,";
		query += "'" + festival.getNom() + "', ";
		query += festival.getNoteMoyenne() + ", ";
		query += "to_date('" + FormatDate.getDateToStringFormat(festival.getDateDebut(), "yyyy-MM-dd")
				+ "','YYYY-MM-DD'), ";
		query += "to_date('" + FormatDate.getDateToStringFormat(festival.getDateFin(), "yyyy-MM-dd")
				+ "','YYYY-MM-DD'), ";
		query += "'" + festival.getTypeF() + "', ";
		query += "'" + festival.getSiteWeb() + "', ";
		query += "'" + festival.getCodePostal() + "', ";
		query += "'" + festival.getVille() + "', ";
		query += "'" + festival.getPays() + "', ";
		query += festival.getCapaciteCategorie0() + ", ";
		query += festival.getCapaciteCategorie1() + ", ";
		query += festival.getCapaciteCategorie2() + ", ";
		query += festival.getTarifCategorie0() + ", ";
		query += festival.getTarifCategorie1() + ", ";
		query += festival.getTarifCategorie2() + ", ";
		query += "'" + festival.getEtat() + "', ";
		query += festival.getLongitude() + ", ";
		query += festival.getLatitude() + ", ";
		query += festival.getNumOrganisateur();
		query += " )";

		OracleBdAccess.update(query);
	}

	@Override
	public Iterator<Disponibilite> getDisponibilites(int idFestival) throws SQLException, RessourceNotFoundException {
		String query = "SELECT dateDeb, dateFin";
		query += " FROM Festival";
		query += " WHERE numFestival =" + idFestival;
		ResultSet rsFestival;
		rsFestival = OracleBdAccess.select(query);
		Date dateDebut, dateFin;
		if (rsFestival.next()) {
			dateDebut = rsFestival.getDate("dateDeb");
			dateFin = rsFestival.getDate("dateFin");
			rsFestival.close();
		} else {
			throw new RessourceNotFoundException();
		}
		ArrayList<Disponibilite> disponibilites = new ArrayList<Disponibilite>();
		for (int i = 0; i < (ChronoUnit.DAYS.between(dateFin.toInstant(), dateDebut.toInstant())); i++) {
			ResultSet rsFestivalbis;

			String querybis = "SELECT capaciteCateg0, capaciteCateg1, capaciteCateg2";
			querybis += " FROM Festival";
			querybis += " WHERE numFestival =" + idFestival;
			rsFestivalbis = OracleBdAccess.select(querybis);
			int capa1 = rsFestivalbis.getInt("capaciteCateg1");
			int capa2 = rsFestivalbis.getInt("capaciteCateg2");
			int capa0 = rsFestivalbis.getInt("capaciteCateg0");
			Disponibilite disponibilite = new Disponibilite(idFestival, Date.from(
					LocalDate.from(dateDebut.toInstant()).plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant()),
					capa1, capa2, capa0);
			disponibilites.add(disponibilite);
			rsFestivalbis.close();
		}
		return disponibilites.iterator();
	}
}
