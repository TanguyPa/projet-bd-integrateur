package fr.etu.m1.miage.festibed.model;


public class Personne {
	
	private String numPersonne;
	private String nom;
	private String prenom;
	private String telephone;
	private String mail;
	public Personne(String numPersonne, String nom, String prenom, String telephone, String mail) {
		super();
		this.numPersonne = numPersonne;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.mail = mail;
	}
	public String getNumPersonne() {
		return numPersonne;
	}
	public void setNumPersonne(String numPersonne) {
		this.numPersonne = numPersonne;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	

}
