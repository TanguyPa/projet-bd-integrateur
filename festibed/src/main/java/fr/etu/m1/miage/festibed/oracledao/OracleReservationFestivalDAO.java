package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.ReservationFestivalDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.ReservationFestival;

public class OracleReservationFestivalDAO implements ReservationFestivalDAO {

	@Override
	public int ajouterReservationFestival(String numFestival, String numReservation, String nbCateg0, String nbCateg1, String nbCateg2, String dateRes) throws SQLException {
		String insert = "insert into FestivalReservation values (" + numFestival + "," + numReservation + "," + nbCateg0 + "," + nbCateg1 + "," + nbCateg2 +",TO_DATE('" + dateRes + "','DD/MM/YYYY'))";
		return OracleBdAccess.update(insert);
	}

	@Override
	public Iterator<ReservationFestival> getReservationFestival(String numReservation) {
		String query ="select * from FestivalReservation where numReservation = " + numReservation ;
		
		System.out.println(query);
		ResultSet rsFestivalRes = null;
		try {
			rsFestivalRes = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<ReservationFestival> festivalRes = new ArrayList<ReservationFestival>();
		
		try {
			while(rsFestivalRes.next()) {
				ReservationFestival res = new ReservationFestival(
						rsFestivalRes.getString("numFestival"),			
						rsFestivalRes.getString("numReservation"),
						rsFestivalRes.getString("nbCategorie0"),
						rsFestivalRes.getString("nbCategorie1"),
						rsFestivalRes.getString("nbCategorie2"),
						rsFestivalRes.getString("datePlaceRes"));
				festivalRes.add(res);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return festivalRes.iterator();
	}

	@Override
	public ReservationFestival getReservationFestivalById(String numFestival, String numReservation,
			String dateReservation) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	/* Supprimer elements panier apres 30 minutes a voir si faisable en Java */
	
	
}
