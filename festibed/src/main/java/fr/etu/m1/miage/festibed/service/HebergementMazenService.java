package fr.etu.m1.miage.festibed.service;

import java.sql.SQLException;
import java.util.Iterator;

import com.google.gson.Gson;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.model.HebergementMazen;
import fr.etu.m1.miage.festibed.oracledao.OracleHebergementMazenDAO;

public class HebergementMazenService {

	public String rechercherHebergementLogement(String dateDeb, String dateFin, String ville)
			throws ParameterErrorException, SQLException {

		LogementMazenService logementService = new LogementMazenService();
		HebergementMazen res;

		OracleHebergementMazenDAO hebergementDAO = new OracleHebergementMazenDAO();

		Iterator<HebergementMazen> hebergements = hebergementDAO.getHebergement(ville);

		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse += "{ \"hebergements\": [";
		while (hebergements.hasNext()) {

			res = hebergements.next();
			logementService.rechercherLogementGsonV1(dateDeb, dateFin, res, res.getNumHebergement(), gson);
			jsonResponse += gson.toJson(res);

			if (hebergements.hasNext()) {
				jsonResponse += ",";
			}
		}

		jsonResponse += "]";
		jsonResponse += "}";

		return jsonResponse;
	}

	public String rechercherUnHebergementLogement(String numHebergement) throws ParameterErrorException, SQLException {

		OracleHebergementMazenDAO hebergementDAO = new OracleHebergementMazenDAO();

		HebergementMazen hebergement = hebergementDAO.getHebergementbyId(numHebergement);

		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse += "{ \"hebergement\": ";

		jsonResponse += gson.toJson(hebergement);

		jsonResponse += "}";

		return jsonResponse;
	}

}
