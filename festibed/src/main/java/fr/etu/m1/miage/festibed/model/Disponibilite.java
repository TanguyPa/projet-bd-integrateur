package fr.etu.m1.miage.festibed.model;

import java.util.Date;

public class Disponibilite {
	
	private int numFestival;
	private Date dateDispo;
	private int capaciteCateg0;
	private int capaciteCateg1;
	private int capaciteCateg2;
	
	public Disponibilite(int numFestival, Date dateDispo, int capaciteCateg0, int capaciteCateg1, int capaciteCateg2) {
		this.numFestival = numFestival;
		this.dateDispo = dateDispo;
		this.capaciteCateg0 = capaciteCateg0;
		this.capaciteCateg1 = capaciteCateg1;
		this.capaciteCateg2 = capaciteCateg2;
	}

	public int getNumFestival() {
		return numFestival;
	}

	public void setNumFestival(int numFestival) {
		this.numFestival = numFestival;
	}

	public Date getDateDispo() {
		return dateDispo;
	}

	public void setDateDispo(Date dateDispo) {
		this.dateDispo = dateDispo;
	}

	public int getCapaciteCateg0() {
		return capaciteCateg0;
	}

	public void setCapaciteCateg0(int capaciteCateg0) {
		this.capaciteCateg0 = capaciteCateg0;
	}

	public int getCapaciteCateg1() {
		return capaciteCateg1;
	}

	public void setCapaciteCateg1(int capaciteCateg1) {
		this.capaciteCateg1 = capaciteCateg1;
	}

	public int getCapaciteCateg2() {
		return capaciteCateg2;
	}

	public void setCapaciteCateg2(int capaciteCateg2) {
		this.capaciteCateg2 = capaciteCateg2;
	}

}
