package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.service.FestivalService;
import fr.etu.m1.miage.festibed.service.LogementMazenService;

public class RechercherLogementServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
	    HttpFormater.formatHeader(resp);
	    resp.setContentType("application/json");
		LogementMazenService festivalService = new LogementMazenService();
		
		String jsonResponse;
		try {
			jsonResponse = festivalService.rechercherLogement(req.getParameter("numHebergement"));
		} catch(ParameterErrorException parameterErrorException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
        try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}

}
