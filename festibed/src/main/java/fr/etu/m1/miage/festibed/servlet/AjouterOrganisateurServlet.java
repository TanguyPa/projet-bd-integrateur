package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.apidao.OrganisateurDAO;
import fr.etu.m1.miage.festibed.model.Organisateur;
import fr.etu.m1.miage.festibed.oracledao.OracleOrganisateurDAO;

public class AjouterOrganisateurServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		String numPersonne;

		try {	
			numPersonne = req.getParameter("numPersonne");
		} catch (NumberFormatException  numberFormatException) {
	        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
   		}
		
		OrganisateurDAO OrganisateurDAO = new OracleOrganisateurDAO();
		Organisateur Organisateur = new Organisateur(numPersonne);
		
		// System.out.println("nom :"+ nom +", prenom :"+ prenom +", mail :"+ mail);
		
		try {
			OrganisateurDAO.ajouterOrganisateur(numPersonne);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print("Nouvelle Organisateur ajoute a la base de donnees");
	
		resp.setStatus(HttpServletResponse.SC_OK);

	}

}
