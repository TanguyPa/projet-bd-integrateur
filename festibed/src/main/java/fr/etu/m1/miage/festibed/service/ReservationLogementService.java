package fr.etu.m1.miage.festibed.service;

import java.util.Iterator;

import com.google.gson.Gson;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.model.Reservation;
import fr.etu.m1.miage.festibed.model.ReservationLogement;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationLogementDAO;

public class ReservationLogementService {

	public String rechercherLogement(String numReservation) throws ParameterErrorException {
		
		OracleReservationLogementDAO reservationDAO = new OracleReservationLogementDAO();
		
		Iterator<ReservationLogement> reservations = reservationDAO.getReservationLogement(numReservation);
		
		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse+="{ \"LogementReservation\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	}
	
	public String rechercherLogementGson(String numReservation, Gson gson) throws ParameterErrorException {
		
		OracleReservationLogementDAO reservationDAO = new OracleReservationLogementDAO();
		
		Iterator<ReservationLogement> reservations = reservationDAO.getReservationLogement(numReservation);
		
		//Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse+="{ \"LogementReservation\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	}
	
	public void rechercherLogementGsonV1(Reservation res, String numReservation, Gson gson) throws ParameterErrorException {
		
		OracleReservationLogementDAO reservationDAO = new OracleReservationLogementDAO();
		
		Iterator<ReservationLogement> reservations = reservationDAO.getReservationLogement(numReservation);
		while(reservations.hasNext()) {
			res.setReservationLogement(reservations.next());
		}

	}
}
