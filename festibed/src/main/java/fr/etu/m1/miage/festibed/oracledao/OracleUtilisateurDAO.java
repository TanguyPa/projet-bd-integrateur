package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.UtilisateurDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.exception.CreationUtilisateurException;
import fr.etu.m1.miage.festibed.model.Utilisateur;

public class OracleUtilisateurDAO {

	public Iterator<Utilisateur> getUtilisateurs() {
		// TODO Auto-generated method stub
		return null;
	}

	

	public String getNumPersonneById(String numPersonne) throws SQLException {
		String numPers;
		ResultSet rs;
		String quest = "select numPersonne from Personne where numPersonne = '" + numPersonne +"'";
		rs = OracleBdAccess.select(quest);
		if (rs.next()) {
			numPers = rs.getString("numPersonne");
		} else {
			return null;
		}
		
		return numPers;
	}
	

	public int getNumUtilisateurById(String numPersonne) throws SQLException {
		int numPers;
		ResultSet rs;
		String quest = "select numUtilisateur from Utilisateur where numPersonne = '" + numPersonne +"'";
		rs = OracleBdAccess.select(quest);
		if (rs.next()) {
			numPers = rs.getInt("numUtilisateur");
		} else {
			return -1;
		}
		
		return numPers;
	}
	
	public int authentifierPersonne(String numPersonne, String nom, String prenom, String telephone, String mail) throws SQLException, CreationUtilisateurException {
		String userNum = this.getNumPersonneById(numPersonne);
		if(userNum == null) {
			this.ajouterPersonne(numPersonne, nom, prenom, telephone, mail);
		}
		userNum =this.getNumPersonneById(numPersonne);
		if(userNum == null) {
			throw new CreationUtilisateurException();
		}
		int utiNum = this.getNumUtilisateurById(numPersonne);
		if(utiNum == -1) {
			this.ajouterUtilisateur(numPersonne);
		}
		utiNum = this.getNumUtilisateurById(numPersonne);
		if(utiNum == -1) {
			throw new CreationUtilisateurException();
		}
		return utiNum;
	}

	public int ajouterUtilisateur(String numPersonne) throws SQLException {
		String insert = "insert into Utilisateur values ((select max(numUtilisateur) from Utilisateur) + 1,'" + numPersonne + "')";
		return OracleBdAccess.update(insert);
	}

	public int ajouterPersonne(String numPersonne, String nom, String prenom, String telephone, String mail) throws SQLException {
		String insert = "insert into Personne values ('"+ numPersonne + "', '"+nom+"','"+prenom+"', '"+telephone+"','"+mail+"')";
		return OracleBdAccess.update(insert);
	}


}
