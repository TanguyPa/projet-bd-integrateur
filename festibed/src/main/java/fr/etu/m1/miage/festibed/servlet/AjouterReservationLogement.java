package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationLogementDAO;

public class AjouterReservationLogement extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		
		String numLogement;
		String dateDeb;
		String dateFin;
		String numUtilisateur;
		
		try {	
			numLogement = req.getParameter("numLogement");
			dateDeb = req.getParameter("dateDeb");
			dateFin = req.getParameter("dateFin");
			numUtilisateur = req.getParameter("numUtilisateur");
		} catch (NumberFormatException  numberFormatException) {
	        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
   		}
		
		OracleReservationLogementDAO logementDAO = new OracleReservationLogementDAO();
		
		try {
			logementDAO.ajouterReservationLogement(numLogement, dateDeb, dateFin, numUtilisateur);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print("Nouvelle ReservationLogement ajoute a la base de donnees");
	
		resp.setStatus(HttpServletResponse.SC_OK);

	}
}
