package fr.etu.m1.miage.festibed.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.etu.m1.miage.festibed.apidao.DAOFactory;
import fr.etu.m1.miage.festibed.apidao.FestivalDAO;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.model.Disponibilite;
import fr.etu.m1.miage.festibed.model.Festival;
import fr.etu.m1.miage.festibed.oracledao.OracleDAOFactory;
import fr.etu.m1.miage.festibed.oracledao.OracleFestivalDAO;

public class FestivalService {

	public void ajouterFestival(HttpServletRequest req, HttpServletResponse resp) {

	}

	public String rechercherDesFestivals(String motsCles, String ville, String categorie, String dateDebut,
			String dateFin, String numPage, String taillePage) throws ParameterErrorException, SQLException {

		categorie = (categorie != null) ? categorie : "";
		ville = (ville != null) ? ville : "";
		motsCles = (motsCles != null) ? motsCles : "";
		categorie = categorie.toUpperCase();
		ville = ville.toUpperCase();
		motsCles = motsCles.toUpperCase();
		dateDebut = (dateDebut != null) ? dateDebut : "";
		dateFin = (dateFin != null) ? dateFin : "";
		// TODO Tester les formats des dates (yyyy-MM-dd)

		int numP = 0;
		int tailleP = 0;
		try {
			numP = Integer.parseInt(numPage);
			tailleP = Integer.parseInt(taillePage);
		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}

		DAOFactory daoFactory = new OracleDAOFactory();

		FestivalDAO festivalDAO = daoFactory.getFestivalDAO();

		Iterator<Festival> festivals = festivalDAO.getFestivals(motsCles, ville, categorie, dateDebut, dateFin, numP,
				tailleP);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonResponse = "";
		jsonResponse += "{ \"festivals\": [";
		while (festivals.hasNext()) {
			jsonResponse += gson.toJson(festivals.next());
			if (festivals.hasNext()) {
				jsonResponse += ",";
			}
		}
		jsonResponse += "]";
		jsonResponse += ",\"numPage\": " + numPage;
		jsonResponse += ",\"taillePage\": " + taillePage;
		jsonResponse += "}";

		return jsonResponse;
	}

	public String rechercherUnFestival(String festivalId) throws RessourceNotFoundException, ParameterErrorException, SQLException {
		System.out.println("Salut");
		int idFestival;
		try {
			idFestival = Integer.parseInt(festivalId);
		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}

		FestivalDAO festivalDAO = new OracleFestivalDAO();

		Festival festival = festivalDAO.getFestivalById(idFestival);

		if (festival == null) {
			throw new RessourceNotFoundException();
		}
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonResponse = "";
		jsonResponse += "{ \"festival\": ";
		jsonResponse += gson.toJson(festival);
		jsonResponse += "}";
		return jsonResponse;
	}

	public String getDomaines() throws SQLException {
		DAOFactory daoFactory = new OracleDAOFactory();

		FestivalDAO festivalDAO = daoFactory.getFestivalDAO();

		Iterator<String> festivals = festivalDAO.getDomaines();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonResponse = "";
		jsonResponse += "{ \"domaines\": [";
		while (festivals.hasNext()) {
			jsonResponse += gson.toJson(festivals.next());
			if (festivals.hasNext()) {
				jsonResponse += ",";
			}
		}
		jsonResponse += "]}";
		return jsonResponse;
	}

	public void ajouterFestival(String nom, String dateDeb, String dateFin, String type, String siteWeb,
			String codePostal, String ville, String pays, String capaciteCateg0, String capaciteCateg1,
			String capaciteCateg2, String noteMoyenne, String tarifCateg0, String tarifCateg1, String tarifCateg2, String etat,
			String longitude, String latitude, String numOrganisateur) throws ParameterErrorException, SQLException {
		if ((nom == null || nom.isEmpty()) || (dateDeb == null || dateDeb.isEmpty())
				|| (dateFin == null || dateFin.isEmpty()) || (type == null || type.isEmpty())
				|| (siteWeb == null || siteWeb.isEmpty()) || (codePostal == null || codePostal.isEmpty())
				|| (ville == null || ville.isEmpty()) || (pays == null || pays.isEmpty())
				|| (capaciteCateg0 == null || capaciteCateg0.isEmpty())
				|| (capaciteCateg1 == null || capaciteCateg1.isEmpty())
				|| (capaciteCateg2 == null || capaciteCateg2.isEmpty())
				|| (capaciteCateg2 == null || capaciteCateg2.isEmpty())
				|| (tarifCateg0 == null || tarifCateg0.isEmpty()) || (tarifCateg1 == null || tarifCateg1.isEmpty())
				|| (tarifCateg2 == null || tarifCateg2.isEmpty()) || (etat == null || etat.isEmpty())
				|| (longitude == null || longitude.isEmpty()) || (latitude == null || latitude.isEmpty())
				|| (numOrganisateur == null || numOrganisateur.isEmpty()) || (noteMoyenne == null || noteMoyenne.isEmpty())) {
			throw new ParameterErrorException();
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateD;
		Date dateF;
		try {

		    dateD = dateFormat.parse(dateDeb);
		    dateF = dateFormat.parse(dateFin);
		} catch (ParseException e) {
			throw new ParameterErrorException();
		}
		
		int cap1, cap2, cap0, noteMoy, numOrga;
		try {
			cap1 = Integer.parseInt(capaciteCateg1);
			cap2 = Integer.parseInt(capaciteCateg2);
			cap0 = Integer.parseInt(capaciteCateg0);
			noteMoy = Integer.parseInt(noteMoyenne);
			numOrga = Integer.parseInt(numOrganisateur);
		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}
		
		long longi, lati, tarif1, tarif2, tarif0;
		try {
			longi = Long.parseLong(longitude);
			lati= Long.parseLong(latitude);
			tarif1 = Long.parseLong(tarifCateg1);
			tarif2= Long.parseLong(tarifCateg2);
			tarif0= Long.parseLong(tarifCateg0);
		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}
		
		DAOFactory daoFactory = new OracleDAOFactory();

		FestivalDAO festivalDAO = daoFactory.getFestivalDAO();
		
		Festival fest = new Festival(-1, nom, dateD, dateF, siteWeb, type, codePostal, ville, pays, cap1, cap2, cap0, noteMoy, tarif1, tarif2, tarif0, etat, longi, lati, numOrga);
		
		
		festivalDAO.ajouterFestival(fest);

	}

	public String rechercherDisponibilites(String idFestival) throws ParameterErrorException, SQLException, RessourceNotFoundException {
		if(idFestival == null || idFestival.isEmpty()) {
			throw new ParameterErrorException();
		}
		DAOFactory daoFactory = new OracleDAOFactory();
		int id;
		try {
			id = Integer.parseInt(idFestival);
		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}
		
		FestivalDAO festivalDAO = daoFactory.getFestivalDAO();
		Iterator<Disponibilite> dispos = festivalDAO.getDisponibilites(id);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonResponse = "";
		while(dispos.hasNext()) {
			jsonResponse += gson.toJson(dispos.next());
			if (dispos.hasNext()) {
				jsonResponse += ",";
			}	
		}
		return jsonResponse;
	}
}
