package fr.etu.m1.miage.festibed.service;

import java.util.Iterator;
import com.google.gson.Gson;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.model.Reservation;
import fr.etu.m1.miage.festibed.model.ReservationFestival;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationFestivalDAO;

public class ReservationFestivalService {
	
	public String rechercherFestival(String numReservation) throws ParameterErrorException {
		
		OracleReservationFestivalDAO reservationDAO = new OracleReservationFestivalDAO();
		
		Iterator<ReservationFestival> reservations = reservationDAO.getReservationFestival(numReservation);
		
		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse+="{ \"FestivalReservation\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	}
	
	public String rechercherFestivalGson(String numReservation,Gson gson) throws ParameterErrorException {
		
		OracleReservationFestivalDAO reservationDAO = new OracleReservationFestivalDAO();
		
		Iterator<ReservationFestival> reservations = reservationDAO.getReservationFestival(numReservation);
		
		//Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse+=",{ \"FestivalReservation\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	}
	
	public void rechercherFestivalGsonV1(Reservation res, String numReservation,Gson gson) throws ParameterErrorException {
		
		OracleReservationFestivalDAO reservationDAO = new OracleReservationFestivalDAO();
		
		Iterator<ReservationFestival> reservations = reservationDAO.getReservationFestival(numReservation);
		
		while(reservations.hasNext()) {
			res.setReservationFestival(reservations.next());
		}

	}
}
