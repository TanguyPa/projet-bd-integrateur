package fr.etu.m1.miage.festibed.model;

import java.util.ArrayList;

public class HebergementMazen {
	
	private String numHebergement;
	private String nomHebergement;
	private String noteMoyenne;
	private String classement;
	private String telephone;
	private String siteWeb;
	private String nom_rue;
	private String codePostal;
	private String ville;
	private String pays;
	private String description;
	private String longitude;
	private String latitude;
	private String numHebergeur;
	private ArrayList<LogementMazen> logements;

	public HebergementMazen(String numHebergement, String nomHebergement, String noteMoyenne, String classement, String telephone,
			String siteWeb, String nom_rue, String codePostal, String ville, String pays, String description,
			String longitude, String latitude, String numHebergeur) {
		
		this.numHebergement = numHebergement;
		this.nomHebergement = nomHebergement;
		this.noteMoyenne = noteMoyenne;
		this.classement = classement;
		this.telephone = telephone;
		this.siteWeb = siteWeb;
		this.nom_rue = nom_rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
		this.description = description;
		this.longitude = longitude;
		this.latitude = latitude;
		this.numHebergeur = numHebergeur;
		this.logements = new ArrayList<LogementMazen>();
	}
	
	public LogementMazen getLogementMazen(){
		int taille = logements.size();
		if(taille > 0) {
			return logements.get(taille - 1);
		}
		return null;
	}
	
	public void setLogementMazen(LogementMazen logement){
		logements.add(logement);
	}
	
	public String getNumHebergement() {
		return numHebergement;
	}

	public void setNumHebergement(String numHebergement) {
		this.numHebergement = numHebergement;
	}

	public String getNomHebergement() {
		return nomHebergement;
	}

	public void setNomHebergement(String nomHebergement) {
		this.nomHebergement = nomHebergement;
	}

	public String getNoteMoyenne() {
		return noteMoyenne;
	}

	public void setNoteMoyenne(String noteMoyenne) {
		this.noteMoyenne = noteMoyenne;
	}

	public String getClassement() {
		return classement;
	}

	public void setClassement(String classement) {
		this.classement = classement;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getNom_rue() {
		return nom_rue;
	}

	public void setNom_rue(String nom_rue) {
		this.nom_rue = nom_rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getNumHebergeur() {
		return numHebergeur;
	}

	public void setNumHebergeur(String numHebergeur) {
		this.numHebergeur = numHebergeur;
	}
	
}
