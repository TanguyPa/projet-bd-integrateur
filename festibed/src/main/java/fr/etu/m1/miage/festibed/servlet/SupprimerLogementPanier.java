	package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.CreationUtilisateurException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.ReservationService;
import fr.etu.m1.miage.festibed.service.UtilisateurService;

public class SupprimerLogementPanier extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		ReservationService reservationService = new ReservationService();
		System.out.println("Authentificaiton");
		try {
			 reservationService.supprimerLogement(req.getParameter("numL"), req.getParameter("numR"),
					req.getParameter("dateD"), req.getParameter("dateF"));
		} catch (SQLException sqlException) {
			System.err.println(sqlException.getMessage());
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;	
		} catch (ParameterErrorException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		resp.setStatus(HttpServletResponse.SC_OK);
	}
}