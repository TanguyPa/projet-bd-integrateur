package fr.etu.m1.miage.festibed.model;

import java.util.Date;

public class Festival {

	private int id;
	
	private String nom;
	
	private Date dateDebut;
	
	private Date dateFin;
	
	private String siteWeb;
	
	private String typeF;
	
	private String ville;
	
	private String codePostal;
	
	private String pays;
	
	private int capaciteCategorie1;
	
	private int capaciteCategorie2;
	
	private int capaciteCategorie0;
	
	private int noteMoyenne;
	
	private long tarifCategorie1;
	
	private long tarifCategorie2;
	
	private long tarifCategorie0;
	
	private String etat;
	
	private long longitude;
	
	private long latitude;
	
	private int numOrganisateur;

	public Festival(int id, String nom, Date dateDebut, Date dateFin, String siteWeb, String typeF, String codePostal,String ville,
			String pays, int capaciteCategorie1, int capaciteCategorie2, int capaciteCategorie0, int noteMoyenne,
			long tarifCategorie1, long tarifCategorie2, long tarifCategorie0, String etat, long longitude, long latitude,
			int numOrganisateur) {
		super();
		this.id = id;
		this.nom = nom;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.siteWeb = siteWeb;
		this.typeF = typeF;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
		this.capaciteCategorie1 = capaciteCategorie1;
		this.capaciteCategorie2 = capaciteCategorie2;
		this.capaciteCategorie0 = capaciteCategorie0;
		this.noteMoyenne = noteMoyenne;
		this.tarifCategorie1 = tarifCategorie1;
		this.tarifCategorie2 = tarifCategorie2;
		this.tarifCategorie0 = tarifCategorie0;
		this.etat = etat;
		this.longitude = longitude;
		this.latitude = latitude;
		this.numOrganisateur = numOrganisateur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getTypeF() {
		return typeF;
	}

	public void setTypeF(String typeF) {
		this.typeF = typeF;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public int getCapaciteCategorie1() {
		return capaciteCategorie1;
	}

	public void setCapaciteCategorie1(int capaciteCategorie1) {
		this.capaciteCategorie1 = capaciteCategorie1;
	}

	public int getCapaciteCategorie2() {
		return capaciteCategorie2;
	}

	public void setCapaciteCategorie2(int capaciteCategorie2) {
		this.capaciteCategorie2 = capaciteCategorie2;
	}

	public int getCapaciteCategorie0() {
		return capaciteCategorie0;
	}

	public void setCapaciteCategorie0(int capaciteCategorie0) {
		this.capaciteCategorie0 = capaciteCategorie0;
	}

	public int getNoteMoyenne() {
		return noteMoyenne;
	}

	public void setNoteMoyenne(int noteMoyenne) {
		this.noteMoyenne = noteMoyenne;
	}

	public long getTarifCategorie1() {
		return tarifCategorie1;
	}

	public void setTarifCategorie1(long tarifCategorie1) {
		this.tarifCategorie1 = tarifCategorie1;
	}

	public long getTarifCategorie2() {
		return tarifCategorie2;
	}

	public void setTarifCategorie2(long tarifCategorie2) {
		this.tarifCategorie2 = tarifCategorie2;
	}

	public long getTarifCategorie0() {
		return tarifCategorie0;
	}

	public void setTarifCategorie0(long tarifCategorie0) {
		this.tarifCategorie0 = tarifCategorie0;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public int getNumOrganisateur() {
		return numOrganisateur;
	}

	public void setNumOrganisateur(int numOrganisateur) {
		this.numOrganisateur = numOrganisateur;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	
	
	
	
}
