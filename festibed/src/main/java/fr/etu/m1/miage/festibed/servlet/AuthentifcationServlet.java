	package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.CreationUtilisateurException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.UtilisateurService;

public class AuthentifcationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		UtilisateurService utilisateurService = new UtilisateurService();
		System.out.println("Authentificaiton");
		String jsonResponse;
		try {
			jsonResponse = utilisateurService.authentifierUtilisateur(req.getParameter("numP"), req.getParameter("nom"),
					req.getParameter("prenom"), req.getParameter("telephone"), req.getParameter("mail"));
		} catch (SQLException sqlException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;	
		} catch (ParameterErrorException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} catch (CreationUtilisateurException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} finally  {
			resp.getWriter().close();
		}
		System.out.println(jsonResponse);
		resp.setStatus(HttpServletResponse.SC_OK);
	}
}