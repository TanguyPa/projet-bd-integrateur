package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Date;

import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.ReservationDAO;
import fr.etu.m1.miage.festibed.database.Concurrence;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;

import fr.etu.m1.miage.festibed.exception.CreationResException;
import fr.etu.m1.miage.festibed.model.FormatDate;

import fr.etu.m1.miage.festibed.model.Reservation;
import fr.etu.m1.miage.festibed.observer.PanierExpiration;
import fr.etu.m1.miage.festibed.observer.PanierObservable;

public class OracleReservationDAO implements ReservationDAO {

	@Override
	public Iterator<Reservation> getReservations(String numPersonne) {

		String query = "select * from Reservation where etatReservation = 'En cours' and numUtilisateur = (select max(numUtilisateur) from Utilisateur where numPersonne = '"
				+ numPersonne + "')";

		System.out.println(query);
		ResultSet rsRes = null;
		try {
			rsRes = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<Reservation> reservations = new ArrayList<Reservation>();

		try {
			while (rsRes.next()) {
				Reservation res = new Reservation(rsRes.getString("numReservation"), rsRes.getString("dateReservation"),
						rsRes.getString("dateValidation"), rsRes.getString("etatReservation"),
						rsRes.getString("montanTotal"), rsRes.getString("nbEnfant"), rsRes.getString("nbAdulte"),
						rsRes.getString("numUtilisateur"));
				reservations.add(res);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reservations.iterator();
	}

	public int ajouterReservation(String dateValidation, String montanTotal, String nbAdulte, String nbEnfant,
			String numUtilisateur) throws SQLException {
		// TODO Auto-generated method stub
		String insert = "Insert into Reservation values ((select max(numReservation) from Reservation)+1,sysdate,null,'En cours',0,"
				+ nbEnfant + "," + nbAdulte + "," + numUtilisateur + ")";
		return OracleBdAccess.update(insert);
	}

	@Override
	public int validerReservation(String numReservation) throws SQLException {
		String insert = "Update Reservation set dateValidation = sysdate ,etatReservation = 'Valide' where numReservation = " + numReservation;
		return OracleBdAccess.update(insert);
	}

	@Override
	public void expirerReservation(int numResa) throws SQLException {
		// Vérification qu'aucune réservation "panier" existe
		String query = "SELECT ETATRESERVATION";
		query += " FROM Reservation" + " WHERE ETATRESERVATION = 'En cours'" + " AND NUMRESERVATION = " + numResa;
		ResultSet rsRes = null;
		rsRes = OracleBdAccess.select(query);
		if (rsRes.next()) {
			String etatRes = rsRes.getString("ETATRESERVATION");
			if (etatRes.equals("En cours")) {
				String queryExpire = "UPDATE RESERVATION" + " SET ETATRESERVATION = 'Annule'"
						+ " WHERE NUMRESERVATION = " + numResa;
				OracleBdAccess.update(queryExpire);
			}
		}
	}

	public void ajouterReservationFestival(int numUtilisateur, int numFestival, int nbPlaceCateg0, int nbPlaceCateg1,
			int nbPlaceCateg2, Date dateResa) throws SQLException, CreationResException {
		int numRes = this.getReservationPanier(numUtilisateur);
		String query = "INSERT INTO FESTIVALRESERVATION (NUMFESTIVAL," + " NUMRESERVATION, NBCATEGORIE0, NBCATEGORIE1,"
				+ " NBCATEGORIE2, DATEPLACERES) VALUES(" + numFestival + ", " + numRes + ", " + nbPlaceCateg0 + ","
				+ " " + nbPlaceCateg1 + ",  " + nbPlaceCateg2 + ", " + "to_date('"
				+ FormatDate.getDateToStringFormat(dateResa, "yyyy-MM-dd") + "', 'YYYY-MM-DD')	" + ")";
		Concurrence.waitConcurrence();	
		OracleBdAccess.update(query);
	}

	@Override
	public int ajouterReservationLogement(int numLogement, Date dateDeb, Date dateFin, int numUtilisateur, int nbAd, int nbEn)
			throws SQLException, CreationResException {
		int numRes = this.getReservationPanier(numUtilisateur);
		String insert = "insert into ReservationLogement values (" + numLogement + "," + numRes + "," + "TO_DATE('"
				+ FormatDate.getDateToStringFormat(dateDeb, "yyyy-MM-dd") + "'," + "'YYYY-MM-DD')," + "TO_DATE('"
				+ FormatDate.getDateToStringFormat(dateFin, "yyyy-MM-dd") + "'," + "'YYYY-MM-DD'))";
		Concurrence.waitConcurrence();
//		String insert = "insert into ReservationLogement values (" + numLogement + "," + numRes + "," + "TO_DATE('"
//				+ FormatDate.getDateToStringFormat(dateDeb, "yyyy-MM-dd") + "'," + "'YYYY-MM-DD')," + "TO_DATE('"
//				+ FormatDate.getDateToStringFormat(dateFin, "yyyy-MM-dd") + "'," + "'YYYY-MM-DD')," + nbAd + ", " + nbEn + ")";
		return OracleBdAccess.update(insert);
	}

	/**
	 * Renvoie le numéro de réservation correspondant au panier (réservation en
	 * cours), si aucune existe, on la créer
	 * 
	 * @return
	 * @throws SQLException
	 * @throws CreationResException
	 */
	public int getReservationPanier(int numUtilisateur) throws SQLException, CreationResException {
		int numRes;
		// Vérification qu'aucune réservation "panier" existe
		String query = "SELECT NUMRESERVATION";
		query += " FROM Reservation" + " WHERE ETATRESERVATION = 'En cours'" + " AND NUMUTILISATEUR = "
				+ numUtilisateur;
		ResultSet rsRes = null;
		rsRes = OracleBdAccess.select(query);
		if (rsRes.next()) {
			numRes = rsRes.getInt("numReservation");
		} else {
			String createNewPanierQuery = "INSERT INTO RESERVATION"
					+ " (NUMRESERVATION, DATERESERVATION, DATEVALIDATION,"
					+ " ETATRESERVATION, MONTANTOTAL, NBENFANT, NBADULTE, NUMUTILISATEUR)"
					+ " VALUES((SELECT MAX(NUMRESERVATION) FROM Reservation)+1," + " (SELECT CURRENT_DATE FROM DUAL),"
					+ " NULL, 'En cours', 0, 0, 0," + numUtilisateur + ")";
			OracleBdAccess.update(createNewPanierQuery);
			String queryResBis = "SELECT NUMRESERVATION" + " FROM Reservation" + " WHERE ETATRESERVATION = 'En cours'"
					+ " AND NUMUTILISATEUR = " + numUtilisateur;

			ResultSet rsResBis = null;
			rsResBis = OracleBdAccess.select(queryResBis);
			if (rsResBis.next()) {
				numRes = rsResBis.getInt("numReservation");
				PanierObservable observable = new PanierObservable(numRes);
				observable.addObserver(PanierExpiration.getObserver());
			} else {
				throw new CreationResException();
			}
		}
		return numRes;
	}

	public void supprimerReservationFestival(int numF, Date dateRF, int numR) throws SQLException {
		String supprQuery = "DELETE FROM FESTIVALRESERVATION" + " WHERE NUMFESTIVAL= " + numF + " AND NUMRESERVATION= "
				+ numR + " AND DATEPLACERES = to_date('" + FormatDate.getDateToStringFormat(dateRF, "yyyy-MM-dd") + "',"
				+ "'YYYY-MM-DD')";
		OracleBdAccess.update(supprQuery);
	}

	public void supprimerReservationLogement(int numL, int numR, Date dateD, Date dateF) throws SQLException {
		String supprQuery = "DELETE FROM WAELM.RESERVATIONLOGEMENT" + " WHERE NUMLOGEMENT= " + numL + " AND NUMRESERVATION=" + numR 
				+ " AND DATEDEBUT= to_date('" + FormatDate.getDateToStringFormat(dateD, "yyyy-MM-dd") + "',"
				+ "'YYYY-MM-DD')" + " AND DATEFIN = to_date('" + FormatDate.getDateToStringFormat(dateF, "yyyy-MM-dd")
				+ "'," + "'YYYY-MM-DD')";
		OracleBdAccess.update(supprQuery);
	}

}
