package fr.etu.m1.miage.festibed.model;

public class Organisateur {
	private String numOrganisateur;

	public Organisateur(String numOrganisateur) {
		super();
		this.numOrganisateur = numOrganisateur;
	}

	public String getNumOrganisateur() {
		return numOrganisateur;
	}

	public void setNumOrganisateur(String numOrganisateur) {
		this.numOrganisateur = numOrganisateur;
	}
	
}
