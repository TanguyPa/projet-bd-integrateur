package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.LogementMazenDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.LogementMazen;

public class OracleLogementMazenDAO implements LogementMazenDAO {

	@Override
	public Iterator<LogementMazen> getLogementDisponible(String dateDeb, String dateFin, String numHebergement) {
		String query = "";
		if (dateDeb.isEmpty() == false && dateFin.isEmpty() == false && numHebergement.isEmpty() == false) {
			query = "select * from Logement where ROWNUM <= 10 and numLogement in (select numLogement from LogementDispo natural join Disponibilite where dateDispo > TO_DATE('"
					+ dateDeb + "','DD/MM/YYYY') and dateDispo < TO_DATE('" + dateFin
					+ "','DD/MM/YYYY')) and numHebergement = " + numHebergement;
		} else if (dateDeb.isEmpty() == false && dateFin.isEmpty() && numHebergement.isEmpty() == false) {
			query = "select * from Logement where ROWNUM <= 10 and numLogement in (select numLogement from LogementDispo natural join Disponibilite where dateDispo > TO_DATE('"
					+ dateDeb + "','DD/MM/YYYY')) and numHebergement = " + numHebergement;
		} else if (dateDeb.isEmpty() && dateFin.isEmpty() == false && numHebergement.isEmpty() == false) {
			query = "select * from Logement where ROWNUM <= 10 and numLogement in (select numLogement from LogementDispo natural join Disponibilite where dateDispo < TO_DATE('"
					+ dateFin + "','DD/MM/YYYY')) and  numHebergement = " + numHebergement;
		} else if (dateDeb.isEmpty() && dateFin.isEmpty() && numHebergement.isEmpty() == false) {
			query = "select * from Logement where ROWNUM <= 10 and numLogement in (select numLogement from LogementDispo natural join Disponibilite) and numHebergement = "
					+ numHebergement;
		} else {
			query = "select * from Logement where ROWNUM <= 10 and numLogement in (select numLogement from LogementDispo natural join Disponibilite)";
		}

		ResultSet rsFestivalRes = null;
		try {
			rsFestivalRes = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<LogementMazen> logementRes = new ArrayList<LogementMazen>();

		try {
			while (rsFestivalRes.next()) {
				LogementMazen res = new LogementMazen(rsFestivalRes.getString("numLogement"),
						rsFestivalRes.getString("description"), rsFestivalRes.getString("type_chambre"),
						rsFestivalRes.getString("nbEnfant"), rsFestivalRes.getString("nbAdulte"),
						rsFestivalRes.getString("capaciteMax"), rsFestivalRes.getString("prixLogement"),
						rsFestivalRes.getString("numHebergement"));
				logementRes.add(res);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return logementRes.iterator();
	}

	public Iterator<LogementMazen> getLogementDisponibleV1(String numHebergement) {

		String query = " SELECT L.NUMLOGEMENT , L.DESCRIPTION , L.TYPE_CHAMBRE , L.NBENFANT , L.NBADULTE , L.CAPACITEMAX , L.PRIXLOGEMENT , L.NUMHEBERGEMENT"
				+ " FROM Logement L JOIN LogementDispo LD ON L.NUMLOGEMENT = LD.NUMLOGEMENT"
				+ " WHERE numHebergement = " + numHebergement
				+ " GROUP BY L.NUMLOGEMENT , L.DESCRIPTION , L.TYPE_CHAMBRE , L.NBENFANT , L.NBADULTE , L.CAPACITEMAX , L.PRIXLOGEMENT , L.NUMHEBERGEMENT";
		ResultSet rsFestivalRes = null;
		try {
			rsFestivalRes = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<LogementMazen> logementRes = new ArrayList<LogementMazen>();

		try {
			while (rsFestivalRes.next()) {
				LogementMazen res = new LogementMazen(rsFestivalRes.getString("numLogement"),
						rsFestivalRes.getString("description"), rsFestivalRes.getString("type_chambre"),
						rsFestivalRes.getString("nbEnfant"), rsFestivalRes.getString("nbAdulte"),
						rsFestivalRes.getString("capaciteMax"), rsFestivalRes.getString("prixLogement"),
						rsFestivalRes.getString("numHebergement"));
				logementRes.add(res);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return logementRes.iterator();
	}

}
