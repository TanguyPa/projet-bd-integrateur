package fr.etu.m1.miage.festibed.model;

public class ReservationLogement {
	
	private String numLogement;
	private String numReservation;
	private String dateDeb;
	private String dateFin;
	private String description;
	private String numHebergement;
	
	public ReservationLogement(String numLogement, String numReservation, String dateDeb, String dateFin,String description, String nomHebergement) {
		super();
		this.numLogement = numLogement;
		this.numReservation = numReservation;
		this.dateDeb = dateDeb;
		this.dateFin = dateFin;
		this.description = description;
		this.numHebergement = nomHebergement;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNomHebergement() {
		return numHebergement;
	}

	public void setNomHebergement(String nomHebergement) {
		this.numHebergement = nomHebergement;
	}

	public String getNumLogement() {
		return numLogement;
	}

	public void setNumLogement(String numLogement) {
		this.numLogement = numLogement;
	}

	public String getNumReservation() {
		return numReservation;
	}

	public void setNumReservation(String numReservation) {
		this.numReservation = numReservation;
	}

	public String getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(String dateDeb) {
		this.dateDeb = dateDeb;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

}
