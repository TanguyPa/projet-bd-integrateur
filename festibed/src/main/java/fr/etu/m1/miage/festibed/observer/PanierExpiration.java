package fr.etu.m1.miage.festibed.observer;

public class PanierExpiration {

	private static PanierObserver panierObserver;
	
	
	public static PanierObserver getObserver() {
		if(panierObserver == null) {
			panierObserver = new PanierObserver();
		}
		return panierObserver;
	}
	
	
 }
