package fr.etu.m1.miage.festibed.apidao;

public interface DAOFactory {
	FestivalDAO getFestivalDAO();
	
	LogementDAO getLogementDAO(String type);

	ReservationDAO getReservationDAO();
	
}
