package fr.etu.m1.miage.festibed.service;

import java.sql.SQLException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.etu.m1.miage.festibed.exception.CreationUtilisateurException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.oracledao.OracleUtilisateurDAO;

public class UtilisateurService {

	public String authentifierUtilisateur(String numPersonne, String nom, String prenom, String telephone, String mail)
			throws ParameterErrorException, SQLException, CreationUtilisateurException {
		OracleUtilisateurDAO dao = new OracleUtilisateurDAO();
		int numUtil = dao.authentifierPersonne(numPersonne, nom, prenom, telephone, mail);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonString = gson.toJson(numUtil);
		return jsonString;
	}
}
