package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.Hebergeur;

public interface HebergeurDAO {
	
	Iterator<Hebergeur> getHebergeurs();

	int ajouterHebergeur(String numPersonne) throws SQLException;

	int getNumHebergeurById(int numPersonne);
	
}
