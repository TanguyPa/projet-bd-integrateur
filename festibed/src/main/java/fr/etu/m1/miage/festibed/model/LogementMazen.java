package fr.etu.m1.miage.festibed.model;

public class LogementMazen {

	
	private String numLogement;
    private String description;
    private String type_chambre;
    private String nbEnfant;
    private String nbAdulte; 
    private String capaciteMax; 
    private String prixLogement;
    private String numHebergement;
	
    public LogementMazen(String numLogement, String description, String type_chambre, String nbEnfant, String nbAdulte,
			String capaciteMax, String prixLogement, String numHebergement) {
		super();
		this.numLogement = numLogement;
		this.description = description;
		this.type_chambre = type_chambre;
		this.nbEnfant = nbEnfant;
		this.nbAdulte = nbAdulte;
		this.capaciteMax = capaciteMax;
		this.prixLogement = prixLogement;
		this.numHebergement = numHebergement;
	}
	
    public String getNumLogement() {
		return numLogement;
	}
	public void setNumLogement(String numLogement) {
		this.numLogement = numLogement;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType_chambre() {
		return type_chambre;
	}
	public void setType_chambre(String type_chambre) {
		this.type_chambre = type_chambre;
	}
	public String getNbEnfant() {
		return nbEnfant;
	}
	public void setNbEnfant(String nbEnfant) {
		this.nbEnfant = nbEnfant;
	}
	public String getNbAdulte() {
		return nbAdulte;
	}
	public void setNbAdulte(String nbAdulte) {
		this.nbAdulte = nbAdulte;
	}
	public String getCapaciteMax() {
		return capaciteMax;
	}
	public void setCapaciteMax(String capaciteMax) {
		this.capaciteMax = capaciteMax;
	}
	public String getPrixLogement() {
		return prixLogement;
	}
	public void setPrixLogement(String prixLogement) {
		this.prixLogement = prixLogement;
	}
	public String getNumHebergement() {
		return numHebergement;
	}
	public void setNumHebergement(String numHebergement) {
		this.numHebergement = numHebergement;
	}
    	
}
