package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.apidao.HebergeurDAO;
import fr.etu.m1.miage.festibed.model.Hebergeur;
import fr.etu.m1.miage.festibed.oracledao.OracleHebergeurDAO;

public class AjouterHebergeurServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("application/json");
		String numPersonne;

		try {	
			numPersonne = req.getParameter("numPersonne");
		} catch (NumberFormatException  numberFormatException) {
	        resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return;
   		}
		
		HebergeurDAO HebergeurDAO = new OracleHebergeurDAO();
		Hebergeur Hebergeur = new Hebergeur(numPersonne);
		
		// System.out.println("nom :"+ nom +", prenom :"+ prenom +", mail :"+ mail);
		
		try {
			HebergeurDAO.ajouterHebergeur(numPersonne);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print("Nouvelle Hebergeur ajoute a la base de donnees");
	
		resp.setStatus(HttpServletResponse.SC_OK);

	}
	
}
