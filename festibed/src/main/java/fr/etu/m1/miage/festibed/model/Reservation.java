package fr.etu.m1.miage.festibed.model;

import java.util.ArrayList;

public class Reservation {
	
	private String numReservation;
	private String dateReservation;
	private String dateValidation;
	private String etatReservation;
	private String montanTotal;
	private String nbEnfant;
	private String nbAdulte;
	private String numUtilisateur;
	private ArrayList<ReservationFestival> resFestival;
	private ArrayList<ReservationLogement> resLogement;
	
	public Reservation(String numReservation, String dateReservation, String dateValidation, String etatReservation,
			String montanTotal, String nbEnfant, String nbAdulte, String numUtilisateur) {
		super();
		this.numReservation = numReservation;
		this.dateReservation = dateReservation;
		this.dateValidation = dateValidation;
		this.etatReservation = etatReservation;
		this.montanTotal = montanTotal;
		this.nbEnfant = nbEnfant;
		this.nbAdulte = nbAdulte;
		this.numUtilisateur = numUtilisateur;
		this.resFestival = new ArrayList<ReservationFestival>();
		this.resLogement = new ArrayList<ReservationLogement>();
	}
	
	public Reservation(String montanTotal, String nbEnfant, String nbAdulte, String numUtilisateur) {
		super();
		this.numReservation = "0";
		this.dateReservation = " ";
		this.dateValidation = " ";
		this.etatReservation = "En cours";
		this.montanTotal = montanTotal;
		this.nbEnfant = nbEnfant;
		this.nbAdulte = nbAdulte;
		this.numUtilisateur = numUtilisateur;
		this.resFestival = new ArrayList<ReservationFestival>();
		this.resLogement = new ArrayList<ReservationLogement>();
	}
	
	public ReservationFestival getReservationFestival() {
		int taille = this.resFestival.size();
		if (taille > 0) {
			return this.resFestival.get(taille - 1);
		}
		return null;
	}
	
	public ReservationLogement getReservationLogement() {
		int taille = this.resLogement.size();
		if (taille > 0) {
			return this.resLogement.get(taille - 1);
		}
		return null;
	}
	
	public void setReservationFestival(ReservationFestival res) {
		this.resFestival.add(res);
	}
	
	public void setReservationLogement(ReservationLogement res) {
		this.resLogement.add(res);
	}

	public String getNumReservation() {
		return numReservation;
	}

	public void setNumReservation(String numReservation) {
		this.numReservation = numReservation;
	}

	public String getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(String dateReservation) {
		this.dateReservation = dateReservation;
	}

	public String getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(String dateValidation) {
		this.dateValidation = dateValidation;
	}

	public String getEtatReservation() {
		return etatReservation;
	}

	public void setEtatReservation(String etatReservation) {
		this.etatReservation = etatReservation;
	}

	public String getMontanTotal() {
		return montanTotal;
	}

	public void setMontanTotal(String montanTotal) {
		this.montanTotal = montanTotal;
	}

	public String getNbEnfant() {
		return nbEnfant;
	}

	public void setNbEnfant(String nbEnfant) {
		this.nbEnfant = nbEnfant;
	}

	public String getNbAdulte() {
		return nbAdulte;
	}

	public void setNbAdulte(String nbAdulte) {
		this.nbAdulte = nbAdulte;
	}

	public String getNumUtilisateur() {
		return numUtilisateur;
	}

	public void setNumUtilisateur(String numUtilisateur) {
		this.numUtilisateur = numUtilisateur;
	}
	
}
