package fr.etu.m1.miage.festibed.service;

import java.util.Iterator;

import com.google.gson.Gson;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.model.HebergementMazen;
import fr.etu.m1.miage.festibed.model.LogementMazen;
import fr.etu.m1.miage.festibed.oracledao.OracleLogementMazenDAO;

public class LogementMazenService {
	
	
	public String rechercherLogement(String numHebergement) throws ParameterErrorException {
		
		OracleLogementMazenDAO reservationDAO = new OracleLogementMazenDAO();
		
		Iterator<LogementMazen> reservations = reservationDAO.getLogementDisponibleV1(numHebergement);
		
		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse+="{ \"Logements\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	}
	
	/*
	
	public String rechercherLogementGson(String numHebergement,Gson gson) throws ParameterErrorException {
		
		OracleLogementMazenDAO reservationDAO = new OracleLogementMazenDAO();
		
		Iterator<LogementMazen> reservations = reservationDAO.getLogementDisponible(numHebergement);
		
		
		String jsonResponse = "";
		jsonResponse+=",{ \"Logements\": [";
		while(reservations.hasNext()) {
			jsonResponse += gson.toJson(reservations.next());
			if(reservations.hasNext())  {
				jsonResponse += ",";	
			}
		}
		jsonResponse+="]";
		jsonResponse+="}";
		
		return jsonResponse;
	} */
	
	
	public void rechercherLogementGsonV1(String dateDeb , String dateFin,HebergementMazen hebergement,String numHebergement,Gson gson) throws ParameterErrorException {
		
		OracleLogementMazenDAO reservationDAO = new OracleLogementMazenDAO();
		
		Iterator<LogementMazen> reservations = reservationDAO.getLogementDisponible(dateDeb,dateFin,numHebergement);
		
		while(reservations.hasNext()) {
			hebergement.setLogementMazen(reservations.next());
		}

	}
}
