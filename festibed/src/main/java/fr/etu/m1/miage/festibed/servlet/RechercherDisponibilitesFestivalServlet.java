package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.service.FestivalService;

public class RechercherDisponibilitesFestivalServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp){
	    HttpFormater.formatHeader(resp);
	    resp.setContentType("application/json");
		
		FestivalService festivalService = new FestivalService();
		String jsonResponse;
		try {
			jsonResponse = festivalService.rechercherDisponibilites(req.getParameter("id"));
		} catch (ParameterErrorException parameterErrorException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} catch (SQLException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} catch (RessourceNotFoundException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
        try {
			resp.getWriter().write(jsonResponse);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}
}