drop trigger nb_Adulte_and_nb_Enfant;
drop trigger placefestivaldisponible;
drop trigger logementcamping;
drop trigger noteFestival;
drop trigger noteHebergement;
drop trigger dateReservationFestival;
drop trigger dateReservationLogement;
drop trigger supprDispoLogement;
drop trigger montanTotaLogement;
drop trigger montanTotalFestival;
drop trigger LogementDisponible;
drop trigger AjouterDispo;

Create or replace trigger nb_Adulte_and_nb_Enfant
  Before insert on ReservationLogement
  For each row
  Declare
 	 numL integer;
  Begin
   numL := 0;
   
   select count(numLogement) into numL from Logement L , Reservation R where L.nbAdulte >= R.nbAdulte and L.nbEnfant >= R.nbEnfant and L.capaciteMax >= (R.nbAdulte + R.nbEnfant) and R.numReservation = :new.numReservation and L.numLogement = :new.numLogement;    
   
   if numL = 0 then
   		raise_application_error(-20010, 'Nombre d enfants ou d adultes ne correspond pas a la capacite du logement');
   end if;
   
  End;
  /

Create or replace trigger placefestivaldisponible
  Before insert on FestivalReservation
  For each row
  Declare
 	 sumPlaceCateg0 integer;
 	 sumPlaceCateg1 integer;
 	 sumPlaceCateg2 integer;
 	 cap0 integer;
 	 cap1 integer;
 	 cap2 integer;
  Begin
   sumPlaceCateg0 := 0; 
   sumPlaceCateg1 := 0;
   sumPlaceCateg2 := 0;

   select sum(nbCategorie0) into sumPlaceCateg0 from FestivalReservation where datePlaceRes = :new.datePlaceRes and numFestival = :new.numFestival;  
   select sum(nbCategorie1) into sumPlaceCateg1 from FestivalReservation where datePlaceRes = :new.datePlaceRes and numFestival = :new.numFestival;
   select sum(nbCategorie2) into sumPlaceCateg2 from FestivalReservation where datePlaceRes = :new.datePlaceRes and numFestival = :new.numFestival;

   select capaciteCateg0 into cap0 from Festival where numFestival = :new.numFestival;
   select capaciteCateg1 into cap1 from Festival where numFestival = :new.numFestival;
   select capaciteCateg2 into cap2 from Festival where numFestival = :new.numFestival;
   
   sumPlaceCateg0 := sumPlaceCateg0 + :new.nbCategorie0;
   sumPlaceCateg1 := sumPlaceCateg1 + :new.nbCategorie1;
   sumPlaceCateg2 := sumPlaceCateg2 + :new.nbCategorie2;

   if sumPlaceCateg0 > cap0 or sumPlaceCateg1 > cap1 or sumPlaceCateg2 > cap2 then
   		raise_application_error(-20010, 'Pas assez de places disponible dans Festival');
   end if;
  
  End;
  /

Create or replace trigger logementcamping
  Before insert or update on Logement
  For each row
  Declare
 	 maxm number;
 	 numH number;
  Begin

   select numHebergement into numH from Camping where numHebergement = :new.numHebergement;
   maxm := :new.capaciteMax;
   
   if maxm > 6 and numH > 0 then
   		raise_application_error(-20010,'Un camping a maximum 6 places');
   end if;

   EXCEPTION
   	WHEN NO_DATA_FOUND THEN
   	maxm := 0;
  
  End;
  /

 -- insert into Logement values (seq_logement.nextval,'Emplacement Camping','Autre',4,4,10,202,1);


  Create or replace trigger noteFestival
  Before insert or update on AvisFestival
  For each row
  Declare
 	 nbavis number;
 	 sumnote number;
  Begin

   select sum(note) into sumnote from AvisFestival where numFestival = :new.numFestival;
   select count(*) into nbavis from AvisFestival where numFestival = :new.numFestival;
   
   update Festival set noteMoyenne = (sumnote/nbavis) where numFestival = :new.numFestival;
  
  End;
  /

-- insert into AvisHebergement values (seq_avishebergement.nextval,'Donde esta la playa',9,4,2);
-- select noteMoyenne from Hebergement where numHebergement = 4;
-- select note from AvisHebergement where numHebergement = 4;

  Create or replace trigger noteHebergement
  Before insert or update on AvisHebergement
  For each row
  Declare
 	 nbavis number;
 	 sumnote number;
  Begin

   select sum(note) into sumnote from AvisHebergement where numHebergement = :new.numHebergement;
   select count(*) into nbavis from AvisHebergement where numHebergement = :new.numHebergement;
   
   update Hebergement set noteMoyenne = (sumnote/nbavis) where numHebergement = :new.numHebergement;
  
  End;
  /


  Create or replace trigger LogementDisponible
  Before insert on ReservationLogement
  For each row
  Declare
   numL number;
  Begin
   select count(numLogement) into numL from LogementDispo natural join Disponibilite  where numLogement = :new.numLogement and dateDispo >= :new.dateDebut and dateDispo <= :new.dateFin;

   if numL = 0 then
      raise_application_error(-20010, 'Logement non disponible pour la periode indiquee');
   end if;
  End;
  /

  Create or replace trigger AjouterDispo
  After delete on ReservationLogement
  For each row
  Declare
   CURSOR c1 IS select numDispo as nb from Disponibilite where dateDispo >= :old.dateDebut and dateDispo <= :old.dateFin;
   UnTuple c1%ROWTYPE;
  Begin
    OPEN c1;
    Fetch c1 into UnTuple;
    While c1%FOUND LOOP
      insert into LogementDispo values(:old.numLogement,UnTuple.nb);
      FETCH c1 INTO UnTuple;
    end LOOP;
    close c1;
  End;
  /

  Create or replace trigger dateReservationFestival
  Before insert on FestivalReservation
  For each row
  Declare
   dateR date;
  Begin
   select dateReservation into dateR from Reservation where numReservation = :new.numReservation;
   if to_date(dateR, 'DD-MM-YYYY') > to_date(:new.datePlaceRes, 'DD-MM-YYYY') then
   		raise_application_error(-20010, 'Date de reservation des places du Festival inferieur a la date de reservation');
   end if;
  End;
  /


  Create or replace trigger dateReservationLogement
  Before insert on ReservationLogement
  For each row
  Declare
 	 dateR date;
  Begin
   select dateReservation into dateR from Reservation where numReservation = :new.numReservation;
   if to_date(dateR, 'DD-MM-YYYY') > to_date(:new.dateDebut, 'DD-MM-YYYY') then
   		raise_application_error(-20010,'Date de reservation des logements inferieur a la date de reservation');
   end if;
  End;
  /

  Create or replace trigger supprDispoLogement
  After insert on ReservationLogement
  For each row
  Declare
 	 CURSOR c1 IS select numDispo as nb from Disponibilite where dateDispo >= :new.dateDebut and dateDispo <= :new.dateFin;
 	 UnTuple c1%ROWTYPE;
  Begin
  	OPEN c1;
  	Fetch c1 into UnTuple;
  	While c1%FOUND LOOP
  		delete from LogementDispo where numLogement = :new.numLogement and numDispo = UnTuple.nb;
  		FETCH c1 INTO UnTuple;
  	end LOOP;
  	close c1;
  End;
  /

  -- insert into AvisFestival values (seq_avisfestival.nextval,'Donde esta la playa',8,3,2);

  Create or replace trigger montanTotaLogement
  After insert or update on ReservationLogement
  For each row
  Declare
    montantL number;
  Begin
  	select sum(prixLogement) into montantL from ReservationLogement natural join Logement where numReservation = :new.numReservation;
  	update Reservation set montanTotal = montantL + montanTotal  where numReservation = :new.numReservation;
  End;
  /    

  drop trigger montanTotaLogement;


  Create or replace trigger montanTotalFestival
  After insert or update on FestivalReservation
  For each row
  Declare
    montantF1 number;
    montantF2 number;
    montantF3 number;
  Begin
  	select tarifCateg0*nbCategorie0,tarifCateg1*nbCategorie1,tarifCateg2*nbCategorie2 into montantF1, montantF2, montantF3  from FestivalReservation natural join Festival where rownum = 1  and numReservation = :new.numReservation;
  	update Reservation set montanTotal = montanTotal + montantF1 + montantF2 + montantF3  where numReservation = :new.numReservation;
  End;
  /  

  drop trigger montanTotalFestival;
  
  -- select noteMoyenne from Festival where numFestival = 2;
  -- select note from AvisFestival where numFestival = 3;

Create or replace trigger prixLogement
  After insert on Logement
  For each row
  Declare
   TEH integer;
   TAH integer;
   TEVV integer;
   TAVV integer;
   th integer;
   tc integer;
   tvv integer;
   tr integer;
   tpr integer;
   tarifR integer;
   tarifC integer;
   tarifPR integer;
  Begin
  	th := 0;
  	tc := 0;
  	tvv := 0;
  	tr := 0;
  	tpr := 0;
  	select count(*) into th from Hotel where numHebergement = :new.numLogement;
  	select count(*) into tc from Camping where numHebergement = :new.numLogement;
  	select count(*) into tvv from VillageVacances  where numHebergement = :new.numLogement;
  	select count(*) into tr from Residence where numHebergement = :new.numLogement;
  	select count(*) into tpr from ParcResidentiel where numHebergement = :new.numLogement;

  	if tvv > 0 then 
  		select tarifEnfant, tarifAdulte  into TEVV,TAVV from VillageVacances  where numHebergement = :new.numLogement;
  		DBMS_OUTPUT.put_line(:new.nbEnfant);
  		update Logement set prixLogement = (:new.nbEnfant*TEVV + :new.nbAdulte*TAVV)  where numLogement = :new.numLogement;
  	elsif th > 0 then 
  		select tarifEnfant, tarifAdulte into TEH,TAH from Hotel where numHebergement = :new.numLogement;
  		DBMS_OUTPUT.put_line(:new.nbEnfant);
  		update Logement set prixLogement = (:new.nbEnfant*TEH + :new.nbAdulte*TAH) where numLogement = :new.numLogement;
  	elsif tr > 0 then 
  		select tarif into tarifR from Residence where numHebergement = :new.numLogement;
		update Logement set prixLogement = :new.capaciteMax*(tarifR) where numLogement = :new.numLogement;
	elsif tc > 0 then 
		select tarif into tarifC from Camping where numHebergement = :new.numLogement;
		update Logement set prixLogement = :new.capaciteMax*(tarifC) where numLogement = :new.numLogement;
	elsif tpr > 0 then 
		select tarif into tarifPR from ParcResidentiel where numHebergement = :new.numLogement;
		update Logement set prixLogement = :new.capaciteMax*(tarifPR) where numLogement = :new.numLogement;
  	end if;

  End;
  /    

  drop trigger prixLogement;
	
  -- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue sur la mer magnifique','Simple',1,1,1,100,2);
