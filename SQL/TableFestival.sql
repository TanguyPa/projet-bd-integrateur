﻿INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BOB''ARTS','Musiques actuelles','','Le Blanc','24/08/2019','25/08/2019','36300','46.6346200347','1.10013221164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de jeunesse de Rouen','Livre et litt�rature','www.festival-livre-rouen.fr','ROUEN','29/11/2019','01/12/2019','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival VO/VF, la parole aux traducteurs','Livre et litt�rature','https://www.festivalvo-vf.com/','Gif Sur Yvette','04/10/2019','06/10/2019','91190','48.6988273634','2.12788365005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon de la bande dessin�e SoBD','Livre et litt�rature','www.sobd.fr','PARIS','06/12/2019','08/12/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHEZ OIM FEST','Musiques actuelles','www.chezoimfest.fr','NOYELLES SOUS BELLONNE','','','62490','50.3091545259','3.02186108116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Art contemporain en milieu rural','Arts plastiques et visuels','https://www.galerie-tem.fr/','GOVILLER','','','54330','48.5051745323','6.01397377452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AGITES DU BOCAL A NIVILLAC','Pluridisciplinaire Spectacle vivant','','Nivillac','','','56130','47.5405533272','-2.24923467173');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES METISSES A COLMAR','Musiques actuelles','http://lezard.org/category/festival-musiques-metisses/','COLMAR','','','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tendance Clown','Cirque et Arts de la rue','http://www.dakiling.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BERRY LAIT','Musiques actuelles','http://festival-berry-lait.fr/','CHATEAUROUX','','','36000','46.8029617828','1.69399812001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DIXIE FOLIES','Musiques actuelles','www.dixie-folies.com','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FACS','Musiques actuelles','','BRESSUIRE','','','79300','46.8545755683','-0.479375922286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RESPIRE JAZZ FESTIVAL','Musiques actuelles','www.respirejazzfestival.com','Montmoreau','','','16190','45.4225777872','0.127349505692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Maghreb, si loin, si proche - Aude/Pyr�n�es Orientales','Cin�ma et audiovisuel','https://www.cinemaginaire.org/festivals/maghreb-si-loin-si-proche','ARGELES SUR MER','','','66700','42.5352193463','3.02429862885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAITES L''EUROPE / FETE DE L''EUROPE','Pluridisciplinaire Musique','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA DUBERIE','Musiques actuelles','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLARI''JAZZ FESTIVAL','Musiques actuelles','www.clarijazz.com','MARIGNAC LASCLARES','','','31430','43.3049878647','1.10662037582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Coul�e Douce','Cirque et Arts de la rue','https://progeniture.fr/coulee-douce/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Archif�te','Cirque et Arts de la rue','https://www.ville-petit-couronne.fr/Services-en-ligne/Agenda/Archifete','Petit Couronne','','','76650','49.3807622557','1.03422612047');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CANAL EN SC�NE','Musiques actuelles','www.artnonyme.jimdo.com','BLAIN','','','44130','47.4628210409','-1.76789479614');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES VAGAMONDES','Transdisciplinaire','www.lafilature.org','MULHOUSE','09/01/2019','19/01/2019','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FOUS RIRES DE BORDEAUX','Divers Spectacle vivant','http://lesfousriresdebordeaux.fr/','BORDEAUX','16/03/2019','23/03/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZENVILLE','Musiques actuelles','www.jazzauconfluent.fr','CONFLANS STE HONORINE','15/03/2019','30/03/2019','78700','49.0001007823','2.0990719052');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRANSFER','Musiques actuelles','https://www.weezevent.com','LYON','07/03/2019','09/03/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HUMOUR EN WEPPES','Divers Spectacle vivant','www.humour-en-weppes.webnode.fr','LA BASSEE','07/03/2019','17/03/2019','59480','50.5397647046','2.80910290453');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE PRINTEMPS ETUDIANT','Musiques actuelles','www.leprintempsetudiant.com','TOULOUSE','02/04/2019','13/04/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TETE DANS LES NUAGES','Pluridisciplinaire Spectacle vivant','www.theatre-angouleme.org','ANGOULEME','09/03/2019','16/03/2019','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEMIN''ARTE','Divers Spectacle vivant','http://www.theatre-tribunal.fr/','ANTIBES','08/03/2019','30/03/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUPER FLUX','Pluridisciplinaire Musique','https://www.letempsmachine.com/','TOURS','29/03/2019','31/03/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Litt�rature au centre','Livre et litt�rature','http://litteratureaucentre.net/','CLERMONT FERRAND','27/03/2019','31/03/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Journ�es du livre et du vin','Transdisciplinaire','www.livreetvin.com','SAUMUR','13/04/2019','14/04/2019','49400','47.2673853525','-0.0830410591988');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DETOURS DE CHANT','Musiques actuelles','www.detoursdechant.com','TOULOUSE','22/01/2019','02/02/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre autour du polar','Livre et litt�rature','https://www.facebook.com/salondulivreNemours/','NEMOURS','27/01/2019','28/01/2019','77140','48.2596067909','2.71107993087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOMIX','Pluridisciplinaire Spectacle vivant','www.momix.org','KINGERSHEIM','31/01/2019','10/02/2019','68260','47.7889765168','7.32299335222');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA ROUTE DU ROCK - COLLECTION HIVER','Musiques actuelles','www.laroutedurock.com','ST MALO','19/02/2019','23/02/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CERNUNNOS PAGAN FEST','Musiques actuelles','www.cernunnos-festival.fr','Noisiel','23/02/2019','24/02/2019','77186','48.8460926469','2.61997100426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WINTOWER','Musiques actuelles','http://www.woodstower.com','LYON','01/02/2019','02/02/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon international du livre rare et de l�objet d�art','Transdisciplinaire','www.salondulivrerare.paris','PARIS','12/04/2019','14/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV''ARTS A GRENOBLE','Cirque et Arts de la rue','http://festiv-arts.com','GRENOBLE','11/04/2019','13/04/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PUY DE M�MES','Pluridisciplinaire Spectacle vivant','http://www.cournon-auvergne.fr/Culture-et-patrimoine/Festival-Puy-de-Momes','COURNON','09/04/2019','18/04/2019','56200','47.7490041075','-2.09816065532');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Peinture Fra�che','Arts plastiques et visuels','https://www.peinturefraichefestival.fr/','LYON','03/05/2019','12/05/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de quiberon','Livre et litt�rature','www.librairiesdeportmaria.fr','QUIBERON','27/04/2019','28/04/2019','56170','47.4881657518','-3.12282400639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ART ET LUMIERE','Musiques actuelles','http://acsl-furdenheim.org','Furdenheim','05/07/2019','07/07/2019','67117','48.6105637831','7.55789773047');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE THAU - ESCALES MUSICALES','Musiques actuelles','www.festivaldethau.com','MEZE','15/07/2019','23/07/2019','34140','43.4292981138','3.57774572973');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU FIL DU CHER','Musiques actuelles','www.agglo-montlucon.fr','LAVAULT STE ANNE','16/07/2019','21/07/2019','3100','46.3086016059','2.60160340266');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIC EN VIGNES','Musiques actuelles','http://www.musicenvignes.fr/','Le Puy Ste Reparade','17/07/2019','19/07/2019','13610','43.6518285649','5.43958389565');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''COCCINELLE','Musiques actuelles','www.festicoccinelle.org','AUXON','16/07/2019','16/07/2019','10130','48.0923181622','3.92346188322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JARDIN SONORE','Musiques actuelles','','VITROLLES','26/07/2019','28/07/2019','13127','43.4497831674','5.26357787665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CABARET FRAPP�','Musiques actuelles','www.cabaret-frappe.com','GRENOBLE','15/07/2019','20/07/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JEUDIS DU PORT','Musiques actuelles','https://www.brest.fr/brest-fr-accueil-3.html','BREST','25/07/2019','15/08/2019','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international d�Art Lyrique d�Aix-en-Provence','Musiques classiques','www.festival-aix.com','AIX EN PROVENCE','03/07/2019','22/07/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN SOL MINEUR','Musiques actuelles','www.jazzsolmineur.org','GREASQUE','04/07/2019','06/07/2019','13850','43.4262029745','5.54685109195');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A COUCHES','Musiques actuelles','https://www.jazzacouches.fr/','COUCHES','03/07/2019','06/07/2019','71490','46.869155076','4.56431598435');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�cibulles','Musiques actuelles','www.decibulles.com','NEUVE EGLISE','12/07/2019','14/07/2019','67220','48.3261151153','7.31059686746');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES JEUDIS LIVE','Musiques actuelles','www.ville-saintraphael.fr','ST RAPHAEL','11/07/2019','22/08/2019','83700','43.4574625431','6.84734210398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE REVE DE L''ABORIGENE','Musiques actuelles','http://www.lerevedelaborigene.org/','AIRVAULT','19/07/2019','21/07/2019','79600','46.8329725103','-0.135880248296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIVADA RODEZ','Musiques actuelles','https://www.ville-rodez.fr/fr/culture-et-loisirs/estivada/le-festival.php','RODEZ','18/07/2019','20/07/2019','12000','44.3582426254','2.5672793892');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ao�t musical de Deauville','Musiques classiques','https://musiqueadeauville.com/','DEAUVILLE','27/07/2019','10/08/2019','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''VAUX','Musiques actuelles','http://www.vaux-sur-mer.com','Vaux sur Mer','06/08/2019','08/08/2019','17640','45.6448017664','-1.05853577703');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAINES D''AUTOMNE','Musiques actuelles','www.grainesdautomne.org','NOZAY','','','44170','47.5728858483','-1.60097664616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES ROYAL''S DU RIRE','Divers Spectacle vivant','www.mandelieu.fr/actualites-mandelieu/manifestations-mandelieu_royals-rire.php','MANDELIEU LA NAPOULE','','','6210','43.5380510468','6.91808936542');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d�architecture d�Orl�ans','Domaines divers','','ORLEANS','','','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTE LE SON','Musiques actuelles','www.paris-bibliotheques.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RHINO JAZZ(S) FESTIVAL','Musiques actuelles','www.rhinojazz.com','ST CHAMOND','','','42400','45.4698319517','4.50184989506');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ & GARONNE','Musiques actuelles','http://www.djazzetgaronne.com','MARMANDE','','','47200','44.5055176845','0.172174280849');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES INDISCIPLINEES','Musiques actuelles','www.lesindisciplinees.com','LORIENT','','','56100','47.7500486947','-3.37823200917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIM, Festival Itin�rant de Marionnettes','Divers Spectacle vivant','http://www.fim-marionnette.com/','VIEUX CONDE','','','59690','50.4735638651','3.56606734565');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ENTENTE NOCTURNE FESTIVAL','Musiques actuelles','http://lekilowatt.fr/agenda/entente-nocturne/','VITRY SUR SEINE','','','94400','48.7882828307','2.39412680533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECOUTER POUR L''INSTANT','Musiques actuelles','http://manege-music.fr/','QUEYSSAC','','','24140','44.9151488173','0.532361895474');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ENFANTAISIES','Pluridisciplinaire Spectacle vivant','http://enfantaisies.free.fr/','LA ROCHE SUR YON','','','85000','46.6675261644','-1.4077954093');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONCERTS D''AUTOMNE','Musiques classiques','www.concerts-automne.com','TOURS','','','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Jean Carmet','Cin�ma et audiovisuel','www.cinebocage.com','Moulins','','','3000','46.5624641056','3.32662040221');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRANDS ZYEUX PTITES ZOREILLES','Transdisciplinaire','www.beziers-mediterranee.fr','BEZIERS','','','34500','43.3475883319','3.23076754164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HUMOUR DE COLMAR','Divers Spectacle vivant','www.festival-humour-colmar.fr/','COLMAR','','','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOIREES D''AUTOMNE','Pluridisciplinaire Spectacle vivant','www.soireesdautomne.com','Le Barroux','','','84330','44.1547724058','5.09918948911');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UGINE CITY ROCK','Musiques actuelles','http://www.ugine.com','UGINE','','','73400','45.7718876928','6.43266345275');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GAMERZ','Arts plastiques et visuels','www.festival-gamerz.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LANVELLEC','Musiques classiques','www.festival-lanvellec.fr','LANVELLEC','','','22420','48.6072769742','-3.52795086821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� Viva Villa !','Transdisciplinaire','www.vivavilla.info','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GAFFER FESTIVAL','Musiques actuelles','http://www.gafferfest.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRASS OPEN DE LESQUIN - RENCONTRE EUROPENNE DE CUIVRES','Pluridisciplinaire Musique','http://brassopen.free.fr/','Lesquin','','','59810','50.5892285666','3.11191934713');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CELTES EN OUCHE','Musiques actuelles','www.routeducelte.fr','CONCHES EN OUCHE','','','27190','48.9547259686','0.93199579313');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LUBERON MUSIC FESTIVAL','Musiques actuelles','http://www.luberonmusicfestival.com','APT','31/05/2019','02/06/2019','84400','43.879393265','5.38921757843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festi''val d''Olt','Pluridisciplinaire Spectacle vivant','www.festivaldolt.org','LE BLEYMARD','30/05/2019','01/06/2019','48190','44.5107452501','3.74294007436');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Cin�maginaire d�Argel�s-sur-mer','Cin�ma et audiovisuel','http://www.cinemaginaire.org','ARGELES SUR MER','29/05/2019','02/06/2019','66700','42.5352193463','3.02429862885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nuits sonores','Musiques actuelles','www.nuits-sonores.com','LYON','29/05/2019','02/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOISSONS ROCK','Musiques actuelles','www.moissonsrock.org','JUVIGNY','29/05/2019','01/06/2019','51150','49.0158806281','4.27525107126');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MADE FESTIVAL','Musiques actuelles','www.made-festival.fr','RENNES','24/05/2019','25/05/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SKA BUZ FESTIVAL','Musiques actuelles','http://skabuz.com/','Buzancais','25/05/2019','25/05/2019','36500','46.8875380464','1.41254913131');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DIME ON FEST','Musiques actuelles','','NICE','26/05/2019','26/05/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV''IMPRO','Th��tre','http://www.impronet.net/festivimpro-2018/','VERSAILLES','23/05/2019','25/05/2019','78000','48.8025669671','2.11789297191');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Friction(s)','Pluridisciplinaire Spectacle vivant','www.chateau-rouge.net','ANNEMASSE','23/05/2019','25/05/2019','74100','46.1909730986','6.24250704322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIG LOVE','Musiques actuelles','http://crabcakecorporation.com','RENNES','07/06/2019','09/06/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du go�land masqu�','Livre et litt�rature','https://goelandmasque.fr/','Penmarch','08/06/2019','10/06/2019','29760','47.8121478804','-4.34003367039');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VYV Les Solidarit�s','Musiques actuelles','https://vyv-les-solidarites.org/','DIJON','08/06/2019','09/06/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PSYMIND ORIGINS','Musiques actuelles','','Rognes','08/06/2019','08/06/2019','13840','43.6648781836','5.35218247017');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AIX EN PROVOCK','Musiques actuelles','provock.fr','AIX EN PROVENCE','18/05/2019','19/05/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AEROBD','Livre et litt�rature','http://aerobd-istres.fr/','ISTRES','18/05/2019','19/05/2019','13118','43.5502689105','4.9511813524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA NUIT DE L''ERDRE','Musiques actuelles','www.lanuitdelerdre.fr','NORT SUR ERDRE','28/06/2019','30/06/2019','44390','47.447122285','-1.51824792558');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK TON BLED','Musiques actuelles','www.rocktonbled.fr','MONTASTRUC LA CONSEILLERE','29/06/2019','29/06/2019','31380','43.7221234765','1.58906433836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Travers�es Tatihou','Musiques actuelles','https://www.manche.fr/culture/traversees-tatihou.aspx','St  Vaast la Hougue','','','50550','49.5993508281','-1.27306891683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS CAJUN DE SAULIEU','Musiques actuelles','www.bayouprod.com','SAULIEU','','','21210','47.2840306915','4.23192469784');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Ramatuelle','Th��tre','www.festivalderamatuelle.com','RAMATUELLE','','','83350','43.2186126186','6.63754734174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Soir�es lyriques de Sanxay','Musiques classiques','http://www.operasanxay.fr/','SANXAY','','','86600','46.4960318018','-0.00840229241339');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE BUGUELES','Musiques actuelles','https://www.festivaldebugueles.fr/','Penvenan','','','22710','48.816014407','-3.30224745841');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PYRENEAN WARRIORS OPEN AIR','Musiques actuelles','www.leshordesmetalliques.com','TORREILLES','','','66440','42.7533924686','3.00860614432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR LES PLACES','Musiques actuelles','www.jazzsurlesplaces.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HAPPY DAYS','Musiques actuelles','www.festivalhappydays.com','FONTANIL CORNILLON','','','38120','45.2540718462','5.66423363739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FACE ET SI','Musiques actuelles','www.festival-faceetsi.com','MOUILLERON LE CAPTIF','','','85000','46.7104764993','-1.46129661418');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Rue Bucolique','Cirque et Arts de la rue','http://letourp.com/evenements/la-rue-bucolique/','La Hague','','','50440','49.6631647329','-1.84267363083');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Etats g�n�raux du documentaire de Lussas','Cin�ma et audiovisuel','www.lussasdoc.org','Lussas','','','7170','44.6192177092','4.46630461903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAULXURES FESTI LAC','Musiques actuelles','http://festilac.saulxures-sur-moselotte.fr/','Saulxures sur Moselotte','','','88290','47.9512129215','6.77493861259');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Troubles Ville','Cirque et Arts de la rue','www.lestroublesville.com','Connerre','','','72160','48.0507447506','0.467977281249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BITTER SWEET (PARADISE) FESTIVAL','Musiques actuelles','www.baladessonores.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film de montagne','Cin�ma et audiovisuel','https://festival-autrans.com/fr','Autrans Meaudre en Vercors','','','38880','45.1321769623','5.52869303726');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d�art contemporain de Strasbourg','Arts plastiques et visuels','http://biennale-strasbourg.eu/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COMME CA NOUS CHANTE','Musiques actuelles','www.cafeplum.org','LAUTREC','','','81440','43.7052468491','2.12929403349');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLASSIQ A STAINS','Musiques classiques','www.ville-stains.fr/epe','Stains','','','93240','48.9567775699','2.3854495629');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARIONNETTISSIMO','Divers Spectacle vivant','https://www.marionnettissimo.com','TOURNEFEUILLE','','','31170','43.5781918597','1.33500697752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A FAREINS','Musiques actuelles','www.jazzafareins.com','FAREINS','','','1480','46.020997974','4.7620080898');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL VRRRR','Transdisciplinaire','www.manoeuvrrrr.fr','TOULON','','','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SCENES AU BAR','Pluridisciplinaire Spectacle vivant','www.scenes-au-bar.fr','METZ','12/03/2019','16/03/2019','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA JOURNEE DE LA GUITARE A MUTZIG','Musiques actuelles','www.jdlg.eu','Mutzig','16/03/2019','17/03/2019','67190','48.538054845','7.45530315563');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Pouce !','Danse','lamanufacture-cdcn.org','BORDEAUX','06/02/2019','16/02/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE BEAUTIFUL SWAMP BLUES FESTIVAL','Musiques actuelles','https://www.facebook.com/events/2096433980594730/','CALAIS','26/04/2019','28/04/2019','62100','50.9502072754','1.87575566132');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�RE DE RIEN','Transdisciplinaire','www.festival-lerederien.com','REZE','26/04/2019','28/04/2019','44400','47.1762338904','-1.54966399893');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRUISME','Musiques actuelles','https://lieumultiple.org/','POITIERS','27/06/2019','30/06/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''VEYLE','Musiques actuelles','','ST Cyr Sur Menthon','28/06/2019','29/06/2019','1380','46.2754005606','4.96757105158');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A SAINT REMY','Musiques actuelles','www.jazzasaintremy.fr','ST REMY DE PROVENCE','','','13210','43.7834249015','4.85366665286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PUCELLE','Musiques actuelles','www.festivaldelapucelle.fr','STE CATHERINE DE FIERBOIS','','','37800','47.1566893573','0.676679379745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOIREES CREPUSCULE','Pluridisciplinaire Spectacle vivant','www.soireescrepuscule.fr','MONTAUBAN','','','82000','44.0222594578','1.36408636501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''AUTOMNE DE MORTHOMIERS','Musiques actuelles','http://www.automne-morthomiers.com','MORTHOMIERS','','','18570','47.0436630553','2.27235598629');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES CLOWNS DES BURLESQUES ET DES EXCENTRIQUES','Cirque et Arts de la rue','www.lesamovar.net','BAGNOLET','','','93170','48.8690836308','2.42274096688');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSCADEATH','Musiques actuelles','http://muscadeath.fr/','Vallet','','','44330','47.1715153308','-1.26146566077');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Villes des musiques du monde','Musiques actuelles','www.villesdesmusiquesdumonde.com','AUBERVILLIERS','','','93300','48.9121722626','2.38445513768');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Mois Kr�yol','Pluridisciplinaire Spectacle vivant','https://lemoiskreyol.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Overlook','Musiques actuelles','www.rocksane.com','BERGERAC','','','24100','44.8543751872','0.486529423457');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRUIT DES VOIX','Musiques actuelles','www.lefruitdesvoix.com','LONS LE SAUNIER','','','39000','46.6744796278','5.55733212947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Internationales du Cin�ma des Antipodes � Saint-Tropez','Cin�ma et audiovisuel','www.festivaldesantipodes.org','ST Tropez','','','83990','43.262314383','6.66343039908');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PARIS BANLIEUES TANGO','Musiques actuelles','www.festival-paris-banlieues-tango.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GERSON FAIT SON FESTIVAL','Divers Spectacle vivant','http://www.espacegerson.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vendanges litt�raires','Livre et litt�rature','http://www.vendangeslitteraires.com/','Rivesaltes','','','66600','42.7785910442','2.87687296538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BACO REGGAE FESTIVAL','Musiques actuelles','https://www.bacorecords.fr/baco-reggae-festival/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TELERAMA DUB FESTIVAL','Musiques actuelles','www.teleramadubfestival.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES MUSIQUES INTERDITES','Musiques classiques','www.musiques-interdites.eu','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK THIS TOWN A VALENCE','Musiques actuelles','www.mistralpalace.com','VALENCE','','','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELEKT''RHONE FESTIVAL','Musiques actuelles','http://elektrhone.com/','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MINIFEST JAZZ AU EL CAMINO','Musiques actuelles','www.el-camino-cafe.fr','CAEN','','','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MUSICAL D''AUTOMNE DES JEUNES INTERPRETES / FIMAJ','Musiques classiques','www.fmaji.com','MONTMORENCY','','','95160','48.9918643363','2.32119797848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SOY','Musiques actuelles','https://festival.soy/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ACCES)S( CULTURE(S) �LECTRONIQUE(S(','Transdisciplinaire','www.acces-s.org','BILLERE','','','64140','43.3045259937','-0.393492580041');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Travers�es','Cin�ma et audiovisuel','https://www.ccjb.fr/','PONTARLIER','','','25300','46.9119730882','6.38914602031');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�tes le pont (�dition mai)','Cirque et Arts de la rue','www.cnarsurlepont.fr','LA ROCHELLE','17/05/2019','19/05/2019','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Wazemmes l''Accord�on','Musiques actuelles','www.flonflons.eu','LILLE','18/05/2019','15/06/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EBULLI''SON','Musiques actuelles','https://www.facebook.com/festivalebullison/','Montfaucon','17/05/2019','18/05/2019','25660','47.2418571296','6.08660837587');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA POULE A FACETTES','Musiques actuelles','assoartlequin.wix.com/lapouleafacettes','Cormery','17/05/2019','18/05/2019','37320','47.2592432669','0.845873952159');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUMMER DE GRAINES','Musiques actuelles','www.summerdegraines.com','Villemur Sur Tarn','18/05/2019','19/05/2019','31340','43.8644095962','1.49043812706');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SUDS EN HIVER (ARLES)','Musiques actuelles','http://www.suds-arles.com/fr/2018','ARLES','02/03/2019','10/03/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZAT (ZONE ARTISTIQUE TEMPORAIRE)','Cirque et Arts de la rue','https://www.montpellier-tourisme.fr/','MONTPELLIER','08/06/2019','28/07/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LOIR EN ZIC','Musiques actuelles','','Brives Charensac','14/06/2019','16/06/2019','43700','45.0463267089','3.92401115104');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Bonne Aventure','Musiques actuelles','','DUNKERQUE','22/06/2019','23/06/2019','59430','51.0307229078','2.33752414095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MORZINE HARLEY DAYS','Musiques actuelles','https://www.morzine-avoriaz.com/morzine-avoriaz-harley-days-2019/','MORZINE','11/07/2019','14/07/2019','74110','46.1732135057','6.74252144427');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIMOME','Cirque et Arts de la rue','http://www.xn--festimme-93a.fr/','AUBAGNE','18/07/2019','21/07/2019','13400','43.2934843764','5.56331273477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Plage Musicale en Bangor','Musiques classiques','http://www.belleilemusique.com/','BANGOR','15/07/2019','26/07/2019','56360','47.313014109','-3.1920046614');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE GRAND SON','Musiques actuelles','www.legrandson.fr','ST PIERRE DE CHARTREUSE','18/07/2019','21/07/2019','38380','45.331212571','5.79772583855');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DE BOUCHE A OREILLE EN PETITE MONTAGNE','Pluridisciplinaire Musique','www.festival-jura.com','ST JULIEN','06/07/2019','26/07/2019','39320','46.3963208','5.4577858737');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEMAINE ACADIENNE','Musiques actuelles','','ST AUBIN SUR MER','08/08/2019','15/08/2019','14750','49.3224328819','-0.39282763592');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international d''op�ra baroque de Beaune','Musiques classiques','https://www.festivalbeaune.com/','Beaune','05/07/2019','28/07/2019','21200','47.0255189366','4.83767985985');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIDREUZ','Musiques actuelles','http://www.festidreuz.fr/','FOUESNANT','05/07/2019','06/07/2019','29170','47.8783078862','-4.01834628474');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOMADE REGGAE FESTIVAL','Musiques actuelles','http://www.nomadereggaefestival.com','Frangy','02/08/2019','04/08/2019','74270','46.0231025542','5.92267414864');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DE LA TERRASSE ET DEL CATET','Pluridisciplinaire Spectacle vivant','www.sortieouest.fr','THEZAN LES BEZIERS','29/07/2019','04/08/2019','34490','43.4132925116','3.15810877475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bastid''Art','Cirque et Arts de la rue','http://bastidart.org/','MIRAMONT DE GUYENNE','02/08/2019','04/08/2019','47800','44.5833450029','0.356429133423');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DANS TON KULTE','Musiques actuelles','www.k-potes.com','VIRIAT','05/07/2019','06/07/2019','1440','46.2508985632','5.22302249216');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival De pages en plages','Livre et litt�rature','www.depagesenplages.fr','LECCI','05/07/2019','07/07/2019','20137','41.6654830708','9.31906773443');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DU BRUIT A SAINT NOLFF','Musiques actuelles','','ST Nolff','06/07/2019','07/07/2019','56250','47.6984675237','-2.66583210925');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PYHC FEST','Musiques actuelles','http://www.pyhc.fr/','PONT SUR YONNE','19/07/2019','20/07/2019','89140','48.2787429774','3.1934739546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma belge � N�mes et en Garrigues','Cin�ma et audiovisuel','www.festivalcinemabelge.fr','NIMES','19/07/2019','25/07/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA MER','Musiques actuelles','http://www.festivaldelamer.com','Landunvez','26/07/2019','26/07/2019','29840','48.5343990673','-4.72936031932');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HORTUS LIVE','Musiques actuelles','http://hortuslive.com','Valflaunes','20/07/2019','20/07/2019','34270','43.8020094105','3.86188285063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INSANE FESTIVAL','Musiques actuelles','http://www.insanefestival.com','APT','10/08/2019','11/08/2019','84400','43.879393265','5.38921757843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Classique au Vert','Musiques classiques','www.lesclayessousbois.fr','PARIS','10/08/2019','01/09/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFRICOLOR','Musiques actuelles','www.africolor.com','ST DENIS','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AUTOMNALES DU FESTIVAL DE THAU','Transdisciplinaire','http://www.festivaldethau.com','SETE','','','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film de Sarlat','Cin�ma et audiovisuel','www.festivaldufilmdesarlat.com','Sarlat','','','24200','44.8983558068','1.2066486429');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Traces de vie - Festival du film documentaire de Clermont-Ferrand / Vic-le-Comte','Cin�ma et audiovisuel','https://tracesdevies.org/','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Nuits du Jazz de Nantes','Musiques actuelles','www.nuitdujazz.com','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale des Arts du Mime et du Geste','Divers Spectacle vivant','http://www.collectifartsmimegeste.com/index.php','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival tous courts d''Aix en Provence','Cin�ma et audiovisuel','www.festivaltouscourts.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Plein(s) Ecran(s)','Cin�ma et audiovisuel','https://www.facebook.com/pleinsecrans','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BONSOIR PARIS !','Musiques actuelles','www.festivalbonsoirparis.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ''N''KLEZMER','Musiques actuelles','http://www.jazznklezmer.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre en Bretagne','Livre et litt�rature','https://www.festivaldulivre-carhaix.bzh/fr/','CARHAIX PLOUGER','26/10/2019','27/10/2019','29270','48.2687578823','-3.56626363851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des litt�ratures europ�ennes de Cognac','Livre et litt�rature','www.litteratures-europeennes.com','COGNAC','14/11/2019','17/11/2019','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE ACTION','Pluridisciplinaire Musique','www.musiqueaction.com','VANDOEUVRE LES NANCY','','','54500','48.6579999356','6.16531229307');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EPICURIEUX FESTIVAL','Musiques actuelles','','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�CLATS DE VOIX (PREIGNAN)','Musiques actuelles','www.eclatsdevoix.com','PREIGNAN','','','32810','43.7210485292','0.636835530591');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Chainon manquant','Pluridisciplinaire Spectacle vivant','www.lechainon.fr','LAVAL','17/09/2019','22/09/2019','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Foire en sc�ne','Musiques actuelles','www.foireenscene.fr','CHALONS EN CHAMPAGNE','30/08/2019','09/09/2019','51000','48.9640892125','4.37883539725');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS BLANCHES','Musiques actuelles','https://www.lesnuitsblanches.org/','LE THORONET','26/08/2019','28/07/2019','83340','43.458933028','6.28265623456');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE GRAND PRUNEAU SHOW','Musiques actuelles','http://www.grandpruneaushow.fr/','AGEN','30/08/2019','01/09/2019','47000','44.2028139104','0.625583928763');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRA INCOGNITA','Musiques actuelles','https://terraincognita.me/','Carelles','23/08/2019','24/08/2019','53120','48.3887876245','-0.899702305853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI WOOD','Musiques actuelles','','ST Vincent de Barres','23/08/2019','24/08/2019','7210','44.6573798022','4.70779936262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''ENERGIES DE SOULAC SUR MER','Musiques actuelles','http://www.festivaldesenergies.org','SOULAC SUR MER','','','33780','45.493296382','-1.10918123147');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAULE D''AIRS','Cirque et Arts de la rue','https://www.commune-baule.fr/culture-et-associations/associations/baule-d-airs','BAULE','','','45130','47.8106703162','1.66824027351');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Douchynoiseries','Cirque et Arts de la rue','http://douchy-les-mines.com/programme-des-douchynoiseries/','Douchy les Mines','','','59282','50.2994352925','3.38947295524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Fanfares de Montpellier','Musiques actuelles','http://festivalfanfare.free.fr/','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BATEAU MUSIC FESTIVAL','Musiques actuelles','www.bateaumusic.com','Les Mesnuls','','','78490','48.754542649','1.83436184333');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WHAT THE FEST','Musiques actuelles','http://whatthefest.com','Vendargues','','','34740','43.6610457488','3.96283033113');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA LOUPE FAIT SON FESTIVAL','Musiques actuelles','http://www.festivallaloupe-24.webself.net','LA LOUPE','','','28240','48.473175806','1.02235330632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PALAIS EN JAZZ','Musiques actuelles','www.palaisenjazz.com','COMPIEGNE','','','60200','49.3990601478','2.85317249363');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SIESTES ELECTRONIQUES A TOULOUSE','Musiques actuelles','www.les-siestes-electroniques.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA NUIT DES SABLES BLANCS','Musiques actuelles','http://lanuitdessablesblancs.fr/','DOUARNENEZ','','','29100','48.0806526556','-4.32784220122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HORS BORD FESTIVAL','Musiques actuelles','http://www.horsbordfestival.fr/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RENDEZ-VOUS DE L''ERDRE','Musiques actuelles','www.rendezvouserdre.com','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN PIANO SOUS LES ARBRES','Musiques actuelles','http://unpianosouslesarbres.com','Lunel Viel','','','34400','43.6826642395','4.08422296335');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Th�lokaliz�','Musiques actuelles','http://thelokalize.fr/','ST THELO','','','22460','48.2459300421','-2.84098515337');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS D''O','Musiques actuelles','www.domaine-do-34.eu','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Sabl�','Musiques classiques','www.festivaldesable.fr','SABLE SUR SARTHE','','','72300','47.8378265007','-0.354705885665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE JAZZ DE VILLENEUVE LES BEZIERS','Musiques actuelles','','VILLENEUVE LES BEZIERS','','','34500','43.3172489997','3.28958494349');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Summerlied','Musiques actuelles','www.summerlied.org','OHLUNGEN','','','67170','48.8121908123','7.69421854068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�T� MUSICAL EN BERGERAC','Pluridisciplinaire Spectacle vivant','www.festivalbergerac.com','BERGERAC','','','24100','44.8543751872','0.486529423457');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX ET ROUTE ROMANE','Musiques classiques','www.voix-romane.com','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ACONTRALUZ','Musiques actuelles','http://www.acontraluz.fr/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REGAL SOUND','Musiques actuelles','www.musicalsol.fr','Caunes Minervois','','','11160','43.3327963198','2.51635482478');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOROS Y SALSA','Musiques actuelles','http://torosysalsa.dax.fr/','DAX','','','40100','43.7006746973','-1.06014429759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Nuits Cara�bes','Pluridisciplinaire Spectacle vivant','https://www.nuits-caraibes.com/','POINTE NOIRE','14/02/2019','28/02/2019','97116','16.227093809','-61.7632677647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES GIVRES A MELLE','Musiques actuelles','http://www.larondedesjurons.fr/','MELLE','08/02/2019','09/02/2019','79500','46.2309583731','-0.147888712984');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Filmer le travail','Cin�ma et audiovisuel','http://filmerletravail.org/','POITIERS','08/02/2019','17/02/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Break Storming','Danse','https://www.espace-des-arts.com/','CHALON SUR SAONE','02/04/2019','06/04/2019','71100','46.7900288793','4.85191555008');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EQUINOX FESTIVAL DE GRENOBLE','Musiques actuelles','https://www.equinox-festival.fr','GRENOBLE','21/03/2019','22/03/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANTONS SOUS LES PINS','Musiques actuelles','www.chantonssouslespins.org','Pontonx sur l Adour','01/03/2019','31/03/2019','40465','43.7980252651','-0.940526888731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAR''SCENE ROCK''SON','Musiques actuelles','https://fr-fr.facebook.com/carscenerockson/','CARCEN PONSON','13/04/2019','13/04/2019','40400','43.871874652','-0.81936363917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma br�silien de Paris','Cin�ma et audiovisuel','http://www.festivaldecinemabresilienparis.com','PARIS','09/04/2019','16/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Grand M�nage','Cirque et Arts de la rue','http://www.legrandmenage.fr/accueil.html','Cucuron','18/04/2019','21/04/2019','84160','43.778224355','5.44361580359');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL A PARTIR DU REEL','Th��tre','https://www.larenaissance-mondeville.fr','MONDEVILLE','10/01/2019','07/02/2019','14120','49.1693649378','-0.310691187417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GOOD ROCKIN TONIGHT','Musiques actuelles','http://www.bluemonday01.com','ATTIGNAT','25/04/2019','29/04/2019','1340','46.2861802203','5.1795233845');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE DE CHAMBRE A ARCACHON','Musiques classiques','http://www.ville-arcachon.fr/','ARCACHON','23/04/2019','27/04/2019','33120','44.6529002838','-1.17429790933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cin�-jeune de l''Aisne','Cin�ma et audiovisuel','www.cinejeune02.wordpress.com','LAON','26/03/2019','14/04/2019','2000','49.5679724897','3.62089561902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres cin�ma d''Am�rique Latine de Toulouse','Cin�ma et audiovisuel','http://www.cinelatino.fr/','TOULOUSE','22/03/2019','31/03/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PAS DES POISSONS, DES CHANSONS !','Musiques actuelles','www.smac07.com','ANNONAY','28/03/2019','31/03/2019','7100','45.2460902392','4.65026947295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ O CHATEAU','Musiques actuelles','www.jazzochateau.fr','Treveneuc','07/05/2019','12/05/2019','22410','48.6598480306','-2.87375934756');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Turbulentes','Cirque et Arts de la rue','http://lesturbulentes.com','VIEUX CONDE','03/05/2019','05/05/2019','59690','50.4735638651','3.56606734565');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ART ROCK','Pluridisciplinaire Spectacle vivant','www.artrock.org','ST BRIEUC','07/06/2019','09/06/2019','22000','48.5149806053','-2.76154552773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KADANS CARAIBE','Musiques actuelles','http://www.kadans-caraibe.com','MARSEILLE','11/05/2019','18/05/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOSH FEST','Musiques actuelles','www.toutafond.com','ST JEAN DE VEDAS','10/05/2019','11/05/2019','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon polar de Fargues','Livre et litt�rature','www.fargues-saint-hilaire.fr','Fargues Saint Hilaire','11/05/2019','11/05/2019','33370','44.819815277','-0.439992385579');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL TAMBOURS ET TROPIQUES','Musiques actuelles','https://www.tamboursettropiques.com/','NANTES','22/05/2019','25/05/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DROLES DE ZEBRES','Divers Spectacle vivant','http://www.fihdz.com','STRASBOURG','21/05/2019','24/05/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'We Love Green','Musiques actuelles','www.welovegreen.fr','PARIS','01/06/2019','02/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''EDITION FESTIVAL','Musiques actuelles','http://www.ledition-festival.fr/','MARSEILLE','01/06/2019','02/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS HIP HOP','Musiques actuelles','www.paris-hiphop.com','PARIS','01/06/2019','23/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESQUIROCK','Musiques actuelles','','STE Helene','01/06/2019','01/06/2019','33480','44.9726615806','-0.913525242529');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival cin�ma & musique de film de La Baule','Cin�ma et audiovisuel','www.festival-labaule.com','LA BAULE','','','44500','47.2909720179','-2.3538291745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chang� d''air','Pluridisciplinaire Spectacle vivant','http://festivalchangedair.blogspot.fr/','CHANGE','','','53810','48.1079801148','-0.800104772023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL NOVA ONA','Musiques actuelles','www.fuzzproduction.portfoliobox.net','CANET EN ROUSSILLON','','','66140','42.6841288047','3.01161744752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICALES D''AUTOMNE EN HAUTE BIEVRE','Musiques classiques','www.musique-et-patrimoine-hb.fr','Bievres','','','2860','49.4921822965','3.7137356904');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTHENTIKS','Musiques actuelles','www.festival-authentiks.com','VIENNE','19/07/2019','19/07/2019','38200','45.520578372','4.88135156154');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CUIVRO''FOLIZ','Musiques actuelles','www.cuivrofoliz.com','FLEURANCE','19/07/2019','21/07/2019','32500','43.8402794076','0.64245586546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� LA FOLIE PAS DU TOUT','Musiques actuelles','www.monastere-de-brou.fr','BOURG EN BRESSE','20/07/2019','31/08/2019','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A PORQUEROLLES','Musiques actuelles','http://www.jazzaporquerolles.org','PORQUEROLLES','06/07/2019','10/07/2019','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS JAZZ FESTIVAL','Musiques actuelles','www.parisjazzfestival.paris.fr','PARIS','06/07/2019','28/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musilac','Musiques actuelles','www.musilac.com','AIX LES BAINS','11/07/2019','14/07/2019','73100','45.6978541675','5.90388626955');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Festival d''Alba','Cirque et Arts de la rue','http://lefestivaldalba.org','Alba la Romaine','09/07/2019','14/07/2019','7400','44.5544439142','4.59597695147');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Festival de Musiques d�aujourd�hui � demain','Pluridisciplinaire Musique','http://clunycontemporaine.org/','CLUNY','06/07/2019','10/07/2019','71250','46.4303628582','4.67033276327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL A TOUT BOUT D''CHAMP','Musiques actuelles','https://www.festivalatoutboutdchamp.com','Chantenay Villedieu','20/07/2019','20/07/2019','72430','47.9198250308','-0.154140183084');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de La Roque d''Anth�ron','Musiques classiques','http://www.festival-piano.com','LA ROQUE D ANTHERON','20/07/2019','18/08/2019','13640','43.7176409517','5.30157633559');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FETE DU COGNAC','Musiques actuelles','www.lafeteducognac.fr','COGNAC','25/07/2019','27/07/2019','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A LUZ','Musiques actuelles','www.jazzaluz.com','LUZ ST SAUVEUR','12/07/2019','15/07/2019','65120','42.8320053922','0.0163906780322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PETE THE MONKEY','Musiques actuelles','http://petethemonkeyfestival.com','ST AUBIN SUR MER','11/07/2019','13/07/2019','76740','49.8888204893','0.875914099002');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTA MAIO','Musiques actuelles','https://festamaio.corsica/','BASTIA','11/07/2019','13/07/2019','20200','42.6864768806','9.42502133338');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUR LE CHAMP','Musiques actuelles','http://www.festival-surlechamp.fr/','VALENCE','17/07/2019','20/07/2019','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COUNTRY ROQUE FESTIVAL','Musiques actuelles','https://www.countryroque.com/','LA ROQUE D ANTHERON','13/07/2019','14/07/2019','13640','43.7176409517','5.30157633559');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PLAGES ELECTRONIQUES DE CANNES','Musiques actuelles','www.plages-electroniques.com','CANNES','09/08/2019','11/08/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRIBAL ELEK FESTIVAL','Musiques actuelles','http://www.tribalelek.fr/','ANDILLY','09/08/2019','10/08/2019','17230','46.2630044811','-1.01765756102');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MONTIGNAC','Musiques actuelles','http://www.festivaldemontignac.fr/','MONTIGNAC','29/07/2019','04/08/2019','24290','45.0627368774','1.15422675286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CH�TEAU PERCH� FESTIVAL','Musiques actuelles','https://chateauperche.com/','AINAY LE VIEIL','25/07/2019','28/07/2019','18200','46.6641114038','2.54417379933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Check In Party','Musiques actuelles','','GUERET','26/07/2019','27/07/2019','23000','46.1632121428','1.87078672735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT JAZZ CAP FERRAT','Musiques actuelles','www.saintjeancapferrat.fr','ST JEAN CAP FERRAT','07/08/2019','10/08/2019','6230','43.687178893','7.32993632437');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST IN PIA','Musiques actuelles','www.festinpia.fr','PIA','02/08/2019','03/08/2019','66380','42.7487012331','2.91497381059');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les 24 Heures du Swing','Musiques actuelles','www.swing-monsegur.com','MONSEGUR','05/07/2019','07/07/2019','33580','44.6462089079','0.0892904208174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Pamparina','Musiques actuelles','www.pamparinalefestival.com','THIERS','05/07/2019','07/07/2019','63300','45.8620554106','3.53945258847');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Main Square','Musiques actuelles','www.mainsquarefestival.fr','ARRAS','05/07/2019','07/07/2019','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rock les Bains','Musiques actuelles','https://www.rocklesbains.com/','Plombieres les Bains','10/08/2019','11/08/2019','88370','47.9697475958','6.42879961974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AY-ROOP [Temps Fort arts du cirque]','Cirque et Arts de la rue','www.ay-roop.com','RENNES','14/03/2019','30/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DES BRETELLES','Musiques actuelles','www.printempsdesbretelles.fr','ILLKIRCH GRAFFENSTADEN','15/03/2019','24/03/2019','67400','48.5200498962','7.73129588098');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POLAR FESTIVAL','Transdisciplinaire','http://www.polarfestival.com','PARIS','10/04/2019','22/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cin�mas du Sud','Cin�ma et audiovisuel','https://www.regardsud.com/','LYON','10/04/2019','13/04/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAMBROUSS''RIES','Divers Spectacle vivant','http://art-maure-spectacles.com','Maure de Bretagne','29/03/2019','13/04/2019','35330','47.8971012974','-1.99867854689');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PEPITES SONORES','Musiques actuelles','http://www.lesax-acheres78.fr/les-pepites-sonores/','ACHERES','02/04/2019','19/04/2019','18250','47.2826534372','2.45877786434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DOOINIT FESTIVAL','Musiques actuelles','http://www.dooinit-festival.com','RENNES','02/04/2019','07/04/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ANDAIN''RIES','Divers Spectacle vivant','https://www.lesandainries.fr/','BAGNOLES DE L ORNE NORMANDIE','03/04/2019','13/04/2019','61140','48.5556585202','-0.419510749596');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GUITARES AU BEFFROI','Pluridisciplinaire Musique','www.guitaresaubeffroi.com','MONTROUGE','22/03/2019','24/03/2019','92120','48.8153032159','2.31648921432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival L�zard Ti Show','Cirque et Arts de la rue','http://www.lezardtishow.fr/','LE CARBET','13/03/2019','17/03/2019','97221','14.7110039815','-61.1647565483');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN FOUGERES','Musiques actuelles','www.le-coquelicot.fr/','FOUGERES','29/03/2019','06/04/2019','35300','48.3524697115','-1.19431177241');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Francos','Th��tre','','Mantes la Jolie','23/03/2019','13/04/2019','78200','48.9981665392','1.69337806821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hip Opsession','Musiques actuelles','http://www.hipopsession.com/','NANTES','14/02/2019','03/03/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAMPIONNAT NATIONAL DE BRASS BAND','Musiques actuelles','https://www.brassband.cmf-musique.org/','AMIENS','23/02/2019','24/02/2019','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Printemps du livre','Livre et litt�rature','www.pdl.terresdemontaigu.fr','MONTAIGU','05/04/2019','07/04/2019','85600','46.9759800852','-1.31364530268');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AGLA''SC�NES','Musiques actuelles','www.aglascenes.com','EGLY','06/04/2019','06/04/2019','91520','48.5778313904','2.22141176606');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre d''expression populaire','Livre et litt�rature','https://www.coleresdupresent.com/salon-du-livre/','ARRAS','06/04/2019','01/05/2019','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Ruelles','Cirque et Arts de la rue','https://www.festival-les-ruelles-auriac.fr/','Auriac sur Vendinelle','27/04/2019','27/04/2019','31460','43.519408834','1.82440314638');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE PASSAGE','Musiques actuelles','www.smj.fontenay-sous-bois.fr','FONTENAY SOUS BOIS','21/04/2019','26/04/2019','94120','48.8511046382','2.47395409774');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''EURE POETIQUE & MUSICALE - FESTIVAL EN NORMANDIE SUD','Musiques classiques','https://www.festivaleure.com','Breteuil sur Iton','19/04/2019','26/05/2019','27160','48.8525387012','0.899890466074');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du court-m�trage de Clermont-Ferrand','Cin�ma et audiovisuel','https://clermont-filmfest.org/','CLERMONT FERRAND','01/02/2019','09/02/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Kolorz Festival Edition hiver','Musiques actuelles','http://kolorzfestival.com','CARPENTRAS','08/02/2019','09/02/2019','84200','44.0593802565','5.06134844776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La plage aux �crivains','Livre et litt�rature','http://www.arcachon.com/plage_aux_ecrivains.html','ARCACHON','04/05/2019','05/05/2019','33120','44.6529002838','-1.17429790933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTICOLOR','Musiques actuelles','www.festicolor.com','Meung Sur Loire','23/05/2019','25/05/2019','45130','47.8397230739','1.69811579508');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�CHAPP�E BELLE','Pluridisciplinaire Spectacle vivant','www.carrecolonnes.fr','ST MEDARD EN JALLES','23/05/2019','26/05/2019','33160','44.8832620816','-0.784239883546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAVAL''AIR JAZZ FESTIVAL','Musiques actuelles','www.cavalairejazz.fr','CAVALAIRE SUR MER','','','83240','43.1819055812','6.52121721395');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN PLESCOP','Musiques actuelles','www.jazzinplescop.fr','PLESCOP','','','56890','47.6973490259','-2.83080852214');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRONTENAY JAZZ FESTIVAL','Musiques actuelles','http://www.frontenayjazz.fr/','FRONTENAY','','','39210','46.7884051607','5.63215279855');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU BONHEUR DES M�MES','Pluridisciplinaire Spectacle vivant','www.aubonheurdesmomes.com','LE GRAND BORNAND','','','74450','45.9562174482','6.47641784295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'B�DARIEUX VOIX D''ORGUES','Musiques actuelles','www.bedarieux-voixdorgues.com','BEDARIEUX','','','34600','43.6125569074','3.16680787195');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE BOURG-MADAME','Pluridisciplinaire Musique','www.festivalbourgmadame.fr','BOURG MADAME','','','66760','42.4379971277','1.96090815348');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RUES EN MUSIQUE','Musiques actuelles','http://www.ville-arles.fr/','ARLES','','','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Abracadagrasses','Musiques actuelles','www.abracadagrasses.fr','Lagrasse','','','11220','43.0957230349','2.60436046295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la C�te d''Opale','Musiques actuelles','','BOULOGNE SUR MER','','','62200','50.7271332663','1.60756334802');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'APERO''ZIQUES','Musiques actuelles','www.nefdesfous.wordpress.com','ST Pern','','','35190','48.2809544263','-1.98293675576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE DIAMANT VERT','Musiques actuelles','https://www.diamantvert.org/','TEISSIERES LES BOULIES','','','15130','44.8209679734','2.53827855316');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALPIN''DUB','Musiques actuelles','http://alpindub.fr/','Sept Laux Prapoutel','','','38190','45.2586070645','5.99155979387');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INTERNATONAL DE BOOGIE-WOOGIE DE LAROQUEBROU','Musiques actuelles','www.boogie-laroquebrou.com','LA ROQUEBROU','','','15150','44.974801878','2.19344155664');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AP�ROS MUSIQUE DE BLESLE','Musiques actuelles','www.aperos-musique-blesle.com','BLESLE','','','43450','45.3179155382','3.17686146037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARJAC EN JAZZ','Musiques actuelles','www.tourisme-barjac.com','BARJAC','','','30430','44.3122415211','4.34841481782');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Village et Compagnies','Cirque et Arts de la rue','https://marimarti.wixsite.com/villageetcompagnies','CHABRILLAN','','','26400','44.713669067','4.96072243187');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOVOSONIC','Musiques actuelles','http://novosonic.wixsite.com/novosonic9','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du court m�trage de Lille','Cin�ma et audiovisuel','www.festivalducourt-lille.com','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Un dimanche au Wadada','Musiques actuelles','www.wadadafestival.fr','Louvigne du Desert','','','35420','48.4868885908','-1.12369660788');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CODA FESTIVAL','Musiques actuelles','www.codafestival.fr/','Bondues','','','59910','50.7096656233','3.09529083038');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ILE AUX PERCU SONS','Musiques actuelles','www.olizamba.wixsite.com/olizamba/appel-a-candidature-2018','CAZAUX','','','33260','44.5561823053','-1.17530826791');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TEUF DE TOULOUSE','Transdisciplinaire','http://www.teuftoulouse.fr/fr/','TOULOUSE','13/05/2019','27/05/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''AISNE','Musiques actuelles','http://www.rockaisne.com','CHAUNY','11/05/2019','11/05/2019','2300','49.619880673','3.21875355041');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Com�die du livre','Livre et litt�rature','https://comediedulivre.fr/','MONTPELLIER','17/05/2019','19/05/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ DANS LE BOCAGE','Musiques actuelles','www.jazzdanslebocage.com','ROCLES','24/05/2019','01/06/2019','3240','46.4445146062','3.03677199137');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NAME FESTIVAL','Musiques actuelles','https://www.lenamefestival.com/portail/','LILLE','25/05/2019','25/05/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DES ARENES','Transdisciplinaire','https://www.nuitsdesarenes.com','PARIS','12/06/2019','16/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX DE FEMMES A MAURY','Musiques actuelles','www.festival-voixdefemmes.fr','MAURY','13/06/2019','15/06/2019','66460','42.8152827939','2.61292653306');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU CONTE EN UZEGE','Divers Spectacle vivant','http://www.festivalduconte-enuzege.fr/','Collias','10/06/2019','23/06/2019','30210','43.9490522277','4.47176606244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Gare � la Rochette','Musiques actuelles','www.figureslibres.org','Thor� la Rochette','14/06/2019','16/06/2019','41100','47.7859819999','0.970946922736');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Sully et du Loiret','Musiques classiques','http://www.festival-sully.fr/','SULLY SUR LOIRE','06/06/2019','23/06/2019','45600','47.7526229305','2.35647713689');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Furies, festival de cirque et de th��tre de rue de Ch�lons-en-Champagne','Cirque et Arts de la rue','https://www.furies.fr/','CHALONS EN CHAMPAGNE','03/06/2019','09/06/2019','51000','48.9640892125','4.37883539725');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUATUORS � BORDEAUX','Musiques classiques','www.quatuorabordeaux.com','BORDEAUX','05/06/2019','12/06/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'K-LIVE','Transdisciplinaire','www.k-live.fr','SETE','03/06/2019','09/06/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOIX''R DE RUE','Cirque et Arts de la rue','http://foixrderue.com','FOIX','28/06/2019','30/06/2019','9000','42.9658502274','1.61037495894');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Val de Rock','Musiques actuelles','http://www.valdeuropeagglo.fr/','CHESSY','28/06/2019','30/06/2019','77700','48.8723310998','2.77039964715');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIKAIR','Musiques actuelles','http://musikair.apps-1and1.net/','MONTARGIS','28/06/2019','30/06/2019','45200','47.9988427684','2.73588922015');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHANT DES GROLES','Musiques actuelles','http://chant-des-groles.fr/','Vivonne','28/06/2019','29/06/2019','86370','46.412040698','0.257005369348');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musiques M�tisses','Musiques actuelles','www.musiques-metisses.com','ANGOULEME','31/05/2019','02/06/2019','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COULEURS URBAINES','Musiques actuelles','http://festival-couleursurbaines.com','LA SEYNE SUR MER','31/05/2019','01/06/2019','83500','43.0880294967','5.87089841754');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLEIN AIR DE ROCK','Musiques actuelles','www.pleinairderockjarny.fr','JARNY','01/06/2019','01/06/2019','54800','49.1495784035','5.88121522823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INGLORIOUS FESTIVAL','Musiques actuelles','http://www.verdun.fr/agenda','VERDUN','14/06/2019','15/06/2019','55100','49.1454831903','5.36141821261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOUS LES PAVES... L''ART','Cirque et Arts de la rue','https://souslespaveslart.com/','ST OMER','08/06/2019','10/06/2019','62500','50.7679781717','2.26334881482');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TOUR MET LES WATTS','Musiques actuelles','','VOISINS LE BRETONNEUX','08/06/2019','08/06/2019','78960','48.758677648','2.04850691768');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PIEDS DANS LA VASE','Musiques actuelles','http://www.lespiedsdanslavase.fr/','KERVIGNAC','08/06/2019','09/06/2019','56700','47.7686555667','-3.2479141613');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE NIMES','Musiques actuelles','www.festivaldenimes.com','NIMES','23/06/2019','20/07/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE A MARSEILLE','Divers Spectacle vivant','','MARSEILLE','15/06/2019','15/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Champs Elys�es Film Festival','Cin�ma et audiovisuel','www.champselyseesfilmfestival.com','PARIS','18/06/2019','25/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Milatsika','Musiques actuelles','www.festivalmilatsika.com','CHICONI','','','97670','-12.821522233','45.1132970298');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�CLATS D''ARFI','Musiques actuelles','www.arfi.org','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Avant Curieux','Cirque et Arts de la rue','http://www.theatreonyx.fr/programme/avant-curieux-1','ST HERBLAIN','','','44800','47.2243762412','-1.63434818692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A TANT REVER DU ROI','Musiques actuelles','www.atrdr.net/festival','PAU','','','64000','43.3200189773','-0.350337918181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX CROISEES','Pluridisciplinaire Musique','www.festivalvoixcroisees.com','ESCALQUENS','','','31750','43.5209125935','1.55243226997');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres cin�matographiques de Cerb�re-Portbou','Cin�ma et audiovisuel','http://www.rencontrescerbere.org/','CERBERE','','','66290','42.4439076617','3.14835500726');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS COURTES','Musiques actuelles','http://www.lesnuitscourtes.com','Fontenay Le Comte','','','85200','46.4563186117','-0.793449510859');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Zoom Festival','Musiques actuelles','https://www.ville-dunkerque.fr/','DUNKERQUE','','','59430','51.0307229078','2.33752414095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale Selest''Art','Arts plastiques et visuels','https://www.selestat.fr/','SELESTAT','','','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK EN MAINE','Musiques actuelles','www.festimaine.jimdo.com','Aigrefeuille sur Maine','','','44140','47.0719256611','-1.41556455507');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL OSTARA','Musiques actuelles','http://www.ostara.fr/','ST Pierre en Faucigny','','','74800','46.063686335','6.37244945345');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RICHES HEURES DE LA REOLE','Musiques classiques','www.lesrichesheuresdelareole.fr','La Reole','','','33190','44.5860904495','-0.042370404021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES COREADES','Musiques classiques','www.coream.org','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELDORADO MUSIC FESTIVAL','Musiques actuelles','http://www.cafedeladanse.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CINECOMEDIES','Cin�ma et audiovisuel','www.cinecomedies.com','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''IMAGINAIRE','Musiques actuelles','www.festivaldelimaginaire.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL EUROPEEN DE MUSIQUE RENAISSANCE','Musiques classiques','www.vinci-closluce.com','AMBOISE','','','37400','47.3917237652','1.00023994675');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du conte Paul Henri G�rard','Divers Spectacle vivant','https://zigzagprod.fr/','MANA','','','97360','4.98285747902','-53.6480364932');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL EMERGENCES','Musiques actuelles','www.festivalemergences.fr','TOURS','','','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KOA JAZZ FESTIVAL','Musiques actuelles','www.festivalrencontreskoajazz.wordpress.com','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TINTAMARRE','Musiques actuelles','http://www.musiquesvivantes.com','VICHY','','','3200','46.1300051383','3.42442081174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Trajectoires','Danse','http://ccnnantes.fr/festival-trajectoires/','NANTES','19/01/2019','27/01/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES RAPIECES','Musiques actuelles','','Ouville l Abbaye','23/08/2019','25/08/2019','76760','49.6944501885','0.866542166561');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAVEYARD FEST','Musiques actuelles','','Riorges','31/08/2019','31/08/2019','42153','46.0429114311','4.03375590925');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�ma d''Iran','Cin�ma et audiovisuel','http://www.cinemasdiran.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vive l''art rue !','Pluridisciplinaire Spectacle vivant','https://www.ville-creteil.fr/festival-vive-lart-rue-2018','CRETEIL','','','94000','48.7837401836','2.45463530415');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Petites Bobines','Cin�ma et audiovisuel','https://www.cinebelair.org/','MULHOUSE','','','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Manufacture d''id�es','Livre et litt�rature','http://lamanufacturedidees.org/','Hurigny','','','71870','46.3429969068','4.79636186257');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MIRAGE FESTIVAL','Musiques actuelles','www.miragefestival.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARDI GRAVES','Musiques classiques','http://mardigraves.free.fr/','ST JEAN DE VEDAS','','','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Giboul�es de la Marionnette','Divers Spectacle vivant','','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Livres en vignes','Livre et litt�rature','www.livresenvignes.com','Vougeot','28/09/2019','29/09/2019','21640','47.174271631','4.95989664941');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WALDEN','Musiques actuelles','http://www.petitbain.org','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DEJANTE','Musiques actuelles','http://festival-dejante.com','ST Goueno','','','22330','48.2937312654','-2.52246803693');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'En Bas de chez Vous','Cirque et Arts de la rue','www.festival-en-bas-de-chez-vous.com','LA GACILLY','','','56200','47.7728317091','-2.15651114837');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rue d�Tourn�e','Cirque et Arts de la rue','http://www.tourville-la-riviere.fr/','Tourville la Riviere','','','76410','49.3274627821','1.09185339831');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Ann�es Jou�','Cirque et Arts de la rue','https://www.anneesjoue.fr/','JOUE LES TOURS','','','37300','47.333556074','0.654546300116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL COURANTS ROCK','Musiques actuelles','https://85.agendaculturel.fr/','LA TRANCHE SUR MER','','','85360','46.3564601605','-1.43136322126');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URBAN TRANCE FESTIVAL','Musiques actuelles','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE GRAND SABBAT','Musiques actuelles','','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�days. Festival parisien du design','Arts plastiques et visuels','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�A JAZZ � BLET','Musiques actuelles','www.cajazzablet.fr','BLET','','','18350','46.8822848044','2.73104674271');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�D�CHANSONS','Musiques actuelles','www.fedechansons.fr','ST GERMAIN EN LAYE','','','78100','48.9407041394','2.09870929375');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la bande annonce du Bois d�Arcy','Cin�ma et audiovisuel','','BOIS D ARCY','','','78390','48.8044185716','2.01911294303');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bonjour l''hiver','Cirque et Arts de la rue','http://www.annemasse.fr/Culture/Vie-culturelle/Festivals/Bonjour-L-Hiver','ANNEMASSE','','','74100','46.1909730986','6.24250704322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREEZE MUSIC','Musiques actuelles','www.associationadonf.fr','MONTENDRE','','','17130','45.2880726146','-0.387479585943');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Emancip��s','Transdisciplinaire','https://www.festival-lesemancipees.bzh/','VANNES','19/03/2019','24/03/2019','56000','47.6597493766','-2.75714329498');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 2 Valenciennes','Cin�ma et audiovisuel','www.festival2valenciennes.com','Valenciennes','19/03/2019','24/03/2019','59300','50.3588666163','3.51567664819');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cours z''y vite','Pluridisciplinaire Spectacle vivant','https://www.sainte-savine.fr/','Ste Savine','08/03/2019','24/03/2019','10300','48.2963408998','4.02333022061');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Editeuriales','Livre et litt�rature','www.bm-poitiers.fr','POITIERS','12/03/2019','23/03/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre des balkans','Livre et litt�rature','http://livredesbalkans.eklablog.com/','PARIS','12/04/2019','13/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de jeunesse','Livre et litt�rature','www.fetedulivre.villeurbanne.fr','VILLEURBANNE','06/04/2019','07/04/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Canneseries','Cin�ma et audiovisuel','http://canneseries.com/fr/','CANNES','05/04/2019','10/04/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les �lanc�es, festival des arts du geste','Cirque et Arts de la rue','http://www.scenesetcines.fr/','ISTRES','01/02/2019','10/02/2019','13118','43.5502689105','4.9511813524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International de la Bande Dessin�e','Livre et litt�rature','http://www.bdangouleme.com/','ANGOULEME','24/01/2019','27/01/2019','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Reims Sc�nes d''Europe','Pluridisciplinaire Spectacle vivant','http://www.scenesdeur','REIMS','24/01/2019','07/02/2019','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX D''HIVER','Musiques actuelles','http://mclgauchy.fr/','GAUCHY','29/01/2019','08/02/2019','2430','49.8234688599','3.28649189661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Art Danse','Danse','https://art-danse.org/','DIJON','21/01/2019','06/02/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FLAMENCO AZUL','Musiques actuelles','www.festivalflamenco-azul.com','AIX EN PROVENCE','18/04/2019','05/05/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HISTOIRE D''EN RIRE','Divers Spectacle vivant','http://www.mairie-neuvecelle.fr/','Neuvecelle','27/04/2019','27/04/2019','74500','46.3921186949','6.60933233238');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d''architecture et de paysage � Versailles','Domaines divers','http://bap-idf.com/','VERSAILLES','03/05/2019','13/07/2019','78000','48.8025669671','2.11789297191');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Cornouaille','Musiques actuelles','http://www.festival-cornouaille.bzh/fr/','QUIMPER','23/07/2019','28/07/2019','29000','47.9971425162','-4.09111944455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CUBAIN BAYAMO','Musiques actuelles','http://www.bayamo.fr/','LA SEYNE SUR MER','09/07/2019','21/07/2019','83500','43.0880294967','5.87089841754');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hop Hop Hop','Cirque et Arts de la rue','http://hophophop.eu/','METZ','11/07/2019','14/07/2019','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS MUSICALES EN VENDEE ROMANE','Pluridisciplinaire Musique','www.festival-vendee.com','FOUSSAIS PAYRE','12/07/2019','13/08/2019','85240','46.5230750581','-0.687135962627');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA BAULE JAZZ FESTIVAL','Musiques actuelles','www.mairie-labaule.fr/','LA BAULE','13/07/2019','10/08/2019','44500','47.2909720179','-2.3538291745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MILLAU JAZZ FESTIVAL','Musiques actuelles','www.millaujazz.fr','MILLAU','13/07/2019','20/07/2019','12100','44.0976252203','3.11705384129');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Ch�teaux de Bruniquel','Musiques classiques','www.chaufferdanslanoirceur.org','Bruniquel','25/07/2019','04/08/2019','82800','44.0521980341','1.66009719751');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Aymon Folk Festival','Musiques actuelles','www.aymonfolkfestival.fr/','Bogny sur Meuse','26/07/2019','27/07/2019','8120','49.8477354025','4.7508301548');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV ALLIER','Pluridisciplinaire Spectacle vivant','www.festivallier48.fr','Langogne','31/07/2019','03/08/2019','48300','44.7237405653','3.83363110341');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE CONFOLENS','Musiques actuelles','','Confolens','12/08/2019','18/08/2019','16500','46.0191386034','0.658522279035');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du cin�ma italien','Cin�ma et audiovisuel','http://www.dolcecinema.com/les-rencontres-du-cinema-italien','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres des cin�mas d�Europe','Cin�ma et audiovisuel','www.maisonimage.eu','AUBENAS','','','7200','44.6102127084','4.39638981424');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival peuples et musiques au cin�ma','Cin�ma et audiovisuel','www.peuplesetmusiquesaucinema.org','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Clowns not dead','Cirque et Arts de la rue','www.polejeunepublic.com','LE REVEST LES EAUX','','','83200','43.1824918424','5.94565392204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d''accord�on de Drancy M�dard Ferrero','Musiques actuelles','www.drancy.net','DRANCY','','','93700','48.9234246259','2.44492688692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEMAINE DE L''INSOLENCE','Divers Spectacle vivant','http://theatredescollines.annecy.fr/festivals/semaine-de-linsolence/','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MANCA','Musiques classiques','http://www.cirm-manca.org','NICE','','','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film Qu�b�cois des Grands Lacs de Biscarrosse','Cin�ma et audiovisuel','https://www.festival-cine-quebec-biscarrosse.com/','BISCARROSSE','','','40600','44.409080109','-1.1773616947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES VENDANGES DE SURESNES','Cirque et Arts de la rue','https://www.suresnes.fr/Temps-libre/Les-rendez-vous-annuels/Festival-des-Vendanges','SURESNES','','','92150','48.8698479132','2.21965517625');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COURANT D''AIRS BORDEAUX','Musiques actuelles','http://www.bordeaux-chanson.org','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Expressifs','Pluridisciplinaire Spectacle vivant','www.lesexpressifs.com','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR SEINE','Musiques actuelles','www.parisjazzclub.net','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KARAVEL','Danse','www.festivalkaravel.com','BRON','','','69500','45.7344856902','4.91168159471');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INDETOUR FESTIVAL','Musiques actuelles','https://fr-fr.facebook.com/indetour/','KINGERSHEIM','','','68260','47.7889765168','7.32299335222');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Court M�trange - Rennes','Cin�ma et audiovisuel','www.courtmetrange.eu','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BULLY ON ROCKS','Musiques actuelles','www.bullyonrocks.fr','Bully Les Mines','','','62160','50.4451131556','2.71911165536');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival CharivaRue','Cirque et Arts de la rue','www.esat-evasion.fr','SELESTAT','','','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Actoral','Pluridisciplinaire Spectacle vivant','http://www.actoral.org','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Poulpaphone','Musiques actuelles','www.agglo-boulonnais.fr/','BOULOGNE SUR MER','','','62200','50.7271332663','1.60756334802');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RACK'' ESTIVAL','Musiques actuelles','www.lerackam.com','BRETIGNY SUR ORGE','','','91220','48.6025113399','2.3021623232');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FEMME(S)','Musiques actuelles','lepax.fr/','ST ETIENNE','','','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LAON','Musiques classiques','www.festival-laon.fr','LAON','','','2000','49.5679724897','3.62089561902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OFNI','Musiques actuelles','www.ofni.biz','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREAKSHOW FESTIVAL','Musiques actuelles','www.freakshow-festival.com','GISORS ET LOZERON','','','26400','44.8113617403','5.11358630739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAPH''ART''NAUM','Musiques actuelles','https://www.caphartsnaum.com/','LAFAUCHE','','','52700','48.3053380759','5.49239986278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RAISMES FEST','Musiques actuelles','www.raismesfest.fr','RAISMES','','','59590','50.410089317','3.48909318774');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU FILM SUBVERSIF DE METZ','Cin�ma et audiovisuel','www.festival-subversif.com','METZ','','','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE TEMPS D''AIMER LA DANSE','Danse','http://www.letempsdaimer.com','BIARRITZ','','','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUMELE','Pluridisciplinaire Spectacle vivant','www.toumele.org','MAULE','','','78580','48.9078548612','1.83959534526');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES NOTES ET DES TOILES','Transdisciplinaire','www.desnotesetdestoiles.fr','BISCARROSSE','','','54700','48.9219828048','6.05392690748');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIANO AUX JACOBINS','Musiques classiques','www.pianojacobins.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONTRE-PLONG�ES DE L''�T�','Transdisciplinaire','www.clermont-ferrand.fr/','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU LAC','Musiques actuelles','www.jazzaulac','St  Vincent sur Graon','','','85540','46.5034756656','-1.382954206');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL GRANGE','Musiques actuelles','http://www.chantmorin.com','BERGERES SOUS MONTMIRAIL','','','51210','48.8453830392','3.59711725011');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PAGES MUSICALES DE LAGRASSE','Musiques classiques','www.festival-lagrasse.fr','Lagrasse','','','11220','43.0957230349','2.60436046295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COUNTRY LE BARP FESTIVAL','Musiques actuelles','http://countrybarpfestival.free.fr','LE BARP','','','33114','44.6268462922','-0.746697894211');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MENS ALORS !','Musiques actuelles','www.mensalors.jimdo.com','MENS','','','38710','44.8147716057','5.75218343717');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRUIS EN JAZZ','Musiques actuelles','www.cruisenjazz.fr','CRUIS','','','4230','44.0831626991','5.83846949092');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GARTEMPE BLUES FESTIVAL','Musiques actuelles','www.gartempeblues.com','ST SAVIN','','','86310','46.5832866024','0.840222181095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CARGESE SOUND SYSTEM','Musiques actuelles','http://www.cargesesoundsystem.com','CARGESE','','','20130','42.1544254567','8.62659836115');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'20/20','Pluridisciplinaire Spectacle vivant','https://fr-fr.facebook.com/vingtsurvingtfestival/','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTHUR''S DAY FESTIVAL','Musiques actuelles','www.arthurs-day-festival.com','GRANDVILLIERS','','','60210','49.666016357','1.9349239261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Ah? dans la rue','Cirque et Arts de la rue','https://www.ahsaisonetfestival.com/','PARTHENAY','16/05/2019','27/05/2019','79200','46.6453493842','-0.233028439198');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ONZE BOUGE','Pluridisciplinaire Spectacle vivant','www.festivalonze.org','PARIS','30/05/2019','03/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sunny side of the doc','Cin�ma et audiovisuel','https://www.sunnysideofthedoc.com/fr/','LA ROCHELLE','24/06/2019','27/06/2019','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAND BASTRINGUE DE CLUNY','Musiques actuelles','www.grandbastringue.com','CLUNY','07/06/2019','08/06/2019','71250','46.4303628582','4.67033276327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Graines de Rue','Cirque et Arts de la rue','http://www.grainesderue.fr/','Bessines sur Gartempe','06/06/2019','09/06/2019','87250','46.1088644019','1.3601193629');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PETITES REVERIES','Pluridisciplinaire Spectacle vivant','http://www.lespetitesreveries.com','BRINON SUR BEUVRON','06/06/2019','09/06/2019','58420','47.2843863072','3.49020583859');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JEUDIS MUSICAUX','Musiques classiques','https://www.agglo-royan.fr/','ST GEORGES DE DIDONNE','06/06/2019','19/09/2019','17110','45.6036278448','-0.980098297165');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POP CORNES FESTIVAL','Musiques actuelles','https://pop-cornes-festival.fr/','Le Russey','07/06/2019','09/06/2019','25210','47.1549185878','6.73440290446');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE WEEK-END DES CURIOSITES','Musiques actuelles','www.leweekenddescuriosites.com','RAMONVILLE ST AGNE','31/05/2019','02/06/2019','31520','43.5441837911','1.47782372823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RASTAF ENTRAY','Musiques actuelles','http://rastaf-entray.fr/','Entraygues sur Truyere','08/06/2019','09/06/2019','12140','44.6544227279','2.57843798744');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FESTEJADES','Transdisciplinaire','http://www.ville-gruissan.fr/','GRUISSAN','07/06/2019','09/06/2019','11430','43.1040163547','3.08229513652');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOCTURNES DE LA CHAMBRE D''AMOUR','Musiques actuelles','www.anglet.fr','ANGLET','28/06/2019','29/06/2019','64600','43.4917846595','-1.51623373371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SINGULIERS','Transdisciplinaire','http://www.104.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Rendez-vous de l''histoire','Domaines divers','http://www.rdv-histoire.com/','BLOIS','09/10/2019','14/10/2019','41000','47.5817013938','1.30625551583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Foire du livre de Brive','Livre et litt�rature','http://foiredulivredebrive.net/','Brive la Gaillarde','08/11/2019','10/11/2019','19100','45.1435830664','1.51936836063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVALCEOU','Musiques actuelles','www.festivalceou.com','Concores','30/08/2019','01/09/2019','46310','44.6672951054','1.39090015895');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hip hop Rendez vous','Musiques actuelles','https://rendezvoushiphop.culture.gouv.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Confluences Nomades','Cirque et Arts de la rue','www.cirquejulesverne.fr','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du Cirque de Saint-Romain de Colbosc','Cirque et Arts de la rue','www.caux-estuaire.fr/','ST Romain de Colbosc','','','76430','49.5286231796','0.363829451738');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KULTURARTE FESTIVAL DES CULTURES DU MONDES EN CORSE','Musiques actuelles','http://www.kulturarte.com','PORTICCIO','','','20128','41.8837141916','8.87384830361');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Couarail','Cirque et Arts de la rue','https://www.couarail-norroy.fr/','Norroy le Veneur','','','57140','49.179562869','6.11099249211');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVOX','Musiques actuelles','www.levox.fr','CH�TEAU RENARD','','','45220','47.9266379127','2.92209191737');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PYRAMID FESTIVAL','Musiques actuelles','www.pyramid-festival.com','LA GRANDE MOTTE','','','34280','43.5685394142','4.0758328467');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PASSAGES','Pluridisciplinaire Spectacle vivant','http://festival-passages.org','METZ','','','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Crotoy Jazz - Baie de Somme','Musiques actuelles','','Le Crotoy','','','80550','50.2439123347','1.62316246487');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A LOUVIERS','Musiques actuelles','www.jazzalouviers.fr','LOUVIERS','','','27400','49.2206099164','1.15340030158');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIBRA''MOMES','Musiques actuelles','http://www.flers-agglo.fr/','FLERS','','','61100','48.7399319125','-0.562509564641');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS D''ISTRES','Musiques actuelles','www.istres.fr','ISTRES','','','13118','43.5502689105','4.9511813524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FLAMB�ES CELTIK','Musiques actuelles','http://centreculturel.fougeres-communaute.bzh/','FOUGERES','19/01/2019','31/01/2019','35300','48.3524697115','-1.19431177241');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film de Cannes','Cin�ma et audiovisuel','www.festival-cannes.org','CANNES','14/05/2019','25/05/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine internationale de la critique (SIC)','Cin�ma et audiovisuel','www.semainedelacritique.com','CANNES','15/05/2019','23/05/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES � SENART EN ESSONNE','Musiques actuelles','www.bluesasenart.com','ST PIERRE DU PERRAY','17/05/2019','17/05/2019','91280','48.6058282604','2.51614926696');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTAN EN FANFARE','Musiques actuelles','www.autanenfanfare.com','St Felix Lauragais','17/05/2019','19/05/2019','31540','43.4505941916','1.90172958444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LULUBERLU','Pluridisciplinaire Spectacle vivant','www.festival-luluberlu.fr','BLAGNAC','22/05/2019','26/05/2019','31700','43.6421867862','1.37886941086');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Caract�res','Livre et litt�rature','https://www.festival-caracteres.fr/','AUXERRE','24/05/2019','26/05/2019','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bloody week end','Cin�ma et audiovisuel','https://bloodyweekend.fr/','AUDINCOURT','31/05/2019','02/06/2019','25400','47.4811676376','6.85493983157');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tours d''horizons','Danse','http://www.ccntours.com','TOURS','04/06/2019','15/06/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES QUATRE SAISONS','Pluridisciplinaire Spectacle vivant','','MONTBELIARD','13/06/2019','15/06/2019','25200','47.5155169816','6.79148147353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Livr''a Vannes','Livre et litt�rature','http://www.livreavannes.fr/','VANNES','14/06/2019','16/06/2019','56000','47.6597493766','-2.75714329498');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFRICA F�TE','Musiques actuelles','www.africafete.com','MARSEILLE','15/06/2019','30/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chor�gies d''Orange','Musiques classiques','https://www.choregies.fr','ORANGE','19/06/2019','03/08/2019','84100','44.128913537','4.8098792403');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TAMAZGHA','Musiques actuelles','www.festivaltamazgha.org','MARSEILLE','19/06/2019','22/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival pas pareil - Les Invites de Villeurbanne','Cirque et Arts de la rue','http://www.invites.villeurbanne.fr/','VILLEURBANNE','19/06/2019','22/06/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICAVES','Musiques actuelles','www.lesmusicaves.fr','MELLECEY','26/06/2019','30/06/2019','71640','46.8104311793','4.7565417637');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AND CHEESE A MONTBARDON','Musiques actuelles','www.jazzandcheese.fr','Ch�teau Ville Vieille','27/06/2019','30/06/2019','5350','44.7677289666','6.8000656504');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international des Jardins de Chaumont-sur-Loire','Domaines divers','http://www.domaine-chaumont.fr/fr/festival-international-des-jardins','Chaumont sur Loire','25/04/2019','03/11/2019','41150','47.4655792877','1.19931554619');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOULEGAN A L''OSTAL','Musiques actuelles','https://www.festival-boulegan.com','ST JEAN DU GARD','19/04/2019','21/04/2019','30270','44.1071750382','3.87785905992');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE PAQUES A AIX EN PROVENCE','Musiques classiques','www.festivalpaques.com','AIX EN PROVENCE','13/04/2019','28/04/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES Z''ECLECTIQUES (COLLECTION HIVER)','Musiques actuelles','www.leszeclectiques.com','Chemille en Anjou','06/02/2019','09/02/2019','49092','47.3777256876','-0.609937724094');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ DE MARS','Musiques actuelles','www.jazzdemars.com','CHARTRES','02/03/2019','30/03/2019','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ASTROPOLIS L''HIVER','Musiques actuelles','','BREST','05/02/2019','10/02/2019','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INUIT FESTIVAL','Musiques actuelles','www.inuit-festival.com','MARSEILLE','01/02/2019','02/02/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de litt�rature & culture italiennes Italissimo','Livre et litt�rature','www.italissimofestival.com','PARIS','03/04/2019','09/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA SEMAINE DU ROCK','Musiques actuelles','www.progres-son.fr','TOULOUSE','25/03/2019','31/03/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Cin�ma Espagnol de Nantes','Cin�ma et audiovisuel','www.cinespagnol-nantes.com','NANTES','28/03/2019','07/04/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELECTROCHIC','Musiques actuelles','http://www.festivalelectrochic.fr/','Velizy Villacoublay','14/03/2019','17/03/2019','78140','48.783985823','2.19707485523');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Mont-Blanc d�humour','Divers Spectacle vivant','https://fr-fr.facebook.com/montblancdhumour/','ST GERVAIS LES BAINS','16/03/2019','22/03/2019','74170','45.8571864862','6.74016257718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTE DANS L''BUS','Musiques actuelles','www.6par4.com','LAVAL','13/03/2019','23/03/2019','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SONIC PROTEST','Musiques actuelles','www.sonicprotest.com','PARIS','22/03/2019','06/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Th��tre en livres','Transdisciplinaire','https://www.editionstheatrales.fr/','PARIS','22/03/2019','06/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''EVEIL DU BOUCAN','Musiques actuelles','http://www.noznroll.org','VANNES','06/04/2019','22/04/2019','56000','47.6597493766','-2.75714329498');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'IND''HIP''HOP','Musiques actuelles','https://hiphopinfosfrance.com','STRASBOURG','04/04/2019','09/04/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres � lire','Livre et litt�rature','https://www.dax.fr/evenement/les-rencontres-a-lire/','DAX','12/04/2019','14/04/2019','40100','43.7006746973','-1.06014429759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LICENCES','Pluridisciplinaire Musique','www.revuelicences.com','PARIS','08/03/2019','10/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Convenanza','Musiques actuelles','http://convenanzafestival.com','CARCASSONNE','','','11000','43.2093798444','2.34398855385');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dinard Film Festival','Cin�ma et audiovisuel','www.festivaldufilm-dinard.com','DINARD','','','35800','48.6241805945','-2.0619828606');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRAGMENTS','Transdisciplinaire','http://www.carreaudutemple.eu/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RISING FEST','Musiques actuelles','https://risingfest.wixsite.com/risingfest','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIANO EN VALOIS','Musiques classiques','www.piano-en-valois.fr','ANGOULEME','','','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURES HIP HOP','Pluridisciplinaire Spectacle vivant','www.cultureshiphopfestival.com','QUIMPER','','','29000','47.9971425162','-4.09111944455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HUMOUR DE VILLIEU LOYES','Divers Spectacle vivant','https://www.mairievlm.fr/a545-festival-de-l-humour.html','Villieu-Loyes-Mollon','','','1800','45.932014998','5.22953071636');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE A SAINT RAPHAEL','Divers Spectacle vivant','http://www.saint-raphael.com','ST RAPHAEL','','','83700','43.4574625431','6.84734210398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GREN FEST','Pluridisciplinaire Spectacle vivant','www.grenfest.com','Izel les Hameaux','','','62690','50.3147014081','2.53156899896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEERMAGEDDON FEST','Musiques actuelles','www.battlesbeer.org','Magny les Hameaux','','','78114','48.7410207273','2.0519793397');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES LIBRES','Musiques actuelles','http://www.aspro-impro.fr/','BESANCON','','','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TARTINE FESTIVAL','Musiques actuelles','www.tartine-festival.com','CHAMBERY','','','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sc�nes d''�t�','Musiques actuelles','https://culture.beauvais.fr/acteur-culturel/scenes-d-�t�','BEAUVAIS','','','60000','49.4365523321','2.08616123661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZBULLES','Musiques actuelles','http://www.ot-saumur.fr/','SAUMUR','','','49400','47.2673853525','-0.0830410591988');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTS � LA POINTE','Musiques actuelles','www.artsalapointe.com','AUDIERNE','','','29770','48.0241077574','-4.54448750165');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PUYM''JAZZ','Musiques actuelles','www.puymjazz.com','PUYMIROL','','','47270','44.1878890197','0.805532157902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES F�TES MUSICALES DU CH�TEAU DE PIONSAT','Musiques classiques','www.musicalesdepionsat.com','PIONSAT','','','63330','46.1199954597','2.68534957248');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chemins de l''imaginaire','Pluridisciplinaire Spectacle vivant','','Terrasson','','','24120','45.1181463598','1.29916463367');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KIND OF BELOU','Musiques actuelles','www.kindofbelou.com','TREIGNAC','','','19260','45.5524776279','1.80105006647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAP JAZZ','Musiques actuelles','https://cap-dail.fr/','CAP D AIL','','','6320','43.7247392327','7.40161617522');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN R�','Musiques actuelles','www.jazzenre.fr','ST MARTIN DE RE','','','17410','46.1990908569','-1.36683779933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAND & CHOPIN EN SEYNE','Musiques classiques','www.festivalsandetchopinenseyne.com','LA SEYNE SUR MER','','','83500','43.0880294967','5.87089841754');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''AIN A L''AUTRE','Pluridisciplinaire Spectacle vivant','https://www.festivaaal.com/','TREVOUX','','','1600','45.940671655','4.7714415007');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOULE BLEUE','Musiques actuelles','www.laboulebleue.org','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL AU TOP','Pluridisciplinaire Spectacle vivant','www.letop.org','DIGNE LES BAINS','','','4000','44.0908723554','6.23590323452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES GUEILLES DE BONDE DE MACAU','Pluridisciplinaire Spectacle vivant','www.festivalgueillesdebonde.com','Macau','','','33460','45.004972156','-0.612661871132');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUVRE LA VOIX','Musiques actuelles','www.rockschool-barbey.com','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES DE CHANTS POLYPHONIQUES DE CALVI','Musiques actuelles','http://www.afiletta.com/fr_rencontres-chants-polyphoniques-calvi.php','CALVI','','','20260','42.5454874755','8.7595487404');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BEATLES','Musiques actuelles','http://www.unejourneeaveclesbeatles.com','SALON DE PROVENCE','','','13300','43.6462885238','5.06814649576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTHO''ROCK','Musiques actuelles','https://www.facebook.com/FesthoRock/','THORENS GLIERES','29/06/2019','29/06/2019','74570','45.9923733891','6.28837579239');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ARTS OSES','Musiques actuelles','www.lesartsoses.fr','THOUARS','29/06/2019','30/06/2019','79100','46.9828628407','-0.199581467021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Poupet','Musiques actuelles','www.festival-poupet.com','ST MALO DU BOIS','01/07/2019','19/07/2019','85590','46.9248120291','-0.914182961099');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTELIMAR AGGLO FESTIVAL','Musiques actuelles','www.ideehall.com','MONTELIMAR','02/07/2019','07/07/2019','26200','44.5540845734','4.74869387765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONFLUENT D''ARTS DU CHATEAU DE LA RIVIERE','Pluridisciplinaire Spectacle vivant','www.festivaldeconfolens.com','La Riviere','04/07/2019','06/07/2019','33126','44.9377237217','-0.316177006649');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l''Arpenteur','Transdisciplinaire','www.scenes-obliques.eu','LES ADRETS','05/07/2019','13/07/2019','38190','45.2586070645','5.99155979387');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international d�op�ra baroque','Musiques classiques','www.festivalbeaune.com','BEAUNE','05/07/2019','28/07/2019','21200','47.0255189366','4.83767985985');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TILLIACUM FESTIVAL','Musiques actuelles','www.tilliacum.com','Teille','05/07/2019','06/07/2019','72290','48.1849465917','0.188449766884');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Guitare en sc�ne','Musiques actuelles','www.guitare-en-scene.com','ST JULIEN EN GENEVOIS','11/07/2019','14/07/2019','74160','46.139365545','6.07881171095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Entrelac�s','Cirque et Arts de la rue','https://www.lesentrelaces.com/','LASSAY LES CHATEAUX','13/07/2019','14/07/2019','53110','48.4421089808','-0.498821106653');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ''Y KRAMPOUEZH','Musiques actuelles','http://www.jazzy-krampouezh.fr/','Nevez','14/07/2019','18/07/2019','29920','47.8154082656','-3.77625131302');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU VIGAN','Musiques classiques','www.festivalduvigan.fr','Le VIGAN','16/07/2019','22/08/2019','30120','43.9886565575','3.62522883405');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHIEN JAUNE FESTIVAL DU POLAR DE CONCARNEAU','Livre et litt�rature','http://www.lechienjaune.fr/','CONCARNEAU','19/07/2019','21/07/2019','29900','47.8966260003','-3.90716871821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN BAIE','Musiques actuelles','www.jazzenbaie.com','CAROLLES','24/07/2019','04/08/2019','50740','48.749037599','-1.55955935077');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Summer Vibration Reggae Festival','Musiques actuelles','http://www.zone51.net','SELESTAT','25/07/2019','27/07/2019','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS SECRETES','Musiques actuelles','www.lesnuitssecretes.com','AULNOYE AYMERIES','26/07/2019','28/07/2019','59620','50.2058749072','3.83263118494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK N BEER','Musiques actuelles','http://www.rocknbeer.fr/','BRETIGNOLLES SUR MER','27/07/2019','27/07/2019','85470','46.6374826705','-1.86324200464');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Biguine Jazz','Musiques actuelles','www.biguinejazz.com','FORT DE FRANCE','09/08/2019','15/08/2019','97234','14.6411114389','-61.0691889244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon L�Ile aux livres','Livre et litt�rature','www.ile-aux-livres.fr','Le Bois Plage en R�','09/08/2019','11/08/2019','17580','46.1825283655','-1.37625908104');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA CHANSON FRANCAISE DE MONTLUCON','Musiques actuelles','http://www.montlucontourisme.com','MONTLUCON','16/08/2019','18/08/2019','3100','46.3385883496','2.60390499777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VERS SOLIDAIRES','Musiques actuelles','www.vers-solidaires.org','ST GOBAIN','16/08/2019','18/08/2019','2410','49.5953887213','3.39411143672');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES LUMIERES DE MONTMORILLON','Musiques classiques','www.festival-des-lumieres.com','Montmorillon','22/08/2019','24/08/2019','86500','46.4263978846','0.895458347632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Eclat(s) de rue','Cirque et Arts de la rue','http://caen.fr/eclatsderue','CAEN','30/08/2019','31/08/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Visa pour l''image','Arts plastiques et visuels','http://www.visapourlimage.com','PERPIGNAN','30/08/2019','15/09/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOLCANIC BLUES FESTIVAL','Musiques actuelles','www.volcanic-blues.com','MONT DORE','26/09/2019','29/09/2019','63240','45.5760999131','2.80995918175');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon r�gional du livre pour la jeunesse','Livre et litt�rature','http://www.lecture-loisirs.com/salon/','TROYES','10/10/2019','13/10/2019','10000','48.2967099637','4.07827967525');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon de la revue','Livre et litt�rature','https://www.entrevues.org/','PARIS','11/10/2019','13/10/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Assises de la traduction litt�raire','Livre et litt�rature','www.atlas-citl.org','ARLES','08/11/2019','10/11/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MO''FO','Musiques actuelles','www.festivalmofo.org','ST OUEN','','','93400','48.909806575','2.33257042205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Village � Bascule','Pluridisciplinaire Spectacle vivant','http://www.levillageabascule.fr/','GERBEVILLER','','','54830','48.4934294526','6.51089844698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Ateliers ouverts','Arts plastiques et visuels','http://www.ateliersouverts.net/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'De jour // De nuit','Cirque et Arts de la rue','http://ruzo.fr/','La Norville','','','91290','48.5814225294','2.26569127873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Extradanse / CDC P�le Sud','Danse','','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Contrebande','Cirque et Arts de la rue','www.festivalcontrebande.fr','Revin','','','8500','49.9343612918','4.69218802167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOUD''ROCK','Musiques actuelles','www.foudrock.fr','MAGNY LES HAMEAUX','','','78114','48.7410207273','2.0519793397');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVA''SON','Musiques actuelles','http://www.fabrica-son.org','MALAKOFF','','','92240','48.8169695977','2.29693599517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L�ACID � Cannes','Cin�ma et audiovisuel','www.lacid.org','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ImagiNieul','Divers spectacle vivant','https://www.facebook.com/aurora.imaginieul','NIEUL','','','87510','45.9214559183','1.18018067279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRACTEUR BLUES','Musiques actuelles','http://www.blues-sur-seine.com','Sailly','','','78440','49.0430008047','1.79368235837');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INDIGENES','Musiques actuelles','http://www.stereolux.org','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUCHES DE JAZZ','Musiques actuelles','www.touchesdejazz.org','BEYNES','','','78650','48.854444414','1.86989017513');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Annecy Paysages','Transdisciplinaire','www.annecy-paysages.com','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Grains de sel','Livre et litt�rature','http://grainsdesel.aubagne.fr/','AUBAGNE','','','13400','43.2934843764','5.56331273477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TYRANT FEST','Musiques actuelles','https://www.tyrantfest.com','OIGNIES','','','62590','50.4644483399','2.99306842451');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Aux �crans du r�el (Chroma)','Cin�ma et audiovisuel','http://www.assochroma.com/','LE MANS','','','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Martinique Jazz Festival','Musiques actuelles','https://fr-fr.facebook.com/martiniquejazzfest/','FORT DE FRANCE','','','97234','14.6411114389','-61.0691889244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Hivernales du documentaire','Cin�ma et audiovisuel','http://leshivernalesdudoc.fr/','ST ANTONIN NOBLE VAL','','','82140','44.156093011','1.73640541242');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'STRASBOURG-M�DITERRAN�E','Musiques actuelles','www.strasmed.com','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THERAP RIRE','Divers Spectacle vivant','http://www.nantes-spectacles.com','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin� Musafiri','Cin�ma et audiovisuel','https://fr-fr.facebook.com/cinemsafiri/','MAMOUDZOU','','','97600','-12.7899979586','45.1932456026');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WEEK-END SAUVAGE','Musiques actuelles','www.toutafond.com','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CENTRAL 43','Musiques actuelles','https://www.hotelarbrevoyageur.com/node/310','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de cin�ma europ�en des Arcs','Cin�ma et audiovisuel','https://lesarcs-filmfest.com/fr','BOURG ST MAURICE','','','73700','45.6647980545','6.76637903907');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LABEL VALETTE','Arts plastiques et visuels','http://www.labelvalettefest.com','Pressigny les Pins','30/08/2019','31/08/2019','45290','47.8777160651','2.76127674486');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Motocultor','Musiques actuelles','www.motocultor-festival.com','ST NOLFF','16/08/2019','18/08/2019','56250','47.6984675237','-2.66583210925');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARQUES EN SC�NE','Musiques actuelles','http://www.narbonne.fr/barques-scene-2018-0','NARBONNE','22/08/2019','24/08/2019','11100','43.1652399898','3.02023868739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Stage-Festival DARC','Danse','www.danses-darc.com','CHATEAUROUX','11/08/2019','23/08/2019','36000','46.8029617828','1.69399812001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de Saint-Etienne','Livre et litt�rature','https://fetedulivre.saint-etienne.fr/','ST ETIENNE','11/10/2019','13/10/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cirque ou Presque','Cirque et Arts de la rue','www.cirqueoupresque.fr','Pire sur Seiche','','','35150','48.0111877546','-1.42837195377');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma isra�lien Shalom Europa','Cin�ma et audiovisuel','http://shalomeuropa.eu/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BOB (BLUES ON THE BORDER)','Musiques actuelles','','Terville','','','57180','49.3465483422','6.13220160083');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ''VELANET','Musiques actuelles','http://www.lavelanet-culture.com','Lavelanet','','','9300','42.9336867312','1.84586953504');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LAMANO FESTIVAL','Musiques actuelles','','EVRY','','','91000','48.6294831659','2.44008244492');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES AU 13','Musiques actuelles','http://www.theatre13.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DESORDRE','Pluridisciplinaire Spectacle vivant','http://www.penn-ar-jazz.com','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontre autour du Piano','Musiques actuelles','http://www.rencontre-autourdupiano.com/','LES ABYMES','','','97142','16.2727794035','-61.5017044974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�t� Frapp�','Pluridisciplinaire Spectacle vivant','www.macon.fr','MACON','','','71000','46.3205511756','4.81842529639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma de La Foa','Cin�ma et audiovisuel','https://www.festivalcinemalafoa.nc/','La Foa','','','98880','-21.70927','165.828366');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'STARTING BLOCKS','Musiques actuelles','http://www.jazzatours.com','JOUE LES TOURS','','','37300','47.333556074','0.654546300116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES ET TERRASSES','Musiques actuelles','www.musiques-terrasses.fr','VERDUN','','','55100','49.1454831903','5.36141821261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CA VA JAZZER','Musiques actuelles','www.cavajazzer.wix.com','CHATEAU LA VALLIERE','','','37330','47.5271484984','0.329691100223');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BROC''N''ROLL','Musiques actuelles','www.desbruitsdecasseroles.fr','MESSEIN','','','54850','48.6104817604','6.14212766773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FONTAINE DU RIRE','Divers Spectacle vivant','http://www.la-tete-de-mule.fr/','DIJON','11/01/2019','13/04/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Contes en Maisons','Divers Spectacle vivant','www.fdfr77.org','BLANDY LES TOURS','15/03/2019','31/03/2019','77720','48.5601991238','2.79501080828');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUAND LES SOURIS DANSENT','Musiques actuelles','http://www.lacocotteprod.com','LYON','16/03/2019','30/03/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ELECTR()CUTION','Musiques actuelles','www.ensemblesillages.com','BREST','20/03/2019','23/03/2019','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DRUMS SUMMIT','Musiques actuelles','www.drumssummit.com','TOULOUSE','01/03/2019','02/04/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA BOITE EN ZINC FAIT SON FESTIVAL','Musiques actuelles','https://www.tuberculture.fr/sujet/boite-zinc/','CHANTEIX','29/03/2019','31/03/2019','19330','45.3124205901','1.6226623783');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EN GRANGEONS LA MUSIQUE','Pluridisciplinaire Musique','www.engrangeonslamusique.fr','BOURG EN BRESSE','31/03/2019','17/11/2019','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THIS IS ENGLAND','Musiques actuelles','tafproduction.blogspot.fr/','ST JEAN DE VEDAS','08/03/2019','09/03/2019','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HANDICLAP','Pluridisciplinaire Spectacle vivant','handiclap.fr','NANTES','14/03/2019','17/03/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GUITARES DU MONDE','Pluridisciplinaire Musique','www.espacegerardphilipe.com','ST ANDRE LES VERGERS','15/03/2019','21/03/2019','10120','48.2782659904','4.04840305057');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN DES SENS','Musiques actuelles','www.undessens.fr','RENNES','25/03/2019','29/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE JAZZ D''HIRSON','Musiques actuelles','https://transfrontalieres.wordpress.com/','Hirson','29/03/2019','30/03/2019','2500','49.9426458618','4.10483732841');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BORDEAUX ROCK FESTIVAL','Musiques actuelles','http://www.bordeauxrock.com','BORDEAUX','23/01/2019','27/01/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OFF PARIS','Musiques actuelles','http://www.pointephemere.org/event/OFFF-dhiver-ByinrIe6m','PARIS','18/01/2019','19/01/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des cr�ations t�l�visuelles de Luchon','Cin�ma et audiovisuel','http://www.festivaldeluchon.tv/','Luchon','06/02/2019','10/02/2019','31110','42.736147731','0.626640395614');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rendez-vous de l�Histoire du monde arabe','Livre et litt�rature','www.imarabe.org','PARIS','11/04/2019','14/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Avril des Clowns','Cirque et Arts de la rue','https://www.avrildesclowns.com/','VENDEMIAN','19/04/2019','21/04/2019','34230','43.5790567885','3.56253401848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REPERKUSOUND','Musiques actuelles','www.reperkusound.com','VILLEURBANNE','19/04/2019','21/04/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Th��tre en Cour(s)','Cirque et Arts de la rue','https://www.theatreencours.org/accueil','AUBENAS','18/04/2019','20/04/2019','7200','44.6102127084','4.39638981424');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE PAQUES DE DEAUVILLE','Musiques classiques','www.musiqueadeauville.com','DEAUVILLE','20/04/2019','04/05/2019','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EVENSTAR FESTIVAL','Musiques actuelles','https://evenstarfestival.fr','LA ROCHE SUR YON','25/04/2019','25/04/2019','85000','46.6675261644','-1.4077954093');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Saint Barth Film Festival','Cin�ma et audiovisuel','https://www.stbarthff.org/','ST BARTHELEMY','28/04/2019','04/05/2019','97133','','');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Filmeurs - Rencontre cin�matographique de Conteville','Cin�ma et audiovisuel','https://www.festivallesfilmeurs.fr/','Conteville','05/07/2019','07/07/2019','27210','49.4174960871','0.391276015721');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DOUVE BLANCHE','Musiques actuelles','https://www.ladouveblanche.com','Egreville','05/07/2019','07/07/2019','77620','48.1821285238','2.88437118271');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE PUYCELSI','Musiques classiques','www.festivalpuycelsi.com','PUYCELSI','16/07/2019','27/07/2019','81140','43.9765161784','1.70709054548');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS MUSICALES D''UZ�S','Musiques classiques','nuitsmusicalesuzes.org','UZES','17/07/2019','29/07/2019','30700','44.0136590402','4.41600031556');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VAGUE DE JAZZ','Musiques actuelles','www.vaguedejazz.com','LES SABLES D OLONNE','26/07/2019','27/07/2019','85100','46.5007612799','-1.79255128677');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ESCALES','Musiques actuelles','www.les-escales.com','ST NAZAIRE','26/07/2019','28/07/2019','44600','47.2802857028','-2.25379927249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Pablo Casals','Musiques classiques','www.prades-festival-casals.com','Prades','26/07/2019','13/08/2019','66500','42.6145021996','2.43074849066');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Vendredis de l''�t�','Cirque et Arts de la rue','http://www.bagnolesdelorne.com/voir-faire/agenda/festivals-dete/les-vendredis-de-lete','BAGNOLES DE L ORNE NORMANDIE','12/07/2019','23/08/2019','61140','48.5556585202','-0.419510749596');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DETOURS DU MONDE','Musiques actuelles','www.detoursdumonde.org','CHANAC','14/07/2019','28/07/2019','48230','44.4502200567','3.34457977647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVOCE','Musiques actuelles','www.centreculturelvoce.org','PIGNA','13/07/2019','16/07/2019','20220','42.599340833','8.90040924261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE SAINT CERE','Musiques classiques','http://festival-saint-cere.com/','ST Cere','24/07/2019','13/08/2019','46400','44.8531827667','1.89258045388');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FOUS CHANTANTS D''ALES','Musiques actuelles','https://www.fouschantants.org/','ALES','20/07/2019','27/07/2019','30100','44.1250099126','4.08828501262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRANSHUMANCES MUSICALES','Musiques actuelles','www.transhumances-musicales.com','Laas','25/07/2019','26/07/2019','32170','43.4720603975','0.301177050148');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAVEURS JAZZ FESTIVAL','Musiques actuelles','www.saveursjazzfestival.com','SEGRE','03/07/2019','07/07/2019','49500','47.7016654227','-0.863354104119');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Binbins','Cirque et Arts de la rue','http://www.ville-douai.fr/index.php/Festival-des-binbins?idpage=200','DOUAI','03/07/2019','05/07/2019','59500','50.3823195335','3.09145683114');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIREVOLT�S','Cirque et Arts de la rue','www.lesvirevoltes.org','VIRE','01/07/2019','09/07/2019','14500','48.8512498219','-0.889601911342');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Megascene','Musiques actuelles','www.megascene.org','ST COLOMBAN','05/07/2019','06/07/2019','44310','47.0251492396','-1.55906118865');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le murmure du son','Musiques actuelles','www.murmureduson.org','EU','12/07/2019','13/07/2019','76260','50.0413268784','1.42861757801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L�ZART VERT','Musiques actuelles','www.festival-lezartvert.com','ST ETIENNE DE FURSAC','18/07/2019','20/07/2019','23290','46.1269796893','1.53252260223');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HUMOUR ET EAU SALEE','Divers Spectacle vivant','https://www.crea-sgd.org/humour-eau-salee/','ST GEORGES DE DIDONNE','27/07/2019','02/08/2019','17110','45.6036278448','-0.980098297165');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chamrousse en Piste','Cirque et Arts de la rue','https://www.chamrousse.com/festival-chamrousse-en-piste-office-du-tourisme-de-chamrousse-215248.html','CHAMROUSSE','02/08/2019','04/08/2019','38410','45.1197427134','5.89438297795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LITTLE FESTIVAL','Musiques actuelles','http://www.little-festival.fr/','Seignosse Le Penon','31/07/2019','04/08/2019','40510','43.7021964029','-1.3970873051');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GUINGUETTES DE L''AUZON','Musiques actuelles','www.guinguettes-auzon.com','CARPENTRAS','02/08/2019','03/08/2019','84200','44.0593802565','5.06134844776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ENFERMES DEHORS ENCORE','Musiques actuelles','https://www.lanaute.com','Champagnat','','','23190','46.0202394723','2.28670592425');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'1001 BASS MUSIC FESTIVAL','Musiques actuelles','http://www.1001bass.net','ST ETIENNE','','','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ritournelles','Livre et litt�rature','http://permanencesdelalitterature.fr/ritournelles/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MIRR FESTIVAL','Musiques actuelles','www.mirr.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A TEMPO','Musiques classiques','www.a-tempo.fr','Lavaur','','','81500','43.6895628711','1.79437213708');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FERTOIS ROCK IN FEST','Musiques actuelles','','La Fert� sous Jouarre','','','77260','48.9540164057','3.12762174384');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HEAVEN''S DOOR','Musiques actuelles','www.heavensdoor.fr','Eckbolsheim','','','67201','48.5790053064','7.68216295597');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Un Festival c�est trop court � Festival du court m�trage de Nice','Cin�ma et audiovisuel','www.nicefilmfestival.com','NICE','','','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIANOSCOPE','Musiques classiques','www.pianoscope.beauvais.fr','BEAUVAIS','','','60000','49.4365523321','2.08616123661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRASHMUSETTE','Musiques actuelles','http://www.crashmusette.fr/','LA VOULTE SUR RHONE','','','7800','44.8015403943','4.78018830634');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV JAZZ A CHABEUIL','Musiques actuelles','http://www.festivjazz.fr/','Chabeuil','','','26120','44.9082255842','5.00743034189');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BALLADES EN NOVEMBRE','Musiques actuelles','http://viarmes.fr/','Viarmes','','','95270','49.1231107932','2.37799676025');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SULFUROCK','Musiques actuelles','www.sulfurock.com','Bassens','','','33530','44.9084140601','-0.525868280903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MELTIN''ART','Musiques actuelles','meltinart-festival.blogspot.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival europ�en des Artistes de cirque, Saint-Paul-l�s-Dax','Cirque et Arts de la rue','https://www.festivalcirquesaintpaul.com/','ST Paul les Dax','','','40990','43.7464083139','-1.08127980711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHANT DES PIERRES','Pluridisciplinaire Spectacle vivant','http://www.ekleknopopulace.fr/','ST JEAN DES VIGNES','','','69380','45.8746263893','4.68243695399');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX DU ROCK (COUH�)','Musiques actuelles','www.lavoixdurock.fr','COUHE','31/05/2019','01/06/2019','86700','46.2960515844','0.179497066899');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JUNE EVENTS','Danse','','PARIS','01/06/2019','15/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FATCHE DE - FESTIVAL DU RIRE DE MARSEILLE','Divers Spectacle vivant','http://marseille.festirire.com','MARSEILLE','15/06/2019','15/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OSTREOID FEST','Musiques actuelles','','PARIS','15/06/2019','16/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film d�aventure','Cin�ma et audiovisuel','http://www.auboutdureve.fr/','ST DENIS','04/05/2019','17/05/2019','97400','-20.9329708192','55.446867167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Parlemonde','Transdisciplinaire','http://parlemonde.mascenenationale-creative.com/','MONTBELIARD','09/05/2019','11/05/2019','25200','47.5155169816','6.79148147353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA MAGNIFIQUE SOCIETY','Musiques actuelles','www.lamagnifiquesociety.com','REIMS','13/06/2019','15/06/2019','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RUES ET CIES','Cirque et Arts de la rue','http://www.epinal.fr/culture/festivals/rues-et-cies','EPINAL','14/06/2019','16/06/2019','88000','48.1631202656','6.47989286928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Saint-Etienne Live by Paroles et Musiques','Musiques actuelles','https://www.saint-etienne-live.fr/','ST ETIENNE','14/06/2019','15/06/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� RIO LOCO ! GARONNE, LE FESTIVAL','Musiques actuelles','www.rio-loco.org','TOULOUSE','13/06/2019','16/06/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUAND JE PENSE A FERNANDE','Musiques actuelles','www.festival-fernande.com','SETE','22/06/2019','26/06/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Abbayes en Lorraine','Musiques classiques','http://www.festivaldesabbayes.com/','LA PETITE RAON','26/06/2019','24/08/2019','88210','48.4194366522','6.99291726168');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PAVILLONS JAZZ FESTIVAL','Musiques actuelles','www.espace-des-arts.fr','LES PAVILLONS SOUS BOIS','17/05/2019','19/05/2019','93320','48.9082060253','2.50297448267');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WOLFI JAZZ','Musiques actuelles','http://wolfijazz.com','Wolfisheim','26/06/2019','30/06/2019','67202','48.5869426788','7.66313638698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EUROPAVOX','Musiques actuelles','www.europavox.com','CLERMONT FERRAND','27/06/2019','30/06/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ART''ZIMUT�S','Transdisciplinaire','www.lesartzimutes.com','CHERBOURG EN COTENTIN','27/06/2019','29/06/2019','50100','49.633412156','-1.63390160204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU MONASTIER LA MUSIQUE DES CUIVRES','Musiques actuelles','www.festivaldumonastier.fr','LE MONASTIER SUR GAZEILLE','','','43150','44.9345419113','4.00710354299');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE ROCAMADOUR','Musiques classiques','www.rocamadourfestival.com','Rocamadour','','','46500','44.8148525753','1.62220346544');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIBERGES','Musiques actuelles','','ST BAUZILLE DE PUTOIS','','','34190','43.9007200249','3.75901045062');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CASTELJALOUX JAZZ � TABLE FESTIVAL','Musiques actuelles','','CASTELJALOUX','','','47700','44.3114746788','0.0845386197153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FALZ''ART','Musiques actuelles','https://mairie-fayauxloges.fr/culture/155:falz-art','FAY AUX LOGES','','','45450','47.9326007256','2.16025043268');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Poule des Champs','Musiques actuelles','www.lapouledeschamps.com','AUBERIVE','','','51600','49.1972757573','4.41096827282');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOYAGES EN GUITARE','Pluridisciplinaire Musique','http://www.ville-chinon.com/vie-culturelle/festivals/voyages-en-guitare/','CHINON','','','37500','47.1723001595','0.245084664558');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTOMNE MUSICAL DE TAVERNY','Musiques classiques','www.automnemusicaltaverny.com','Taverny','','','95150','49.0267316203','2.22116067166');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES DEBROUSSAILLEUSES','Musiques actuelles','http://lesdebroussailleuses.docteurparadi.com','Trementines','','','49340','47.1227107975','-0.801475622875');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVINI','Transdisciplinaire','www.festivini.com','SAUMUR','','','49400','47.2673853525','-0.0830410591988');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK EN PLAINE','Musiques actuelles','www.rockenplaine.org','Varennes Vauzelles','','','58640','47.0372879565','3.14354311851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Film Francophone d''Angoul�me','Cin�ma et audiovisuel','http://www.filmfra','ANGOULEME','','','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Argelliers Jazz Estival','Musiques actuelles','www.argelliersjazzestival.wordpress.com','Argelliers','','','34380','43.7249523063','3.67420720253');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d''art contemporain Chemin d''art','Arts plastiques et visuels','http://www.chemindart.fr/','ST FLOUR','','','15100','45.0278919825','3.08320563232');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A LA GRANGE','Musiques actuelles','https://www.evianresort.com/evenements/jazz-a-la-grange/','Evian Les Bains','','','74500','46.3924102379','6.58612507528');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOINS D''HIVER','Musiques actuelles','www.lesfoinsdhiver.com','LAVAL','','','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEAUNE BLUES BOOGIE FESTIVAL','Musiques actuelles','www.jpboogie.com','BEAUNE','','','21200','47.0255189366','4.83767985985');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUAND ON CONTE','Divers spectacle vivant','quandonconte.free.fr','Nouaille Maupertuis','15/03/2019','30/03/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREE SONS DIVERS','Musiques actuelles','www.freesonsdivers.com','LES HERBIERS','08/03/2019','09/03/2019','85500','46.8666125813','-1.02216086186');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Garorock','Musiques actuelles','www.garorock.com','MARMANDE','27/06/2019','30/06/2019','47200','44.5055176845','0.172174280849');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Contentpourien','Divers Spectacle vivant','http://www.contentpourien.fr/','Mantes la Ville','25/06/2019','07/07/2019','78711','48.9747939435','1.71205571778');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Renc''arts de rue','Cirque et Arts de la rue','https://www.cs-bievre-est.fr/1279-festival-renc-arts-rue.html','Le Grand Lemps','28/06/2019','29/06/2019','38690','45.3962642718','5.4186427969');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Convivencia','Musiques actuelles','www.festivalconvivencia.eu','Revel','30/06/2019','28/07/2019','31250','43.4656037799','1.99689394052');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MACKI MUSIC FESTIVAL','Musiques actuelles','http://www.mackimusicfestival.fr/','Carrieres sur Seine','29/06/2019','30/06/2019','78420','48.9119335064','2.17838235323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'O LES CH�URS Brive','Musiques actuelles','https://elizabethmydear.fr/','Brive la Gaillarde','','','19100','45.1435830664','1.51936836063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL D�ART SACR� D�ANTIBES','Musiques classiques','http://www.antibesjuanlespins.com/fr/evenements/festival-d%E2%80%99art-sacr%C3%A9','ANTIBES','','','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK FORT FEST','Musiques actuelles','www.rockfortfest.com','Bainville sur Madon','','','54550','48.5900970896','6.08780771245');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon international du livre oc�anien','Livre et litt�rature','https://fr-fr.facebook.com/Salon.international.du.livre.oceanien/','PAPEETE','','','98714','-17.552056','-149.55886');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZE FIESTIVAL','Musiques actuelles','www.zefiestival.net','MARSANNE','','','26740','44.640273082','4.88311372596');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATTENTION LES FEUILLES !','Musiques actuelles','http://theatredescollines.annecy.fr/festivals/attention-les-feuilles/','MEYTHET','','','74960','45.9184414725','6.09688746113');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres de Films en Bretagne � Saint-Quay-Portrieux','Cin�ma et audiovisuel','www.filmsenbretagne.com','ST QUAY PORTRIEUX','','','22410','48.6542475006','-2.84382337656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�S JAZZ IN RIAD FESTIVAL','Musiques actuelles','','JOUARS PONTCHARTRAIN','','','78760','48.7904544648','1.90784227893');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Lumi�re','Cin�ma et audiovisuel','www.festival-lumiere.org','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARMAILLE','Pluridisciplinaire Spectacle vivant','www.lillicojeunepublic.fr','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTOMNE MUSICAL DU VESINET','Musiques classiques','www.automnemusicalduvesinet.fr','LE VESINET','','','78110','48.8938640591','2.13039312004');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RADE SIDE','Musiques actuelles','www.radeside.com','TOULON','','','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BAR - BLUES D''AUTOMNE EN RABELAISIE','Musiques actuelles','https://www.festival-bar.fr/','Beaumont En Veron','','','37420','47.1939476684','0.186243013287');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DERNIER CRI','Transdisciplinaire','https://www.facebook.com/festivalderniercri/','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Yaouank','Musiques actuelles','http://yaouank.bzh/','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres chor�graphiques de Seine-Saint-Denis','Danse','http://www.rencontreschoregraphiques.com/','ST DENIS','17/05/2019','22/06/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival �poque','Livre et litt�rature','http://caen.fr/epoque','CAEN','17/05/2019','19/05/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Hip Hop �volution','Danse','https://fr-fr.facebook.com/hiphopevolutionmayotte/','BANDRABOUA','01/03/2019','09/03/2019','97650','-12.7232292527','45.1181212208');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAND UNISSON','Musiques actuelles','www.ville-saintjeandelaruelle.fr','ST JEAN DE LA RUELLE','14/06/2019','15/06/2019','45140','47.9116141172','1.87108144709');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ & BLUES A CHANCELADE','Musiques actuelles','http://jazz-blues-chancelade.fr/','CHANCELADE','13/06/2019','15/06/2019','24650','45.2100121087','0.655273444534');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COULEURS DU MONDE','Musiques actuelles','www.leplancher.com','LANGONNET','31/05/2019','02/06/2019','56630','48.1318811919','-3.48128235286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOUGERES MUSICALES','Musiques classiques','https://www.festivalfougeresmusicales.com','FOUGERES','31/05/2019','15/06/2019','35300','48.3524697115','-1.19431177241');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�CHO � VENIR','Musiques actuelles','www.echoavenir.fr','BORDEAUX','24/06/2019','26/06/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''VAL EN SEINE','Musiques actuelles','https://festivalenseine.fr/','ST MAMMES','15/06/2019','16/06/2019','77670','48.3867865582','2.81899706389');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Paris l''�t�','Pluridisciplinaire Spectacle vivant','http://www.parislete.fr/','PARIS','12/07/2019','03/08/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � JUAN','Musiques actuelles','www.jazzajuan.com','JUAN LES PINS','12/07/2019','21/07/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Electrobeach','Musiques actuelles','http://www.electrobeach.com','Port Barcares','12/07/2019','14/07/2019','66420','42.8127710984','3.02834125149');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JAZZ A BERNE','Musiques actuelles','http://www.jazzfestivalbern.ch/','Lorgues','12/07/2019','25/08/2019','83510','43.4814630298','6.35892744946');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAUFFER DANS LA NOIRCEUR','Musiques actuelles','www.chaulnesmetalfest.fr','MONTMARTIN SUR MER','12/07/2019','14/07/2019','50590','48.9910212765','-1.52994382531');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK AND ROLL CIRCUS FESTIVAL','Musiques actuelles','http://rockandrollcircusfestival.fr/','Villeneuve Sur Lot','12/07/2019','14/07/2019','47300','44.4251092866','0.742526167632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COOKSOUND FESTIVAL','Musiques actuelles','http://www.cooksound.com','Forcalquier','18/07/2019','21/07/2019','4300','43.9599082374','5.7882827572');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE SACREE ET ORGUE EN AVIGNON','Musiques classiques','https://www.musique-sacree-en-avignon.org/','AVIGNON','06/07/2019','22/07/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAUSSE TES TONGS','Musiques actuelles','http://www.festival-chausse-tes-tongs.com','TREVOU TREGUIGNEC','09/08/2019','11/08/2019','22660','48.8121355763','-3.34499178706');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CORDES EN BALLADE','Musiques classiques','www.cordesenballade.com','VIVIERS','02/07/2019','14/07/2019','7220','44.4834547319','4.66986126314');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Beauregard','Musiques actuelles','www.festivalbeauregard.com','Herouville ST Clair','04/07/2019','07/07/2019','14200','49.2073560619','-0.331022626025');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Voies Off','Arts plastiques et visuels','http://voies-off.com/','ARLES','01/07/2019','06/07/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHIEN A PLUMES','Musiques actuelles','www.chienaplumes.fr','DOMMARIEN','02/08/2019','04/08/2019','52190','47.6892296711','5.35283846982');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Electrobotik Invasion','Musiques actuelles','www.electrobotikinvasion.com','BAGNOLS SUR CEZE','02/08/2019','03/08/2019','30200','44.1622496736','4.62477380854');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ESCALES DU CARGO','Musiques actuelles','www.escales-cargo.com','ARLES','19/07/2019','21/07/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � BRIGNOLES','Musiques actuelles','www.jazzabrignoles.net','BRIGNOLES','25/07/2019','27/07/2019','83170','43.3992941902','6.07740244296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU COMMINGES','Musiques classiques','https://www.festival-du-comminges.com/','Saint Gaudens','20/07/2019','24/09/2019','31800','43.1199260811','0.726613564199');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST-JAZZ DE CH�TEAUNEUF DU FAOU','Musiques actuelles','www.fest-jazz.com','CHATEAUNEUF DU FAOU','26/07/2019','28/07/2019','29520','48.1889732859','-3.82126129764');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE DE GRAVELINES','Divers Spectacle vivant','www.tourisme-gravelines.fr/fr/agenda/festival-du-rire','GRAVELINES','','','59820','50.9975626638','2.14413046485');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Mer est loin','Livre et litt�rature','http://lamerestloin.com/','Fontenay Le Comte','','','85200','46.4563186117','-0.793449510859');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AURORES MONTREAL','Musiques actuelles','https://www.oocto.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WKND HIPHOP','Musiques actuelles','https://bel-air-open-air.com/le-phare/evenements/wknd-hiphop-2/','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREESTYLE','Danse','lavillette.com/evenement/freestyle-18','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ORCHESTRES EN FETE','Musiques classiques','www.orchestresenfete.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIDDIM COLLISION','Musiques actuelles','www.riddimcollision.org','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Petites fugues','Livre et litt�rature','http://www.lespetitesfugues.fr/','BESANCON','','','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REBEL ESCAPE','Musiques actuelles','http://www.talowa.fr/rebel-escape-fr/','TOURNEFEUILLE','','','31170','43.5781918597','1.33500697752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Download Festival','Musiques actuelles','www.downloadfestival.fr','Bretigny Sur Orge','29/05/2020','31/05/2020','91220','48.6025113399','2.3021623232');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International de la Photo Animali�re et de Nature','Arts plastiques et visuels','https://www.photo-montier.org/','MONTIER EN DER','14/11/2019','17/11/2019','52220','48.4874732749','4.78668890124');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Colmar','Livre et litt�rature','http://festivaldulivre.colmar.fr/','COLMAR','23/11/2019','24/11/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'B-SIDE REGGAE FESTIVAL','Musiques actuelles','http://www.assolaruche.fr/','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HOLOCENE','Musiques actuelles','lestival-holocene.fr','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ENDLESS PROJECT','Musiques actuelles','','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GOUTTE D''OR EN F�TE','Musiques actuelles','www.gouttedorenfete.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dans Mon Village','Musiques actuelles','','Magalas','','','34480','43.4808688877','3.21744589266');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Pont-Leveque','Livre et litt�rature','www.lireapontleveque.fr','Pont L Eveque','27/09/2019','28/09/2019','14130','49.2805619214','0.187666368193');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FANFARFELUES','Musiques actuelles','http://lesfanfarfelues.bzh','VITRE','23/08/2019','25/08/2019','35500','48.1140815063','-1.19370720718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU ROI ARTHUR','Musiques actuelles','www.festivalduroiarthur.fr','BREAL SOUS MONTFORT','23/08/2019','25/08/2019','35310','48.044637167','-1.86571618785');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Chopin � Nohant','Musiques classiques','','Nohant Vic','','','36400','46.6401357121','1.95677952761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FETE DANS LA RUE A CORBIE','Cirque et Arts de la rue','www.mairie-corbie.fr/fete-dans-la-rue-a-corbie','Corbie','','','80800','49.9202042489','2.49498506906');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DU TRAVAILLEUR CATALAN','Musiques actuelles','http://www.letc.fr/','ARGELES SUR MER','','','66700','42.5352193463','3.02429862885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GALL�SIE EN F�TE','Musiques actuelles','www.gallesie-monterfil.org','MONTERFIL','','','35160','48.059933472','-1.99008537755');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ POURPRE EN PERIGORD','Musiques actuelles','www.jazzpourpre.com','BERGERAC','','','24100','44.8543751872','0.486529423457');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BANG BANG PARTY','Musiques actuelles','http://bangmusic.free.fr/','ST QUENTIN','','','2100','49.8472336321','3.27769499462');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'100 MIL V�AULT','Musiques actuelles','','Ault','','','80460','50.0925488309','1.44717519345');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAMILLIACUS JAZZ FESTIVAL','Musiques actuelles','www.jazzchemille.com','CHEMILLE','','','49120','47.2272268469','-0.730188160448');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES MUSICALES DE VEZELAY','Musiques classiques','www.rencontresmusicalesdevezelay.com','Vezelay','','','89450','47.4524539513','3.71058845441');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES TRANSES CEVENOLES','Pluridisciplinaire Spectacle vivant','www.lestranses.org','SUMENE','','','30440','43.9885776737','3.73217518553');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN PLACE','Musiques actuelles','www.jazzenplace.fr','DINAN','','','22100','48.4557709953','-2.04816149693');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film de Nancy','Cin�ma et audiovisuel','https://www.fifnl.com/','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU PR� DE MON ANE','Musiques actuelles','','EVRES','','','55250','48.9823527638','5.1255521249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COLOMB''IN ROCK','Musiques actuelles','https://colombelesvesoul.fr/','COLOMBE LES VESOUL','','','70000','47.6121777534','6.22089003773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN AIR DE JAZZ','Musiques actuelles','www.unairdejazz.com','ST CAST LE GUILDO','','','22380','48.6037155601','-2.24912296088');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Fest''Arts','Cirque et Arts de la rue','https://www.festarts.com/','LIBOURNE','','','33500','44.9130767869','-0.234709332068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU SOMMET','Musiques actuelles','www.jazzausommet.com','ST GENEST MALIFAUX','','','42660','45.3472458116','4.43132626452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLIMAX FESTIVAL','Musiques actuelles','http://climaxfestival.fr/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Coconut Music Festival','Musiques actuelles','http://www.coconutmusicfestival.org/','SAINTES','','','17100','45.7422300162','-0.649623035643');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Toiles dans la Ville','Cirque et Arts de la rue','www.leprato.fr','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Cin�ma am�ricain de Deauville','Cin�ma et audiovisuel','www.festival-deauville.com','DEAUVILLE','','','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETES ESCALES A VENISSIEUX','Musiques actuelles','www.ville-venissieux.fr/','VENISSIEUX','','','69200','45.7037728826','4.88137668221');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ''TITUDES','Musiques actuelles','www.jazztitudes.org','LAON','01/03/2019','31/05/2019','2000','49.5679724897','3.62089561902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Journ�es cin�matographiques dionysiennes','Cin�ma et audiovisuel','https://www.lecranstdenis.org/','ST DENIS','06/02/2019','12/02/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEAUGRENELLE MUSIC FESTIVAL','Musiques actuelles','www.beaugrenelle-paris.com/','PARIS','05/04/2019','14/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�-court anim� � Festival international du court m�trage d''animation de Roanne','Cin�ma et audiovisuel','www.cinecourtanime.com','ROANNE','18/03/2019','24/03/2019','42300','46.0449112487','4.0797045647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A MONTBRISON','Musiques actuelles','http://www.theatredespenitents.fr','MONTBRISON','21/03/2019','11/04/2019','42600','45.6008365515','4.0713982073');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT-FONS JAZZ FESTIVAL','Musiques actuelles','www.saint-fons-jazz.fr','ST FONS','22/01/2019','02/02/2019','69190','45.7011050588','4.85046367008');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTAN DE BLUES','Musiques actuelles','http://www.bolegason.org/','CASTRES','25/01/2019','03/02/2019','81100','43.6156511237','2.23787231587');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANTS D''(H)IVER','Musiques actuelles','www.lacigaliere.fr','SERIGNAN','24/01/2019','26/01/2019','34410','43.2733185307','3.29725083254');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Amiens Europe','Pluridisciplinaire Spectacle vivant','http://www.maisondelaculture-amiens.com/','AMIENS','16/01/2019','25/01/2019','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'7 BIS CHEMIN DE TRAVERSE','Pluridisciplinaire Musique','http://www.resonancecontemporaine.org','BOURG EN BRESSE','30/03/2019','14/04/2019','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAULNES METAL FEST','Musiques actuelles','http://www.chaulnesmetalfest.fr/','CHAULNES','19/04/2019','20/04/2019','80320','49.8169359721','2.80153906029');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du premier roman et des litt�ratures contemporaines de Laval','Livre et litt�rature','http://www.festivalpremierroman.fr/','LAVAL','02/05/2019','05/05/2019','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''PAILLE A SONS','Musiques actuelles','http://lpailleasons-festival.com','CHARTRES','07/06/2019','08/06/2019','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d''Auvers-sur-Oise','Musiques classiques','http://festival-auvers.com/','AUVERS SUR OISE','07/06/2019','07/06/2019','95430','49.0803858577','2.15488070885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES MUSIQUES','Musiques classiques','gmem.org/festivals/les-musiques','MARSEILLE','09/05/2019','18/05/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAMILY FESTIVAL','Transdisciplinaire','https://www.fondationlouisvuitton.fr/fr/activites-visites-ateliers/jeunes-publics-en-famille/evenement/family-festival.html','PARIS','10/05/2019','12/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de musique ancienne et baroque de Saint-Michel','Musiques classiques','www.festival-saint-michel.fr','ST MICHEL','02/06/2019','30/06/2019','2830','49.9259700696','4.15288229364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NORMANDY METAL FEST','Musiques actuelles','','Louviers','01/06/2019','01/06/2019','27400','49.2206099164','1.15340030158');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SORBONNE MUSIC DAYS','Musiques actuelles','http://sorbonnemusicdays.fr/','PARIS','04/06/2019','07/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAISONS LAFFITTE JAZZ FESTIVAL','Musiques actuelles','www.mljazzfestival.com','Maisons Laffitte','14/06/2019','23/06/2019','78600','48.9523369002','2.15161594901');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES VOIX A MOISSAC','Musiques actuelles','','Moissac','14/06/2019','23/06/2019','82200','44.1262518079','1.09834758014');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECLAT(S) D''�T� Festival des cultures et musiques du monde','Pluridisciplinaire Spectacle vivant','www.ville-gap.fr/','GAP','','','5000','44.5798600596','6.06486052138');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Shake La Rochelle','Danse','http://shakelarochelle.com/','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SONS D''AUTOMNE A QUESSOY','Musiques actuelles','https://lessonsdautomne.wordpress.com','Quessoy','','','22120','48.4272969207','-2.65707016677');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZDOR','Musiques actuelles','www.jazzdor.com','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RETRO C TROP','Musiques actuelles','http://retroctrop.fr/','Tilloloy','29/06/2019','30/06/2019','80700','49.6416492977','2.74418919931');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE CARCASSONNE','Pluridisciplinaire Musique','www.festivaldecarcassonne.fr','CARCASSONNE','02/07/2019','31/07/2019','11000','43.2093798444','2.34398855385');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la V�z�re','Musiques classiques','www.festival-vezere.com','Brive la Gaillarde','09/07/2019','22/08/2019','19100','45.1435830664','1.51936836063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nice Classic Live','Musiques classiques','','NICE','22/07/2019','05/08/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DE SAINT JACQUES','Musiques actuelles','www.nuitsdesaintjacques.com','LE PUY EN VELAY','11/07/2019','13/07/2019','43000','45.0276366659','3.89535229067');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAHORS BLUES FESTIVAL','Musiques actuelles','www.cahorsbluesfestival.com','CAHORS','12/07/2019','16/07/2019','46000','44.4507370916','1.44075837848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Eclectiques','Cirque et Arts de la rue','http://www.festival-leseclectiques.fr/','Carvin','12/07/2019','14/07/2019','62220','50.4895185959','2.94858062937');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Marseille Jazz des cinq continents','Musiques actuelles','www.marseillejazz.com','MARSEILLE','17/07/2019','27/07/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CELTE EN GEVAUDAN','Musiques actuelles','www.festivalengevaudan.com','Saugues','08/08/2019','10/08/2019','43170','44.9482034569','3.53883236387');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DURANCE LUBERON','Musiques actuelles','www.festival-durance-luberon.com','AVIGNON','09/08/2019','24/08/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARCACHON EN SC�NE','Musiques actuelles','http://www.arcachon.com/arcachon_en_scene.html','ARCACHON','28/07/2019','02/08/2019','33120','44.6529002838','-1.17429790933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon � Biographie et histoire �','Livre et litt�rature','http://www.hossegor.fr/fr/decouvrir/les-grands-evenements/salon-livre/','Hossegor','05/07/2019','07/07/2019','40150','43.6675688674','-1.41245896656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POINTU FESTIVAL','Musiques actuelles','http://www.pointufestival.fr/','SIX FOURS LES PLAGES','05/07/2019','07/07/2019','83140','43.086818602','5.82924464931');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz au Phare','Musiques actuelles','www.jazzauphare.com','ST Clement des Baleines','03/08/2019','07/08/2019','17590','46.2363966414','-1.53823691013');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE TARENTAISE','Musiques classiques','www.festivaldetarentaise.com','La Cote d''Aime','01/08/2019','13/08/2019','73210','45.6105383975','6.66721481397');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CA JAZZ A BRIDES','Musiques actuelles','http://www.jazzabrides.com','BRIDES LES BAINS','04/07/2019','07/07/2019','73570','45.4538077442','6.56840188785');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLANE R FEST','Musiques actuelles','http://www.planerfest.com','Colombier Saugnieu','05/07/2019','06/07/2019','69124','45.7178013267','5.10337456661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � TOUTE HEURE','Musiques actuelles','www.jazzatouteheure.com','ST ARNOULT EN YVELINES','15/03/2019','14/04/2019','78730','48.5725220079','1.93463847942');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d�un jour - Valence','Cin�ma et audiovisuel','www.lequipee.com','VALENCE','18/03/2019','23/03/2019','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Rochefort Pacifique � Cin�ma des ailleurs','Transdisciplinaire','http://www.rochefortpacifique.org/','Rochefort','21/03/2019','24/03/2019','17300','45.9432344536','-0.972356445692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film de femmes de Cr�teil','Cin�ma et audiovisuel','www.filmsdefemmes.com','CRETEIL','22/03/2019','30/03/2019','94000','48.7837401836','2.45463530415');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SULFURE FESTIVAL','Musiques actuelles','http://sulfure-festival','PARIS','09/03/2019','31/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du grand reportage / FIGRA','Cin�ma et audiovisuel','http://www.figra.fr/','ST OMER','13/03/2019','17/03/2019','62500','50.7679781717','2.26334881482');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL JAZZ A MEGEVE','Musiques actuelles','https://www.jazzamegeve.com','MEGEVE','28/03/2019','30/03/2019','74120','45.8401573581','6.62348394015');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LOUNGE MUSIC FESTIVAL','Musiques actuelles','www.lesgets.com','LES GETS','23/03/2019','31/03/2019','74260','46.1542288253','6.66014566051');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Prise de CirQ''','Cirque et Arts de la rue','www.cirqonflex.fr','DIJON','26/03/2019','28/04/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARS EN BAROQUE','Musiques classiques','https://www.marsenbaroque.com','MARSEILLE','26/02/2019','31/03/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA PIPOSA','Musiques actuelles','www.piposa.fr','SAILLY SUR LA LYS','05/04/2019','07/04/2019','62840','50.6532332701','2.79249849805');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NOCES FELINES','Musiques actuelles','http://velours-prod.com/','REIMS','04/04/2019','06/04/2019','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontre des jonglages de La Courneuve','Cirque et Arts de la rue','http://festival.maisondesjonglages.fr/','La Courneuve','05/04/2019','05/05/2019','93120','48.9322569546','2.39978064801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l�oiseau et de la nature d�Abbeville','Transdisciplinaire','www.festival-oiseau-nature.com','ABBEVILLE','13/04/2019','22/04/2019','80100','50.1083582944','1.83209170207');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bloody Fleury','Livre et litt�rature','http://bloody.fleurysurorne.fr/','Fleury sur Orne','01/02/2019','03/02/2019','14123','49.1444292345','-0.377619145275');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Maghreb-Orient des livres','Livre et litt�rature','http://coupdesoleil.net/','PARIS','08/02/2019','10/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Travelling de Rennes','Cin�ma et audiovisuel','www.clairobscur.info','RENNES','05/02/2019','12/02/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FLOREAL MUSICAL','Musiques classiques','https://www.lorraineaucoeur.com','EPINAL','30/04/2019','11/05/2019','88000','48.1631202656','6.47989286928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE DE CAVAILLON','Divers Spectacle vivant','http://www.mjccavaillon.fr/','CAVAILLON','20/05/2019','25/05/2019','84300','43.8508361826','5.03606300916');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICA NIGELLA','Musiques classiques','http://www.musica-nigella.fr/','BERCK','22/05/2019','26/05/2019','62600','50.4141574341','1.58229663833');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BATOJAZZ (Bat�Jazz)','Musiques actuelles','www.batojazz.com','Chanaz','','','73310','45.8034782998','5.79654546056');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOMAHAWK','Musiques actuelles','http://www.tomahawk-music.eu/','Querrien','','','29310','47.9553937645','-3.54781313088');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JOUR & NUIT','Musiques actuelles','www.la-belle-electrique.com','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Accroches C�urs','Cirque et Arts de la rue','www.angers.fr/accrochecoeurs','ANGERS','','','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL POT''ARTS','Pluridisciplinaire Spectacle vivant','www.cc-secteurdillfurth.fr','Tagolsheim','','','68720','47.6548634844','7.26381080113');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES GRANGES','Musiques actuelles','www.festivaldesgranges.com','LAIMONT','','','55800','48.8425240872','5.04206568758');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHARIVARI','Cirque et Arts de la rue','www.billomrenaissance.fr','BILLOM','','','63160','45.7251184657','3.32304115809');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Week-End des Arts de la Rue','Cirque et Arts de la rue','http://www.ville-bagneresdebigorre.fr/','BAGNERES DE BIGORRE','','','65200','42.9758668859','0.132640694318');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES �PIC�ES','Musiques actuelles','www.musiques-epicees.com','ST AULAYE','','','24410','45.1823303924','0.136303399419');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la Chabriole','Musiques actuelles','','ST Michel de Chabrillanoux','','','7360','44.8373734527','4.61203758015');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'De Chap � Chap','Pluridisciplinaire Spectacle vivant','','CHAPAREILLAN','','','38530','45.461591172','5.95800759236');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VACH DE ROCK','Musiques actuelles','www.vachderock.fr','Jeandelaincourt','','','54114','48.846525852','6.24307502417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Rue de Ramonville','Cirque et Arts de la rue','http://www.festivalramonville-arto.fr/','RAMONVILLE ST AGNE','','','31520','43.5441837911','1.47782372823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PERCUFOLIES','Musiques actuelles','https://www.percufolies.fr/','LIGUEIL','','','37240','47.0423190397','0.817374235768');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Blues au Ch�teau','Musiques actuelles','http://www.bluesauchateau.com/','La Cheze','','','22210','48.1346308811','-2.66260913976');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''EstiVAL','Pluridisciplinaire Spectacle vivant','https://www.valeyrieux.fr/au-quotidien/culture/l-estival/','LE CHEYLARD','','','7160','44.9076391858','4.42108479359');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les inattendus de Malbrouck, festival des arts du cirque et de la rue de Manderen','Cirque et Arts de la rue','https://www.lorraineaucoeur.com/evt-1343/les-inattendus-de-malbrouck-arts-du-cirque/moselle-manderen/fete-animation','Manderen','','','57480','49.4535376683','6.44989622561');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vocafolies','Pluridisciplinaire Musique','https://fr-fr.facebook.com/vocafolies/','AIRE SUR ADOUR','','','40800','43.6983525736','-0.265145513574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE MUSIQUE D''HYERES','Musiques classiques','www.festivalhyeres.org','HYERES','','','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival citoyen Ecran Vert','Cin�ma et audiovisuel','https://festivalecranvert.fr/','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Photaumnales de Beauvais','Arts plastiques et visuels','https://www.photaumnales.fr/','BEAUVAIS','','','60000','49.4365523321','2.08616123661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Via Aeterna','Musiques classiques','www.via-aeterna.com','Le Mont St Michel','','','50170','48.6222106276','-1.53309065436');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FESTIN DE PIERRES','Cirque et Arts de la rue','www.festindepierres.com','ST JEAN DE VEDAS','','','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ACCORDEON PLURIEL','Pluridisciplinaire Musique','www.accordeonpluriel.fr','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MV','Transdisciplinaire','www.festivalmv.com/','DIJON','08/05/2019','12/05/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA VOIX EST LIBRE JAZZ NOMADES','Musiques actuelles','www.jazznomades.net','PARIS','24/05/2019','25/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musique en Omois','Musiques actuelles','http://www.musique-en-omois.com','CHATEAU THIERRY','28/06/2019','02/08/2019','2400','49.0564070801','3.38160004843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nuits de Nacre','Musiques actuelles','www.nuitsdenacre.com','TULLE','27/06/2019','30/06/2019','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film d''animation d''Annecy','Cin�ma et audiovisuel','www.annecy.org','ANNECY','10/06/2019','15/06/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'M''IMPROVISE','Musiques actuelles','https://mimprovise.com','Etampes','13/06/2019','16/06/2019','91150','48.4211282308','2.13838881712');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'IMAGES SINGULIERES','Arts plastiques et visuels','www.imagesingulieres.com','SETE','29/05/2019','16/06/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL RALLY OF CULTURE','Musiques actuelles','https://www.rallyofculture.fr/','LYON','31/05/2019','06/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VERJUX SAONE SYSTEM','Musiques actuelles','http://www.verjuxsaonesystem.com/','VERJUX','15/06/2019','15/06/2019','71590','46.877805287','4.97685538795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A l�Ouest','Cirque et Arts de la rue','http://lablaiserie.centres-sociaux.fr/festivites-programmation/festival-de-rue-a-louest/','POITIERS','14/06/2019','16/06/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ELYZIKS','Musiques actuelles','','ST Quentin','15/06/2019','15/06/2019','2100','49.8472336321','3.27769499462');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lyon BD festival','Livre et litt�rature','https://www.lyonbd.com/','LYON','08/06/2019','09/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FLANERIES MUSICALES DE REIMS','Musiques actuelles','http://www.flaneriesreims.com','REIMS','19/06/2019','11/07/2019','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS FLAMENCAS DE CHATEAUVALLON','Musiques actuelles','http://www.chateauvallon.com','Chateauvallon','22/06/2019','23/06/2019','83190','43.1386133658','5.85369204547');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES EMPREINTES SONORES','Pluridisciplinaire Musique','www.terresdempreintes.com','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BREIZH FOLIES','Musiques actuelles','www.gwenmenez.com/breizhfolies','Guemene Penfao','','','44290','47.6278891238','-1.83077595154');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BALAD''A ZIC','Musiques actuelles','http://www.aquiletour95.com','Ecouen','','','95440','49.0260643173','2.38485695599');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international des �crits de femmes','Livre et litt�rature','http://ecritsdefemmes.fr/','St  SAUVEUR EN PUISAYE','','','89520','47.6390792978','3.20437842755');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DECIBELS VENDANGES','Musiques actuelles','www.fetedesvendangesdemontmartre.com/evenement/decibels-vendanges','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cirko Galop','Cirque et Arts de la rue','https://www.facebook.com/events/687941388213851/','MUIZON','','','51140','49.2724383199','3.88749263388');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZCONTREBAND','Musiques actuelles','www.jazzcontreband.com','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFRIK'' AU COEUR','Transdisciplinaire','https://www.afrikaucoeur.fr/home','AUXERRE','','','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cirque et Cimes','Cirque et Arts de la rue','','Lus La Croix Haute','','','26620','44.6828031377','5.72316122016');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin� 32 Ind�pendance (s) et cr�ation','Cin�ma et audiovisuel','http://www.cine32.com/','AUCH','','','32000','43.6534300414','0.575190250459');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Bulles Sonores','Musiques actuelles','www.lesbullessonores.com','LIMOUX','','','11300','43.0499293279','2.23958811407');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL GRENAD''IN','Musiques actuelles','https://fr-fr.facebook.com/grenadin.schoolfest/','GRENADE','','','31330','43.7640507757','1.28038974975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES INTERNATIONALES DE LA GUITARE','Musiques actuelles','www.internationalesdelaguitare.com','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS ROMANTIQUES DU LAC DU BOURGET','Musiques classiques','www.nuitsromantiques.com','AIX LES BAINS','','','73100','45.6978541675','5.90388626955');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te des vendanges','Cirque et Arts de la rue','http://bagneux92.fr','BAGNEUX','','','92220','48.7983229866','2.30989995212');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival europ�en du film court de Brest','Cin�ma et audiovisuel','www.filmcourt.fr','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU POLAR','Livre et litt�rature','http://www.polar-villeneuvelezavignon.fr/','VILLENEUVE LEZ AVIGNON','','','30400','43.9771673582','4.79466040665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZDOR','Musiques actuelles','http://www.jazzdor.com/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECLATS D''EMAIL','Musiques actuelles','www.eclatsdemail.com','LIMOGES','','','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TECHNO STORY','Musiques actuelles','lautrecanalnancy.fr/TECHNO-STORY-7-DERRICK-MAY-TOXIC-OLY-MC-LRMCH','NANCY','18/01/2019','19/01/2019','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bruit de la musique','Pluridisciplinaire Musique','http://www.lebruitdelamusique.org/','ST Silvain sous Toulx','22/08/2019','24/08/2019','23140','46.2595513591','2.18852889263');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU CHATEAU A CLERMONT EN GENEVOIS','Musiques actuelles','http://www.jazzclubannecy.com','CLERMONT EN GENEVOIS','23/08/2019','25/08/2019','74270','45.9734444894','5.90937510902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WATTS A BAR','Musiques actuelles','www.wattsabar.fr','BAR LE DUC','30/08/2019','31/08/2019','55000','48.7642280465','5.16346492169');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Lisle Noir','Livre et litt�rature','www.polarsurgaronne.fr','Lisle sur Tarn','20/09/2019','22/09/2019','81310','43.8974980421','1.78200143861');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale de la Photographie de Mulhouse','Arts plastiques et visuels','http://www.biennale-photo-mulhouse.com/2018/','MULHOUSE','','','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOUN''ESTIVAL','Cirque et Arts de la rue','www.leplancherdeschevres.fr','TOULON','','','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'March� de la po�sie','Livre et litt�rature','http://www.marche-poesie.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOP IN HUMOUR','Divers Spectacle vivant','http://www.topinhumour.fr','CHARTRES','08/11/2019','24/11/2019','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'March� de la po�sie jeunesse de Tinqueux','Livre et litt�rature','https://www.danslalune.org/','Tinqueux','','','51430','49.2496666793','3.98795391117');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Cirque Actuel � CIRCa','Cirque et Arts de la rue','http://www.circa.auch.fr/index.php/fr/','AUCH','18/10/2019','27/10/2019','32000','43.6534300414','0.575190250459');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon Aller-retour dans le noir','Livre et litt�rature','https://www.unallerretourdanslenoir.com/','PAU','05/10/2019','06/10/2019','64000','43.3200189773','-0.350337918181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lire en poche','Livre et litt�rature','www.lireenpoche.fr','Gradignan','11/10/2019','13/10/2019','33170','44.7709411201','-0.616137682116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRAVER''C� MUSICALES','Musiques actuelles','www.ville-lespontsdece.fr/','LES PONTS DE CE','','','49130','47.4272536106','-0.512677371617');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIMU','Musiques actuelles','http://www.fimu.com','BELFORT','','','90000','47.6471539414','6.84541206237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES TRAVERS�ES, RENCONTRES MUSICALES DE NOIRLAC','Musiques classiques','www.abbayedenoirlac.fr','BRUERE ALLICHAMPS','','','18200','46.7757107356','2.42892835888');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Courants d�arts','Pluridisciplinaire Spectacle vivant','www.lescourants.com','Gentilly','','','94250','48.8132044389','2.34420659702');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEU AU LAC','Musiques actuelles','www.festival-lefeuaulac.fr','VERN SUR SEICHE','','','35770','48.0525245014','-1.60521106164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival la Caravane des Caravanes','Pluridisciplinaire Spectacle vivant','','MELUN','','','77000','48.5444723553','2.65795821917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Constellation Imaginaire','Pluridisciplinaire Spectacle vivant','','ARRAS','','','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RHIZOMES','Musiques actuelles','www.festivalrhizomes.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ESTIVALES DE PONT A MOUSSON','Musiques actuelles','http://www.ville-pont-a-mousson.fr/','PONT A MOUSSON','','','54700','48.9219828048','6.05392690748');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA DEFERLANTE D''ETE','Pluridisciplinaire Spectacle vivant','www.ladeferlante.com','LA ROCHE SUR YON','','','85000','46.6675261644','-1.4077954093');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�T� DU TEMPS DU TANGO','Musiques actuelles','www.letempsdutango.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURE BAR BARS','Musiques actuelles','http://festival.bar-bars.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film d''histoire de Pessac','Domaines divers','https://www.cinema-histoire-pessac.com/','PESSAC','','','33600','44.7915990521','-0.676303166277');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZINCS CHANTENT','Musiques actuelles','http://www.smac07.com','ANNONAY','','','7100','45.2460902392','4.65026947295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONNEXIONS FESTIVAL','Transdisciplinaire','https://superforma.fr/festival-connexions','LE MANS','01/04/2019','30/04/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l''Astre','Th��tre','http://cie-astre.com/','PARIS','29/03/2019','26/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK THE PISTES','Musiques actuelles','http://www.rockthepistes.com','CHATEL','17/03/2019','23/03/2019','74390','46.2490325973','6.81712790779');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARCKOLSWING','Musiques actuelles','www.marckolswing.fr','MARCKOLSHEIM','21/03/2019','23/03/2019','67390','48.1496222393','7.56101805973');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BABEL MINOTS','Musiques actuelles','http://www.villesdesmusiquesdumonde.com','MARSEILLE','12/03/2019','23/03/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vid�oformes � festival international d�arts num�riques de ClermontFerrand','Transdisciplinaire','https://videoformes.com/','CLERMONT FERRAND','14/03/2019','17/03/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la bande dessin�e d''Alpe d''Huez','Livre et litt�rature','www.isere-tourisme.com','HUEZ','12/04/2019','14/04/2019','38750','45.0969425761','6.08474810987');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LARSENIK FESTIVAL','Musiques actuelles','http://ville-labourboule.com/sortir/lagenda-des-animations/event/1782-larsenik-festival.html','LA BOURBOULE','25/02/2019','01/03/2019','63150','45.5796236201','2.7501805247');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE MANS POP FESTIVAL','Musiques actuelles','http://www.lemanscitechanson.com','LE MANS','02/03/2019','07/04/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le livre � Metz - Festival litt�rature et journalisme','Livre et litt�rature','http://www.lelivreametz.com/','METZ','05/04/2019','07/04/2019','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'les 48h de la BD','Livre et litt�rature','http://www.48hbd.com/','PARIS','05/04/2019','06/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOUVELLE(S) SCENE(S)','Musiques actuelles','www.nouvelles-scenes.com','NIORT','21/03/2019','30/03/2019','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DU RIRE - LE FESTIVAL D''HUMOUR DE TOULOUSE','Divers Spectacle vivant','www.printempsdurire.com','TOULOUSE','22/03/2019','21/04/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Quais du polar','Livre et litt�rature','www.quaisdupolar.com','LYON','29/03/2019','31/03/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vendanges graphiques','Livre et litt�rature','https://fr-fr.facebook.com/pg/Vendanges-graphiques-296551497126116/posts/','Condrieu','23/03/2019','24/03/2019','69420','45.4743083818','4.75671075164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Avec ou sans fils','Divers Spectacle vivant','','VENDOME','29/01/2019','09/02/2019','41100','47.8013026692','1.06106057175');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival ICI&LA','Danse','http://laplacedeladanse.com/icietla2019','TOULOUSE','22/01/2019','09/02/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PALMIPEDE','Musiques actuelles','http://www.mjcpalaiseau.com','PALAISEAU','19/04/2019','20/04/2019','91120','48.7146765876','2.22881488083');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE GRAND BAZAR A VAY','Musiques actuelles','https://d.facebook.com/Festival-Le-Grand-Bazar-668336236541973/','Vay','19/04/2019','21/04/2019','44170','47.5453431813','-1.70810408082');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LITTLE WEEK END','Musiques actuelles','https://little-festival.fr','Seignosse','19/04/2019','20/04/2019','40510','43.7021964029','-1.3970873051');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de N�oules','Musiques actuelles','https://festival-de-neoules.fr','NEOULES','18/07/2019','20/07/2019','83136','43.2945310996','6.02592739077');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NIGHT''N BULLES','Transdisciplinaire','http://www.gratienmeyer.com','SAUMUR','19/07/2019','30/08/2019','49400','47.2673853525','-0.0830410591988');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL RADIO FRANCE OCCITANIE � MONTPELLIER','Musiques classiques','http://lefestival.eu','MONTPELLIER','10/07/2019','26/07/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Villeneuve en Sc�ne','Th��tre','http://www.festivalvilleneuveenscene.com/','VILLENEUVE LEZ AVIGNON','09/07/2019','21/07/2019','30400','43.9771673582','4.79466040665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GRANDS CRUS MUSICAUX','Musiques classiques','','BORDEAUX','09/07/2019','25/07/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� la crois�e des chap''','Pluridisciplinaire Spectacle vivant','https://alacroiseedeschap.wordpress.com','ST Symphorien sur Coise','12/07/2019','13/07/2019','69590','45.6308546357','4.45042661825');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'M(ART)DIS DE RIVIER ALP','Musiques actuelles','https://www.instinctaf.net','Les Echelles','15/07/2019','15/08/2019','73360','45.4514870099','5.74305650824');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A L''HOSPITALET','Musiques actuelles','http://www.chateau-hospitalet.com','NARBONNE','24/07/2019','28/07/2019','11100','43.1652399898','3.02023868739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LEZ''ART FESTIVAL A LAGUEPIE','Musiques actuelles','http://lezart.org','Laguepie','25/07/2019','27/07/2019','82250','44.163304209','1.95842635186');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK N HORSES','Musiques actuelles','http://lajumentverte.com','Courlans','31/07/2019','04/08/2019','39570','46.6757012909','5.48680231929');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIANISSIMO','Musiques actuelles','https://www.sunset-sunside.com/','PARIS','29/07/2019','31/08/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DE L''ORANGERIE','Transdisciplinaire','www.les-nuits-de-lorangerie.fr','MONTELEGER','04/07/2019','05/07/2019','26760','44.8558469973','4.92784684928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la correspondance Grignan','Livre et litt�rature','http://www.grignan-festivalcorrespondance.com/','GRIGNAN','02/07/2019','06/07/2019','26230','44.4369175751','4.90180143483');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHARLIE JAZZ FESTIVAL','Musiques actuelles','http://charliejazzfestival.com','VITROLLES','05/07/2019','07/07/2019','13127','43.4497831674','5.26357787665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAP FERRET MUSIC FESTIVAL','Musiques classiques','www.ferretfestival.com','CAP FERRET','06/07/2019','13/07/2019','33950','44.7609440223','-1.19293697437');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chants d�elles','Musiques actuelles','www.festivalchantsdelles.org','ROUEN','','','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de BD de Colomiers','Livre et litt�rature','http://www.ville-colomiers.fr','Colomiers','','','31770','43.611551508','1.32700218407');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'I love techno Europe','Musiques actuelles','http://www.ilovetechnoeurope.com','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Petites formes','Th��tre','http://tropiques-atrium.fr/','FORT DE FRANCE','15/01/2019','27/01/2019','97234','14.6411114389','-61.0691889244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL IMPRESSIONS VISUELLES ET SONORES','Musiques actuelles','www.impressionsvisuellesetsonores.fr','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Micht�','Transdisciplinaire','http://festivalmichto.weebly.com/','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des cin�mas diff�rents et exp�rimentaux de Paris','Cin�ma et audiovisuel','http://www.cjcinema.org/pages/festival_edition.php','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HILLBILLY BOP','Musiques actuelles','www.festival-hillbilly-bop.com','Rouziers de Touraine','','','37360','47.5311263676','0.647449146554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MaMA','Musiques actuelles','www.mamafestival.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN DOUA DE JAZZ','Musiques actuelles','http://www.undouadejazz.com','VILLEURBANNE','','','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Chacun son court','Cin�ma et audiovisuel','http://chacunsoncourt.eu/fr/accueil/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Second Geste','Cirque et Arts de la rue','https://www.saintpairsurmer.fr/','ST Pair sur Mer','','','50380','48.8023738223','-1.54696599127');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE DE VERVINS','Divers Spectacle vivant','http://www.divan-production.com','VERVINS','','','2140','49.8351798708','3.92522602825');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Film Politique de Porto-Vecchio','Cin�ma et audiovisuel','http://www.festivaldufilmpolitique.com/','Porto Vecchio','','','20137','41.5924932873','9.25363087024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOUVELLES VOIX EN BEAUJOLAIS','Musiques actuelles','http://www.theatredevillefranche.com','VILLEFRANCHE SUR SAONE','','','69400','45.9874596667','4.73174007305');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DEMAIN C''EST BIEN','Musiques actuelles','http://mixarts.org/association/','ST MARTIN D HERES','','','38400','45.1762012553','5.76476360737');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA GUITARE A ISSOUDUN','Musiques actuelles','www.issoudun-guitare.com','ISSOUDUN','','','36100','46.9489390293','2.00053790133');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Annecy Cin�ma italien','Cin�ma et audiovisuel','www.annecycinemaitalien.com','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Kalypso','Danse','https://ccncreteil.com/kalypso/presentation/festival-kalypso','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SOUS LES BIGARADIERS','Musiques actuelles','assowhat.free.fr','LA GAUDE','','','6610','43.721423765','7.16039926444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NEXT','Danse','www.nextfestival.eu','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTETANGO','Pluridisciplinaire Spectacle vivant','www.artetango-festival.com','ALBI','','','81000','43.9258213622','2.14686328555');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SAINT SULPICE DE RIRE','Divers Spectacle vivant','http://www.stsulpicederire.fr/','St Sulpice La Pointe','','','81370','43.7589885163','1.68659997767');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cirque en Marche','Cirque et Arts de la rue','https://www.polecirqueverrerie.com/','ALES','','','30100','44.1250099126','4.08828501262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HELL''OWEEN FESTIVAL','Musiques actuelles','http://www.helloweenfestival.com','SAINTES','','','17100','45.7422300162','-0.649623035643');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de La Basse Cour','Cirque et Arts de la rue','www.labassecour.com','COLLIAS','','','30210','43.9490522277','4.47176606244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festiv''art','Musiques actuelles','www.myfestivart.com','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BANC PUBLIC','Pluridisciplinaire Spectacle vivant','https://www.bancpublic.org','ST BRIEUC','','','22000','48.5149806053','-2.76154552773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA RUE EST A MOY','Cirque et Arts de la rue','http://www.aveyron-culture.com/diffusio/contact/moyrazes/anim-a-moy_TFO118397.php','MOYRAZES','','','12160','44.3275804769','2.42736716891');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film insulaire','Cin�ma et audiovisuel','www.filminsulaire.com','GROIX','','','56590','47.6372394221','-3.4644268016');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jours de f�te','Pluridisciplinaire Spectacle vivant','www.theatreonyx.fr/','ST HERBLAIN','','','44800','47.2243762412','-1.63434818692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MANIFESTO','Arts plastiques et visuels','www.festival-manifesto.org','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL 100% CLASSIQUE','Musiques classiques','www.festival-centpourcent-classique.fr','LA GRANDE MOTTE','','','34280','43.5685394142','4.0758328467');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELEKTRIC PARK FESTIVAL','Musiques actuelles','https://www.elektricpark.com','CHATOU','','','78400','48.8965574645','2.15393458329');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MIMI','Musiques actuelles','www.amicentre.biz','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS INTERNATIONAL FESTIVAL OF PSYCHEDELIC MUSIC/ PARIS PSYCH FEST','Musiques actuelles','www.parispsychfest.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CROQUEURS DE PAVE','Cirque et Arts de la rue','http://www.agoradevesines.com','Chalette sur Loing','','','45120','48.0188121595','2.73578852299');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TEHRAN UNDERGROUND MUSIC FESTIVAL IN PARIS','Musiques actuelles','www.cafekhayyam.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BIG UP 974','Musiques actuelles','http://www.bigup974.re/','ST DENIS','','','97400','-20.9329708192','55.446867167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BEG CHOPIN','Musiques actuelles','www.begchopin.fr','Treguier','','','22220','48.7850465816','-3.23178776538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Th��tres d�Outre-Mer en Avignon au Festival d''Avignon','Th��tre','http://www.verbeincarne.fr/fr/','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WATT THE FUNK','Musiques actuelles','https://federationuniversellefunk.wordpress.com/festival/','Besseges','','','30160','44.290395291','4.10784015546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BRAVOS DE LA NUIT','Th��tre','http://lesbravosdelanuit.fr/wordpress/','Pelussin','','','42410','45.4230806352','4.65991656851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Newave Festival','Musiques actuelles','www.newavefestival.com','Seignosse','','','40510','43.7021964029','-1.3970873051');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musiciennes � Ouessant','Musiques classiques','http://www.musiciennesaouessant.com/','OUESSANT','','','29242','48.4601042319','-5.08534533544');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Cin�ma de Douarnenez Gouel Ar Filmou','Cin�ma et audiovisuel','www.festival-douarnenez.com','DOUARNENEZ','','','29100','48.0806526556','-4.32784220122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ao�t of jazz','Musiques actuelles','','Capbreton','','','40130','43.6323255498','-1.42907243956');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AFFOUAGES','Musiques actuelles','www.lesaffouages.fr','ST AMAND MONTROND','','','18200','46.726171431','2.52013626979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FestiLoir','Pluridisciplinaire Spectacle vivant','www.pays-valleeduloir.fr/','LE MANS','','','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Contre Courant','Pluridisciplinaire Spectacle vivant','','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUR LA ROUTE DES ORGUES EN PAYS DE SAINT MALO','Musiques classiques','www.laroutedesorgues.weebly.com','Cancale','11/05/2019','02/06/2019','35260','48.6835209896','-1.8650827351');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sunshine Reggae Festival','Musiques actuelles','http://www.sunshinereggaefestival.com','Lauterbourg','17/05/2019','18/05/2019','67630','48.9637116345','8.18637725776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Leu Tempo Festival','Cirque et Arts de la rue','www.lesechoir.com','ST LEU','15/05/2019','18/05/2019','97424','-21.1665966424','55.3331335776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DANSES ET MUSIQUES DU MONDE DE MONTATAIRE','Musiques actuelles','www.mairie-montataire.fr/','MONTATAIRE','29/05/2019','30/05/2019','60160','49.2632645948','2.43135724334');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRAD''ENVIE','Musiques actuelles','www.tradenvie.fr','PAVIE','29/05/2019','01/06/2019','32550','43.605614575','0.589850870884');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE BON AIR','Musiques actuelles','www.bi-pole.org','MARSEILLE','24/05/2019','26/05/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de jeunesse et de BD de Cherbourg','Livre et litt�rature','http://festivaldulivre.com/','CHERBOURG EN COTENTIN','23/05/2019','26/05/2019','50100','49.633412156','-1.63390160204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAGIC BUS','Musiques actuelles','www.retourdescene.net','GRENOBLE','23/05/2019','25/05/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRESSTIVAL','Musiques actuelles','','BLETTERANS','25/05/2019','25/05/2019','39140','46.7316211339','5.42781411933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAPITOSCOPE','Cirque et Arts de la rue','http://www.festivalchapitoscope.com','CREON','22/05/2019','26/05/2019','33670','44.770425384','-0.343606134456');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''LAV','Musiques actuelles','','Lavernay','24/05/2019','25/05/2019','25170','47.2443013496','5.81882619384');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE DANSE JAZZ','Danse','http://www.theatredesgrandsenfants.com/theatre/festival-de-danse-jazz','CUGNAUX','25/05/2019','26/05/2019','31270','43.5449211242','1.34286223759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES COUCHETARDS','Musiques actuelles','http://lescouchetards.e-monsite.com/','ST Joachim','01/06/2019','01/06/2019','44720','47.3721622371','-2.24296601109');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Marsatac','Musiques actuelles','https://www.marsatac.com/','MARSEILLE','14/06/2019','16/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon Saint-Maur en poche','Livre et litt�rature','www.saintmaurenpoche.com','ST MAUR DES FOSSES','15/06/2019','16/06/2019','94100','48.7990677797','2.49386450452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE MAS DES ESCARAVATIERS','Musiques actuelles','www.lemas-concert.com','PUGET SUR ARGENS','01/06/2019','10/07/2019','83480','43.4717671252','6.68628195857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Nuits de Fourvi�re','Pluridisciplinaire Spectacle vivant','https://www.nuitsdefourviere.com/','LYON','01/06/2019','30/07/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CLARTES MUSICALES','Musiques actuelles','http://https//www.facebook.com/events/304051623596296/','ST Paterne Racan','31/05/2019','01/06/2019','37370','47.5816905056','0.466637692187');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FLORILEGE VOCAL DE TOURS','Musiques classiques','','TOURS','31/05/2019','02/06/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIBRA M�MES','Musiques actuelles','','SEES','01/06/2019','08/06/2019','61500','48.6048296774','0.169026415284');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WADADA FESTIVAL','Musiques actuelles','www.wadada.fr','Louvigne du Desert','08/06/2019','09/06/2019','35420','48.4868885908','-1.12369660788');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRE DE BLUES','Musiques actuelles','www.terredeblues.com','GRAND BOURG','07/06/2019','10/06/2019','97112','15.9060600751','-61.2943821122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ZACCROS D''MA RUE','Cirque et Arts de la rue','http://www.zaccros.org','NEVERS','01/07/2019','07/07/2019','58000','46.9881194908','3.15689130958');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RENCONTRES TRANS MUSICALES DE RENNES','Musiques actuelles','www.lestrans.com','RENNES','04/12/2019','08/12/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lettres du monde','Livre et litt�rature','https://lettresdumonde33.com/','BORDEAUX','15/11/2019','24/11/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival M�me pas peur','Cin�ma et audiovisuel','http://www.festivalmemepaspeur.com/','ST Philippe','','','97442','-21.3040005374','55.7454668204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de BD de Solli�s-Ville','Livre et litt�rature','www.festivalbd.com','Sollies Ville','23/08/2019','25/08/2019','83210','43.1784166306','6.01726722545');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Toulouse, polars du sud','Livre et litt�rature','http://www.toulouse-polars-du-sud.com/','TOULOUSE','11/10/2019','13/10/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Livres dans la boucle','Transdisciplinaire','www.livresdanslaboucle.fr','BESANCON','20/09/2019','22/09/2019','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES OREILLES DU RENARD','Musiques actuelles','www.lesoreillesdurenard.net','ST Sauveur de Montagut','','','7190','44.8202271347','4.57255179328');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du court m�trage de Bourg-en-Bresse','Cin�ma et audiovisuel','http://www.festivals-connexion.com/','BOURG EN BRESSE','','','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMERICAN JOURNEYS','Musiques actuelles','www.american-journeys.com','CAMBRAI','','','59400','50.1702775977','3.24221021072');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lire sur la vague','Livre et litt�rature','http://www.hossegor.fr/fr/','Soorts Hossegor','','','40150','43.6675688674','-1.41245896656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival C�t� Court','Cin�ma et audiovisuel','www.festival-cotedopale.fr','PANTIN','','','93500','48.8983093876','2.40872147475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Photo de La Gacilly','Arts plastiques et visuels','https://www.festivalphoto-lagacilly.com/','LA GACILLY','','','56200','47.7728317091','-2.15651114837');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TILT FESTIVAL','Musiques actuelles','www.tilt-festival.org','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A L''ART RUE','Pluridisciplinaire Spectacle vivant','www.alartlibre.com','LA PREVIERE','','','49520','47.7629344972','-1.19300189799');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Chemins de Traverse','Pluridisciplinaire Spectacle vivant','','Noisy le Grand','','','93160','48.8361825401','2.56443736814');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESCALES D''AILLEURS','Pluridisciplinaire Spectacle vivant','http://lessentiel-plaisir.fr/','PLAISIR','','','78370','48.8126082296','1.94698515715');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POLYMUSICALES DE BOLLENE','Musiques actuelles','http://www.ville-bollene.fr','BOLLENE','','','84500','44.2882016409','4.75235956257');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CLAYESCIBELS','Musiques actuelles','http://www.lesclayescibels.com','LES CLAYES SOUS BOIS','','','78340','48.8186406601','1.9840595554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALTITUDE JAZZ FESTIVAL','Musiques actuelles','www.altitudejazz.com','BRIANCON','19/01/2019','02/02/2019','5100','44.8994986041','6.64947524018');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DU BLEU EN HIVER - JAZZ(S) EN T�TE','Musiques actuelles','http://dubleuenhiver.fr/','TULLE','24/01/2019','02/02/2019','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du polar de St Laurent du Var','Livre et litt�rature','https://www.saintlaurentduvar.fr/festival-du-polar','ST LAURENT DU VAR','27/04/2019','28/04/2019','6700','43.6859625772','7.18218791815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES JOYEUSERIES','Pluridisciplinaire Spectacle vivant','www.lesjoyeuseries.com','La Chauss�e Saint-Victor','03/05/2019','05/05/2019','41260','47.6111559006','1.35763609326');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICORA','Pluridisciplinaire Musique','http://musicora.com','BOULOGNE BILLANCOURT','03/05/2019','05/05/2019','92100','48.8365843138','2.23913599058');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAP Toulouse','Arts plastiques et visuels','https://map-photo.fr/','TOULOUSE','03/05/2019','19/05/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARLY JAZZ FESTIVAL','Musiques actuelles','www.marlyjazzfestival.com','MARLY','06/05/2019','09/05/2019','57155','49.0658509672','6.15231485365');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Grain de sel','Musiques actuelles','http://www.festivalgraindesel.com','Castelsarrasin','17/05/2019','19/05/2019','82100','44.0490326525','1.12348123121');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NAME FESTIVAL','Pluridisciplinaire Spectacle vivant','www.lenamefestival.com','ROUBAIX','25/05/2019','25/05/2019','59100','50.6879774811','3.18258434623');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ASCENSION DU SON','Musiques actuelles','www.ascensionduson.com','Plemy','29/05/2019','30/05/2019','22150','48.3378874008','-2.67639170288');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LAPLAGE A GLAZART','Musiques actuelles','www.glazart.com','PARIS','31/05/2019','01/08/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOULEGAN - FESTIVAL DE LA SAINT JEAN','Musiques actuelles','https://www.boulegan.fr/','MARSEILLE','05/06/2019','09/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CARAVANSERAIL','Musiques actuelles','http://festival-caravanserail.com','MARSEILLE','07/06/2019','08/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KILOWATT DUB FEST','Musiques actuelles','http://lekilowatt.fr/','IVRY SUR SEINE','15/06/2019','15/06/2019','94200','48.8128063812','2.38778023612');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONCERTS DU PALAIS ID�AL','Musiques actuelles','www.facteurcheval.com','HAUTERIVES','21/06/2019','04/07/2019','26390','45.2531318961','5.03890051162');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VILLE EN MUSIQUES','Musiques actuelles','www.maisonduboulanger.com','TROYES','22/06/2019','04/08/2019','10000','48.2967099637','4.07827967525');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SAINT MED ART ROCK','Musiques actuelles','https://www.stmedartrock.com/','ST Medard d Aunis','22/06/2019','22/06/2019','17220','46.1592023268','-0.967137851085');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARD�CHE ALUNA FESTIVAL','Musiques actuelles','www.aluna-festival.fr','RUOMS','27/06/2019','29/06/2019','7120','44.4479030757','4.34786189328');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HAUTES TERRES','Musiques actuelles','www.festivalhautesterres.fr','ST FLOUR','28/06/2019','30/06/2019','15100','45.0278919825','3.08320563232');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cabourg mon Amour','Musiques actuelles','http://cabourgmonamour.fr/','Cabourg','28/06/2019','30/06/2019','14390','49.2834863414','-0.125512945279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE SIRK','Musiques actuelles','http://www.riskparty.com','CHENOVE','02/04/2019','30/04/2019','21300','47.2926136721','5.00488353457');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hors limites','Livre et litt�rature','http://www.hors-limites.fr/','ST DENIS','23/03/2019','13/04/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HORS PISTES','Musiques actuelles','www.festivalhorspistes.com','ANNECY','13/03/2019','17/03/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS MUSIC','Musiques actuelles','www.paris-music.com','PARIS','14/03/2019','16/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�ma du r�el','Cin�ma et audiovisuel','http://www.cinemadureel.org/fr','PARIS','15/03/2019','24/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHOPIN � PARIS','Musiques classiques','www.frederic-chopin.com','PARIS','20/03/2019','23/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRON RVB''N','Transdisciplinaire','https://rvbn.fr/2019/','BRON','04/04/2019','11/04/2019','69500','45.7344856902','4.91168159471');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TENDANCES JAZZ','Musiques actuelles','www.festival-cotedopale.fr','BOULOGNE SUR MER','12/03/2019','16/03/2019','62200','50.7271332663','1.60756334802');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Francofolies de La R�union','Musiques actuelles','www.francofolies.re','ST PIERRE','08/03/2019','10/03/2019','97410','-21.3123242427','55.4936155164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK THIS TOWN','Musiques actuelles','www.lemelies.net','PAU','27/04/2019','05/05/2019','64000','43.3200189773','-0.350337918181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VENDANGES MUSICALES','Musiques actuelles','www.lesvendangesmusicales.fr','CHARNAY','','','69380','45.891187551','4.66063186811');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Printemps de septembre','Transdisciplinaire','www.printempsdeseptembre.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIENS CHERCHER BONHEUR','Musiques actuelles','www.vienschercherbonheur.com','MEIGNE LE VICOMTE','','','49490','47.5125846986','0.188043785469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DESOBEISSANT AU THEATRE DES DEUX ANES','Divers Spectacle vivant','www.2anes.com/festivaldesobeissant','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des francophonies en Limousin','Pluridisciplinaire Spectacle vivant','www.lesfrancophonies.com','LIMOGES','','','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ METIS A MONTREUIL','Musiques actuelles','https://festivaljazzmetis.wordpress.com','MONTREUIL','','','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTOMNE MUSICAL DE NIMES','Musiques classiques','www.automne-musical-nimes.org','NIMES','','','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EL CEP','Musiques actuelles','','LA GRIGONNAIS','','','44170','47.5178441484','-1.67992818531');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELLIPSE FESTIVAL','Musiques actuelles','https://ellipsefestival.fr/','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Art''Pantin Forum R�gional de la Marionnette','Divers Spectacle vivant','https://arema-lr.fr/','VERGEZE','','','30310','43.7375360363','4.23360990528');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES DES DEUX RIVIERES','Musiques actuelles','www.festival-blues-bretagne.fr','BELLE ISLE EN TERRE','','','22810','48.5310285208','-3.38188859591');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU COURT-M�TRAGE DE SAINT-MAUR-DES-FOSS�S','Cin�ma et audiovisuel','http://www.saintmaur-court-metrage.com/','ST MAUR DES FOSSES','','','94100','48.7990677797','2.49386450452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SURREALIZM','Pluridisciplinaire Spectacle vivant','https://fr-fr.facebook.com/Surrealizmfestival/','CARCASSONNE','','','11000','43.2093798444','2.34398855385');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SALON DE LA GUITARE A COLMAR','Musiques actuelles','https://www.guitarmaniaks.org/','COLMAR','','','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ECLATS DE VOIX EN BOCAGE BRESSUIRAIS','Musiques actuelles','www.voix-danses.fr','BRESSUIRE','','','79300','46.8545755683','-0.479375922286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE GUITARE DE PUTEAUX','Musiques actuelles','http://www.puteaux.fr/Agenda/Festival-Guitare-2018','PUTEAUX','','','92800','48.8837094969','2.23834178314');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Echos d''ici, Echos d''ailleurs','Cin�ma et audiovisuel','http://www.echosdudoc.fr/','LABASTIDE ROUAIROUX','','','81270','43.4826602247','2.63557921234');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''Europ�enne de cirques','Cirque et Arts de la rue','http://www.la-grainerie.net','BALMA','','','31130','43.6115258731','1.50459138348');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Nuits de Champagne','Musiques actuelles','www.nuitsdechampagne.com','TROYES','','','10000','48.2967099637','4.07827967525');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A L''HEURE D''ETE','Musiques actuelles','http://www.jazzpourtous.com','ANGERS','','','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AMARINOIS','Musiques actuelles','www.jazz-amarinois.fr','ST AMARIN','','','68550','47.887678929','7.04124424443');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COMIC CON PARIS','Transdisciplinaire','www.comic-con-paris.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PITCHFORK AVANT GARDE','Musiques actuelles','https://pitchforkmusicfestival.fr/pitchfork-avant-garde/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� LES CHOEURS','Musiques actuelles','https://elizabethmydear.fr/','TULLE','','','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Jean Rouch','Cin�ma et audiovisuel','http://www.comitedufilmethnographique.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEMAPHORE EN CHANSON','Musiques actuelles','www.cebazat.fr/','CEBAZAT','','','63118','45.8318909179','3.10802814876');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film d''Amiens','Cin�ma et audiovisuel','http://www.filmfestamiens.org/','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES SUR SEINE','Musiques actuelles','www.blues-sur-seine.com','Mantes La Jolie','','','78200','48.9981665392','1.69337806821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE SOUS LES EMBRUNS','Musiques actuelles','www.portenbessin-huppain.fr','PORT EN BESSIN HUPPAIN','','','14520','49.3396014282','-0.772708434394');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Blues up orchestra','Musiques actuelles','https://fr-fr.facebook.com/amjbeca/','LE MONT DORE','','','98810','-22.205263','166.510694');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�motions de rue','Cirque et Arts de la rue','http://www.mjc-voiron.org/culturel/emotions-de-rue/','VOIRON','','','38500','45.3791720843','5.58240310671');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRANSAT EN VILLE','Transdisciplinaire','www.tourisme-rennes.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz � St Cannat','Musiques actuelles','','ST CANNAT','','','13760','43.6136286667','5.30698257258');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A PORTEE DE RUE','Musiques classiques','www.ville-castres.fr/kiosque/festival-portee-de-rue-programme-2018','CASTRES','','','81100','43.6156511237','2.23787231587');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Envies Rh�nements','Pluridisciplinaire Spectacle vivant','','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK ON THE L''OULE','Musiques actuelles','www.rockontheloule.org','LA MOTTE CHALANCON','','','26470','44.4953213004','5.38478825225');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Bouillie � Sosso','Musiques actuelles','www.labouillieasosso.fr','Cheffois','','','85390','46.6786935635','-0.782949851125');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES CHORALES DE L''OLIVIER','Musiques classiques','www.coazart.com','COARAZE','','','6390','43.8664901786','7.29913349222');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLKOLOR','Danse','www.festivalmontrejeau.fr','MONTREJEAU','','','31210','43.0885884936','0.558463262724');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN PIANO DANS LA PINEDE','Musiques actuelles','http://www.legrandvillageplage.fr','LE GRAND VILLAGE PLAGE','','','17370','45.8620787516','-1.24274513266');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL NATIONAL DES HUMORISTES','Divers Spectacle vivant','www.festivaldeshumoristes.com','Tournon sur Rhone','','','7300','45.0535734386','4.81493474284');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE CUGAND','Musiques actuelles','http://www.festival-cugand.fr/','CUGAND','','','85610','47.0602388146','-1.25289811103');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Malices & Merveilles','Cirque et Arts de la rue','https://culture.beauvais.fr/acteur-culturel/malices-et-merveilles','BEAUVAIS','','','60000','49.4365523321','2.08616123661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URBAN SPACES - RENCONTRES DES CULTURES URBAINES A BAYEUX','Transdisciplinaire','https://www.bayeux.fr/fr/urban-spaces','BAYEUX','','','14400','49.2776559195','-0.704625495378');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECLATS, LE FESTIVAL DE LA VOIX AU PAYS DE DIEULEFIT','Musiques actuelles','https://www.eclats-dieulefit.com/','DIEULEFIT','','','26220','44.5310329912','5.06528375642');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''IMPREVU','Pluridisciplinaire Spectacle vivant','http://www.imprevu-festival.fr/','MONTEMBOEUF','','','16310','45.7738787675','0.551136998408');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZABAR','Musiques actuelles','www.jazzabar.com','BAR SUR AUBE','','','10200','48.2339751025','4.71595700167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Arabesques','Transdisciplinaire','www.festivalarabesques.fr','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA VOIX DANS TOUS LES SENS','Pluridisciplinaire Spectacle vivant','www.asso-ardev.com','SENS','','','89100','48.1956727015','3.29793332552');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RESO''NANTES (ANCIEN ELAN ROCK)','Musiques actuelles','http://shamrock.audencia.com/festival/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES EMBUSCADES','Divers Spectacle vivant','http://www.lesembuscades.fr','COSSE LE VIVIEN','','','53230','47.94585879','-0.934606329089');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SONS D''UNE NUITS D''ETE','Musiques actuelles','http://www.festivalnuits.fr/','NUITS ST GEORGES','02/07/2019','06/07/2019','21700','47.146518392','4.9385500417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ABBAYE DE SAINT-RIQUIER','Musiques classiques','www.ccr-abbaye-saint-riquier.fr','ST RIQUIER','02/07/2019','12/07/2019','80135','50.1279792191','1.94290239471');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE COLMAR','Musiques classiques','www.festival-colmar.com','COLMAR','04/07/2019','14/07/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival R4','Musiques actuelles','www.festivalr4.fr','REVELLES','05/07/2019','07/07/2019','80540','49.8400277548','2.1143709231');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL AVIGNON OFF','Pluridisciplinaire Spectacle vivant','www.avignonleoff.com','AVIGNON','05/07/2019','28/07/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CITADELLE EN BORD�ES','Musiques actuelles','www.citadelleenbordees.fr','DUNKERQUE','05/07/2019','05/07/2019','59430','51.0307229078','2.33752414095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOULSTOCK','Musiques actuelles','https://www.voulstock.com/','VOULANGIS','06/07/2019','06/07/2019','77580','48.8403121801','2.88971628574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ID�KLIC','Pluridisciplinaire Spectacle vivant','www.ideklic.fr','MOIRANS EN MONTAGNE','10/07/2019','13/07/2019','39260','46.4388080084','5.73795329823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT-PAUL SOUL JAZZ FESTIVAL','Musiques actuelles','www.saintpaul-souljazz.com','ST PAUL TROIS CHATEAUX','11/07/2019','14/07/2019','26130','44.3482808478','4.75744934383');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RYTHME EN CEVENNES','Musiques actuelles','www.festivaldurythme.fr/','ST JEAN DU PIN','17/07/2019','20/07/2019','30140','44.1164232741','4.0327398577');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PLURALIES','Pluridisciplinaire Spectacle vivant','www.pluralies.net','LUXEUIL LES BAINS','17/07/2019','20/07/2019','70300','47.823919724','6.36363973342');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GANNAT LES CULTURES DU MONDE','Musiques actuelles','http://www.cultures-traditions.org','GANNAT','19/07/2019','28/07/2019','3800','46.0984192073','3.17790823629');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Heures Musicales de l''Abbaye de Lessay','Musiques classiques','http://www.heuresmusicalesdelessay.com/','LESSAY','19/07/2019','16/08/2019','50430','49.2167143742','-1.52478862014');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHOEURS LAUR�ATS','Musiques classiques','www.festivaldeschoeurslaureats.com','VAISON LA ROMAINE','21/07/2019','24/07/2019','84110','44.247701867','5.06040782309');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU ROCHER','Musiques actuelles','http://www.ville-pierrelatte.fr/83-actualites/1335-festival-du-rocher-2','PIERRELATTE','21/07/2019','23/07/2019','26700','44.3606937732','4.68715113615');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HARDELOT','Musiques classiques','http://www.festival-hardelot.fr/','HARDELOT','22/07/2019','13/08/2019','62152','50.6171328385','1.61729338887');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MARGUERITE','Musiques actuelles','https://www.tousvoisins.fr/','AIGUES MORTES','24/07/2019','28/07/2019','30220','43.5507249635','4.18349802063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MUSICALES DE CORMEILLES EN PAYS D''AUGE','Pluridisciplinaire Musique','www.musicalesdecormeilles.com','CORMEILLES','27/07/2019','17/08/2019','27260','49.2517321161','0.384551732765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARJAC M''EN CHANTE','Musiques actuelles','http://barjacmenchante.org','BARJAC','27/07/2019','01/08/2019','30430','44.3122415211','4.34841481782');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOB MARLEY ONE LOVE CELEBRATION','Musiques actuelles','https://webcache.googleusercontent.com','FRONTIGNAN','16/08/2019','17/08/2019','34110','43.4482458873','3.74923522416');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAPINS BARBUS FESTIVAL','Musiques actuelles','https://www.sapinsbarbus.com','Dommartin les Remiremont','16/08/2019','17/08/2019','88200','47.99107923','6.6482307377');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Quai des bulles','Livre et litt�rature','www.quaidesbulles.com','ST MALO','25/10/2019','27/10/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lettres d''automne','Livre et litt�rature','www.lettresdautomne.org','MONTAUBAN','18/11/2019','01/12/2019','82000','44.0222594578','1.36408636501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Ville aux livres - Salon du livre et de la BD','Livre et litt�rature','http://www.lavilleauxlivres.com/','CREIL','22/11/2019','24/11/2019','60100','49.2533487138','2.48472669359');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SANCY SNOW JAZZ','Musiques actuelles','www.sancy-snowjazz.com','MONT DORE','08/02/2020','15/02/2020','63240','45.5760999131','2.80995918175');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Tandem','Livre et litt�rature','https://www.tandemnevers.fr/','NEVERS','','','58000','46.9881194908','3.15689130958');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ECHANGEUR DE SONS','Musiques actuelles','http://www.lechangeurdesons.com','CHAMBERY','','','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZIKENSTOCK','Musiques actuelles','','Le Cateau Cambresis','','','59360','50.0920093814','3.53922472873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MON MOUTON EST UN LION','Pluridisciplinaire Spectacle vivant','www.mouton-lion.org','SAVERNE','','','67700','48.7402250066','7.34206238199');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INOX FESTIVAL','Musiques actuelles','','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Mois Moli�re','Pluridisciplinaire Spectacle vivant','http://www.moismoliere.com/','VERSAILLES','','','78000','48.8025669671','2.11789297191');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Regards 9-la Bd autrement','Livre et litt�rature','http://www.rgrd9.com/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cirk�Markstein','Cirque et Arts de la rue','http://www.cirkomarkstein.com/','Lautenbach','','','68610','47.9527349295','7.16433010313');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Viva Cit�','Cirque et Arts de la rue','http://www.atelier231.fr/fr/evenements/viva-cite.html','SOTTEVILLE LES ROUEN','','','76300','49.4104887444','1.09509946531');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'North Summer Festival','Musiques actuelles','','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Esclaffades','Cirque et Arts de la rue','http://www.festival-esclaffades.com/','ST Helen','','','22100','48.4661154661','-1.95919641216');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL A LA RUE !','Cirque et Arts de la rue','www.lecarroi.fr','MENETOU SALON','','','18510','47.241732115','2.49021091816');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BELLES RENCONTRES DU JAZZ','Musiques actuelles','www.hotel-imperator.com','NIMES','','','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES ARTS DU CIRQUE DE CUGNAUX (EX FESTIMANOIR)','Cirque et Arts de la rue','http://la-grainerie.net','CUGNAUX','','','31270','43.5449211242','1.34286223759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Z''�clectiques (Automne)','Musiques actuelles','www.leszeclectiques.com','Chemille en Anjou','','','49092','47.3777256876','-0.609937724094');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN AUFFARGIS (automne)','Musiques actuelles','http://www.jazzinauffargis.fr/','LES ESSARTS LE ROI','','','78690','48.716019265','1.8952517006');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DON QUIJOTE','Th��tre','http://www.festivaldonquijote.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHARABIA FESTIVAL','Musiques actuelles','https://www.charabiafestival.com','REIMS','','','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HADRA TRANCE FESTIVAL','Musiques actuelles','http://www.hadra.net','VIEURE','29/08/2019','01/09/2019','3430','46.5001110432','2.88222237454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ososph�re','Transdisciplinaire','https://www.ososphere.org/','STRASBOURG','13/09/2019','22/09/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International de Th��tre de Rue d''Aurillac','Cirque et Arts de la rue','https://www.aurillac.net/index.php/fr/le-festival-d-aurillac/programmation','AURILLAC','21/08/2019','24/08/2019','15000','44.9245233686','2.44162453828');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BOREALES','Transdisciplinaire','http://www.lesboreales.com','CAEN','14/11/2019','24/11/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'S�ries mania','Cin�ma et audiovisuel','https://seriesmania.com/fr','LILLE','20/03/2020','28/03/2020','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Banquet d�automne � Lagrasse','Livre et litt�rature','www.lamaisondubanquet.fr','Lagrasse','01/11/2019','03/11/2019','11220','43.0957230349','2.60436046295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les nuits d''Eole','Cirque et Arts de la rue','http://www.lesnuitsdeole.fr/','Montigny Les Metz','','','57950','49.0953199106','6.15452599506');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FOLIES VOCALES D''AGEN','Musiques actuelles','www.foliesvocales.com','AGEN','','','47000','44.2028139104','0.625583928763');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MUSICALES DE BAGATELLE','Musiques classiques','http://www.lesmusicalesdebagatelle.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DEMANDEZ NOUS LA LUNE','Cirque et Arts de la rue','www.halle-verriere.fr','MEISENTHAL','','','57960','48.9707983496','7.34611209492');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES FAITS D''HIVER de Louviers','Musiques actuelles','https://fr-fr.facebook.com/festivalfaitsdhiver/','LOUVIERS','','','27400','49.2206099164','1.15340030158');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Regard','Arts plastiques et visuels','http://www.festivalduregard.fr/fr/index.html','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film de La Rochelle','Cin�ma et audiovisuel','http://www.festival-larochelle.org','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCKORAMA','Musiques actuelles','www.rockorama.fr','TOULON','','','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CASSEL CORNEMUSES','Musiques actuelles','www.ot-cassel.fr','STE MARIE CAPPEL','','','59670','50.780208006','2.51589636551');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POLY''SONS DE MONTBRISON','Musiques actuelles','http://www.theatredespenitents.fr/','MONTBRISON','11/01/2019','20/02/2019','42600','45.6008365515','4.0713982073');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALORS ON SEME','Divers Spectacle vivant','http://jardinsdurire.com','ANGERS','21/03/2019','23/03/2019','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LI�VIN METAL FEST','Musiques actuelles','https://arcenciel.lievin.fr/lievin-metal-fest-4eme-edition/','LIEVIN','15/03/2019','16/03/2019','62800','50.4242782538','2.77291885384');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAFE DE LA MARINE','Musiques actuelles','www.trianontransatlantique.com','SOTTEVILLE LES ROUEN','01/03/2019','02/04/2019','76300','49.4104887444','1.09509946531');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Art Up !','Arts plastiques et visuels','https://lille.art-up.com/','LILLE','28/02/2019','03/03/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES P''TITS BOUCHONS','Musiques actuelles','www.lesptitsbouchons.net','GAILLAC','04/04/2019','07/04/2019','81600','43.9170968776','1.88647213801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GOSPEL TOUCH','Musiques actuelles','http://www.gospeltouchfestival.com','TOURNEFEUILLE','29/03/2019','31/03/2019','31170','43.5781918597','1.33500697752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FRANCOPHONIDES','Musiques actuelles','https://www.lesfrancophonides.com','VERNAISON','30/03/2019','30/03/2019','69390','45.6493899066','4.80930520738');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAIR TOUR','Musiques actuelles','http://lefair.org','PARIS','10/03/2019','20/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIX DE FEMMES','Musiques actuelles','http://cdc-smc.fr/','ST Martin de Crau','09/03/2019','23/03/2019','13310','43.6119214615','4.85701475567');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A TOUT BOUT DE CHANT','Pluridisciplinaire Spectacle vivant','www.magny-les-hameaux.fr','MAGNY LES HAMEAUX','15/03/2019','07/04/2019','78114','48.7410207273','2.0519793397');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES 3J COMIQUES','Divers Spectacle vivant','http://www.pugetsurargens-tourisme.com','PUGET SUR ARGENS','27/03/2019','29/03/2019','83480','43.4717671252','6.68628195857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CASSEROLES','Musiques actuelles','http://lescasseroles.wix.com','ST Jean De Braye','13/04/2019','13/04/2019','45800','47.9178497622','1.97241570235');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRESNE(S) ZIK','Musiques actuelles','http://mjcdefresnes.free.fr/','Fresnes','14/04/2019','26/04/2019','94260','48.7571343016','2.32612839743');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POPOPPIDUM FESTIVAL','Musiques actuelles','http://www.popoppidum.com','CHAMPAGNOLE','13/04/2019','13/04/2019','39300','46.7445389751','5.8988102947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELECTRIC PALACE','Musiques actuelles','www.electricpalace.fr','CLERMONT FERRAND','01/02/2019','08/02/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES JAZZERIES D''HIVER','Musiques actuelles','www.gagajazz.com','ST ETIENNE','01/02/2019','15/02/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Go�t des autres','Livre et litt�rature','https://legoutdesautres.lehavre.fr/','LE HAVRE','17/01/2019','20/01/2019','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival ceux d''en face','Pluridisciplinaire Spectacle vivant','http://www.animakt.fr/Festival-Ceux-d-en-Face','Saulx les Chartreux','09/04/2019','14/04/2019','91160','48.6850264714','2.26634749221');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te des Tulipes','Transdisciplinaire','','ST DENIS','20/04/2019','21/04/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pyr�ne Festival','Musiques actuelles','http://www.pyrenefestival.fr/','Bordes','05/07/2019','06/07/2019','64510','43.2436324189','-0.272861349005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NOUVELLES DU RIRE','Divers Spectacle vivant','','PORT LA NOUVELLE','05/07/2019','07/07/2019','11210','43.0210644931','3.03976442192');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ENFANTS DU JAZZ FESTIVAL DE JAZZ DE BARCELONNETTE','Musiques actuelles','www.barcelonnette.com','BARCELONNETTE','16/07/2019','28/07/2019','4400','44.3785614205','6.65215089713');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BINIC FOLK BLUES FESTIVAL','Musiques actuelles','www.binic-folks-blues-festival.com','BINIC','26/07/2019','28/07/2019','22520','48.6255350477','-2.8413899481');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE GREEN ESCAPE','Musiques actuelles','http://www.festivaldecraponne.com/','Craponne sur Arzon','26/07/2019','28/07/2019','43500','45.3318549451','3.85314004519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MarveLoz'' Pop Festival','Transdisciplinaire','www.marveloz.fr','Marvejols','12/07/2019','14/07/2019','48100','44.5581181858','3.28780886743');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN VIOLON SUR LE SABLE','Musiques classiques','http://www.violonsurlesable.com','ROYAN','20/07/2019','26/07/2019','17200','45.6346574238','-1.01791403375');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN SANGUINET','Musiques actuelles','www.sanguinet.com','SANGUINET','23/07/2019','26/07/2019','40460','44.4778702748','-1.06434519608');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATOM FESTIVAL','Pluridisciplinaire Spectacle vivant','','Castelnaudary','26/07/2019','28/07/2019','11400','43.3230173159','1.96073752488');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RAMATUELLE MONTE LE SON','Musiques actuelles','https://fr-fr.facebook.com/pages/category/Concert-Tour/Ramatuelle-Monte-Le-Son-131554030266665/','RAMATUELLE','04/07/2019','06/07/2019','83350','43.2186126186','6.63754734174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUBER''JAZZ''DAY','Musiques actuelles','http://www.auberjazzday.com/','AUBERVILLIERS','04/07/2019','06/07/2019','93300','48.9121722626','2.38445513768');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Arte Flamenco','Musiques actuelles','https://arteflamenco.landes.fr/','MONT DE MARSAN','02/07/2019','06/07/2019','40000','43.899361404','-0.490722577455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BAZAR LE JOUR BIZ''ART LA NUIT','Cirque et Arts de la rue','https://bjbn.fr/','BETTON','29/06/2019','30/06/2019','35830','48.1812360423','-1.64589552684');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festiv�arcandiers','Musiques actuelles','','Vailly sur Sauldre','29/06/2019','29/06/2019','18260','47.4563676608','2.65132659906');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international d''orgues de Chartres','Musiques classiques','http://www.orgues-chartres.org/','CHARTRES','07/07/2019','25/08/2019','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUX HEURES D''�T�','Musiques actuelles','www.auxheuresete.com','NANTES','09/07/2019','16/08/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFRICAJARC','Musiques actuelles','www.africajarc.com','CAJARC','18/07/2019','21/07/2019','46160','44.494117576','1.83859373388');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Brive Festival','Musiques actuelles','http://brivefestival.com/','Brive la Gaillarde','19/07/2019','22/07/2019','19100','45.1435830664','1.51936836063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZION D''HIVER','Musiques actuelles','www.ziongarden.fr','BAGNOLS SUR CEZE','18/07/2019','20/07/2019','30200','44.1622496736','4.62477380854');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRI DU COL','Musiques actuelles','www.criducol.com','MONSOLS','01/08/2019','04/08/2019','69860','46.2227835629','4.51394181008');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Roche Bluegrass Festival','Musiques actuelles','http://www.larochebluegrass.org/accueil.html','La Roche sur Foron','31/07/2019','04/08/2019','74800','46.0431484138','6.30107653595');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NO LOGO FESTIVAL','Musiques actuelles','www.nologofestival.fr','FRAISANS','09/08/2019','11/08/2019','39700','47.1244743587','5.75661818843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES VOIX, DES LIEUX... DES MONDES','Musiques actuelles','http://moissac-culture.fr','MOISSAC','','','82200','44.1262518079','1.09834758014');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRE DE LA CHANSON FRANCAISE A TOURNUS','Musiques actuelles','','Tournus','','','71700','46.5651726015','4.90012429206');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CATALACUM','Musiques actuelles','www.lesilex.fr/','CHAPTELAT','','','87270','45.9176663551','1.24668895205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film de Saint-Paul-Trois-Ch�teaux','Cin�ma et audiovisuel','http://www.festivaldufilm-stpaul.com','ST Paul Trois Chateaux','','','26130','44.3482808478','4.75744934383');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR LA VILLE','Musiques actuelles','www.jazzsurlaville.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d�Augenblick','Cin�ma et audiovisuel','https://festival-augenblick.fr/fr/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival VIA','Pluridisciplinaire Spectacle vivant','http://www.lemanege.com/','MAUBEUGE','','','59600','50.283630719','3.96328053037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AUTOMNALES DE L''ORGUE','Musiques classiques','www.provenceguide.com/fetes-et-manifestations/avignon/xxvie-edition-des-automnales-de-lorgue/provence-4808266-1.html','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZYCOLORS','Musiques actuelles','www.ficep.info/jazzycolors','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LE MILLESIME','Musiques classiques','www.lemillesime.fr','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PORNIC CLASSIC','Musiques classiques','www.pornicclassic.weebly.com','PORNIC','','','44210','47.1223972452','-2.05182334479');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Court c''est court ! � Rencontres autour du Court M�trage � Cabri�res d''Avignon','Cin�ma et audiovisuel','http://cinambule.org','Cabrieres d Avignon','','','84220','43.8965322686','5.15279801626');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ESTIVAL DE SAINT GERMAIN EN LAYE','Musiques actuelles','www.lestival.net','ST GERMAIN EN LAYE','','','78100','48.9407041394','2.09870929375');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RED BULL MUSIC FESTIVAL PARIS','Musiques actuelles','https://paris.redbullmusicacademy.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival TOTAL DANSE','Danse','www.teat.re','ST DENIS','','','97400','-20.9329708192','55.446867167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIM�','Transdisciplinaire','www.fimefestival.fr','TOULON','','','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ESPRIT DU PIANO','Musiques classiques','http://www.espritdupiano.fr/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du Film d''Animation','Cin�ma et audiovisuel','','BASTIA','','','20200','42.6864768806','9.42502133338');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COULEURS GUITARE','Musiques actuelles','http://www.festival-couleurs-guitare.com','MEJANNES LE CLAP','','','30430','44.2309207985','4.36426612667');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JACQUES BREL','Musiques actuelles','www.theatre-edwige-feuillere.fr','VESOUL','','','70000','47.6320408648','6.1548458149');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Sons d''Automne','Pluridisciplinaire Spectacle vivant','www.crr.annecy.fr','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DU CHAT','Musiques actuelles','www.lesnuitsduchat.com','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE HAILLAN CHANT�','Musiques actuelles','http://www.ville-lehaillan.fr/','LE HAILLAN','04/06/2019','08/06/2019','33185','44.8690837903','-0.684443512134');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Jardin du Michel','Musiques actuelles','','Bulligny','31/05/2019','02/06/2019','54113','48.5723193391','5.84368578538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MELLERAN PART EN LIVE','Musiques actuelles','https://melleranpartenlive.wixsite.com','MELLERAN','15/06/2019','15/06/2019','79190','46.1300825848','0.00534971717862');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MUSICALES DANS LES VIGNES','Pluridisciplinaire Musique','http://lesmusicalesdanslesvignes.blogspot.fr/','HYERES','21/06/2019','31/08/2019','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Livre d''Art et du Film (FILAF)','Transdisciplinaire','https://www.filaf.com/','PERPIGNAN','17/06/2019','23/06/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre audio de Strasbourg','Livre et litt�rature','www.laplumedepaon.com','STRASBOURG','09/05/2019','18/05/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Quinzaine des R�alisateurs','Cin�ma et audiovisuel','www.quinzaine-realisateurs.com','CANNES','15/05/2019','25/05/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICALIES EN SOLOGNE','Musiques actuelles','http://www.ucps.fr/musicaliesensologne.html','Pierrefitte Sur Sauldre','03/05/2019','05/05/2019','41300','47.5270515255','2.12726759593');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de performances interm�dias','Transdisciplinaire','https://www.facebook.com/InactFestivalDePerformancesMixmedias/','STRASBOURG','03/05/2019','05/05/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Film de Cabourg','Cin�ma et audiovisuel','www.festival-cabourg.com','Cabourg','12/06/2019','16/06/2019','14390','49.2834863414','-0.125512945279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musiques au pays de Pierre Loti','Transdisciplinaire','https://www.festival-mppl.com/','LA ROCHELLE','25/05/2019','01/06/2019','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''humour des notes','Cirque et Arts de la rue','www.humour-des-notes.com','HAGUENAU','25/05/2019','02/06/2019','67500','48.8417047695','7.83010404968');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA DEFERLANTE DE PRINTEMPS','Pluridisciplinaire Spectacle vivant','','LA ROCHE SUR YON','25/05/2019','10/06/2019','85000','46.6675261644','-1.4077954093');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA PETITE VAGUE','Musiques actuelles','https://lanouvellevague.org/actions-culturelles/la-petite-vague/','ST MALO','24/05/2019','26/05/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival L''Histoire � venir','Domaines divers','https://2019.lhistoireavenir.eu/','TOULOUSE','23/05/2019','26/05/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Cosne sur Loire','Livre et litt�rature','http://www.ot-cosnesurloire.com','COSNE COURS SUR LOIRE','24/05/2019','26/05/2019','58200','47.4029099422','2.94284103771');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TROIS6NEUF','Musiques actuelles','http://www.theatre-latalante.com','PARIS','23/05/2019','25/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BON MOMENT ESTIVAL','Musiques actuelles','','NANCY','24/05/2019','25/05/2019','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LET''S DOCKS','Musiques actuelles','www.lesdocks-cahors.fr','CAHORS','05/06/2019','08/06/2019','46000','44.4507370916','1.44075837848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Philosophia','Livre et litt�rature','http://www.festival-philosophia.com/','ST EMILION','22/05/2019','26/05/2019','33330','44.8999911844','-0.169955105989');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ZICOPHONIES','Musiques actuelles','www.asso-claj.net','CLERMONT FERRAND','17/05/2019','18/05/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Bellac','Pluridisciplinaire Spectacle vivant','www.theatre-du-cloitre.fr','BELLAC','26/06/2019','30/06/2019','87300','46.1049122554','1.04116467219');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SOUS LES CHATAIGNIERS','Musiques actuelles','www.jazz-roquefere.com','ROQUEFERE','','','11380','43.3939652788','2.37477905225');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE BIG BAND DE PERTUIS','Musiques actuelles','http://www.festival-jazz-bigband-pertuis.com','Pertuis','','','84120','43.6872357464','5.52052480281');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz aux Fronti�res','Musiques actuelles','www.jazzauxfrontieres.com','MONTGENEVRE','','','5100','44.9389932221','6.72778259009');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le FARSe','Cirque et Arts de la rue','https://ete.strasbourg.eu/farse-festival-arts-de-la-rue','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � JUH�GUES','Musiques actuelles','www.torreilles.fr','TORREILLES','','','66440','42.7533924686','3.00860614432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festisis, festival du cirque et des arts vivants de la r�gion du Val d''Aisne, Pargny-Filain :','Cirque et Arts de la rue','','PARGNY FILAIN','','','2000','49.4629055692','3.53929108659');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L��tang du Folk','Musiques actuelles','','Chevenon','','','58160','46.9145671045','3.23010283481');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JACQUES A DIT','Pluridisciplinaire Spectacle vivant','http://forumcarros.com/','CARROS','','','6510','43.7850390592','7.18414159904');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE EUROPEAN BLUES CRUISE','Musiques actuelles','www.europeanbluescruise.com/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de musique de Besan�on Franche-Comt�','Musiques classiques','www.festival-besancon.com','BESANCON','','','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAINA''ZIK','Musiques actuelles','www.chainazik-festival.com','CHAINAZ LES FRASSES','','','74540','45.7784395221','5.98960566934');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FLUME ENCHANTEE','Musiques actuelles','www.festival-laflumeenchantee.fr','Geveze','','','35850','48.2133254705','-1.79930703575');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES INOUIES','Musiques classiques','www.musiqueenrouelibre.com','ARRAS','','','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TREFFL FESTIF','Cirque et Arts de la rue','http://www.treffestif.fr/','Trefflean','','','56250','47.6781213314','-2.62203549701');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INTER''VAL D''AUTOMNE','Musiques actuelles','www.interval.ccvl.fr','VAUGNERAY','','','69670','45.7318792519','4.64378667979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MARTIGUES','Musiques actuelles','www.festivaldemartigues.com','MARTIGUES','','','13117','43.3798920489','5.04945402314');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTESQUIOU ON THE ROCK''S','Musiques actuelles','www.montesquiourock.fr','MONTESQUIOU','','','32320','43.5780506568','0.33230525991');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MALGUENAC FESTIVAL','Musiques actuelles','www.festival-malguenac.fr','MALGUENAC','','','56300','48.0690022602','-3.05620760639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SILLIUSFEST','Musiques actuelles','','AUTUN','','','71400','46.945536773','4.31060069532');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROYAUMONT','Musiques classiques','www.royaumont.com','ASNIERES SUR OISE','','','95270','49.1407338587','2.3800597257');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NEUVIL''EN JAZZ','Musiques actuelles','http://www.neuville-de-poitou.com/Festival-Neuvil-en-Jazz.html','NEUVILLE DE POITOU','','','86170','46.6823102624','0.254645073675');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES LYRES D''�T�','Pluridisciplinaire Spectacle vivant','www.blois.fr','BLOIS','','','41000','47.5817013938','1.30625551583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''AIR D''EN RIRE','Divers Spectacle vivant','www.airdenrire.fr','ST DENIS LA CHEVASSE','','','85170','46.8325959261','-1.3830312677');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU THEATRE','Musiques actuelles','http://www.festivaldjangoreinhardt.com','FONTAINEBLEAU','','','77300','48.4066856508','2.68031396389');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RISE AND FALL FESTIVAL','Musiques actuelles','http://www.riseandfallfestival.com/','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Traverse Vid�o - Toulouse','Cin�ma et audiovisuel','https://traverse-video.org/','TOULOUSE','13/03/2019','31/03/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon de la BD','Livre et litt�rature','www.sevrierbd.fr','SEVRIER','27/04/2019','28/04/2019','74320','45.8555012829','6.13806338899');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARCH�O JAZZ FESTIVAL','Musiques actuelles','www.archeojazz.com','BLAINVILLE CREVON','26/06/2019','29/06/2019','76116','49.504416715','1.29186032794');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PESTACLES','Musiques actuelles','www.lespestacles.fr','PARIS','26/06/2019','04/09/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURES DU MONDE','Musiques actuelles','www.folklore-voiron.fr','VOIRON','27/06/2019','05/07/2019','38500','45.3791720843','5.58240310671');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Design Parade (Villa Noailles)','Arts plastiques et visuels','','HYERES','27/06/2019','30/06/2019','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SWING SOUS LES ETOILES','Musiques actuelles','www.swingsouslesetoilesmiribel.com','MIRIBEL','28/06/2019','03/07/2019','1700','45.8442099203','4.94106746787');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Z�Arp�tes','Cirque et Arts de la rue','http://www.leszarpetes.com','Villenave d�Ornon','28/06/2019','29/06/2019','33140','44.773185218','-0.558212256384');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUTRE FESTIVAL','Musiques actuelles','https://www.ville-wissembourg.eu','WISSEMBOURG','28/06/2019','30/06/2019','67160','49.0184562539','7.96174575912');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHARTRESTIVALES','Musiques actuelles','https://www.chartrestivales.com','CHARTRES','28/06/2019','24/08/2019','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROQUEMAURE 2 RIRE','Divers Spectacle vivant','www.roquemaure2rire.fr','Roquemaure','28/06/2019','29/06/2019','30150','44.0414756862','4.7565701251');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'S�rie S�ries','Cin�ma et audiovisuel','www.serieseries.fr','FONTAINEBLEAU','01/07/2019','03/07/2019','77300','48.4066856508','2.68031396389');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Jours [et nuits] de cirque(s)','Cirque et Arts de la rue','www.joursetnuitsdecirques.fr','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'I''m from Rennes','Musiques actuelles','http://www.imfromrennes.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE ANCIENNE DE RIBEAUVILLE','Musiques classiques','www.musiqueancienneribeauville.eu','RIBEAUVILLE','','','68150','48.2065139236','7.28672468283');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DISQUE ET BD','Transdisciplinaire','http://festivaldeldisc.fr/','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZAC EN SCENE','Musiques actuelles','http://www.zacenscene.com','La Boisse','','','1120','45.8446213549','5.02783224111');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE PRESSOIR','Cirque et Arts de la rue','www.acrocsproductions.com','TARGON','','','33760','44.7345410468','-0.267821650496');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CARREMENT A L''OUEST','Cirque et Arts de la rue','http://lecitronjaune.com/carrement-a-louest/','PORT ST LOUIS DU RHONE','','','13230','43.4148193741','4.80679663589');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES HIVERNALES DU FESTIVAL D''ANJOU','Th��tre','www.festivaldanjou.com/hivernales','ANGERS','','','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA VALLEE DES CONTES','Divers Spectacle vivant','http://valleedescontes.org/','MUNSTER','','','68140','48.0448514207','7.14149744282');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ DU PLATEAU PICARD','Musiques actuelles','www.plateaupicard.fr/','LE PLESSIER SUR ST JUST','','','60130','49.5081515622','2.4596143307');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du cin�ma m�diterran�en de Montpellier (CINEMED)','Cin�ma et audiovisuel','http://www.cinemed.tm.fr/','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ TEMPO','Musiques actuelles','www.crdj.org','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE EN CATALOGNE ROMANE','Musiques classiques','www.musiqueencatalogneromane.com','ST GENIS DES FONTAINES','','','66740','42.5501690702','2.92022187545');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL IMAG''IN','Musiques actuelles','','TOURS','','','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''ZAC','Musiques actuelles','www.festizac.fr','Ambazac','','','87240','45.9534226621','1.39930873296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Apr�s les vendanges','Musiques actuelles','http://www.apreslesvendanges.com','VAISON LA ROMAINE','','','84110','44.247701867','5.06040782309');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PRIMEURS DE MASSY','Musiques actuelles','www.primeurs-massy.com','MASSY','','','91300','48.7277896426','2.27513131903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHARIVARUES','Cirque et Arts de la rue','http://www.theatrejacquescarat.fr/','CACHAN','18/05/2019','19/05/2019','94230','48.7916121646','2.33153313164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE DE TOULON - LE FESTIVAL ESTIVAL','Musiques classiques','http://www.festivalmusiquetoulon.com/','SIX FOURS LES PLAGES','12/06/2019','10/07/2019','83140','43.086818602','5.82924464931');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GARE AU GORILLE','Cirque et Arts de la rue','https://www.carre-magique.com','LANNION','30/05/2019','02/06/2019','22300','48.7433496707','-3.46159799691');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JEAN FERRAT A CHIRENS','Musiques actuelles','','Chirens','31/05/2019','02/06/2019','38850','45.4166318565','5.556774983');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE FRANCO AMERICAINE DE THIAIS','Musiques actuelles','http://www.ville-thiais.fr','Thiais','22/06/2019','23/06/2019','94320','48.7607841928','2.38537729524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Afropunk Festival','Musiques actuelles','www.afropunkfest.com','PARIS','13/07/2019','14/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROOTSTOCK FESTIVAL A POMMARD','Musiques actuelles','http://rootstockburgundy.com','Pommard','13/07/2019','14/07/2019','21630','47.0136884402','4.79517699243');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOTES D''ETE A IRANCY','Musiques classiques','https://www.notesmusicales-irancy.com/le-festival','Irancy','17/07/2019','21/07/2019','89290','47.7118538877','3.68956867667');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAMILY PIKNIK FESTIVAL','Musiques actuelles','http://www.familypiknikfestival.com','MONTPELLIER','03/08/2019','05/08/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU GRES DU JAZZ','Musiques actuelles','http://www.ot-paysdelapetitepierre.com/','La Petite Pierre','10/08/2019','18/08/2019','67290','48.8657597248','7.32734776032');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''ART LYRIQUE DE SALON','Musiques classiques','','SALON DE PROVENCE','08/08/2019','11/08/2019','13300','43.6462885238','5.06814649576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE BOBITAL L''ARMOR A SONS','Musiques actuelles','http://www.bobital-festival.fr','BOBITAL','05/07/2019','06/07/2019','22100','48.4144897659','-2.09704568121');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOLEILS BLEUS','Musiques actuelles','www.onyx-culturel.org','ST HERBLAIN','04/07/2019','07/07/2019','44800','47.2243762412','-1.63434818692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS FLAMENCAS','Musiques actuelles','www.nomadeskultur.com','AUBAGNE','04/07/2019','06/07/2019','13400','43.2934843764','5.56331273477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU BOUT DU MONDE','Musiques actuelles','www.festivalduboutdumonde.com','CROZON','02/08/2019','04/08/2019','29160','48.2449715382','-4.49170347769');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''N POCHE FESTIVAL','Musiques actuelles','www.rocknpoche.com','HABERE POCHE','02/08/2019','03/08/2019','74420','46.2555235551','6.47324374853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Interceltique de Lorient','Musiques actuelles','www.festival-interceltique.bzh','LORIENT','02/08/2019','11/08/2019','56100','47.7500486947','-3.37823200917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Spectacles de Grands Chemins en Vall�es d''Ax','Cirque et Arts de la rue','https://www.ax-animation.com/636/spectacles-de-grands-chemins-3','AX LES THERMES','30/07/2019','03/08/2019','9110','42.6873576342','1.81825994194');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ocus Pocus','Musiques actuelles','http://www.opuspocus.re/','ST PAUL','02/08/2019','25/08/2019','97460','-21.0445317522','55.3223334298');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PANIC ! OPEN AIR FEST','Musiques actuelles','http://www.panicfest.fr','ST FELIX','26/07/2019','27/07/2019','17330','46.0937313231','-0.603804881739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LABEAUME EN MUSIQUES','Musiques classiques','http://www.labeaume-festival.org','LABEAUME','26/07/2019','10/08/2019','7120','44.4680870605','4.31105082566');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 37� � l''ombre','Musiques classiques','','TOURS','28/07/2019','18/08/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL AU VILLAGE','Pluridisciplinaire Spectacle vivant','http://festivalauvillage.free.fr/','BRIOUX SUR BOUTONNE','05/07/2019','12/07/2019','79170','46.1418337503','-0.218953610699');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMERICAN TOURS FESTIVAL','Musiques actuelles','www.tours-evenements.com','TOURS','05/07/2019','07/07/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Fous Cav�s','Musiques actuelles','https://www.lesfouscaves.fr/','Port d Envaux','19/07/2019','20/07/2019','17350','45.818051845','-0.672067298755');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Errobiko Festibala','Pluridisciplinaire Musique','www.errobikofestibala.fr','BAYONNE','19/07/2019','22/07/2019','64100','43.4922254016','-1.46607674358');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ZENDIMANCHES','Pluridisciplinaire Spectacle vivant','www.spectacles-en-retz.com','ST Hilaire de Chaleons','19/07/2019','21/07/2019','44680','47.0958948849','-1.87983637967');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OH ! BUGEY','Musiques actuelles','https://www.ohbugeyfestival.fr/','Oyonnax','19/07/2019','20/07/2019','1100','46.2605435859','5.65344320923');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � TOULON','Musiques actuelles','http://www.jazzatoulon.com','TOULON','19/07/2019','28/07/2019','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tempo Latino','Musiques actuelles','www.tempo-latino.com','VIC FEZENSAC','26/07/2019','28/07/2019','32190','43.762029942','0.297842185088');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIRE EN VIGNES','Divers Spectacle vivant','http://www.chateaudesaintmartin.com','Taradeau','24/07/2019','25/07/2019','83460','43.4612575373','6.43066360323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RESONANCE A AVIGNON','Musiques actuelles','www.festival-resonance.fr','AVIGNON','23/07/2019','28/07/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ch�ries-Ch�ris, festival de films gays, lesbiens, trans et ++++ de Paris','Cin�ma et audiovisuel','https://cheries-cheris.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�-festival en pays de Fayence','Cin�ma et audiovisuel','www.cine-festival.org','Montauroux','','','83440','43.5990101065','6.78985995347');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN OUCHE','Musiques actuelles','www.jazzenouche.com','L AIGLE','','','61300','48.7561549553','0.611126420423');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL NOTES D''AUTOMNE','Musiques classiques','http://festivalnotesdautomne.fr/','Le Perreux Sur Marne','','','94170','48.8423952383','2.50406848573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Utopiales','Transdisciplinaire','http://www.utopiales.org','NANTES','30/10/2019','04/11/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU C�UR & GUITARE � L''�ME','Musiques actuelles','www.espace-icare.net','ISSY LES MOULINEAUX','','','92130','48.82347434','2.26449823277');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN �T� � BOURGES','Musiques actuelles','www.ville-bourges.fr','BOURGES','','','18000','47.0749572013','2.40417137557');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DROP''N''ROCK','Musiques actuelles','www.dropnrock.com','STE MARGUERITE','','','88100','48.2675581117','6.97340042686');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pianoctambule','Pluridisciplinaire Musique','https://www.pianoctambule.com/','LE MANS','15/10/2019','20/10/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival D�Ambronay','Musiques classiques','http://festival.ambronay.org','AMBRONAY','12/09/2019','06/10/2019','1500','46.0055913782','5.35760660735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARFUM DE JAZZ','Musiques actuelles','www.parfumdejazz.com','BUIS LES BARONNIES','11/08/2019','25/08/2019','26170','44.276084918','5.27102720538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SPIRIT IN BLACK','Musiques actuelles','http://www.associationhopla.com','COLMAR','14/08/2019','17/08/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE PETIT FESTIVAL DES DINDES FOLLES','Pluridisciplinaire Spectacle vivant','www.dindesfolles.com','RIVOLET','','','69640','46.0065781442','4.58017136765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VAL D''AULNAY','Pluridisciplinaire Musique','www.festivaldaulnay.fr','CHATENAY MALABRY','','','92290','48.7681690197','2.26282598525');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Fumetti','Livre et litt�rature','http://www.maisonfumetti.fr/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Parade(s), festival des arts de la rue de Nanterre','Cirque et Arts de la rue','http://www.nanterre.fr/','NANTERRE','','','92000','48.8960701282','2.20671346353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE A L''EUILLE','Musiques actuelles','www.mairie-targon.fr/','TARGON','','','33760','44.7345410468','-0.267821650496');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HAPPY THOIRY FESTIVAL','Musiques actuelles','https://www.thoiry.net/happy-thoiry-festival','Thoiry','','','78770','48.8724624004','1.79780062062');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AHOY MUSIQUES DE PLAISANCE','Pluridisciplinaire Spectacle vivant','','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RUTILANTS','Musiques actuelles','www.9-9bis.com','OIGNIES','','','62590','50.4644483399','2.99306842451');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ACCORDS & � CRIS','Musiques actuelles','http://accordsetacris-fougeres.fr/','FOUGERES','','','35300','48.3524697115','-1.19431177241');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFFAIRE EST DANS L''SAX','Musiques actuelles','www.saxalouest.fr','ST MALO','','','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOOGIE SWING 137','Musiques actuelles','www.boogieswing137.info','TINTENIAC','','','35190','48.3231993668','-1.83043308458');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cabaret des 1e ann�e','Pluridisciplinaire Spectacle vivant','','BAGNOLET','','','93170','48.8690836308','2.42274096688');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES COURANTS ALTERNATIFS','Musiques actuelles','','Uzes','','','30700','44.0136590402','4.41600031556');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR LE TOIT','Musiques actuelles','http://www.ot-cassis.com','CASSIS','','','13260','43.2230470065','5.55117810991');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TEMPS F�TE, FESTIVAL MARITIME','Transdisciplinaire','www.tempsfete.com','DOUARNENEZ','','','29100','48.0806526556','-4.32784220122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES MUSIQUES INSOLENTES','Pluridisciplinaire Spectacle vivant','www.mdlc-lef.com','LORGUES','','','83510','43.4814630298','6.35892744946');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''RANCE','Musiques actuelles','','ST SAMSON SUR RANCE','','','22100','48.4914561731','-2.01805917808');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Belles Journ�es','Musiques actuelles','www.bellesjournees.fr','BOURGOIN JALLIEU','','','38300','45.6022027954','5.27393762111');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EMERGENCE A LYON','Musiques actuelles','https://mairie5.lyon.fr/evenement/festival/festival-emergence-0','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URIAGE EN VOIX','Musiques actuelles','http://www.uriage-les-bains.com','ST MARTIN D URIAGE','','','38410','45.1533114965','5.85697188801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Rue Dell Arte','Cirque et Arts de la rue','http://festivalrda.wixsite.com/festivalruedellarte','MONCONTOUR','','','22510','48.3581980369','-2.63225581837');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�tours en Tournugeois','Cirque et Arts de la rue','http://www.legalpon.com','Tournus','','','71700','46.5651726015','4.90012429206');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLACE AUX ARTISTES','Musiques actuelles','https://www.saintquayportrieux.com/','ST QUAY PORTRIEUX','','','22410','48.6542475006','-2.84382337656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Off-Courts de Trouville','Cin�ma et audiovisuel','www.off-courts.com','TROUVILLE SUR MER','','','14360','49.3721246762','0.102123470052');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELECTRO ALTERNATIV','Musiques actuelles','www.electro-alternativ.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international des cin�mas d�Asie','Cin�ma et audiovisuel','http://www.cinemas-asie.com','VESOUL','05/02/2019','12/02/2019','70000','47.6320408648','6.1548458149');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZION GARDEN COLLECTION HIVER','Musiques actuelles','www.ziongarden.fr','BAGNOLS SUR CEZE','22/02/2019','23/02/2019','30200','44.1622496736','4.62477380854');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HOT CLUB JAZZ FESTIVAL','Musiques actuelles','www.hotclubjazz.com','LYON','04/04/2019','07/04/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale de danse du Val-de-Marne','Danse','http://www.alabriqueterie.com/fr/','VITRY SUR SEINE','21/03/2019','19/04/2019','94400','48.7882828307','2.39412680533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Printemps du livre','Livre et litt�rature','http://printempsdulivre.bm-grenoble.fr/','GRENOBLE','20/03/2019','24/03/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMIS DU JAZZ TRADITIONNEL','Musiques actuelles','www.ajtderochegude.com','ROCHEGUDE','22/03/2019','24/03/2019','26790','44.2556674573','4.83716476858');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 48 images seconde - Florac','Cin�ma et audiovisuel','www.48imagesseconde.fr','Florac Trois Rivi�res','10/04/2019','14/04/2019','48400','44.3162079295','3.58273527435');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'P�riph�rique','Musiques actuelles','www.hiero.fr','COLMAR','06/04/2019','19/04/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU FILM PANAFRICAIN','Cin�ma et audiovisuel','http://fifp.fr/','CANNES','17/04/2019','21/04/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SO NORD','Musiques actuelles','www.sallequatrevents.com','Rouziers de Touraine','01/02/2019','09/02/2019','37360','47.5311263676','0.647449146554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Regards noirs','Livre et litt�rature','http://regardsnoirs.niort.fr/','NIORT','01/02/2019','02/02/2019','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film de com�die de l�Alpe d�Huez','Cin�ma et audiovisuel','http://www.festival-alpedhuez.com','HUEZ','15/01/2019','20/01/2019','38750','45.0969425761','6.08474810987');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PACO TYSON','Musiques actuelles','www.pacotyson.fr','NANTES','19/04/2019','21/04/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'XARNEGU EGUNA','Musiques actuelles','https://xarnegueguna.fr/','Bardos','30/04/2019','05/05/2019','64520','43.4681224323','-1.22301840969');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Journ�es de la BD','Livre et litt�rature','www.journeesbd.fr','Le Pellerin','27/04/2019','28/04/2019','44640','47.2229017049','-1.8250130027');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Acoustic Festival','Musiques actuelles','www.acoustic-festival.fr','Le Poire sur Vie','22/03/2019','24/03/2019','85170','46.769919754','-1.50488626452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'festival de la BD de Colmar','Livre et litt�rature','http://lesprit-bd.com/','COLMAR','23/03/2019','24/03/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MUSICALES DE COLMAR','Musiques classiques','www.les-musicales.com','COLMAR','05/05/2019','12/05/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Saperlipopette','Cirque et Arts de la rue','http://www.domainedo.fr/spectacles/saperlipopette','MONTPELLIER','04/05/2019','05/05/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'METAL CULTURE(S)','Musiques actuelles','http://metalcultures.com','GUERET','08/05/2019','11/05/2019','23000','46.1632121428','1.87078672735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ODP','Musiques actuelles','http://www.festival-odp.com','BORDEAUX','07/06/2019','09/06/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Coming of Rock','Musiques actuelles','','Le Russey','07/06/2019','08/06/2019','25210','47.1549185878','6.73440290446');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'IDA Y VUELTA','Musiques actuelles','www.idayvuelta-festival.fr','PERPIGNAN','06/06/2019','09/06/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Quartier du livre','Livre et litt�rature','www.quartierdulivre.fr','PARIS','15/05/2019','22/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TEMPOS DU MONDE','Musiques actuelles','www.temposdumonde.com','ST PAUL LES DAX','22/05/2019','25/05/2019','40990','43.7464083139','-1.08127980711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WINE NAT / WHITE HEAT','Musiques actuelles','http://www.winenatwhiteheat.com/','NANTES','24/05/2019','26/05/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GYPSY LYON FESTIVAL','Musiques actuelles','www.gypsylyonfestival.com','LYON','23/05/2019','26/05/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Assises internationales du roman','Livre et litt�rature','http://www.villagillet.net/festivals/assises-internationales-du-roman','LYON','20/05/2019','26/05/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VILLETTE SONIQUE','Musiques actuelles','www.villettesonique.com','PARIS','06/06/2019','09/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVRAC','Musiques actuelles','www.festivrac.com','PONT DE VAUX','','','1190','46.4422864477','4.92862459427');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAY CAR BLUES','Musiques actuelles','www.baycarbluesfestival.fr','GRANDE SYNTHE','','','59760','51.0167874642','2.29071143357');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HAUTE FR�QUENCE','Musiques actuelles','http://www.haute-frequence.fr/','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Au Fil de la marionnette','Divers Spectacle vivant','http://spectaclevivant.hautbearn.fr/','Oloron STE Marie','','','64400','43.1560871189','-0.587546244432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � CAUD�RAN','Musiques actuelles','www.actionjazz.fr','MERIGNAC','','','33700','44.8322953289','-0.681733084891');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sorties de Bain','Cirque et Arts de la rue','www.sortiesdebain.com','GRANVILLE','04/07/2019','07/07/2019','50400','48.8327078372','-1.56670866413');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURES ET TRADITIONS DU MONDE','Musiques actuelles','www.empi-et-riaume.com','ROMANS SUR ISERE','03/07/2019','07/07/2019','26100','45.0547531985','5.03797756012');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te des Sottises !','Cirque et Arts de la rue','http://lacaze.aux.sottises.free.fr/saison_festival.html','Salies de Bearn','19/07/2019','21/07/2019','64270','43.4683239252','-0.917789447505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JEAN FERRAT','Musiques actuelles','www.jean-ferrat-antraigues.com','ANTRAIGUES','19/07/2019','21/07/2019','7530','44.7393183247','4.35517589387');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Internationales de Musique M�di�vale du Thoronet','Musiques classiques','','LE THORONET','18/07/2019','23/07/2019','83340','43.458933028','6.28265623456');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DE LA GUITARE A PATRIMONIO','Musiques actuelles','www.festival-guitare-patrimonio.com','PATRIMONIO','20/07/2019','27/07/2019','20253','42.7066867042','9.3670488048');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SUDS A ARLES','Musiques actuelles','www.suds-arles.com','ARLES','08/07/2019','14/07/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS BRESSANES','Musiques actuelles','http://www.bresse-bourguignonne.com','Louhans','12/07/2019','13/07/2019','71500','46.6351883548','5.23839292789');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENC''ARTS � PORNICHET','Musiques actuelles','www.rencarts.fr','PORNICHET','16/07/2019','13/08/2019','44380','47.2615791665','-2.31415650821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Messiaen au Pays de la Meije','Musiques classiques','http://www.festival-messiaen.com','LA GRAVE','26/07/2019','04/08/2019','5320','45.0601102401','6.28406977587');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MYSTERES DE L''OUEST','Cirque et Arts de la rue','https://www.belle-ile.com/faire/sortir/festivals-et-theatre/2429836-festival-les-mysteres-de-louest','SAUZON','30/07/2019','02/08/2019','56360','47.353750697','-3.23035128949');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 48�me de Rue','Cirque et Arts de la rue','www.48emederue.org','MENDE','05/07/2019','07/07/2019','48000','44.5294508592','3.48086881176');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DE L''EUROPE','Transdisciplinaire','http://www.amilly.com','AMILLY','05/07/2019','07/07/2019','45200','47.9867116879','2.78217519303');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS ESTIVALES DU CHATEAU','Pluridisciplinaire Spectacle vivant','www.nuitsestivales.fr','MOUANS SARTOUX','05/07/2019','31/08/2019','6370','43.6185304023','6.96494426706');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DE LA CONQUE','Musiques actuelles','http://locepon.pagesperso-orange.fr/laconque/laconque.htm','VENCE','05/07/2019','06/07/2019','6140','43.7384640641','7.10194436087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIC IN - MUSIC AOUT','Musiques actuelles','http://music.tradfrance.com','HOERD','02/08/2019','04/08/2019','67720','48.6905674036','7.78483478217');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Route du rock','Musiques actuelles','www.laroutedurock.com','ST MALO','14/08/2019','17/08/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE D''ERBALUNGA','Musiques actuelles','','BRANDO','11/08/2019','13/08/2019','20222','42.7792996993','9.45029993821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE SACR�E DE PERPIGNAN','Musiques classiques','www.mairie-perpignan.fr/','PERPIGNAN','10/04/2019','20/04/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUTBREAK METAL FEST','Musiques actuelles','www.theoutbreakfest.com','BLOIS','10/04/2019','13/04/2019','41000','47.5817013938','1.30625551583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DO DISTURB','Transdisciplinaire','http://www.palaisdetokyo.com','PARIS','09/04/2019','11/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RARES TALENTS','Musiques actuelles','www.rarestalents.com','MONTREUIL','30/03/2019','07/04/2019','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre jeunesse d''Eaubonne','Livre et litt�rature','www.valdoise-tourisme.com','EAUBONNE','30/03/2019','31/03/2019','95600','48.9909940165','2.27800925488');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SALAISE BLUES FESTIVAL','Musiques actuelles','www.salaisebluesfestival.fr','SALAISE SUR SANNE','22/03/2019','06/04/2019','38150','45.3442511828','4.80860532784');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES EN MARS','Musiques actuelles','https://www.wattrelos-tourisme.com/fr/','WATTRELOS','08/03/2019','29/03/2019','59150','50.7056367285','3.21628221056');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de g�opolitique de Grenoble','Transdisciplinaire','https://www.festivalgeopolitique.com/','GRENOBLE','13/03/2019','16/03/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Reflets du cin�ma','Cin�ma et audiovisuel','http://www.lesrefletsducinema.com/','LAVAL','08/03/2019','19/03/2019','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ NATUREL','Musiques actuelles','http://www.mairie-orthez.fr/','ORTHEZ','05/03/2019','17/03/2019','64300','43.4943173399','-0.779874853399');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DE LA ROULOTTE','Pluridisciplinaire Spectacle vivant','www.lesnuitsdelaroulotte.com','CHAMBERY','01/03/2019','09/03/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BD � Bastia - Rencontres de la bande dessin�e & de l�illustration','Livre et litt�rature','http://una-volta.com/bd-a-bastia-2019/','BASTIA','04/04/2019','07/04/2019','20200','42.6864768806','9.42502133338');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival D�am�Bulle','Livre et litt�rature','www.festivalbdpornichet.com','PORNICHET','06/04/2019','07/04/2019','44380','47.2615791665','-2.31415650821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festirues','Cirque et Arts de la rue','http://www.morcenx.fr/Morcenx/Sports-culture/La-vie-culturelle','Morcenx','27/04/2019','28/04/2019','40110','44.0410936254','-0.888623636398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Printemps de Bourges','Musiques actuelles','www.printemps-bourges.com','BOURGES','16/04/2019','21/04/2019','18000','47.0749572013','2.40417137557');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival L''autre Cirque','Cirque et Arts de la rue','https://www.ladrome.fr/festival-lautre-cirque-0','Eurre','13/04/2019','14/04/2019','26400','44.758262486','4.97623533568');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOURNEE EUROPAVOX','Musiques actuelles','www.europavox.com','CLERMONT FERRAND','02/05/2019','11/05/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les 3 El�phants','Musiques actuelles','www.les3elephants.com','LAVAL','21/05/2019','26/05/2019','53000','48.0608565468','-0.766005687142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOCA FESTIVAL','Transdisciplinaire','http://le-moca.com/','PARIS','23/05/2019','25/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre d''Alen�on','Livre et litt�rature','www.salondulivrealencon.fr','ALENCON','18/05/2019','19/05/2019','61000','48.4318193082','0.0915406916107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SIROCCO','Transdisciplinaire','http://www.theatredelacomplicite.info/festival-sirocco/','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Coup de chauffe','Cirque et Arts de la rue','https://www.ville-cognac.fr/Coup-de-Chauffe-festival-des-arts-de-la-rue.html','COGNAC','','','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''N''FER','Musiques actuelles','www.legueulardplus.fr','UUCKANGE','','','57270','49.3058767055','6.14931216735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRIBAL FESTIVAL','Musiques actuelles','www.tribalroch.asso.fr','PEYMEINADE','','','6530','43.6305294544','6.88166520884');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Rias','Cirque et Arts de la rue','https://www.lefourneau.com/les-rias-2018.html','QUIMPERLE','','','29300','47.8561303355','-3.55642894655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FOLIESCENIES','Musiques actuelles','https://lesfoliescenies.com/','Salies du Salat','','','31260','43.1013456235','0.962933495319');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�SERTIVAL','Musiques actuelles','www.desertival.com','ILLE SUR TET','','','66130','42.6774223178','2.61639804475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DEMI FESTIVAL','Musiques actuelles','','SETE','','','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le festival de th��tre de Phalsbourg','Pluridisciplinaire Spectacle vivant','https://www.phalsbourg.fr/','PHALSBOURG','','','57370','48.7666597988','7.26359977418');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ZINZAN','Musiques actuelles','http://www.zinzan.festival.sitew.fr/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LONGEVITY FESTIVAL','Musiques actuelles','www.longevity-festival.com','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Silhouette � Festival de Courts en plein-air de Paris','Cin�ma et audiovisuel','www.association-silhouette.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du nouveau Th��tre Populaire','Th��tre','www.festivalntp.com','LES BOIS D ANJOU','','','49250','47.4829167789','-0.174198642479');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES AERIENNES','Musiques actuelles','https://www.lesaeriennes.com','MENDE','','','48000','44.5294508592','3.48086881176');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HOP POP HOP','Musiques actuelles','www.hoppophop.fr','ORLEANS','','','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''EFFET PAPILLON A MARTIGNAS SUR JALLE','Musiques actuelles','https://www.festival-effet-papillon.fr/','MARTIGNAS SUR JALLE','','','33127','44.8450267218','-0.795499117928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festi''Arts de la Rue','Cirque et Arts de la rue','https://festiartsdelarue.wixsite.com/festi-montoir','Montoir de Bretagne','','','44550','47.3232368527','-2.1467366036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AWARANDA','Musiques actuelles','http://awaranda.fr/','IGUERANDE','','','71340','46.2165272056','4.07699692762');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon International d''Art d''Argel�s-sur-mer','Arts plastiques et visuels','','ARGELES SUR MER','','','66700','42.5352193463','3.02429862885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A MONTERBLANC','Musiques actuelles','http://www.notesetmots.fr/','MONTERBLANC','','','56250','47.7315728601','-2.69647257441');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA VOIX A CHATEAUROUX','Pluridisciplinaire Musique','www.festivaldelavoix-chateauroux.fr','CHATEAUROUX','16/05/2019','19/05/2019','36000','46.8029617828','1.69399812001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLAP YOUR HANDS','Musiques actuelles','www.cafedeladanse.com','PARIS','08/05/2019','13/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VERSAILLES JAZZ FESTIVAL','Musiques actuelles','versaillesjazzfestival.fr/','VERSAILLES','14/05/2019','22/05/2019','78000','48.8025669671','2.11789297191');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PELPASS FESTIVAL','Musiques actuelles','https://pelpass.net/','STRASBOURG','16/05/2019','18/05/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RENDEZ VOUS DE SAINT LYPHARD','Cirque et Arts de la rue','http://www.tombesdelalune.com','La Ferte Bernard','23/05/2019','26/05/2019','72400','48.1842560568','0.634694806211');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CICADA FESTIVAL','Musiques actuelles','www.cercle-rouge.com','UZES','25/05/2019','25/05/2019','30700','44.0136590402','4.41600031556');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU TONTON','Musiques actuelles','https://www.festival-du-tonton.com','OYTIER ST OBLAS','24/05/2019','25/05/2019','38780','45.5646344835','5.03134323358');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NAMASTE A ASPRES SUR BUECH','Musiques actuelles','http://namasteasso05.wixsite.com','ASPRES SUR BUECH','28/06/2019','29/06/2019','5140','44.5617512104','5.77203298744');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN AIACCIU','Musiques actuelles','www.jazzinaiacciu.com','AJACCIO','25/06/2019','29/06/2019','20167','41.9347926638','8.70132275974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS NEW YORK HERITAGE FESTIVAL','Musiques actuelles','pnyhfestival.com','PARIS','12/06/2019','16/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'On a march� sur la bulle - Rendez-Vous de la Bande Dessin�e d''Amiens','Livre et litt�rature','http://bd.amiens.com/','AMIENS','01/06/2019','02/06/2019','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chapitre Nature','Transdisciplinaire','http://www.chapitrenature.com/','Le Blanc','07/06/2019','09/06/2019','36300','46.6346200347','1.10013221164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTS EN JAZZ A COURS ET JARDINS','Musiques actuelles','','LYON','07/06/2019','16/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DUB STATION FESTIVAL','Musiques actuelles','http://www.musicalriot.org','VITROLLES','28/06/2019','29/06/2019','13127','43.4497831674','5.26357787665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT-EMILION JAZZ FESTIVAL','Musiques actuelles','www.saint-emilion-jazz-festival.com','ST EMILION','28/06/2019','30/06/2019','33330','44.8999911844','-0.169955105989');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WORLDWIDE FESTIVAL','Musiques actuelles','www.worldwidefestival.com','SETE','28/06/2019','06/07/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE CHAMBORD','Musiques classiques','https://www.chambord.org','Chambord','28/06/2019','13/07/2019','41250','47.6160275858','1.54153082337');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ FESTIVAL DE MUNSTER','Musiques actuelles','www.jazzmunster.eu','MUNSTER','28/05/2019','01/06/2019','68140','48.0448514207','7.14149744282');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCKALISSIMO','Musiques actuelles','www.rockalissimo.com','ST AUBIN','14/06/2019','15/06/2019','39410','47.0326920185','5.33365017851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KEEP THE FAITH WEEKENDER','Musiques actuelles','http://www.lemoloco.com/keep-the-faith-weekender/','AUDINCOURT','14/06/2019','15/06/2019','25400','47.4811676376','6.85493983157');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Papillons de Nuit','Musiques actuelles','www.papillonsdenuit.com','ST Laurent de Cuves','07/06/2019','09/06/2019','50670','48.7494442333','-1.11520234761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SO GOOD FESTIVAL','Musiques actuelles','https://sogoodfest.com','Canejan','07/06/2019','08/06/2019','33610','44.7598891584','-0.65697711026');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GOUEL BRO LEON','Musiques actuelles','','Plouvorn','09/06/2019','09/06/2019','29420','48.5765978002','-4.02210161181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FERTE JAZZ','Musiques actuelles','www.fertejazz.com','La Ferte sous Jouarre','21/06/2019','23/06/2019','77260','48.9540164057','3.12762174384');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TEMPORA','Pluridisciplinaire Spectacle vivant','www.culture.legrandnarbonne.com','NARBONNE','','','11100','43.1652399898','3.02023868739');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CELTIVALES','Musiques actuelles','www.celtivales.com','PIERREFONTAINE LES VARANS','','','25510','47.2194158941','6.5503792835');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA CUVEE DE PARASSY','Musiques actuelles','http://festivaldeparassy.fr/','PARASSY','','','18220','47.2421364963','2.53814745107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Arte Mare - Festival du film m�diterran�en de Bastia','Cin�ma et audiovisuel','www.arte-mare.corsica','BASTIA','','','20200','42.6864768806','9.42502133338');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SONATES D''AUTOMNE','Musiques classiques','www.sonatesdautomne.fr','Beaulieu les Loches','','','37600','47.1281892877','1.02294110179');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Court M�trage d�Humour de Meudon','Cin�ma et audiovisuel','http://www.festivalmeudon.org','Meudon','','','92190','48.8037275796','2.22696287829');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ISSOUDUN REGGAE TEMPLE','Musiques actuelles','http://www.issoudun-reggaetemple.com','ISSOUDUN','','','36100','46.9489390293','2.00053790133');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SCENES D''AUTOMNE','Musiques actuelles','www.debroussaillons-lexpression.com','ST MAURICE LES BROUSSES','','','87800','45.6997312165','1.24152185921');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUS''ITERRANEE','Musiques actuelles','www.laboiteamus.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NAUTIC & MUSIC','Musiques actuelles','www.festival-nautic-music.org','BONIFACIO','','','20169','41.4354987425','9.18514890376');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NAMASTE A BORDEAUX','Musiques actuelles','http://namastefestival.canalblog.com/','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'En Ribambelle�!','Divers Spectacle Vivant','www.festivalenribambelle.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WORLDSTOCK','Musiques actuelles','www.worldstockfestival.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BERNIQUES EN FOLIE','Musiques actuelles','http://lesberniquesenfolie.fr/','Ile d''Yeu','','','85350','46.7093514816','-2.34712702345');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SON DE GLANE','Pluridisciplinaire Spectacle vivant','www.sondeglane.com','Oradour sur Glane','','','87520','45.9310728054','1.02303381376');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des cin�mas d''Afrique du Pays d''Apt','Cin�ma et audiovisuel','http://www.africapt-festival.fr/','APT','','','84400','43.879393265','5.38921757843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de cin�ma espagnol de Marseille CineHorizontes','Cin�ma et audiovisuel','http://cinehorizontes.com/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POSITIVE EDUCATION','Musiques actuelles','http://positiveeducation.fr/','ST ETIENNE','','','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Cin�matographiques de Dijon','Cin�ma et audiovisuel','www.rencontres-cinematographiques-de-dijon.fr','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTFORT BLUES FESTIVAL','Musiques actuelles','https://roazhonblues.wordpress.com/montfort-blues-festival/','Montfort sur Meu','','','35160','48.1261000349','-1.9630334162');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUJOURD''HUI MUSIQUES','Musiques classiques','http://www.aujourdhuimusiques.com','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL TRACE','Musiques actuelles','festivaltrace.com','CHAVILLE','12/01/2019','16/02/2019','92370','48.8076083562','2.19234061062');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Paris des femmes','Transdisciplinaire','www.parisdesfemmes.com','PARIS','10/01/2019','12/01/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HESTIV''OC','Musiques actuelles','www.hestivoc.com','PAU','23/08/2019','25/08/2019','64000','43.3200189773','-0.350337918181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rock en Seine','Musiques actuelles','www.rockenseine.com','PARIS','23/08/2019','25/08/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CHEMINEES DU ROCK','Musiques actuelles','http://leschemineesdurock.wixsite.com/festival','SAILLAT SUR VIENNE','16/08/2019','17/08/2019','87720','45.8704899446','0.834066842263');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rues Barr�es','Cirque et Arts de la rue','http://www.ot-auxerre.fr/','AUXERRE','17/08/2019','18/08/2019','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre D�une mer � l�autre','Livre et litt�rature','www.collioure.fr/fr','COLLIOURE','06/09/2019','08/09/2019','66190','42.5130107325','3.07254121467');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MANIFESTE','Musiques classiques','www.ircam.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de Pont Saint Esprit','Livre et litt�rature','www.festivallivrepont.fr','PONT ST ESPRIT','19/11/2019','24/11/2019','30130','44.2526637065','4.63901945786');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Caravane des cin�mas d�Afrique','Cin�ma et audiovisuel','https://cinemourguet.com/caravane','STE FOY LES LYON','','','69110','45.7359781395','4.79343173908');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES TOUCOULEURS','Musiques actuelles','www.toucouleurs.fr','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIG FUZZ','Musiques actuelles','www.jonzac-tourisme.com','JONZAC','','','17500','45.4419827405','-0.427614839205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Formula Bula','Livre et litt�rature','http://formulabula.fr/','PARIS','27/09/2019','29/09/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de Mouans Sartoux','Livre et litt�rature','http://www.lefestivaldulivre.fr/','Mouans Sartoux','04/10/2019','06/10/2019','6370','43.6185304023','6.96494426706');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Gaillac','Livre et litt�rature','www.salonlivregaillac.fr','GAILLAC','05/10/2019','06/10/2019','81600','43.9170968776','1.88647213801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAMPAGNOLE JAZZ FESTIVAL','Musiques actuelles','www.champagnole.fr','CHAMPAGNOLE','','','39300','46.7445389751','5.8988102947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Solstice, festival des arts du cirque d''Antony','Cirque et Arts de la rue','http://www.theatrefirmingemier-lapiscine.fr/','ANTONY','','','92160','48.7503412602','2.2993268102');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTS � GAHARD','Musiques actuelles','www.lesartsagahard.org','GAHARD','','','35490','48.2882041864','-1.52746285795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WANAGAIN FESTIVAL','Musiques actuelles','http://wanagain1.wixsite.com/wanagain','Clenay','','','21490','47.4135413138','5.11448988395');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIVESTIVAL','Cirque et Arts de la rue','www.fivestival.org','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'METIS PLAINE COMMUNE (FESTIVAL METIS DE SAINT DENIS)','Musiques actuelles','','ST DENIS','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BUCO''LICQUES','Musiques actuelles','www.bucolicques.org','LICQUES','','','62850','50.7962798733','1.9348768777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA VIE EN REUZ','Musiques actuelles','www.lavieenreuz.com','DOUARNENEZ','','','29100','48.0806526556','-4.32784220122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SABLE SHOW','Musiques actuelles','https://www.indeauville.fr/grands-rendez-vous-culture','VILLERS SUR MER','','','14640','49.3110756382','0.00599012303306');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CARRE D''AS','Musiques classiques','http://elixir.hautetfort.com/festival_carre_d_as/','CHARTRES','','','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LEGEND''AIR','Musiques actuelles','www.legendair.fr','Lesneven','','','29260','48.5801251196','-4.31293757639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK IN HELL','Musiques actuelles','www.livecolmar.com','COLMAR','','','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MILLESIME FESTIVAL WINTER','Musiques actuelles','https://lerocherdepalmer.fr/artistes/millesime.festival/','CENON','','','33150','44.8548325665','-0.521018807062');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELFONDUROCK','Musiques actuelles','www.marcoussis.fr','MARCOUSSIS','29/03/2019','30/03/2019','91460','48.6463321888','2.20621393661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film Policier de Beaune','Cin�ma et audiovisuel','www.beaunefestivalpolicier.com','Beaune','03/04/2019','07/04/2019','21200','47.0255189366','4.83767985985');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Guitare d�Aucamville et du Nord Toulousain','Musiques actuelles','www.guitareaucamville.com','AUCAMVILLE','14/03/2019','06/04/2019','31140','43.6713968117','1.42370910767');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FOUS D''ARCHET','Musiques actuelles','http://www.arpalhands.org','Colomiers','17/03/2019','13/04/2019','31770','43.611551508','1.32700218407');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VAND INFLUENCES','Musiques actuelles','www.vandinfluences.fr','VANDOEUVRE LES NANCY','09/03/2019','24/03/2019','54500','48.6579999356','6.16531229307');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANT SUR PAROLES','Musiques actuelles','http://www.ville-mably.fr/','MABLY','08/03/2019','20/03/2019','42300','46.0919456873','4.0633211833');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AI QUE BOM FESTIVAL','Musiques actuelles','www.aiquebom.com','PARIS','11/04/2019','14/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SKANK MY FEST','Musiques actuelles','http://www.ressourcerieduspectacle.fr/','VITRY SUR SEINE','13/04/2019','13/04/2019','94400','48.7882828307','2.39412680533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Ribambelle','Transdisciplinaire','https://www.champexquis.com/fr/','BLAINVILLE SUR ORNE','20/02/2019','20/03/2019','14550','49.2281536645','-0.304322423176');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES ARTS BURLESQUES','Divers Spectacle vivant','http://ntbeaulieu.fr/','ST ETIENNE','22/02/2019','02/03/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ENCHANTEURS','Musiques actuelles','http://www.festival-lesenchanteurs.com','AIX NOULETTE','22/02/2019','06/04/2019','62160','50.4212424085','2.71263968704');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ OR JAZZ','Musiques actuelles','http://jazzorjazz.fr/','ORLEANS','09/04/2019','13/04/2019','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du 9e art','Livre et litt�rature','http://www.bd-aix.com/','AIX EN PROVENCE','06/04/2019','25/05/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''Effet Sc�nes','Transdisciplinaire','https://www.scenes-nationales.fr/','PARIS','16/02/2019','16/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URBAINES','Pluridisciplinaire Spectacle vivant','www.urbaines.fr','RENNES','20/02/2019','10/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'G�N�RIQ FESTIVAL','Musiques actuelles','www.generiq-festival.com','DIJON','07/02/2019','10/02/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLIZZARD FESTIVAL','Musiques actuelles','','CHATEAUROUX','01/02/2019','02/02/2019','36000','46.8029617828','1.69399812001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TALANT INTERNATIONAL BLUES FESTIVAL','Musiques actuelles','www.jagoblues.com','TALANT','29/03/2019','12/04/2019','21240','47.3388300285','4.99779516001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dr�le de Carnaval','Cirque et Arts de la rue','http://www.arles-info.fr/tag/drole-de-carnaval/','ARLES','24/03/2019','24/03/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AGIT AL SOL','Musiques actuelles','www.musicalsol.fr/','Villegly','22/03/2019','24/03/2019','11600','43.2930208254','2.43699100253');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU SCHMOUL','Musiques actuelles','www.schmoulbrouk.com','BAIN DE BRETAGNE','25/01/2019','26/01/2019','35470','47.8302948158','-1.67649507218');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Folle Journ�e','Musiques classiques','www.follejournee.fr','NANTES','30/01/2019','03/02/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BEAUX JOURS DE LA MUSIQUE','Pluridisciplinaire Spectacle vivant','ville.biarritz.fr/festival-les-beaux-jours','BIARRITZ','19/04/2019','26/04/2019','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Page(s de printemps','Livre et litt�rature','www.pages-paris.com','PARIS','13/04/2019','13/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTIVAL BOBY LAPOINTE','Musiques actuelles','www.printivalbobylapointe.com','PEZENAS','23/04/2019','27/04/2019','34120','43.462990168','3.41651652125');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRASSES DU JEUDI','Musiques actuelles','www.terrassesdujeudi.fr','ROUEN','04/05/2019','25/07/2019','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DINARD COMEDY FESTIVAL (LES ESTIVALES DU RIRE)','Divers Spectacle vivant','http://www.ville-dinard.fr/','DINARD','26/04/2019','30/04/2019','35800','48.6241805945','-2.0619828606');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIEST''A SETE','Musiques actuelles','www.fiestasete.com','SETE','20/07/2019','06/08/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Francofolies de La Rochelle','Musiques actuelles','http://www.francofolies.fr/','LA ROCHELLE','10/07/2019','14/07/2019','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATOUT ARTS','Musiques actuelles','www.theatre-thouars.com','THOUARS','10/07/2019','13/07/2019','79100','46.9828628407','-0.199581467021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du documentaire de Marseille (FID)','Cin�ma et audiovisuel','www.fidmarseille.org','MARSEILLE','09/07/2019','15/07/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST IN RIBERAC','Musiques actuelles','https://www.festin.org','RIBERAC','12/07/2019','13/07/2019','24600','45.2457883325','0.334782848632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOZ''AIQUE','Musiques actuelles','https://mozaique.lehavre.fr/','LE HAVRE','17/07/2019','21/07/2019','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU FIL DU SON','Musiques actuelles','www.lachmiseverte.com','CIVRAY','25/07/2019','27/07/2019','86400','46.1412232275','0.300987364675');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU CHATEAU A SOLLIES PONT','Musiques actuelles','','Sollies Pont','24/07/2019','27/07/2019','83210','43.1907611248','6.06327697157');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU HAUT LIMOUSIN','Musiques classiques','http://www.festivalduhautlimousin.com','VILLEFAVARD','28/07/2019','15/08/2019','87190','46.1696846397','1.2141520992');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ECHAPPEES MUSICALES DU MEDOC','Musiques classiques','http://www.lesechappeesmusicales.fr/','BORDEAUX','29/07/2019','03/08/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DJANGO REINHARDT','Musiques actuelles','www.festivaldjangoreinhardt.com','SAMOIS SUR SEINE','04/07/2019','07/07/2019','77920','48.4550515717','2.75291155262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES JEUDIS DES MUSIQUES DU MONDE','Musiques actuelles','https://www.cmtra.org','VILLEURBANNE','04/07/2019','09/08/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMERICAN JAZZ FESTIV''HALLES','Musiques actuelles','www.sunset-sunside.com','PARIS','05/07/2019','21/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NOCTURNES SAINTE VICTOIRE A PEYNIER','Musiques classiques','https://lesnocturnessaintevictoire.fr/','Trets','02/07/2019','12/07/2019','13790','43.439686405','5.62516789037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Foreztival','Musiques actuelles','www.foreztival.com','TRELINS','02/08/2019','04/08/2019','42130','45.7251404881','4.00078301382');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK N POCHE','Musiques actuelles','','Habere Poche','02/08/2019','03/08/2019','74420','46.2555235551','6.47324374853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PYROCONCERTS DE TALLOIRES','Transdisciplinaire','http://www.talloires-lac-annecy.com','Talloires Montmin','08/08/2019','23/08/2019','74210','45.8357923857','6.23052089871');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Dansem','Danse','http://dansem.org/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du cirque de Massy','Cirque et Arts de la rue','https://www.cirque-massy.com/','MASSY','10/01/2019','13/01/2019','91300','48.7277896426','2.27513131903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE GRAND SOUFFLET','Musiques actuelles','www.legrandsoufflet.fr','Chartres de Bretagne','','','35131','48.0451561683','-1.70650297343');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOMENTS LYRIQUES DE CHARTRES ET D''EURE ET LOIR','Musiques classiques','www.journees-lyriques.com','CHARTRES','','','28000','48.4471464884','1.50570610616');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES JOURNEES RAVEL','Musiques classiques','www.lesjourneesravel.com','Montfort L''Amaury','','','78490','48.771347152','1.80894544858');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MAINTENANT','Transdisciplinaire','https://www.maintenant-festival.fr/2018/','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES DE TRAVERSE','Musiques actuelles','www.latraverse.org','CLEON','','','76410','49.316254089','1.03978678909');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIAM - FESTIVAL FOR UNLIMITED ART & MUSIC','Musiques actuelles','http://www.riam.info','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DU ROCK ET DES VACHES','Musiques actuelles','http://www.durocketdesvaches.com','ANDARD','','','49800','47.4219645317','-0.319673646947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SISMIC HIP HOP FESTIVAL','Musiques actuelles','http://www.lacoope.org','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BD EN PERIGORD','Livre et litt�rature','www.bd-bassillac.com','BASSILLAC','','','24330','45.1789592433','0.820956969026');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAKE SOME NOISE A CANNES','Musiques actuelles','https://fr-fr.facebook.com/makesomenoisefestival/','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATHENA BLUES FESTIVAL','Musiques actuelles','http://bluesinathena.wixsite.com/bluesinathena/blues-in-festival','ST Saulve','','','59880','50.3750172475','3.56642418912');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BANDITS-MAGES (FESTIVAL)','Transdisciplinaire','www.bandits-mages.com','BOURGES','','','18000','47.0749572013','2.40417137557');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEBOP FESTIVAL','Musiques actuelles','www.bebop-festival.com','LE MANS','','','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAMAIN FESTIVAL','Musiques actuelles','samainfest.bzh','LA MEZIERE','','','35520','48.2130666672','-1.75057215298');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIRE ONET','Divers Spectacle vivant','http://la-baleine.eu/','ONET LE CHATEAU','','','12850','44.3825530939','2.56000835023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Label Rue','Cirque et Arts de la rue','www.labelrue.fr/','RODILHAN','','','30230','43.825503937','4.43410669109');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NIMES METROPOLE JAZZ FESTIVAL','Musiques actuelles','http://agglo-jazz.nimes-metropole.fr/','NIMES','','','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DREAM NATION','Musiques actuelles','www.dreamnation.fr','CLICHY','','','92110','48.9035359139','2.30575230452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE LOUP VERT','Musiques actuelles','https://www.leloupvert-festival.com','JUMIEGES','','','76480','49.427073553','0.831659847193');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'34 TOURS','Musiques actuelles','http://www.herault.fr/34-tours','Gignac','','','34150','43.6393937216','3.57384628868');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres d''Averroes','Livre et litt�rature','http://www.rencontresaverroes.com/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ROCK CELTIQUE','Musiques actuelles','www.festivalrockceltique.fr','PLANCY L ABBAYE','','','10380','48.5851575322','3.98528853809');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZIC A NOUIC (ZICANOUIC)','Musiques actuelles','http://www.zicanouic.fr/','Nouic','','','87330','46.0620120225','0.915164140935');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUCS EN SCENE','Musiques actuelles','www.sucsenscene.fr','YSSINGEAUX','','','43200','45.1393761299','4.13104937731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES PIEDS DANS LA SAUCE','Musiques actuelles','http://www.lasauceruralsoundsystem.org','CIVRAY DE TOURAINE','','','37150','47.3394574162','1.04082548728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIMEIL BLUES FESTIVAL','Musiques actuelles','www.limeil-brevannes.fr/2eme-Limeil-Festival-Blues#.W3Uv-84zYdU','Limeil Brevannes','','','94450','48.7450357782','2.48936184357');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ART''SCENES','Musiques classiques','www.lesartscenes.fr','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HUMOUR BOEUF THEATRE','Divers Spectacle vivant','http://www.theatre-tribunal.fr/team-view/boeuf-theatre/','ANTIBES','','','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COLMAR JAZZ FESTIVAL','Musiques actuelles','www.festival-jazz.colmar.fr','COLMAR','','','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MERCI BONSOIR FESTIVAL','Pluridisciplinaire Spectacle vivant','www.mixarts.org/merci-bonsoir','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSES EN TROC','Cirque et Arts de la rue','www.muses-en-troc.org','Le Landreau','','','44430','47.2077664835','-1.29844309328');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEL AIR CLAVIERS FESTIVAL','Musiques classiques','www.rencontresbelair.com','CHAMBERY','','','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EUROCLASSIC','Musiques classiques','http://euroklassik.zweibruecken.de/','Hottviller','','','57720','49.0782664216','7.35808063345');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE FOLKLORE DE CH�TEAU-GOMBERT','Danse','www.roudelet-felibren.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL COURTSCOURTS 2018','Cin�ma et audiovisuel','www.courzik.fr','Tourtour','','','83690','43.5935800875','6.310682972');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LUPUL''IN METEREN','Musiques actuelles','http://lupulin.meteren.org/','METEREN','','','59270','50.7479005121','2.67634884839');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA POUTRE','Musiques actuelles','http://lapoutre.festival.free.fr/','Mauvaisin','','','31190','43.3594273416','1.53862839097');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Rues de l''Etang','Cirque et Arts de la rue','https://www.istres-tourisme.com/les-rues-de-l-etang.html#.W4oJWugzbIU','ISTRES','','','13118','43.5502689105','4.9511813524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIGRAT''S','Musiques actuelles','http://www.prazdelys-sommand.com/','TANINGES','','','74440','46.1265071577','6.60770716363');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TARBES EN TANGO','Musiques actuelles','http://tarbesentango.fr','TARBES','','','65000','43.2347859635','0.0660093937851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Blues en Loire','Musiques actuelles','','La Charite sur Loire','','','58400','47.1835862006','3.02885515594');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DES CHAVANS','Musiques actuelles','www.lachavannee.com','CHATEAU SUR ALLIER','','','3320','46.7817092789','2.98716115012');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Boudu la jongle','Cirque et Arts de la rue','','Gagnac sur Garonne','','','31150','43.7075932093','1.36359468155');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Belledonne en Cirque','Cirque et Arts de la rue','','Revel','','','38420','45.1681206613','5.90991149845');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZELLERAULT','Musiques actuelles','www.festival-jazzellerault.com','CHATELLERAULT','16/05/2019','24/05/2019','86100','46.8156700185','0.552598976936');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Forum du livre de Saint-Louis','Livre et litt�rature','http://forumlivre.fr/','ST LOUIS','10/05/2019','12/05/2019','68300','47.6016553367','7.54061872745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'� Mauvais Buisson','Musiques actuelles','http://www.omauvaisbuisson.fr/','MAUMUSSON','25/05/2019','25/05/2019','44540','47.4827042599','-1.11902046549');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE GUITARE DE LAMBESC','Pluridisciplinaire Musique','http://www.festivalguitare-lambesc.com','Lambesc','24/06/2019','29/06/2019','13410','43.6615217681','5.25191446931');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONTINENTS ET CULTURES','Musiques actuelles','www.ccab.fr','VILLEFRANCHE SUR SAONE','22/06/2019','26/07/2019','69400','45.9874596667','4.73174007305');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES FOR�TS','Musiques classiques','www.festivaldesforets.fr','COMPIEGNE','21/06/2019','17/07/2019','60200','49.3990601478','2.85317249363');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d�An�res - un festival de cin�ma muet dans les Hautes-Pyr�n�es','Cin�ma et audiovisuel','http://www.festival-aneres.fr','Aneres','05/06/2019','09/06/2019','65150','43.0760669312','0.464444450073');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LACHER D''ARTISTES','Musiques actuelles','http://www.manufacturechanson.org','PARIS','04/06/2019','08/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOSGES MOTO ESTIVAL','Transdisciplinaire','http://www.vosgesmotoestival.com/','ST Die des Vosges','07/06/2019','09/06/2019','88100','48.2967093514','6.93809367547');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ESTIVAL DE TRELAZE','Musiques actuelles','http://www.trelaze.fr/','TRELAZE','20/06/2019','31/08/2019','49800','47.451357179','-0.473458788346');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Montmorillon','Livre et litt�rature','http://www.salondulivredemontmorillon.com/','Montmorillon','15/06/2019','16/06/2019','86500','46.4263978846','0.895458347632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTICULE FESTIVAL','Musiques actuelles','www.monticulefestival.com','ST Jean de Laur','19/06/2019','23/06/2019','46260','44.4301259055','1.83118318259');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST INVENTIO','Musiques classiques','https://www.inventio-music.com/','PROVINS','16/06/2019','29/06/2019','77160','48.5633093312','3.28756780085');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PAY''TA TONG','Pluridisciplinaire Spectacle vivant','http://www.paytatong.com','LA FERRIERE','08/06/2019','09/06/2019','85280','46.7215872927','-1.33469332327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DYNAMIC','Musiques actuelles','www.lanuitelectro.org','MONTLUCON','08/06/2019','09/06/2019','3100','46.3385883496','2.60390499777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AUX SOURCES','Musiques actuelles','http://www.jazz-aux-sources.com','CHATEL GUYON','07/06/2019','10/06/2019','63140','45.9206993185','3.0632143777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Avoine Zone Groove','Musiques actuelles','www.avoinezonegroove.fr','AVOINE','28/06/2019','30/06/2019','37420','47.2237767737','0.18754463005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nuits Atypiques','Musiques actuelles','www.nuitsatypiques.org','Langon','01/07/2019','21/07/2019','33210','44.7425058294','0.160918010334');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES D''ICI ET D''AILLEURS','Musiques actuelles','www.musiques-ici-ailleurs.com','CHALONS EN CHAMPAGNE','29/06/2019','28/07/2019','51000','48.9640892125','4.37883539725');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Agrock','Musiques actuelles','http://festival-agrock.fr/Views/home.php','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WOODSTOWER','Musiques actuelles','www.woodstower.com','LYON','29/08/2019','01/09/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COLOR SUMMER FESTIVAL','Musiques actuelles','','Frejus','16/08/2019','16/08/2019','83600','43.4719558114','6.76361597424');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Mondial des Th��tres de Marionnette','Divers Spectacle vivant','http://www.festival-marionnette.com/fr/','CHARLEVILLE MEZIERES','20/09/2019','29/09/2019','8000','49.7752965803','4.71724655966');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES VENTS','Musiques actuelles','http://www.festivaldesvents.com','MORIERES LES AVIGNON','30/08/2019','01/09/2019','84310','43.9337788848','4.90875878315');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du Cirque du Val d''Oise','Cirque et Arts de la rue','http://www.cirqueduvaldoise.fr','Domont','04/10/2019','06/10/2019','95330','49.0296655684','2.32290103729');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BERNARD DIMEY','Musiques actuelles','http://festival-bernard-dimey.fr/','Nogent','','','52800','48.037521428','5.36502804198');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''OREILLE ABSOLUE FAIT SON CIRQUE','Pluridisciplinaire Spectacle vivant','www.loreilleabsoluefaitsoncirque.fr','MICHELBACH LE HAUT','','','68220','47.5627694258','7.43780573745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARBARA','Musiques actuelles','www.diapason-saint-marcellin.fr','ST MARCELLIN','','','38160','45.1534136901','5.31720171085');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AND BLUES FESTIVAL DE LEOGNAN','Musiques actuelles','www.jazzandblues-leognan.fr','LEOGNAN','','','33850','44.7191460436','-0.614707683753');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Le Court en dit long - Paris','Cin�ma et audiovisuel','www.cwb.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Togaether','Transdisciplinaire','http://www.togaether.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Contre-Temps','Musiques actuelles','','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Arr�te ton Cirque','Cirque et Arts de la rue','','Paimpont','','','35380','48.030192406','-2.17342058709');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ FOR VILLE','Musiques actuelles','www.lepoc.fr','ALFORTVILLE','','','94140','48.7960843864','2.42124593241');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES FESTIVES DE L''EPICYCLE','Musiques actuelles','','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FORT-DE-FRANCE','Pluridisciplinaire Spectacle vivant','','FORT DE FRANCE','','','97234','14.6411114389','-61.0691889244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BORDEAUX CITE TANGO FESTIVAL','Musiques actuelles','http://www.bordeaux-tango-festival.com/?lang=fr','BORDEAUX','07/05/2019','12/05/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES ENTRE LES MONDES','Pluridisciplinaire Spectacle vivant','www.associationdeviation.fr','CHABEUIL','10/05/2019','11/05/2019','26120','44.9082255842','5.00743034189');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DEMIGNY ON THE ROCK','Musiques actuelles','http://demignyontherock.weebly.com','Demigny','10/05/2019','11/05/2019','71150','46.9145180669','4.84793098636');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMANAD''ARTS','Transdisciplinaire','https://www.facebook.com/204372450492559/photos/gm.2406403782979224/272600500336420/?type=3&theater','ALBI','24/05/2019','26/05/2019','81000','43.9258213622','2.14686328555');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSI''COLORS','Musiques actuelles','musi-colors.com','CHASSE SUR RHONE','25/05/2019','26/05/2019','38670','45.5797669773','4.81420294395');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Arts dans la Rue de Valleraugue','Cirque et Arts de la rue','https://www.villagenda.com/events/fr/gard/valleraugue','Valleraugue','25/05/2019','26/05/2019','30570','44.0875534961','3.61946476562');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VEYRACOMUSIES','Pluridisciplinaire Spectacle vivant','www.veyracom','VEYRAC','29/05/2019','01/06/2019','87520','45.9034537819','1.09001899064');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN COMMINGES','Musiques actuelles','www.jazzencomminges.com','ST GAUDENS','29/05/2019','02/06/2019','31800','43.1199260811','0.726613564199');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'T�t ou t''Arts','Cirque et Arts de la rue','https://www.festivaltotoutarts.com/','GEX','05/06/2019','15/06/2019','1170','46.3471891747','6.04650555568');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIMU (FESTIVAL INTERNATIONAL DE MUSIQUE UNIVERSITAIRE)','Musiques actuelles','http://www.fimu.com/','BELFORT','06/06/2019','10/06/2019','90000','47.6471539414','6.84541206237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�antibulations','Cirque et Arts de la rue','http://www.acla06.com/','ANTIBES','06/06/2019','09/06/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BERZYK','Musiques actuelles','http://www.berzyk.fr/','BERZY LE SEC','08/06/2019','08/06/2019','2200','49.3172125296','3.29666086699');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JAZZ AND BLUES DE THOMERY','Musiques actuelles','','Thomery','08/06/2019','08/06/2019','77810','48.4042073432','2.77997290314');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vox, Festival urbain de lecture � voix haute et du livre audio','Livre et litt�rature','http://www.festivalvox.com/','MONTREUIL','11/06/2019','16/06/2019','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''ROCHE','Musiques actuelles','www.festiroche.com','ROCHE LA MOLIERE','13/06/2019','16/06/2019','42230','45.4259230436','4.32495559821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MARSEILLE','Pluridisciplinaire Spectacle vivant','www.festivaldemarseille.com','MARSEILLE','14/06/2019','07/07/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TERRASSE ECAM FESTIVAL','Musiques actuelles','','LYON','15/06/2019','16/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GOD SAVE THE KOUIGN','Musiques actuelles','','Penmarch','15/06/2019','15/06/2019','29760','47.8121478804','-4.34003367039');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIEILLES �CLUSES','Musiques actuelles','vieilles.ecluses.fre','CARRIERES SOUS POISSY','15/06/2019','16/06/2019','78955','48.9455638989','2.02913706544');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Camping (Pantin)','Danse','https://www.cnd.fr/fr/page/33-camping','PANTIN','17/06/2019','28/06/2019','93500','48.8983093876','2.40872147475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUPERSPECTIVES','Musiques classiques','https://www.facebook.com/superspectives/','LYON','20/06/2019','14/07/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MINUIT AVANT LA NUIT','Musiques actuelles','','AMIENS','21/06/2019','23/06/2019','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA DEFENSE JAZZ FESTIVAL','Musiques actuelles','www.ladefensejazzfestival.hauts-de-seine.net','PUTEAUX','24/06/2019','30/06/2019','92800','48.8837094969','2.23834178314');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE MUSIQUE SACREE DE LOURDES','Musiques classiques','www.festivaldelourdes.fr','Lourdes','13/04/2019','22/04/2019','65100','43.1074297344','-0.0768069387932');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Pr�sences','Musiques classiques','https://www.maisondelaradio.fr/festival-presences-2019','PARIS','12/02/2019','17/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Premiers plans d''Angers','Cin�ma et audiovisuel','http://www.premiersplans.org/','ANGERS','25/01/2019','03/02/2019','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS GIVREES','Musiques actuelles','aqueduc.dardilly.fr/','DARDILLY','01/02/2019','02/02/2019','69570','45.8120018662','4.74663974001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dr�le d�endroit pour des rencontres � Rencontres du cin�ma fran�ais de Bron','Cin�ma et audiovisuel','www.cinemalesalizes.com','BRON','25/01/2019','27/01/2019','69500','45.7344856902','4.91168159471');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du cirque de Bayeux','Cirque et Arts de la rue','www.festivalcirquebayeux.fr','BAYEUX','27/03/2019','31/03/2019','14400','49.2776559195','-0.704625495378');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA COMEDIE DU RIRE','Divers Spectacle vivant','http://www.montpellier3m.fr/comedie-du-rire-2019','MONTPELLIER','19/03/2019','17/04/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Mars Plan�te Danse','Danse','http://www.avantscene.com','COGNAC','21/03/2019','30/03/2019','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRE DE JAZZ A POUCHARRAMET','Musiques actuelles','https://www.lamaisondelaterre.fr/','POUCHARRAMET','06/04/2019','14/04/2019','31370','43.423058709','1.16684453964');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESCALE''N''JAZZ','Musiques actuelles','http://www.lavilledubois.fr/','La Ville du Bois','05/04/2019','07/04/2019','91620','48.6610194975','2.26503430984');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du chant traditionnel','Musiques actuelles','www.epille.com','BOVEL','12/04/2019','14/04/2019','35330','47.9563331223','-1.95993951204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SPRING REGGAE FEST','Musiques actuelles','www.irieites.net','ARNAGE','09/03/2019','09/03/2019','72230','47.9292220095','0.191344248929');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Settimana teatrale','Th��tre','http://www.bastia.corsica/fr','BASTIA','08/03/2019','14/03/2019','20200','42.6864768806','9.42502133338');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRACES CONTEMPORAINES','Danse','www.tracescontemporaines.com/accueil','CAHORS','','','46000','44.4507370916','1.44075837848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Blues Rock Festival','Musiques actuelles','http://www.bluesrockfestival.fr/','CHATEAURENARD','','','13160','43.8824707581','4.83990004648');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS ELECTRONIC WEEK','Musiques actuelles','www.pariselectronicweek.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CIRCUIT','Musiques actuelles','http://www.confort-moderne.fr/fr/','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRIBU FESTIVAL','Musiques actuelles','www.tribufestival.com','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVENDANGES','Musiques actuelles','http://www.festivendanges.com','SANCERRE','','','18300','47.3256994042','2.82156691385');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WE WILL FOLK YOU','Musiques actuelles','www.4ecluses.com','DUNKERQUE','','','59430','51.0307229078','2.33752414095');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK IN THE BARN','Musiques actuelles','www.rockinthebarn.fr','GIVERNY','','','27620','49.0819438915','1.53288773518');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES LARMES DU RIRE','Pluridisciplinaire Spectacle vivant','www.tourisme-epinal.com/sejourner/agenda/festival-les-larmes-du-rire-281510','EPINAL','','','88000','48.1631202656','6.47989286928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Emergences','Arts plastiques et visuels','https://biennale-emergences.fr/accueil','PANTIN','','','93500','48.8983093876','2.40872147475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA JIMI','Musiques actuelles','www.jimifestivaldemarne.org','IVRY SUR SEINE','','','94200','48.8128063812','2.38778023612');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � LA SOUT','Musiques actuelles','www.jazz.lasout.com','GUERET','','','23000','46.1632121428','1.87078672735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE FESTIF DE GANAM','Musiques actuelles','www.gasny.fr/','GASNY','','','27620','49.1020218463','1.59975187173');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU FILM D''ANIMATION POUR LA JEUNESSE 2018','Cin�ma et audiovisuel','','BOURG EN BRESSE','','','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NEVERS A VIF','Musiques actuelles','http://www.nevers-a-vif.fr/','NEVERS','','','58000','46.9881194908','3.15689130958');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCKOMOTIVES','Musiques actuelles','www.rockomotives.com','VENDOME','','','41100','47.8013026692','1.06106057175');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Animastar','Cin�ma et audiovisuel','http://www.cinema-star.com/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du film de Saint-Jean-de-Luz','Cin�ma et audiovisuel','www.fifsaintjeandeluz.com','ST Jean de Luz','','','64500','43.3947886873','-1.63099021573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUR DE CHAUFFE','Musiques actuelles','www.tourdechauffe.fr','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival TNB','Pluridisciplinaire Spectacle vivant','https://www.t-n-b.fr/programmation/spectacles/festival-tnb','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DES MUSIQUES JUIVES DE LYON','Musiques actuelles','www.espace-hillel.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROUEN REGGAE TOWN','Musiques actuelles','','LE GRAND QUEVILLY','','','76120','49.4093798045','1.03999262153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL RUE(Z)VOUS','Cirque et Arts de la rue','http://www.ville-valbonne.fr/','VALBONNE','','','6560','43.628288325','7.02954476696');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOI(X) L� L''�T� � EPERNAY','Musiques actuelles','www.epernay.fr','EPERNAY','','','51200','49.037000689','3.93144626202');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Grand Festival','Pluridisciplinaire Spectacle vivant','','VERDUN','','','55100','49.1454831903','5.36141821261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Faltaisies','Cirque et Arts de la rue','http://www.paysdefalaise.fr/agenda/les-faltaisies/','FALAISE','','','14700','48.8957800281','-0.193401711782');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festi-Jazz','Musiques actuelles','','ST Cezaire sur Siagne','','','6530','43.6594259137','6.80329376165');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HIX','Musiques actuelles','http://www.bourg-madame.fr/','Bourg Madame','','','66760','42.4379971277','1.96090815348');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sc�ne de cirque','Cirque et Arts de la rue','http://www.scenedecirque.fr/','Puget Theniers','','','6260','43.9521649219','6.90609311435');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Font''Arts','Cirque et Arts de la rue','http://www.perneslesfontaines.fr/evenement/festival-font%27arts-2018','Pernes les Fontaines','','','84210','43.9960550539','5.03959100234');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DE COARTJAZZ','Musiques actuelles','www.coartjazz.fr','COARAZE','','','6390','43.8664901786','7.29913349222');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES EN CHAMPAGNE','Musiques actuelles','www.ccgvm.com','HAUTVILLERS','','','51160','49.085558433','3.94419700261');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ENDEMIK FESTIVAL','Musiques actuelles','http://www.endemik.fr/','CORRENS','','','83570','43.4864185627','6.06826217623');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 40 en paires','Pluridisciplinaire Spectacle vivant','https://www.asso-entracte.fr/festival-40-en-paires-2/','Mugron','','','40250','43.7412885635','-0.752690282166');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d''art contemporain d''Anglet','Arts plastiques et visuels','http://www.lalittorale.anglet.fr/','ANGLET','','','64600','43.4917846595','-1.51623373371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�tobourg','Cirque et Arts de la rue','http://www.ville-mably.fr/fr/actualite/148724/la-fetobourg-2018','MABLY','','','42300','46.0919456873','4.0633211833');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SOLISTES � BAGATELLE','Musiques classiques','www.ars-mobilis.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ENFER FESTIVAL','Transdisciplinaire','http://lenferfestival.blogspot.fr/','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VINO VOCE','Pluridisciplinaire Spectacle vivant','www.festivalvinovoce.com','ST EMILION','','','33330','44.8999911844','-0.169955105989');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIPOP','Musiques actuelles','http://www.festipop.org','FRONTIGNAN','','','34110','43.4482458873','3.74923522416');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU COIN DE MA RUE','Cirque et Arts de la rue','https://fr-fr.facebook.com/aucoindemarue/','ROMILLE','','','35850','48.2223488257','-1.87904298142');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTUAIRE D''EN RIRE','Divers Spectacle vivant','www.estuairedenrire.com','HONFLEUR','','','14600','49.4129449727','0.237579361279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l��crit � l��cran','Cin�ma et audiovisuel','www.actes-en-drome.fr','Montelimar','','','26200','44.5540845734','4.74869387765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR L''HERBE / ANGLET JAZZ FESTIVAL','Musiques actuelles','www.arcad64.fr/','ANGLET','','','64600','43.4917846595','-1.51623373371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK IN REBRECH','Musiques actuelles','http://www.rebrechien.fr/rock-in-rebrech/','REBRECHIEN','29/06/2019','30/06/2019','45470','47.9935958151','2.02397089801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIVALES EN SAVOIE','Musiques actuelles','www.estivalesensavoie.fr','CHAMBERY','02/07/2019','18/07/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vercors Music Festival','Musiques actuelles','www.vercorsmusicfestival.com','Autrans Meaudre en Vercors','04/07/2019','07/07/2019','38880','45.1321769623','5.52869303726');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LOUD & PROUD','Musiques actuelles','https://gaite-lyrique.net','PARIS','04/07/2019','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TAWA','Musiques actuelles','http://latawa.fr/','PLANFOY','05/07/2019','06/07/2019','42660','45.3861155626','4.431442219');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RHINOFEROCK','Musiques actuelles','www.rhinoferock-festival.com','Pernes Les Fontaines','05/07/2019','06/07/2019','84210','43.9960550539','5.03959100234');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rues D''�t� de Graulhet','Cirque et Arts de la rue','www.ruesdete.fr','Graulhet','12/07/2019','13/07/2019','81300','43.7576562195','2.00122512851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COCORICO ELECTRO','Musiques actuelles','','La Ferte ST Aubin','12/07/2019','13/07/2019','45240','47.7119750833','1.92714666074');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Zic � Remparts','Musiques actuelles','http://www.ville-guerande.fr/','Guerande','13/07/2019','13/07/2019','44350','47.3313320913','-2.41703658813');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''ISSOIRE','Pluridisciplinaire Spectacle vivant','www.festival-issoire.fr','ISSOIRE','16/07/2019','21/07/2019','63500','45.5455577894','3.24499324643');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES ARCS','Musiques classiques','https://www.festivaldesarcs.com','BOURG ST MAURICE','17/07/2019','02/08/2019','73700','45.6647980545','6.76637903907');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rock Ici M�mes','Musiques actuelles','www.lentracte-sable.fr','SABLE SUR SARTHE','18/07/2019','18/07/2019','72300','47.8378265007','-0.354705885665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''AMBERT (LA RONDE DES COPAINS DU MONDE)','Musiques actuelles','www.festival-ambert.fr','AMBERT','19/07/2019','20/07/2019','63600','45.5557839461','3.75625356567');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE THEATRE DE FIGEAC','Th��tre','http://festivaltheatre-figeac.com/','FIGEAC','23/07/2019','02/08/2019','46100','44.6055314625','2.02540303775');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Textes en l''air','Transdisciplinaire','http://www.textesenlair.net','ST ANTOINE L ABBAYE','24/07/2019','28/07/2019','38160','45.1722353559','5.21609050981');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MOUREZE (MOUREZE FAIT SON CIRQUE)','Pluridisciplinaire Spectacle vivant','www.festivaldemoureze.com','MOUREZE','24/07/2019','27/07/2019','34800','43.6156849322','3.35101426907');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REGGAE SUN SKA','Musiques actuelles','http://www.reggaesunska.com','Vertheuil','02/08/2019','04/08/2019','33180','45.2541334712','-0.845409908523');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HOOP FESTIVAL','Musiques actuelles','https://hoopfestival.fr/','Excideuil','09/08/2019','10/08/2019','24160','45.3293054433','1.06217894627');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de montagne � Alpes�pages �','Livre et litt�rature','www.salon-livre-montagne.com','Passy','09/08/2019','11/08/2019','74190','45.9545838758','6.74056895873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'IMPERIAL LIVE FESTIVAL','Musiques actuelles','http://www.hotel-imperial-palace.com','ANNECY','14/08/2019','29/08/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KER ZION','Musiques actuelles','http://www.kerzion.com','Guisseny','16/08/2019','17/08/2019','29880','48.6169101259','-4.41110953523');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES CARRIERES SAINT ROCH','Pluridisciplinaire Musique','www.carrieres-st-roch.fr','Luzarches','24/08/2019','25/08/2019','95270','49.1151211125','2.44082640954');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre de Chaumont','Livre et litt�rature','www.salondulivrechaumont.fr','Chaumont','22/11/2019','24/11/2019','52000','48.0980144211','5.14070044621');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''N SOLEX','Musiques actuelles','http://rocknsolex.fr','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VOIX DU PRIEURE','Pluridisciplinaire Musique','http://www.voixduprieure.fr/','Le Bourget','','','73370','45.6441486088','5.84622475132');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CLUNY DANSE','Danse','http://festivalclunydanse.com','Cluny','','','71250','46.4303628582','4.67033276327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOURCHETTES SONIQUES','Musiques actuelles','http://www.fourchettes-soniques.com','TOURS','','','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARFUMS DE MUSIQUES','Musiques actuelles','http://www.adiam94.org','L Hay Les Roses','','','94240','48.7763207843','2.33772915609');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l�Acharni�re','Cin�ma et audiovisuel','http://festival.lacharniere.free.fr/','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chahuts','Pluridisciplinaire Spectacle vivant','www.festivaldechaillol.com','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES CULTURES JUIVES','Transdisciplinaire','http://www.festivaldesculturesjuives.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE LAVOIR �LECTRIQUE','Musiques actuelles','www.lelavoirelectrique.com','VOUNEUIL SOUS BIARD','','','86580','46.5792555716','0.270706459664');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHINON EN JAZZ','Musiques actuelles','www.petitfaucheux.fr','CHINON','','','37500','47.1723001595','0.245084664558');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMITI�','Pluridisciplinaire Spectacle vivant','www.festival-amitie.com','ALTKIRCH','','','68130','47.6210336538','7.24484329622');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES ANCHES D''AZUR','Musiques actuelles','www.festivaldesanchesdazur.jimdo.com','LA CROIX VALMER','','','83420','43.1951890718','6.5862813598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUIT DU BLUES FESTIVAL','Musiques actuelles','www.jazzandblues-leognan.fr','LEOGNAN','','','33850','44.7191460436','-0.614707683753');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VARIATIONS CLASSIQUES - FESTIVAL MUSICAL D''ANNECY','Musiques classiques','www.variations-classiques.com','ANNECY','28/08/2019','31/08/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES ACTUELLES DES TRUCA TAOULES','Musiques actuelles','http://www.truca-taoules.com','MONTGAILLARD','23/08/2019','25/08/2019','11330','42.9046014463','2.62879585851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL RAVEL EN NOUVELLE AQUITAINE','Musiques classiques','www.festivalravel.fr','ST JEAN DE LUZ','25/08/2019','15/09/2019','64500','43.3947886873','-1.63099021573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL 666','Musiques actuelles','www.festival666.com','Cercoux','24/08/2019','24/08/2019','17270','45.1273503043','-0.207698631448');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES EN VIVARAIS-LIGNON','Musiques classiques','www.cc-hautlignon.fr','TENCE','15/08/2019','28/08/2019','43200','45.1393761299','4.13104937731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HESTEJADA DE LAS ARTS UZESTE MUSICAL','Musiques actuelles','http://www.uzeste.org','UZESTE','17/08/2019','24/08/2019','33730','44.4334139021','-0.324575198234');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les correspondances de Manosque','Livre et litt�rature','www.correspondances-manosque.org','MANOSQUE','25/09/2019','29/09/2019','4100','43.8354040831','5.79106654173');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival litt�raire Encres vives','Livre et litt�rature','www.salondulivreprovins.fr','PROVINS','11/11/2019','17/11/2019','77160','48.5633093312','3.28756780085');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La 25e Heure du livre','Livre et litt�rature','www.la25eheuredulivre.fr','LE MANS','11/10/2019','13/10/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLAY MOBILE','Cirque et Arts de la rue','http://www.theatreachatillon.com/play-mobile.php','CHATILLON','','','92320','48.8034091756','2.28799131883');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DU TANGO','Musiques actuelles','www.leprintempsdutango-mulhouse.fr','MULHOUSE','','','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival D�sARTicul�','Cirque et Arts de la rue','https://desarticule.fr/festival/','VITRE','','','35500','48.1140815063','-1.19370720718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'METALGRESIFEST','Musiques actuelles','http://www.metalgresifest.com','ST VINCENT DE MERCUZE','','','38660','45.3749280893','5.95880834631');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Plein la Bobine','Cin�ma et audiovisuel','www.pleinlabobine.com','LA BOURBOULE','','','63150','45.5796236201','2.7501805247');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ACCORD�ONS-NOUS � TRENTELS','Musiques actuelles','www.accordeonsnousatrentels.com','TRENTELS','','','47140','44.4456964361','0.863931953648');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Fadoli�s Circus','Cirque et Arts de la rue','http://www.zimzamcircus.org','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz et musique improvis�e en Franche-Comt�','Musiques actuelles','http://www.aspro-impro.fr/','BESANCON','','','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz Escales � Ouistreham','Musiques actuelles','www.jazzcaen.com','OUISTREHAM','','','14150','49.2745280107','-0.257718317859');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Grand Bain Festival des Arts Nomades','Cirque et Arts de la rue','','Cran Gevrier','','','74960','45.9017933456','6.09977672068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN AIR DE VOYAGE','Musiques actuelles','http://www.premier-dragon.com','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIVRY ALL JAZZ''N''BLUES','Musiques actuelles','www.livry-gargan.fr','LIVRY GARGAN','','','93190','48.9197633213','2.53486592332');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Artefacts','Musiques actuelles','www.artefact.org','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ap�roche - Hip-Hop Garden Party','Musiques actuelles','','MONTREUIL','','','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Calvi Jazz','Musiques actuelles','','CALVI','','','20260','42.5454874755','8.7595487404');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FLAMENCO DE N�MES','Musiques actuelles','www.theatredenimes.com','NIMES','11/01/2019','19/01/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GLISSE EN COEUR','Musiques actuelles','http://2017.glisseencoeur.com','LE GRAND BORNAND','22/03/2019','24/03/2019','74450','45.9562174482','6.47641784295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SPRING','Cirque et Arts de la rue','www.festival-spring.eu','ELBEUF','01/03/2019','05/04/2019','76500','49.2767979948','0.997154590875');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PINK PARADIZE','Pluridisciplinaire Spectacle vivant','https://pinkparadizefestival.com/','RAMONVILLE ST AGNE','02/03/2019','13/04/2019','31520','43.5441837911','1.47782372823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pulp festival','Livre et litt�rature','www.lafermedubuisson.com','NOISIEL','05/04/2019','28/04/2019','77186','48.8460926469','2.61997100426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL A CORPS','Danse','www.festivalacorps.com','POITIERS','05/04/2019','12/04/2019','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE ET CULTURE','Pluridisciplinaire Musique','www.printemps-colmar.com','COLMAR','04/04/2019','18/04/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES DETOURS DE BABEL','Pluridisciplinaire Musique','www.detoursdebabel.fr','GRENOBLE','15/03/2019','07/04/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT MANDE CLASSIC JAZZ FESTIVAL','Musiques actuelles','www.saint-mande-festival.com','ST MANDE','22/03/2019','24/03/2019','94160','48.842440243','2.4192191088');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICA(E)L','Musiques actuelles','http://caelmjc.com','BOURG LA REINE','27/03/2019','06/04/2019','92340','48.7799073627','2.31643010763');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES INTERNATIONALES DE LA GUITARE A ANTONY','Pluridisciplinaire Musique','www.ville-antony.fr','ANTONY','27/03/2019','31/03/2019','92160','48.7503412602','2.2993268102');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CURIEUX VOYAGEURS','Cin�ma et audiovisuel','http://www.curieuxvoyageurs.com/','ST ETIENNE','29/03/2019','31/03/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'S�quence Danse Paris','Danse','http://www.104.fr/fiche-evenement/sequence-danse-paris-2018.html','PARIS','13/04/2019','21/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIPADOC','Cin�ma et audiovisuel','www.fipa.tv','BIARRITZ','22/01/2019','27/01/2019','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ROUE WAROCH','Musiques actuelles','www.roue-waroch.fr','PLESCOP','15/02/2019','17/02/2019','56890','47.6973490259','-2.83080852214');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE DE COMPANS','Musiques actuelles','www.mairiedecompans.fr','Compans','08/02/2019','09/02/2019','77290','48.9892298685','2.6561103822');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Petites Bobines','Cin�ma et audiovisuel','https://www.auxerre.fr/Animee/Agenda-des-sorties/Cinema/Les-P-tites-Bobines','AUXERRE','25/02/2019','03/03/2019','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VINICIRCUS FESTIVAL','Transdisciplinaire','http://www.vinicircus.com','Guipel','12/04/2019','14/04/2019','35440','48.2923904516','-1.72349623823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre d''Autun','Livre et litt�rature','https://www.lireenpaysautunois.fr/','AUTUN','06/04/2019','07/04/2019','71400','46.945536773','4.31060069532');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIVE DES BRUMES','Musiques actuelles','http://www.lesbrumes.fr/','STE SIGOLENE','05/07/2019','06/07/2019','43600','45.2470426213','4.22851598334');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JAZZ A VAUVERT','Musiques actuelles','','Vauvert','05/07/2019','06/07/2019','30600','43.6258468531','4.30597093294');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Chaillol','Pluridisciplinaire Musique','http://www.festivaldechaillol.com','ST Michel de Chaillol','17/07/2019','12/08/2019','5260','44.6888900905','6.16982010891');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film de Vebron','Cin�ma et audiovisuel','www.festivalvebron.fr','Vebron','16/07/2019','20/07/2019','48400','44.2551348683','3.54353418299');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la paille','Musiques actuelles','www.festivalpaille.fr','Metabief','26/07/2019','27/07/2019','25370','46.7648830862','6.35324497812');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de journalisme de Couthures','Domaines divers','https://festivalinternationaldejournalisme.com/','Couthures sur Garonne','12/07/2019','14/07/2019','47180','44.5203730714','0.068285349184');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIC EN AVRE','Musiques actuelles','','ST Lubin des Joncherets','12/07/2019','14/07/2019','28350','48.7509228383','1.18734034626');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'De bouche � oreille � Parthenay','Musiques actuelles','http://www.deboucheaoreille.org','PARTHENAY','24/07/2019','27/07/2019','79200','46.6453493842','-0.233028439198');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS GUITARES DE BEAULIEU','Musiques actuelles','www.lesnuitsguitares.com','BEAULIEU SUR MER','25/07/2019','27/07/2019','6310','43.7079039397','7.33256934881');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES MARC ROBINE','Musiques actuelles','http://lesrencontresmarcrobine.fr/','Chatel Guyon','03/07/2019','14/07/2019','63140','45.9206993185','3.0632143777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GR�SIBLUES FESTIVAL','Musiques actuelles','www.gresiblues.com','GRENOBLE','30/06/2019','05/07/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAOU CHANTE MOZART','Musiques classiques','http://www.saouchantemozart.com','Saou','30/06/2019','23/07/2019','26400','44.6470256973','5.11072905542');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIVALES DE MUSIQUE EN M�DOC','Musiques classiques','https://www.estivales-musique-medoc.com/','BORDEAUX','03/07/2019','12/07/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz � S�te','Musiques actuelles','www.jazzasete.com','SETE','12/07/2019','20/07/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UNE NUIT EN MUSCADET','Musiques actuelles','http://www.unenuitenmuscadet.com','MOUZILLON','06/07/2019','06/07/2019','44330','47.1293076835','-1.27378859416');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA GUERRE DU SON','Musiques actuelles','www.laguerreduson.com','LANDRESSE','19/07/2019','20/07/2019','25530','47.2603830516','6.48915118252');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COURANTS D''AMBOISE','Musiques actuelles','http://www.lescourants.com','AMBOISE','18/07/2019','20/08/2019','37400','47.3917237652','1.00023994675');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival 1001 Notes','Musiques classiques','www.festival1001notes.com','LIMOGES','20/07/2019','09/08/2019','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU CHANT DE MARIN','Musiques actuelles','','PAIMPOL','02/08/2019','04/08/2019','22500','48.7733386239','-3.05459397042');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PLAN B','Pluridisciplinaire Spectacle vivant','http://www.mucem.org/programme/exposition-et-temps-fort/planb-2019','MARSEILLE','01/08/2019','10/08/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BELLE ILE ON AIR','Musiques actuelles','http://belleileonair.org','LE PALAIS','09/08/2019','10/08/2019','56360','47.3426909682','-3.17102952046');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival en Bastides','Cirque et Arts de la rue','http://espaces-culturels.fr/festival-en-bastides-2018/','MONT DE MARSAN','05/08/2019','10/08/2019','40000','43.899361404','-0.490722577455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MI ! FESTIVAL','Musiques actuelles','https://www.festival-mi.com','AJACCIO','10/08/2019','10/08/2019','20167','41.9347926638','8.70132275974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PORTO LATINO','Musiques actuelles','www.portolatino.fr','ST FLORENT','03/08/2019','06/08/2019','45600','47.6846303969','2.45341866592');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OH 2','Musiques actuelles','','ST GERVAIS LES BAINS','','','74170','45.8571864862','6.74016257718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA COMT� AU CLAIR DE LUNE','Musiques actuelles','www.vic-le-comte.fr','VIC LE COMTE','','','63270','45.6440673683','3.24047430326');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU GR� DES ARTS','Pluridisciplinaire Spectacle vivant','http://www.ecouflant.fr/','ECOUFLANT','','','49000','47.5286938274','-0.518454771034');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine de la pop philosophie','Livre et litt�rature','https://www.semainedelapopphilosophie.fr/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tourcoing Jazz','Musiques actuelles','www.tourcoing-jazz-festival.com','TOURCOING','','','59200','50.7254418902','3.15882757215');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK YOUR BRAIN FEST','Musiques actuelles','www.rockyourbrainfest.fr','SELESTAT','','','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Grande �chelle','Pluridisciplinaire Spectacle vivant','http://www.lemonfort.fr/programmation/festival-la-grande-echelle','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival America Molo Man','Cin�ma et audiovisuel','https://www.saintlaurentdumaroni.fr/cinemaletoucan/','ST LAURENT DU MARONI','','','97320','4.96474398632','-53.9832113662');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du cin�ma francophone en Beaujolais','Cin�ma et audiovisuel','http://rencontrescinemavillefranche.blogspot.com/','VILLEFRANCHE SUR SAONE','','','69400','45.9874596667','4.73174007305');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONDEVILLE SUR RIRE','Divers Spectacle vivant','http://www.mondevilleanimation.org','MONDEVILLE','','','14120','49.1693649378','-0.310691187417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EN VIE URBAINE','Transdisciplinaire','www.envieurbaine.com','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROUXTEUR FESTIVAL','Transdisciplinaire','https://www.mainsdoeuvres.org','ST OUEN','','','93400','48.909806575','2.33257042205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI JAZZ LU�ON','Musiques actuelles','','LUCON','','','85400','46.4510564854','-1.16449285012');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film documentaire Kings of Doc','Cin�ma et audiovisuel','https://fr-fr.facebook.com/kingsofdoc/','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES VOLANTES','Musiques actuelles','www.musiques-volantes.org','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INSOLENT COLLECTION AUTOMNE','Musiques actuelles','https://festival-insolent.com/','LORIENT','','','56100','47.7500486947','-3.37823200917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Haut Doubs Festival','Musiques actuelles','www.haut-doubs-festival.com','PONTARLIER','','','25300','46.9119730882','6.38914602031');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TITOUR FESTIVAL','Musiques actuelles','www.titour-festival.fr','La Selle en Cogles','','','35460','48.438498732','-1.33753495005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pariscience � Festival International du Film Scientifique','Cin�ma et audiovisuel','www.pariscience.fr','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZEBRE','Musiques actuelles','www.jazzebre.com','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D''Jazz Nevers Festival','Musiques actuelles','www.djazznevers.com','NEVERS','','','58000','46.9881194908','3.15689130958');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Histoires de cin�ma','Cin�ma et audiovisuel','www.lacinemathequedetoulouse.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film italien d''Ajaccio','Cin�ma et audiovisuel','https://www.iffa.corsica/','AJACCIO','','','20167','41.9347926638','8.70132275974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANTS SACR�S EN M�DITERRAN�E','Musiques actuelles','www.ecume.org','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANTONS SOUS LES TOITS','Musiques actuelles','http://acarthes.free.fr/','Arthes','','','81160','43.9638815505','2.22587013595');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DIABLE EST DANS LA BO�TE','Musiques actuelles','www.boestandiaoul.org','CLOHARS CARNOET','','','29360','47.7943407144','-3.56681364982');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de Nice','Livre et litt�rature','www.lefestivaldulivredenice.com','NICE','31/05/2019','02/06/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A L''EVECHE','Musiques actuelles','http://www.orleans-agglo.fr/','ORLEANS','19/06/2019','22/06/2019','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREE MUSIC FESTIVAL','Musiques actuelles','','MONTENDRE','21/06/2019','23/06/2019','17130','45.2880726146','-0.387479585943');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Escapades Musicales','Musiques classiques','http://www.lesescapadesmusicales.com/','ARCACHON','20/06/2019','20/07/2019','33120','44.6529002838','-1.17429790933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ORIZONS','Transdisciplinaire','www.festival-orizons.fr','PERIGUEUX','16/05/2019','02/06/2019','24000','45.1918704873','0.711825549847');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UZES DANSE','Danse','www.uzesdanse.fr','UZES','14/06/2019','22/06/2019','30700','44.0136590402','4.41600031556');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'M�TIV''SON','Musiques actuelles','','ANTIGNY','09/06/2019','09/06/2019','86310','46.5360016424','0.839149696505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HIPPOCAMPUS','Musiques actuelles','http://www.festival-hippocampus.com','CLERMONT FERRAND','24/05/2019','25/05/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du premier roman de Chambery','Livre et litt�rature','http://www.festivalpremierroman.com','CHAMBERY','23/05/2019','26/05/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ANDALOU DE ST JEAN DE LUZ','Transdisciplinaire','http://www.festivalandalou.com','ST JEAN DE LUZ','06/06/2019','10/06/2019','64500','43.3947886873','-1.63099021573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'6EME CONTINENT','Musiques actuelles','www.sixiemecontinent.net','LYON','06/06/2019','09/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ODP Talence','Musiques actuelles','https://www.festival-odp.com/','TALENCE','06/06/2019','09/06/2019','33400','44.8060817507','-0.591124592873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Marathon des mots','Livre et litt�rature','www.lemarathondesmots.com','TOULOUSE','25/06/2019','30/06/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE BEAU FESTIVAL','Musiques actuelles','www.lebeaufestival.com','PARIS','17/05/2019','19/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Pontons Flingueurs, Festival du polar d''Annecy','Livre et litt�rature','http://pontons.histoiredenparler.info','ANNECY','27/06/2019','29/06/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK IN EVREUX','Musiques actuelles','http://www.rockinevreux.org','Evreux','28/06/2019','30/06/2019','27000','49.02015421','1.14164412464');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES MUSICALES D''EVIAN','Musiques classiques','http://www.rencontres-musicales-evian.fr/','Evian Les Bains','29/06/2019','06/07/2019','74500','46.3924102379','6.58612507528');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MASCARETS','Pluridisciplinaire Spectacle vivant','https://www.ville-pont-audemer.fr/culture/festival-des-mascarets/','PONT AUDEMER','29/06/2019','13/07/2019','27500','49.3463869637','0.533874716445');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HANDPAN FESTIVAL','Musiques actuelles','http://www.festivalhandpan.com','Meze','28/06/2019','30/06/2019','34140','43.4292981138','3.57774572973');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ANRAS FESTIVAL DU SOCIAL ET DU CULTUREL','Transdisciplinaire','https://www.festsocialculturel.com','Flourens','28/06/2019','29/06/2019','31130','43.5977742595','1.55391307823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � DOMERGUE','Musiques actuelles','www.palaisdesfestivals.com','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LARYROCK FESTIVAL','Musiques actuelles','www.laryrock-festival.fr/','CHEVANCEAUX','','','17210','45.2991036157','-0.212745427896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ UP SOUS LES OLIVIERS','Musiques actuelles','www.jazzup06.org','OPIO','','','6650','43.6575610719','7.00860864158');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Fondus du Macadam','Cirque et Arts de la rue','https://www.thononevenements.com/fondus-du-macadam/','THONON LES BAINS','','','74200','46.3704258049','6.48194336071');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AOUTIENNES','Musiques actuelles','https://www.lesaoutiennes.com','BANDOL','','','83150','43.1473544522','5.74789358398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVA�UT','Musiques actuelles','www.festivaout.com','LAUTREC','','','81440','43.7052468491','2.12929403349');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN MONTGENEVRE / JAZZ AUX FRONTIERES','Musiques actuelles','','Montgenevre','','','5100','44.9389932221','6.72778259009');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REGGAE SESSION FESTIVAL','Musiques actuelles','www.reggae-session-festival.com','Montricoux','','','82800','44.1035057733','1.62418942804');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHABLIS BOUGE TON CRU','Musiques actuelles','www.chablis-bouge-son-cru.fr/','CHABLIS','','','89800','47.8075995199','3.79091424228');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ESCALES DE SAINT NAZAIRE','Musiques actuelles','','ST Nazaire','','','44600','47.2802857028','-2.25379927249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU SUD DU NORD','Musiques actuelles','www.ausuddunord.fr','CERNY','','','91590','48.4859798517','2.31068283872');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE PERTHES EN GATINAIS (concert du Pays de Bi�re)','Musiques actuelles','www.concertpaysdebiere.org','PERTHES','','','77930','48.479730606','2.55038052003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Charivari au Village','Cirque et Arts de la rue','http://www.cergy.fr/se-divertir/temps-forts/charivari-au-village/','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Valse des As','Cirque et Arts de la rue','https://fr-fr.facebook.com/pages/category/Community/la-Valse-des-As-123943874282786/','VALREAS','','','84600','44.3771588623','4.99354936197');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PONDEROSA','Musiques actuelles','www.ponderosa.family/?lang=fr','Lauterbourg','','','67630','48.9637116345','8.18637725776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KIOSQUORAMA','Pluridisciplinaire Spectacle vivant','www.kiosquorama.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale europ�enne des patrimoines','Domaines divers','https://www.labiennale.fr/','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERIAKI / SIESTES TERIAKI','Musiques actuelles','www.teriaki.fr','LE MANS','','','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE BUIS BLUES FESTIVAL','Musiques actuelles','www.lebuisbluesfestival.com','LE BUIS','','','87140','46.0341686702','1.20438019379');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FISEL','Musiques actuelles','www.fisel.org','ROSTRENEN','','','22110','48.2215427736','-3.31179704668');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'la Mousson d''�t�','Th��tre','http://www.meec.org/','PONT A MOUSSON','','','54700','48.9219828048','6.05392690748');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MOYENS DU BORD','Musiques actuelles','www.festival-lesmoyensdubord.fr','La Chapelle aux Filtzmeens','','','35190','48.3817396388','-1.8184050063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRES COOL FESTIVAL','Musiques actuelles','www.trescool.fr/index','DEAUVILLE','','','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIKS A MANOSQUE','Musiques actuelles','www.culturedlva.fr','MANOSQUE','','','4100','43.8354040831','5.79106654173');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ECHAPPEES BELLES','Cirque et Arts de la rue','http://www.scenenationale61.com/','ALENCON','','','61000','48.4318193082','0.0915406916107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HET LINDEBOOM','Musiques actuelles','','LOON PLAGE','','','59279','51.0012144614','2.22304802432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUR UN PETIT NUAGE','Pluridisciplinaire Spectacle vivant','http://www.pessac.fr/sur-un-petit-nuage.html','PESSAC','','','33600','44.7915990521','-0.676303166277');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Poitiers Film Festival','Cin�ma et audiovisuel','www.poitiersfilmfestival.com','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COURTS DANS LA VALL�E','Cin�ma et audiovisuel','','Premian','','','34390','43.5395150426','2.82265477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE MAS D''HIVER','Musiques actuelles','http://www.lemas-concert.com','PUGET SUR ARGENS','','','83480','43.4717671252','6.68628195857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du grain � d�moudre de Gonfreville l''Orcher','Cin�ma et audiovisuel','http://dugrainademoudre.net/','Gonfreville l Orcher','','','76700','49.4860837457','0.221985693724');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A TRAVERS CHANTS','Musiques actuelles','www.festivalatraverschants.over-blog.com','COSSE LE VIVIEN','14/03/2019','23/03/2019','53230','47.94585879','-0.934606329089');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'British Screen / Ecrans Britanniques � N�mes','Cin�ma et audiovisuel','http://ecransbritanniques.org','NIMES','08/03/2019','17/03/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musilac Mont Blanc','Musiques actuelles','https://www.musilac.com/fr/mont-blanc/','CHAMONIX MONT BLANC','26/04/2019','28/04/2019','74400','45.9309819111','6.92360096711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WELCOME IN TZIGANIE','Musiques actuelles','http://www.welcome-in-tziganie.com/','AUCH','26/04/2019','28/04/2019','32000','43.6534300414','0.575190250459');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES KAMPAGN''ARTS','Musiques actuelles','http://www.kampagnarts.fr/','ST Paterne Racan','28/06/2019','29/06/2019','37370','47.5816905056','0.466637692187');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BICHOISERIES','Musiques actuelles','www.mafiozik.com','Cerisy Belle Etoile','28/06/2019','29/06/2019','61100','48.7831815078','-0.623679681205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Rencontres d''Arles','Arts plastiques et visuels','https://www.rencontres-arles.com/','ARLES','01/07/2019','22/09/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FEUX DE L''ETE','Musiques actuelles','www.feuxdelete.com','ST PROUANT','28/06/2019','29/06/2019','85110','46.7502017421','-0.974504061491');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JUKE''BOSSE','Musiques actuelles','','Maz�','','','49630','47.4603950447','-0.284298744303');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ABRACADA''SONS','Musiques actuelles','http://www.paysdelauzun.com/fr','MIRAMONT DE GUYENNE','','','47800','44.5833450029','0.356429133423');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PAN !','Musiques actuelles','www.collectifpan.fr','CAEN','','','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZIKAMETZ','Musiques actuelles','www.zikamine.com','METZ','','','57070','49.1081133279','6.1955245421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROANNE BLUES FESTIVAL','Musiques actuelles','https://www.acmm-concerts.com/','ROANNE','','','42300','46.0449112487','4.0797045647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film de La Roche-sur-Yon','Cin�ma et audiovisuel','https://www.fif-85.com/fr/','LA ROCHE SUR YON','','','85000','46.6675261644','-1.4077954093');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nancy Jazz Pulsations','Musiques actuelles','www.nancyjazzpulsations.com','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECHAP & VOUS','Pluridisciplinaire Spectacle vivant','www.ville-lachapellesaintmesmin.fr','LA CHAPELLE ST MESMIN','','','45380','47.8887063362','1.82847184827');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUNNYSIDE FESTIVAL','Musiques actuelles','http://www.sunnyside.fr/','REIMS','','','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Carr�ment � l''Ouest','Cirque et Arts de la rue','http://lecitronjaune.com/carrement-a-louest/','PORT ST LOUIS DU RHONE','','','13230','43.4148193741','4.80679663589');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARAIS VOUS BIEN','Divers Spectacle vivant','www.maraisvousbien.com','ST CIERS SUR GIRONDE','','','33820','45.2980782445','-0.648058188533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DES ARTS DE BORDEAUX M�TROPOLE (FAB)','Pluridisciplinaire Spectacle vivant','www.fab.festivalbordeaux.com','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Village de cirque','Cirque et Arts de la rue','http://www.2r2c.coop/2r2cms/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WORLD OF WORDS','Musiques actuelles','www.assolaruche.fr','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NORDIK IMPAKT','Musiques actuelles','www.nordik.org','CAEN','','','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Planches Contact','Arts plastiques et visuels','https://www.indeauville.fr/','DEAUVILLE','','','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DETONATION','Musiques actuelles','www.larodia.fr','BESANCON','','','25000','47.2553872249','6.01948696494');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REG''ARTS','Pluridisciplinaire Spectacle vivant','www.zicomatic.net','BARBERAZ','','','73000','45.5520596872','5.93925556329');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine du Cin�ma Positif','Cin�ma et audiovisuel','https://semaine-cinemapositif.fr/','CANNES','16/05/2019','23/05/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GAROSNOW','Musiques actuelles','www.garosnow.com','Les Angles','11/01/2019','12/01/2019','66210','42.5797574108','2.05268528008');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COURZIK FESTIVAL','Musiques actuelles','http://www.courzik.fr/','COURCY','08/03/2019','10/03/2019','51220','49.3196081983','4.0061261454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HUMOUR AU FEMININ A MARTIGNAS SUR JALLE','Divers Spectacle vivant','http://www.bordeaux-metropole.fr/Agenda/Festival-Humour-au-Feminin','MARTIGNAS SUR JALLE','02/03/2019','31/03/2019','33127','44.8450267218','-0.795499117928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du Cin�ma Europ�en de Vannes','Cin�ma et audiovisuel','www.cinecran.org','VANNES','06/03/2019','12/03/2019','56000','47.6597493766','-2.75714329498');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de Bron','Livre et litt�rature','http://www.fetedulivredebron.com/','BRON','06/03/2019','10/03/2019','69500','45.7344856902','4.91168159471');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Escapades','Musiques actuelles','http://www.theatredurance.fr/','CHATEAU ARNOUX ST AUBAN','14/06/2019','15/06/2019','4160','44.0854706133','5.99239535024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de bande dessin�e de S�rignan','Livre et litt�rature','www.ville-serignan.fr','SERIGNAN','08/06/2019','09/06/2019','34410','43.2733185307','3.29725083254');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'YEAH !','Musiques actuelles','www.festivalyeah.fr','LOURMARIN','07/06/2019','09/06/2019','84160','43.7780333839','5.36616174066');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Printemps des com�diens','Th��tre','www.printempsdescom','MONTPELLIER','31/05/2019','30/06/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DE LANVELLEC','Musiques classiques','www.festival-lanvellec.fr','LANVELLEC','30/05/2019','02/06/2019','22420','48.6072769742','-3.52795086821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ACCORDEON PLEIN POT','Musiques actuelles','https://www.officeculturel.com/events/festival-accordeon-plein-pot/','ST Quentin La Poterie','28/05/2019','02/06/2019','30700','44.057337074','4.43872425053');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOLVIC VOLCANIC EXPERIENCE','Musiques actuelles','','Volvic','30/05/2019','01/06/2019','63530','45.8681922975','3.01900139762');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES 7 COLLINES','Pluridisciplinaire Spectacle vivant','http://www.festivaldes7collines.com','ST ETIENNE','25/06/2019','08/07/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'S�r�nades en Baronnies','Musiques classiques','http://www.serenadesenbaronnies.fr/','BUIS LES BARONNIES','11/07/2019','25/07/2019','26170','44.276084918','5.27102720538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'C�t� Cour - C�t� Jardin','Cirque et Arts de la rue','http://www.bernaytourisme.fr/manifestation-culturelle-bernay-evnt-3582.html','BERNAY','13/07/2019','11/08/2019','27300','49.0925877345','0.58959175237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sc�nes de rue','Cirque et Arts de la rue','http://www.scenesderue.fr/','MULHOUSE','18/07/2019','21/07/2019','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES INTERNATIONALES DE HARPE CELTIQUE','Musiques actuelles','http://www.harpe-celtique.fr/','DINAN','10/07/2019','14/07/2019','22100','48.4557709953','-2.04816149693');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bouillez!','Cirque et Arts de la rue','http://bouillez.fr/','Bouille ST Paul','06/07/2019','07/07/2019','79290','47.0350297953','-0.314425439362');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cap aux bords - Rencontres cin�ma et �ducation populaire � Sainte Livrade','Cin�ma et audiovisuel','http://www.laligue47.org/culture/cap-aux-bords','STE Livrade sur Lot','06/07/2019','12/07/2019','47110','44.4058755457','0.599870593015');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTJOUX FESTIVAL','Musiques actuelles','www.montjouxfestival.com','THONON LES BAINS','11/07/2019','13/07/2019','74200','46.3704258049','6.48194336071');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOULOUSE D''�T�','Pluridisciplinaire Musique','https://toulousedete.org','TOULOUSE','04/07/2019','28/07/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LABYRINTHE DE LA VOIX','Musiques actuelles','http://www.labyrinthedelavoix.fr/','ROCHECHOUART','26/07/2019','04/08/2019','87600','45.8236032815','0.84280512319');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES IN ATTENDUS','Musiques actuelles','http://www.lesinattendusdegagnieres.com','GAGNIERES','26/07/2019','29/07/2019','30160','44.315356746','4.12257109543');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Nuits du Piano d''Erbalunga','Musiques classiques','http://www.lesnuitsdupianoerbalunga.fr/','Erbalunga','28/07/2019','05/08/2019','20222','42.7792996993','9.45029993821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU FOIN DE LA RUE','Musiques actuelles','www.aufoindelarue.com','ST DENIS DE GASTINES','05/07/2019','06/07/2019','53500','48.3469457308','-0.87082653269');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FERME ELECTRIQUE','Musiques actuelles','http://www.la-ferme-electrique.fr/','TOURNAN EN BRIE','05/07/2019','06/07/2019','77220','48.739865817','2.77820566994');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRUSSOL FESTIVAL','Musiques actuelles','www.crussolfestival.com','ST Peray','06/07/2019','07/07/2019','7130','44.9410716967','4.82459034307');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival R�sistances - Foix','Cin�ma et audiovisuel','http://festival-resistances.fr','FOIX','05/07/2019','13/07/2019','9000','42.9658502274','1.61037495894');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vieilles Charrues','Musiques actuelles','www.vieillescharrues.asso.fr','CARHAIX PLOUGER','18/07/2019','21/07/2019','29270','48.2687578823','-3.56626363851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONC''AIR LES ESTIVALES DE SAINT LOUIS','Musiques actuelles','http://www.concair.alsace/','ST LOUIS','19/07/2019','03/08/2019','68300','47.6016553367','7.54061872745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PLACE AU JAZZ','Musiques actuelles','www.ville-antony.fr','ANTONY','','','92160','48.7503412602','2.2993268102');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE TYMPAN DANS L''OEIL','Transdisciplinaire','www.tympandansloeil.com','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les rencontres photographiques de Guyane','Arts plastiques et visuels','http://www.rencontresphotographiquesdeguyane.com/','CAYENNE','','','97300','5.02173742828','-52.5012511113');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te de l''animation','Cin�ma et audiovisuel','http://www.fete-anim.com/','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Impatience','Th��tre','http://www.festivalimpatience.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma Russe � Honfleur','Cin�ma et audiovisuel','http://festival-honfleur.fr/','Honfleur','','','14600','49.4129449727','0.237579361279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Carbur''en Sc�ne','Pluridisciplinaire Spectacle vivant','http://www.carburenscene.fr/','BOURG EN BRESSE','29/01/2020','01/02/2020','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ecrans VO Festival image par image','Cin�ma et audiovisuel','http://ecransvo.fr/','PONTOISE','','','95300','49.0513737853','2.09487928948');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES LIONS DU RIRE','Divers Spectacle vivant','','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GALETTES DU MONDE','Transdisciplinaire','http://galettesdumonde.free.fr/','Sainte Anne D''Auray','24/08/2019','25/08/2019','56400','47.7009234212','-2.95341644757');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Berlioz','Musiques classiques','http://www.festivalberlioz.com','LA COTE ST ANDRE','17/08/2019','01/09/2019','38260','45.3834360946','5.26068376928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'STRANGE FESTIVAL','Musiques actuelles','www.strangefestival.fr','CHATILLON SUR LOIRE','23/08/2019','24/08/2019','45360','47.5721313447','2.73594191784');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Bangor','Musiques classiques','http://www.belle-ile.com/faire/sortir/festivals-et-theatre/136204-festival-de-bangor','BANGOR','17/08/2019','24/08/2019','56360','47.313014109','-3.1920046614');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PLATEAUX EPHEMERES','Cirque et Arts de la rue','https://www.larenaissance-mondeville.fr','MONDEVILLE','','','14120','49.1693649378','-0.310691187417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les R�volt�s de l�Histoire','Cin�ma et audiovisuel','http://www.revoltesdelhistoire.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AUX ECLUSES','Musiques actuelles','http://www.jazzauxecluses.fr/','HEDE BAZOUGES','','','35630','48.3018808207','-1.77748405021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUARTIERS D''ETE OLORON STE MARIE','Pluridisciplinaire Spectacle vivant','http://pratique.tourisme64.com/animation/oloron-sainte-marie.html','OLORON STE MARIE','','','64400','43.1560871189','-0.587546244432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de Contis','Cin�ma et audiovisuel','http://www.cinema-contis.fr/1.aspx','ST Julien en Born','','','40170','44.0910922901','-1.24835443544');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cin�monde','Cin�ma et audiovisuel','http://www.kdiffusion.com/','BERCK','','','62600','50.4141574341','1.58229663833');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival De Musique Du Haut Jura','Musiques classiques','http://www.festival-musique-baroque-jura.com/','ST CLAUDE','','','39200','46.408600398','5.87556247635');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BUBBLE DJ FESTIVAL','Musiques actuelles','http://www.bubbledjfestival.fr/','Epernay','','','51200','49.037000689','3.93144626202');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAMILIJAZZ','Musiques actuelles','www.familijazz.com','LAON','','','2000','49.5679724897','3.62089561902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sinfonia en P�rigord','Musiques classiques','www.sinfonia-en-perigord.com/','PERIGUEUX','','','24000','45.1918704873','0.711825549847');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAHUT AU CHATEAU','Pluridisciplinaire Spectacle vivant','https://chahutauchateau.com','Gevingey','','','39570','46.6375986496','5.50938673759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DANS LES JARDINS DE WILLIAM CHRISTIE','Musiques classiques','www.evenements.vendee.fr','Thir�','','','85210','46.5433098199','-1.00699777534');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Kolorz Festival Edition �t�','Musiques actuelles','','CARPENTRAS','','','84200','44.0593802565','5.06134844776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Antiques de Glanum','Pluridisciplinaire Spectacle vivant','','ST REMY DE PROVENCE','','','13210','43.7834249015','4.85366665286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIVALES JAZZ DE MOLI�RES','Musiques actuelles','www.bastide-molieres.fr','MOLIERES','','','24480','44.8119248894','0.82474050426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''N''ROLL TRAIN FESTIVAL','Musiques actuelles','http://www.rnr-train.fr/','LONGWY','','','54400','49.5214021779','5.76638370702');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''ORANGERIE DE SCEAUX','Musiques classiques','http://www.festival-orangerie.fr/','Sceaux','','','92330','48.776816369','2.29529414514');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MIMA FESTIVAL','Divers Spectacle vivant','www.mima.artsdelamarionnette.com','MIREPOIX','','','9500','43.1093995072','1.86916991814');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DECOUVRIR','Musiques actuelles','http://www.festivaldecouvrir.com','Conceze','','','19350','45.3604032352','1.33771562463');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SAUTE MOUTON','Musiques actuelles','www.festivalsautemouton.com','ST LAURENT DE NESTE','','','65150','43.0920150867','0.480853074724');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAIGNADE INTERDITE','Musiques actuelles','http://baignadeinterdite-24h.blogspot.fr/','Rivieres','','','81600','43.9157732969','1.96947284122');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Joly Jazz en Avesnois','Musiques actuelles','www.valjoly.com','EPPE SAUVAGE','','','59132','50.1185512931','4.16530475504');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COLLIOURE JAZZY','Musiques actuelles','www.collioure.net','COLLIOURE','','','66190','42.5130107325','3.07254121467');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAMBRE AVEC VUE','Musiques classiques','www.archipel-fouesnant.fr/','FOUESNANT','','','29170','47.8783078862','-4.01834628474');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU SON D''EUH LO !','Musiques actuelles','www.ausondeuhlo.com','TESSY SUR VIRE','','','50420','48.9633310504','-1.08464396412');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A Pas Cont�s','Pluridisciplinaire Spectacle vivant','www.apascontes.fr','DIJON','08/02/2019','22/02/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Atlantide, les mots du monde','Livre et litt�rature','www.atlantide-festival.org','NANTES','28/02/2019','03/03/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCKENSTOCK','Musiques actuelles','www.rockenstock-asso.com','CHALONS EN CHAMPAGNE','30/03/2019','30/03/2019','51000','48.9640892125','4.37883539725');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURES URBAINES A CANTELEU','Pluridisciplinaire Spectacle vivant','www.ville-canteleu.fr/','CANTELEU','31/03/2019','20/04/2019','76380','49.4330992435','1.01355208279');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK SAONE FESTIVAL','Musiques actuelles','http://www.harmoniedeneuville.fr/','Neuville sur Saone','19/03/2019','24/03/2019','69250','45.8829276204','4.84373616973');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film d''Aubagne','Cin�ma et audiovisuel','www.aubagne-filmfest.fr','AUBAGNE','18/03/2019','23/03/2019','13400','43.2934843764','5.56331273477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CORDES SENSIBLES','Pluridisciplinaire Musique','http://accordsetacordes.saintmedardasso.fr/','ST Medard en Jalles','22/03/2019','23/03/2019','33160','44.8832620816','-0.784239883546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GERARDM ELECTRIC','Musiques actuelles','http://mclgerardmer.fr/','GERARDMER','18/04/2019','21/04/2019','88400','48.0653057669','6.85912240068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KAN AR BOBL','Musiques actuelles','http://kanarbobl.org/','PONTIVY','06/04/2019','07/04/2019','56300','48.0729953877','-2.97046592885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FAITS D''HIVER','Danse','www.faitsdhiver.com','PARIS','14/01/2019','20/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE P''TIT MONDE','Pluridisciplinaire Spectacle vivant','www.centreandremalraux.com','HAZEBROUCK','24/04/2019','02/05/2019','59190','50.7262967113','2.53864301455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival national du film d''animation - Bruz (Rennes M�tropole)','Cin�ma et audiovisuel','www.festival-film-animation.fr','RENNES','24/04/2019','28/04/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL VARIATIONS','Musiques actuelles','www.festival-variations.fr','NANTES','23/04/2019','30/04/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SNOWBOXX','Musiques actuelles','http://snowboxx.com','MORZINE','23/03/2019','30/03/2019','74110','46.1732135057','6.74252144427');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ BAT LA CAMPAGNE','Musiques actuelles','www.lejazzbatlacampagne.com','PARTHENAY','22/03/2019','14/07/2019','79200','46.6453493842','-0.233028439198');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE SON DU PORTE VOIX','Musiques actuelles','http://razibus.net/01-05-2019-le-son-porte-voix-numero-12-arras-29051.html','ARRAS','01/05/2019','01/05/2019','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Cin�ma et Soci�t� � Pays de Tulle','Cin�ma et audiovisuel','www.autourdu1ermai.fr','TULLE','01/05/2019','12/05/2019','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SATHONAY BLUES FESTIVAL','Musiques actuelles','www.festivalsathonay.com','SATHONAY VILLAGE','07/06/2019','08/06/2019','69580','45.8402514448','4.88563860144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARMADA DE ROUEN','Transdisciplinaire','https://www.armada.org/','ROUEN','06/06/2019','16/06/2019','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SONS DU LUB','Musiques actuelles','http://www.sonsdulub.fr/','Beaumont De Pertuis','11/05/2019','25/05/2019','84120','43.7379108971','5.69599127224');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EN MAI CHANTE CE KIL TE PLAIT','Musiques actuelles','https://www.tanzmatten.fr/','SELESTAT','16/05/2019','28/05/2019','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Imaginales - Festival des mondes imaginaires','Livre et litt�rature','https://www.imaginales.fr/','EPINAL','23/05/2019','26/05/2019','88000','48.1631202656','6.47989286928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sur les pointes','Musiques actuelles','www.surlespointes.fr','VITRY SUR SEINE','24/05/2019','26/05/2019','94400','48.7882828307','2.39412680533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festivalet','Livre et litt�rature','https://www.tisseursdemots.org/-Le-Festivalet-.html','BLESLE','24/05/2019','26/05/2019','43450','45.3179155382','3.17686146037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LATITUDES CONTEMPORAINES','Danse','http://latitudescontemporaines.com','LILLE','05/06/2019','28/06/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BICHES FESTIVAL','Musiques actuelles','http://www.biches-festival.fr/','Cisai ST Aubin','14/06/2019','16/06/2019','61230','48.7765975635','0.357700893574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cirque en sc�ne fait son n�','Cirque et Arts de la rue','www.cirque-scene.fr/','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ART ET JAZZ DANS MA COUR','Musiques actuelles','www.artetjazzdansmacour.fr','HERMONVILLE','','','51220','49.3318496346','3.9143354087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d''art contemporain de Mulhouse','Arts plastiques et visuels','https://www.tourisme-mulhouse.com/','MULHOUSE','','','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Escales Documentaires de La Rochelle','Cin�ma et audiovisuel','www.escalesdocumentaires.org','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Plan s�quence, festival du film d''Arras','Cin�ma et audiovisuel','www.arrasfilmfestival.com','ARRAS','','','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL THEATRAL DU VAL D''OISE','Th��tre','www.thea-valdoise-public.org','EAUBONNE','','','95600','48.9909940165','2.27800925488');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHRONIQUES','Transdisciplinaire','www.chronique-s.org','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOTS ZIK SOUS LES PINS','Musiques actuelles','https://www.festival-mots-zik.com','ST JACUT LES PINS','','','56220','47.6776522748','-2.21229939133');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARRIERE ENGHIEN JAZZ FESTIVAL','Musiques actuelles','www.enghien-jazz-festival.com','Enghien les Bains','03/07/2019','07/07/2019','95880','48.97016048','2.30485955429');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES TOMBEES DE LA NUIT','Pluridisciplinaire Spectacle vivant','http://www.lestombeesdelanuit.com','RENNES','03/07/2019','14/07/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU CHIEN ROUGE','Musiques actuelles','http://www.lecannetdesmaures.com','LE CANNET DES MAURES','19/07/2019','03/08/2019','83340','43.3737632121','6.3755010333');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIFL''ART','Musiques actuelles','http://festiflart.com','Chauconin Neufmontiers','06/07/2019','06/07/2019','77124','48.9719332783','2.83158634864');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VOLCANIQUES','Musiques actuelles','http://brayauds.fr','ST BONNET PRES RIOM','06/07/2019','10/07/2019','63200','45.9285193382','3.11628669554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZIK ZAC FESTIVAL','Musiques actuelles','www.zikzac.fr','AIX EN PROVENCE','25/07/2019','27/07/2019','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JACK IN THE BOX','Musiques actuelles','www.cabaret-aleatoire.com','MARSEILLE','25/07/2019','27/07/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres d��t� th��tre & lecture','Transdisciplinaire','www.rencontresdete.fr','TROUVILLE SUR MER','20/07/2019','25/08/2019','14360','49.3721246762','0.102123470052');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Mimos','Divers Spectacle vivant','https://www.mimos.fr/','PERIGUEUX','23/07/2019','27/07/2019','24000','45.1918704873','0.711825549847');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRES DU SON','Musiques actuelles','www.terresduson.com','JOUE LES TOURS','12/07/2019','14/07/2019','37300','47.333556074','0.654546300116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ESCALES MUSICALES LATINES','Musiques actuelles','https://www.canetenroussillon.fr/fetes-et-manifestations/escales-musicales-latines/','CANET EN ROUSSILLON','12/07/2019','23/08/2019','66140','42.6841288047','3.01161744752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HORSE FIELD FESTIVAL','Musiques actuelles','https://fr-fr.facebook.com/horsefieldfestival/','Curis au Mont d Or','12/07/2019','12/07/2019','69250','45.8694161345','4.81844287868');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NICE JAZZ FESTIVAL','Musiques actuelles','','NICE','16/07/2019','20/07/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS MUSICALES DU SUQUET','Musiques classiques','http://www.cannes.com','CANNES','18/07/2019','23/07/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST JAZZ A CHATEAUNEUF DU FAOU','Musiques actuelles','http://www.fest-jazz.com','CHATEAUNEUF DU FAOU','26/07/2019','28/07/2019','29520','48.1889732859','-3.82126129764');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CALVI ON THE ROCKS','Musiques actuelles','www.calviontherocks.com','CALVI','05/07/2019','10/07/2019','20260','42.5454874755','8.7595487404');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Th� Vert','Musiques Actuelles','','Nogent le Rotrou','05/07/2019','07/07/2019','28400','48.3176302432','0.804048016354');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE PEACOCK SOCIETY','Musiques actuelles','https://thepeacocksociety.fr/','PARIS','05/07/2019','06/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Lyrique International de Belle Ile en Mer','Musiques classiques','http://festival-belle-ile.com/','LE PALAIS','30/07/2019','16/08/2019','56361','47.5826411591','-2.33551260645');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN LANGOURLA','Musiques actuelles','www.jazzinlangourla.com','Langourla','02/08/2019','04/08/2019','22330','48.2937312654','-2.52246803693');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MOMES EN SCENE','Cirque et Arts de la rue','http://momesenscene.com','NIEDERBRONN LES BAINS','31/07/2019','04/08/2019','67110','48.9638157112','7.63986093383');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES JAZZ DE SAINT RAPHAEL','Musiques actuelles','www.ville-saintraphael.fr','ST RAPHAEL','05/07/2019','09/07/2019','83700','43.4574625431','6.84734210398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GARONNA SHOW','Musiques actuelles','www.garonna-show.com','PORT STE MARIE','05/07/2019','06/07/2019','47130','44.2605042795','0.374453245526');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EUROFONIK','Musiques actuelles','www.eurofonik.fr','NANTES','15/03/2019','23/03/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN AUFFARGIS (printemps)','Musiques actuelles','http://www.jazzinauffargis.fr/','LES ESSARTS LE ROI','16/03/2019','31/03/2019','78690','48.716019265','1.8952517006');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES AUTOUR DU ZINC','Musiques actuelles','www.zincblues.com','BEAUVAIS','15/03/2019','23/03/2019','60000','49.4365523321','2.08616123661');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DU BLUES D''ABBEVILLE','Musiques actuelles','www.nuits-du-blues-abbeville.fr','ABBEVILLE','15/03/2019','16/03/2019','80100','50.1083582944','1.83209170207');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BETIZFEST','Musiques actuelles','www.betizfest.info','CAMBRAI','12/04/2019','13/04/2019','59400','50.1702775977','3.24221021072');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRADITIONNEL  AU PAYS DU GALOUBET','Musiques actuelles','www.les-sauts-du-loup.fr','LA MOTTE','12/04/2019','14/04/2019','83920','43.5078511168','6.55055399937');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LAC IN BLUE','Musiques actuelles','www.lac-in-blue.com','ANNECY','02/04/2019','06/04/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Sc�naristes de Valence','Cin�ma et audiovisuel','http://scenarioaulongcourt.com','VALENCE','03/04/2019','06/04/2019','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ETHIOPIQUES - BAYONNE','Pluridisciplinaire Spectacle vivant','www.ethiopiques.fr','BAYONNE','20/03/2019','23/03/2019','64100','43.4922254016','-1.46607674358');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE JAZZ TRADITIONNEL DE ROCHEGUDE','Musiques actuelles','www.ajtderochegude.com','Rochegude','22/03/2019','24/03/2019','26790','44.2556674573','4.83716476858');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLK EN SCENES','Musiques actuelles','www.mairie-trignac.fr','TRIGNAC','22/03/2019','24/03/2019','44570','47.3125328416','-2.20700045585');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�latino, Rencontres de Toulouse','Cin�ma et audiovisuel','www.cinelatino.com.fr','TOULOUSE','22/03/2019','31/03/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Aspects des musiques d''aujourd''hui','Musiques classiques','http://www.orchestredecaen.fr/','CAEN','19/03/2019','24/03/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AVEC LE TEMPS','Musiques actuelles','www.festival-avecletemps.com','MARSEILLE','12/03/2019','17/03/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PLURIELS','Pluridisciplinaire Spectacle vivant','www.lespluriels.com','ROUEN','13/03/2019','16/03/2019','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Amiens Tout-monde','Transdisciplinaire','http://www.maisondelaculture-amiens.com/','AMIENS','11/03/2019','19/03/2019','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PARADIS ARTIFICIELS','Musiques actuelles','www.lesparadisartificiels.fr','LILLE','23/03/2019','31/03/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOUTEILLE EN BRETELLES','Musiques actuelles','http://www.bouteilleenbretelles.com','BOURG ST ANDEOL','23/03/2019','23/03/2019','7700','44.3830866916','4.61398365069');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE FESTIVAL DU BRUIT QUI PENSE','Musiques classiques','www.bruitquipense.com','Louveciennes','23/03/2019','31/03/2019','78430','48.8590188206','2.11364039022');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN MARS','Musiques actuelles','www.ville-tarnos.fr','TARNOS','06/03/2019','10/03/2019','40220','43.5367457997','-1.4653189934');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU PAYS DU MOME','Pluridisciplinaire Spectacle vivant','https://www.paysdumome.com/','CHANGE','03/03/2019','30/04/2019','53810','48.1079801148','-0.800104772023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU TETARD','Musiques actuelles','http://www.letetard.com/mars-en-jazz-au-tetard/','MARSEILLE','01/03/2019','31/03/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres de la jeune photographie internationale','Arts plastiques et visuels','http://www.cacp-villaperochon.com','NIORT','05/04/2019','07/04/2019','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon de Montrouge','Arts plastiques et visuels','http://www.salondemontrouge.com/','MONTROUGE','27/04/2019','22/05/2019','92120','48.8153032159','2.31648921432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HUMOUR DE BOURG LA REINE','Divers Spectacle vivant','http://caelmjc.com','BOURG LA REINE','26/01/2019','02/02/2019','92340','48.7799073627','2.31643010763');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � L''�TAGE','Musiques actuelles','www.jazzaletage.com','RENNES','09/02/2019','09/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du cin�ma fantastique de G�rardmer','Cin�ma et audiovisuel','www.festival-gerardmer.com','GERARDMER','30/01/2019','03/02/2019','88400','48.0653057669','6.85912240068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon Livre � part','Livre et litt�rature','https://www.mairie-saint-mande.fr/','ST MANDE','26/01/2019','27/01/2019','94160','48.842440243','2.4192191088');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ DAY IN LYON','Musiques actuelles','http://www.jazzday-lyon.com','LYON','30/04/2019','30/04/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLASSIQUE AU LARGE','Musiques classiques','https://www.classiqueaularge.fr/','ST MALO','02/05/2019','05/05/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l''image sous-marine','Cin�ma et audiovisuel','https://fr-fr.facebook.com/Festival-de-lImage-Sous-Marine-de-Mayotte-601258786696224/','Mamoudzou','22/05/2019','26/05/2019','97600','-12.7899979586','45.1932456026');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BALLADE AVEC BRASSENS','Musiques actuelles','http://balladeavecbrassens.fr/','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SIMONE PETE LES WATTS','Musiques actuelles','www.simone.le-label-pas-sage.fr','La Reole','','','33190','44.5860904495','-0.042370404021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L�Etrange festival','Cin�ma et audiovisuel','http://www.etrangefestival.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UVAS PASAS','Musiques actuelles','http://uvaspasas.fr/','MERDRIGNAC','','','22230','48.1936252026','-2.39598361343');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres � la campagne','Cin�ma et audiovisuel','http://www.rencontresalacampagne.org/','Rieupeyroux','','','12240','44.3085754076','2.22948755681');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ NUTS FESTIVAL','Musiques actuelles','jazz-nuts-festival-auriac.fr','AURIAC DU PERIGORD','','','24290','45.1125569337','1.12728719792');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MASCAROCK','Musiques actuelles','http://www.mairie-vayres.fr/Mascarock-2018','Vayres','','','33870','44.8910656059','-0.32583268578');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival O! Les Rues','Cirque et Arts de la rue','https://www.olesrues.fr/','Dolus d Oleron','','','17550','45.9096185439','-1.25750953748');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Oh Fun Bak Festival','Cirque et Arts de la rue','www.ohfunbak.labak.fr','Choisy au Bac','','','60750','49.442089833','2.89567270285');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN AO�T','Musiques actuelles','www.jazzconvergences.com','LA CIOTAT','','','13600','43.1926410861','5.61318955778');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d''astronomie de Fleurance','Domaines divers','http://www.festival-astronomie.fr/','FLEURANCE','','','32500','43.8402794076','0.64245586546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLKLORE (PAYS TARNAIS)','Danse','','GAILLAC','','','81600','43.9170968776','1.88647213801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE QUATUORS A CORDES EN PAYS DE FAYENCE','Musiques classiques','www.quatuors-fayence.com','FAYENCE','','','83440','43.6103345822','6.67843279926');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film Grolandais de Toulouse','Cin�ma et audiovisuel','www.fifigrot.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Gindou Cin�ma','Cin�ma et audiovisuel','www.gindoucinema.org','Gindou','','','46250','44.6266225054','1.26418092568');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS ET LES JOURS DE QUERBES','Transdisciplinaire','www.querbes.fr','ASPRIERES','','','12700','44.555388853','2.13842661504');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Estivales de Puisaye','Musiques classiques','https://www.estivales-puisaye.com/','Champignelles','','','89350','47.7800283506','3.07775894136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'100 D�TOURS','Pluridisciplinaire Spectacle vivant','www.jaspir.com','ST JEAN DE BOURNAY','','','38440','45.4982631773','5.14748470946');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les 24 heures de l''INSA de Lyon','Musiques actuelles','www.24heures.org','VILLEURBANNE','17/05/2019','19/05/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INVASION DE LUCANES','Musiques actuelles','www.invasion-de-lucanes.com','LIBOURNE','24/05/2019','25/05/2019','33500','44.9130767869','-0.234709332068');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTAUBAN EN SCENES','Pluridisciplinaire Spectacle vivant','http://www.montauban-en-scenes.fr/','MONTAUBAN','27/06/2019','30/06/2019','82000','44.0222594578','1.36408636501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLAGE DE ROCK','Musiques actuelles','http://www.plagederock.com','Grimaud','27/06/2019','08/08/2019','83310','43.2820282501','6.53303224114');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz � Vienne','Musiques actuelles','www.jazzavienne.com','Vienne','28/06/2019','13/07/2019','38200','45.520578372','4.88135156154');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NATURAL GAMES','Transdisciplinaire','www.naturalgames.fr','MILLAU','27/06/2019','29/06/2019','12100','44.0976252203','3.11705384129');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA RUE RALE','Transdisciplinaire','https://www.facebook.com/laruerale/','Allaire','28/06/2019','29/06/2019','56350','47.6392193445','-2.17681507496');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL PIC''ARTS','Musiques actuelles','www.festival-picarts.com','SEPTMONTS','28/06/2019','30/06/2019','2200','49.3384597812','3.36210276584');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''ANJOU','Th��tre','www.festivaldanjou.com','Longuen�e-en-Anjou','10/06/2019','29/06/2019','49770','47.5607664036','-0.683468497352');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DE PEROUGES','Musiques actuelles','www.festival-perouges.org','PEROUGES','12/06/2019','27/06/2019','1800','45.8803945768','5.17955874977');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''ANJOU','Th��tre','http://www.festivaldanjou.com/','ANGERS','10/06/2019','29/06/2019','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GUITARE EN SAVE','Musiques actuelles','http://www.guitarensave.fr/','MONTAIGUT SUR SAVE','05/06/2019','09/06/2019','31530','43.6803943619','1.2345663902');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du roman noir (FIRN)','Livre et litt�rature','www.firn-frontignan.fr','FRONTIGNAN','28/06/2019','30/06/2019','34110','43.4482458873','3.74923522416');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES TRANSFRONTALIERES','Musiques actuelles','https://transfrontalieres.wordpress.com/','Hirson','01/06/2019','02/06/2019','2500','49.9426458618','4.10483732841');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA NUIT DU BLUES DE CABANNES','Musiques actuelles','http://lanuitdubluesdecabannes.com','CABANNES','15/06/2019','06/07/2019','13440','43.8584804516','4.95793402726');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SENS DESSUS DESSOUS','Pluridisciplinaire Spectacle vivant','http://tourisme-mallemortdeprovence.com/la-grande-fete/','MALLEMORT','15/06/2019','15/06/2019','13370','43.7285832667','5.18297726548');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE A LA PREE','Musiques classiques','','Segry','07/06/2019','10/06/2019','36100','46.8846862374','2.09589433359');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAITES DE LA CHANSON','Musiques actuelles','www.didouda.net','ARRAS','21/06/2019','29/06/2019','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Montpellier Danse','Danse','www.montpellierdanse.com','MONTPELLIER','22/06/2019','06/07/2019','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL 36H DE SAINT EUSTACHE','Musiques actuelles','http://36h-saint-eustache.com','PARIS','20/06/2019','21/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cin�ma des peuples Anuu-ru-Aboro','Cin�ma et audiovisuel','http://www.anuuruaboro.com/','Poindimie','','','98822','-20.936331','165.330548');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Courant 3D','Cin�ma et audiovisuel','http://www.courant3d-filmfest.com/fr/accueil/','ANGOULEME','','','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AZIMUT FESTIVAL','Musiques actuelles','www.azimutfestival.com','LA PESSE','','','39370','46.286468541','5.85238812673');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film du Croisic De la page � l''image','Cin�ma et audiovisuel','http://www.festivaldufilmducroisic.com/','Le Croisic','','','44490','47.2922743449','-2.52277844422');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUT SIMPLEMENT HIP HOP','Musiques actuelles','www.dastorm.fr','NIMES','','','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WORLD TRANCE FESTIVAL','Musiques actuelles','www.world-trance.fr','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRUIT''FIER ROCK FESTIVAL','Musiques actuelles','www.bruitfier-rock.fr','LA BRUFFIERE','','','85530','47.0148006973','-1.18329758318');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIBRATIONS URBAINES','Musiques actuelles','www.vibrations-urbaines.net','PESSAC','','','33600','44.7915990521','-0.676303166277');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Voix d''�toiles � festival international des voix du cin�ma d''animation','Cin�ma et audiovisuel','www.voixdetoiles.com','LEUCATE','','','11370','42.8993650672','3.02676038573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DANSES ET CONTINENTS NOIRS','Danse','www.jamescarles.com/centre/festival','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL AQUI CUBA','Musiques actuelles','http://www.aquicuba.fr/','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Grandchamp''Bardement','Cirque et Arts de la rue','http://www.grandchampbardement.fr/','Grandchamp des Fontaines','','','44119','47.3606796643','-1.61754475868');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GIVRES A GIVRAND','Musiques actuelles','http://www.lesgivres.fr/','GIVRAND','','','85800','46.6822701061','-1.8787272243');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK N LIVE','Musiques actuelles','https://rocknlive.jimdo.com','Sartrouville','','','78500','48.9396085498','2.17458686513');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATLANTIQUE JAZZ FESTIVAL','Musiques actuelles','www.atlantiquejazzfestival.com','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale d''art contemporain de Rennes','Arts plastiques et visuels','','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUT EN HAUT DU JAZZ','Musiques actuelles','https://www.facebook.com/toutenhautdujazz/','Bethune','','','62400','50.5288922584','2.64242379342');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musiques d�mesur�es','Musiques classiques','http://www.musiquesdemesurees.net','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES EMBRAZZEES','Musiques actuelles','http://www.les-embrazzees.net','Montelimar','','','26200','44.5540845734','4.74869387765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PITCHFORK MUSIC FESTIVAL','Musiques actuelles','www.pitchforkmusicfestival.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Ecrans documentaires','Cin�ma et audiovisuel','www.lesecransdocumentaires.org','ARCUEIL','','','94110','48.8058803597','2.33351024984');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AUX CARR�S','Musiques actuelles','www.mjc-annecylevieux.com','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES QUATRE TEMPS','Musiques actuelles','http://www.festivaldes4temps.com','Rouziers de Touraine','','','37360','47.5311263676','0.647449146554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lire en Polyn�sie','Livre et litt�rature','http://www.lireenpolynesie.com/','PAPEETE','','','98714','-17.552056','-149.55886');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la biographie','Livre et litt�rature','https://www.festivaldelabiographie.com/','NIMES','25/01/2019','27/01/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CROSSOVER SUMMER','Musiques actuelles','www.festival-crossover.com','NICE','23/08/2019','24/08/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN AO�T','Musiques actuelles','www.jazzinout.fr','NIEUL SUR MER','16/08/2019','18/08/2019','17137','46.2087181622','-1.16596945116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SCOPITONE','Musiques actuelles','www.scopitone.org','NANTES','18/09/2019','22/09/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES VERTES ET DES PAS MURES','Musiques actuelles','http://www.festival-dvpm.org','FAREINS','14/09/2019','14/09/2019','1480','46.020997974','4.7620080898');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les rencontres de Chaminadour','Livre et litt�rature','https://www.chaminadour.com/','GUERET','19/09/2019','22/09/2019','23000','46.1632121428','1.87078672735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A L''AIZE FEST','Pluridisciplinaire Spectacle vivant','','AIZENAY','','','85190','46.7384516809','-1.62702889721');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LILLE PIANO(S) FESTIVAL','Musiques classiques','www.lillepianosfestival.fr','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Sans Nom, le polar � Mulhouse','Livre et litt�rature','http://www.festival-sans-nom.fr/','MULHOUSE','19/10/2019','20/10/2019','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEVEN NIGHTS TO BLUES FESTIVAL','Musiques actuelles','http://www.nouveaumondeblues.org','ST ANDRE LEZ LILLE','','','59350','50.6597692969','3.04609387889');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARVERNE REGGAE FESTIVAL','Musiques actuelles','http://arvernereggaefest.wixsite.com/arverne-reggae-fest','Brassac les Mines','','','63570','45.4199156591','3.32061008295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de g�ographie','Livre et litt�rature','http://www.fig.saint-die-des-vosges.fr/','ST Die des Vosges','04/10/2019','06/10/2019','88100','48.2967093514','6.93809367547');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres philosophiques de Langres','Livre et litt�rature','www.rencontresphilosophiqueslangres.fr','LANGRES','27/09/2019','06/10/2019','52200','47.8591544821','5.33880466821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Exhibit','Arts plastiques et visuels','http://letetris.fr/','LE HAVRE','','','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MENUHIN','Musiques classiques','http://www.festivalhommagemenuhin.com','VILLE D AVRAY','','','92410','48.8215895592','2.17761311514');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTS S''EN M�LENT','Musiques actuelles','www.art-zimut.org','LES LANDES GENUSSON','','','85130','46.9663828627','-1.12900644447');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT JAZZ SUR VIE','Musiques actuelles','','ST GILLES CROIX DE VIE','','','85800','46.6904708814','-1.91946363327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'H�T�ROCLITES','Pluridisciplinaire Spectacle vivant','www.artplume.org','ST LO','','','50000','49.1099624249','-1.07755642702');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATOUT COEURS','Musiques actuelles','www.com','BENQUET','','','40280','43.8221288864','-0.507091491966');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES AVAILLES FESTIVAL','Musiques actuelles','www.lanoteblues.com','AVAILLES EN CHATELLERAULT','','','86530','46.7561922634','0.569776256918');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALLIANCES URBAINES','Musiques actuelles','http://alliancesurbaines.fr/','BAGNEUX','','','92220','48.7983229866','2.30989995212');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ360','Musiques actuelles','https://www.jazz360.fr/','CENAC','','','33360','44.7826946723','-0.456684447802');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONVIVENCIA A ARLES','Musiques actuelles','https://convivenciaarles.wixsite.com','ARLES','','','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rue des arts � Barr','Arts plastiques et visuels','https://www.barr.fr/loisirs/rue-des-arts','BARR','','','67140','48.4149512356','7.39930438782');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Under my screen, Festival des films britanniques d''Ajaccio','Cin�ma et audiovisuel','http://www.under-my-screen.com/','AJACCIO','','','20167','41.9347926638','8.70132275974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de cin�ma Du grain � d�moudre','Cin�ma et audiovisuel','http://dugrainademoudre.net/','Gonfrevillel Orcher','','','76700','49.4860837457','0.221985693724');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WINTER CAMP','Musiques actuelles','http://www.wintercampfestival.fr/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te des Lumi�res','Arts plastiques et visuels','http://www.fetedeslumieres.lyon.fr/fr','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine du cin�ma hispanique','Cin�ma et audiovisuel','http://cinehispanique.fr/lassociation','CLERMONT FERRAND','02/04/2019','09/04/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES EN RADE','Musiques actuelles','https://sites.google.com/view/bluesenrade','LOCMIQUELIC','04/04/2019','07/04/2019','56570','47.7303087589','-3.32842997885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FRENETIQUES','Musiques actuelles','www.jazzebre.com','PERPIGNAN','03/04/2019','06/04/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ELECTROCHOC','Musiques actuelles','www.electrochoc-festival.com','BOURGOIN JALLIEU','16/03/2019','30/03/2019','38300','45.6022027954','5.27393762111');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Central Vapeur','Livre et litt�rature','https://centralvapeur.org/','STRASBOURG','21/03/2019','31/03/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HARDKAZE FESTIVAL','Musiques actuelles','hardkaze.com','TOULON','09/03/2019','09/03/2019','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rendez-vous de l�aventure','Livre et litt�rature','https://www.rdv-aventure.fr/','LONS LE SAUNIER','14/03/2019','17/03/2019','39000','46.6744796278','5.55733212947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAGIC MONT-BLANC FESTIVAL','Divers Spectacle vivant','www.chamonix.fr','CHAMONIX MONT BLANC','10/04/2019','14/04/2019','74400','45.9309819111','6.92360096711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival TOTO TOTAL','Pluridisciplinaire Spectacle vivant','www.teat.re','ST DENIS','05/03/2019','16/03/2019','97400','-20.9329708192','55.446867167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTIMINI','Pluridisciplinaire Spectacle vivant','www.ville-bethune.fr','Bethune','05/03/2019','06/04/2019','62400','50.5288922584','2.64242379342');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HARPE EN AVESNOIS','Pluridisciplinaire Musique','http://www.harpeenavesnois.com','MAUBEUGE','26/02/2019','10/03/2019','59600','50.283630719','3.96328053037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cin�ma Nature','Cin�ma et audiovisuel','http://www.rencontres-cinema-nature.eu/','Dompierre sur Besbre','05/04/2019','07/04/2019','3290','46.5305895294','3.67644076991');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jardins d''hiver','Livre et litt�rature','www.leschampslibres.fr','RENNES','01/02/2019','03/02/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU FIL DES VOIX PARIS','Musiques actuelles','www.aufildesvoix.com','PARIS','04/02/2019','16/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ZYGOMATIC FESTIVAL','Divers Spectacle vivant','www.instinctaf.net','CHAMBERY','27/03/2019','13/04/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CHANTS DE MARS','Musiques actuelles','www.leschantsdemars.com','LYON','23/03/2019','30/03/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUX RIRES ETC','Divers Spectacle vivant','http://auxrires.fr/','GRENOBLE','20/01/2019','26/01/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA FOLLE JOURN�E EN R�GION DES PAYS DE LA LOIRE','Musiques classiques','www.fra','NANTES','31/01/2019','04/02/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ISSY FEN�TRE SUR LE MONDE','Pluridisciplinaire Spectacle vivant','http://www.musiquesdumonde.fr/Festival-Issy-Fenetre-sur-le-monde-10345','ISSY LES MOULINEAUX','19/04/2019','21/04/2019','92130','48.82347434','2.26449823277');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VELOSHOW','Transdisciplinaire','https://www.veloshow.fr/','SOMMIERES','21/04/2019','22/04/2019','30250','43.7756118498','4.08232743857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE MODE ET DE PHOTOGRAPHIE DE HYERES','Arts plastiques et visuels','www.villanoailles-hyeres.com','HYERES','25/04/2019','29/04/2019','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHAP','Cirque et Arts de la rue','https://www.chap-festival.com','Viols Le Fort','25/04/2019','28/04/2019','34380','43.7367573026','3.69220324597');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Eldorado','Th��tre','http://theatredelorient.fr/festival-eldorado/','LORIENT','24/04/2019','28/04/2019','56100','47.7500486947','-3.37823200917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JEUDIS ELECTRO','Musiques actuelles','www.lesjeudiselectro.com','THONON LES BAINS','18/07/2019','22/08/2019','74200','46.3704258049','6.48194336071');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARTIST''O CHAMPS','Musiques actuelles','www.artistochamps.jimdo.com','VIHIERS','21/07/2019','21/07/2019','49310','47.1373301524','-0.559450064005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Musique et m�moire','Musiques classiques','http://www.musetmemoire.com/','FAUCOGNEY ET LA MER','19/07/2019','04/08/2019','70310','47.8319641353','6.5834328372');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS SALINES','Musiques actuelles','http://www.lesnuitssalines.com/','BATZ SUR MER','19/07/2019','21/07/2019','44740','47.2844487793','-2.474374647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ILLAC EN SCENE','Musiques actuelles','http://www.illac-en-scene.fr/','ST Jean d Illac','06/07/2019','06/07/2019','33127','44.7934010703','-0.819193904642');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A JUNAS','Musiques actuelles','www.jazzajunas.fr','JUNAS','16/07/2019','20/07/2019','30250','43.7615067362','4.11752159876');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REGGAE FAMILY FESTIVAL','Musiques actuelles','','Cogolin','12/07/2019','14/07/2019','83310','43.2464550515','6.52192234084');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ITIN�RAIRE BAROQUE EN P�RIGORD VERT','Musiques classiques','www.itinerairebaroque.com','RIBERAC','25/07/2019','28/07/2019','24600','45.2457883325','0.334782848632');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz in Marciac','Musiques actuelles','www.jazzinmarciac.com','MARCIAC','25/07/2019','15/08/2019','32230','43.521249291','0.156300856185');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Debussy','Musiques classiques','http://www.festivaldebussy.com','ARGENTON SUR CREUSE','26/07/2019','28/07/2019','36200','46.5787862593','1.4926283101');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'The Green Escape','Musiques actuelles','www.festivaldecraponne.com','CRAPONNE SUR ARZON','26/07/2019','28/07/2019','43500','45.3318549451','3.85314004519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRACK''N''ART FESTIVAL','Musiques actuelles','http://tracknart.fr/','DOUE LA FONTAINE','26/07/2019','27/07/2019','49700','47.1914715686','-0.278336056087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Crest Jazz Vocal','Musiques actuelles','www.crestjazzvocal.com','Crest','26/07/2019','03/08/2019','26400','44.7335665907','5.0221758413');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Eurock�ennes de Belfort','Musiques actuelles','www.eurockeennes.fr','CRAVANCHE','04/07/2019','07/07/2019','90300','47.6518069558','6.82383810256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Meythet la musique','Musiques actuelles','','ANNECY','04/07/2019','25/07/2019','74960','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les D�ferlantes d''Argel�s-sur-Mer','Musiques actuelles','www.festival-lesdeferlantes.com','ARGELES SUR MER','05/07/2019','08/07/2019','66700','42.5352193463','3.02429862885');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT ROCK','Musiques actuelles','www.saintrock.eu','LA CLAYETTE','05/07/2019','06/07/2019','71800','46.2892944317','4.31079967145');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EN NORD BEAT FESTIVAL','Musiques actuelles','http://www.ennordbeat.fr/','BAILLEUL','05/07/2019','06/07/2019','59270','50.7274252798','2.7379912081');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�vasion Festival','Musiques actuelles','','Vaulx en Velin','05/07/2019','06/07/2019','69120','45.7858821061','4.92637767698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEAU C''EST FESTIVAL','Musiques actuelles','https://www.asso-passerelles.fr/','BOSSET','16/08/2019','17/08/2019','24130','44.9518343801','0.362233138174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Banquet du livre d''�t� � Lagrasse','Livre et litt�rature','https://www.lamaisondubanquet.fr/banquets/','Lagrasse','02/08/2019','09/08/2019','11220','43.0957230349','2.60436046295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ANYMAL','Pluridisciplinaire Spectacle vivant','https://www.facebook.com/anymalworld','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Cin�matographiques de Cannes','Cin�ma et audiovisuel','www.cannes-cinema.com','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Th��tre � tout �ge','Th��tre','https://www.tres-tot-theatre.com/','QUIMPER','','','29000','47.9971425162','-4.09111944455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ CONILHAC CORBIERES','Musiques actuelles','','CONILHAC','','','11200','43.1847654132','2.71572755182');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Collectif Jeune Cin�ma -  festival des cin�mas diff�rents et exp�rimentaux','Cin�ma et audiovisuel','http://www.cjcinema.org/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EPHEMERE','Musiques actuelles','','HAUTEVILLE LOMPNES','','','1110','45.9696520061','5.57627261783');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIESTA LA MASS','Musiques actuelles','www.massprod.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international des cin�mas de Guerre War on screen','Cin�ma et audiovisuel','www.cjcinema.org','CHALONS EN CHAMPAGNE','','','51000','48.9640892125','4.37883539725');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRENOBLE ALPES METROPOLE JAZZ FESTIVAL','Musiques actuelles','www.jazzclubdegrenoble.fr','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'R.O.N FESTIVAL','Pluridisciplinaire Spectacle vivant','https://readyornot.fr/events-ron-festival/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre d''Aix','Livre et litt�rature','http://www.citedulivre-aix.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DES VENDANGES DE MONTMARTRE','Transdisciplinaire','www.fetedesvendangesdemontmartre.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIAC (Foire internationale d''art contemporain)','Arts plastiques et visuels','http://www.fiac.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL OFF OFF OFF off des Nuits en Champagne)','Musiques actuelles','www.nuitsdechampagne.com','TROYES','','','10000','48.2967099637','4.07827967525');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIMOMES','Pluridisciplinaire Spectacle vivant','http://www.mjctati.org','ORSAY','','','91400','48.7004093953','2.18737051177');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DROME DE GUITARES','Musiques classiques','http://www.dromedeguitares.org','VALENCE','','','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Charivari','Cirque et Arts de la rue','www.esat-evasion.fr','SELESTAT','','','67600','48.2481136279','7.46224896454');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POP FACTORY','Musiques actuelles','https://legrandmix.com/','ROUBAIX','','','59100','50.6879774811','3.18258434623');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COUP D''PARQUET','Musiques actuelles','www.assoparceque5.wixsite.com/parceque/coup-d-parquet','MAMERS','','','72600','48.3545313197','0.370783494811');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � L''OUEST','Musiques actuelles','www.jazzalouest.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES INSULAIRES !','Musiques actuelles','www.lesinsulaires.com','GROIX','','','56590','47.6372394221','-3.4644268016');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES OREILLES EN POINTE','Musiques actuelles','www.oreillesenpointe.com','Le Chambon Feugerolles','','','42500','45.3886064564','4.33196255695');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cit�philo','Livre et litt�rature','http://www.citephilo.org/','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'STOMPING AT SECRET PLACE','Musiques actuelles','www.tafproduction.blogspot.fr/','ST JEAN DE VEDAS','','','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU FIL DE L''OISE','Musiques actuelles','www.jazzaufildeloise.fr','MONTMORENCY','','','95160','48.9918643363','2.32119797848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'O LES CH�URS Tulle','Musiques actuelles','https://elizabethmydear.fr/','TULLE','','','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES CAMPAGNES','Musiques actuelles','http://fdc.absolu-live.fr/','ST LAURENT NOUAN','','','41220','47.709174366','1.62466277515');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIK FABRIK - XERPILS FESTIVAL','Musiques actuelles','www.musikfabrik.fr','Xertigny','','','88220','48.0387121214','6.38237551394');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROSCELLA BAY FESTIVAL','Musiques actuelles','http://www.roscellabay.fr/','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Paris Summer Jam','Musiques actuelles','https://www.livenation.fr/','NANTERRE','','','92000','48.8960701282','2.20671346353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la Fiction de La Rochelle','Cin�ma et audiovisuel','www.festival-fictiontv.com','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ST HIL JAZZ FESTIVAL','Musiques actuelles','www.sthiljazzfestival.com','ST Hilaire de Chaleons','','','44680','47.0958948849','-1.87983637967');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUAILLE NOTE','Musiques actuelles','www.ouaille-note-festival.fr','VASLES','','','79340','46.5737648617','-0.0494104571753');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KIOSQUES A MUSIQUE','Musiques actuelles','http://www.kiosquesamusique.com','LA VOULTE SUR RHONE','','','7800','44.8015403943','4.78018830634');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Jeudis de Perpignan','Cirque et Arts de la rue','https://fr-fr.facebook.com/jeudisdeperpi/','PERPIGNAN','','','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DROLES DE RUES (SITES EN SCENE)','Pluridisciplinaire Spectacle vivant','http://www.jonzac-tourisme.com','JONZAC','','','17500','45.4419827405','-0.427614839205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SAM AFRICA','Transdisciplinaire','http://www.sam-africa.com','Samatan','','','32130','43.4935426323','0.934596447621');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''PLAGE','Musiques actuelles','www.festi-plage.fr','CHAMPAGNAC DE BELAIR','','','24530','45.400469208','0.69507098451');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EQUIBLUES','Musiques actuelles','http://www.equiblues.com','ST AGREVE','','','7320','45.0052980637','4.41285687419');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL RITE','Musiques actuelles','http://www.bethmalais.com','ST GIRONS','','','9200','42.9752923045','1.15326595533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la Chaise Dieu','Musiques classiques','www.chaise-dieu.com','La Chaise Dieu','','','43160','45.3187166909','3.69286458356');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Crescendo','Musiques actuelles','www.festival-crescendo.com','ST Palais sur Mer','','','17420','45.6579754062','-1.10115118709');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUES JUIVES DE CARPENTRAS','Musiques actuelles','http://www.festival-musiques-juives-carpentras.com','CARPENTRAS','','','84200','44.0593802565','5.06134844776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAMBA AL PAIS','Musiques actuelles','http://www.sambalpais.org','ST ANTONIN NOBLE VAL','','','82140','44.156093011','1.73640541242');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URBAN CULTURE','Pluridisciplinaire Spectacle vivant','www.lafabrique-gueret.fr/','GUERET','14/05/2019','19/05/2019','23000','46.1632121428','1.87078672735');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''EPAU','Musiques classiques','https://epau.sarthe.fr/','LE MANS','21/05/2019','28/05/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICALARUE SUR UN PLATEAU','Musiques actuelles','www.musicalarue.com','LUXEY','11/05/2019','11/05/2019','40430','44.2207948584','-0.515670146445');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ATELIERS JAZZ DE MESLAY-GREZ','Musiques actuelles','www.festivaljazz-meslay.com','MESLAY DU MAINE','25/05/2019','01/06/2019','53170','47.9469156243','-0.558693863131');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THIS IS NOT A LOVE SONG','Musiques actuelles','www.thisisnotalovesong.fr','NIMES','30/05/2019','01/06/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DISCIPLINE FESTIVAL','Musiques actuelles','http://https//www.facebook.com/disciplinetoulouse/','TOULOUSE','23/05/2019','25/05/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN PIC SAINT LOUP','Musiques actuelles','http://www.jazzajunas.fr/festival-jazz-en-pic-st-loup','Le Triadou','14/06/2019','15/06/2019','34270','43.7367810666','3.85739515015');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'WHEELS AND WAVES','Transdisciplinaire','https://www.wheels-and-waves.com/','BIARRITZ','12/06/2019','16/06/2019','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PROG SUD','Musiques actuelles','http://progsudfestival.nuxit.net','Les Pennes Mirabeau','30/05/2019','01/06/2019','13170','43.4026791235','5.31547094057');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ABSOLUTE GOSPEL','Musiques actuelles','www.absolutegospel.net','LYON','01/06/2019','01/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Banquet de printemps de Lagrasse','Livre et litt�rature','www.lamaisondubanquet.fr','Lagrasse','31/05/2019','02/06/2019','11220','43.0957230349','2.60436046295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK & CARS','Musiques actuelles','','Lavaur','08/06/2019','09/06/2019','81500','43.6895628711','1.79437213708');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES ET RACINES','Musiques actuelles','http://rencontresetracines.audincourt.fr/','AUDINCOURT','28/06/2019','30/06/2019','25400','47.4811676376','6.85493983157');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DU TRAVAILLEUR ALPIN','Musiques actuelles','https://fete.travailleur-alpin.fr/','FONTAINE','28/06/2019','30/06/2019','38600','45.1930502486','5.67763066542');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BI HARRIZ LAU XORI','Pluridisciplinaire Spectacle vivant','https://biarritz-culture.com/evenement/bi-harriz-lau-xori/','BIARRITZ','','','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'30/30 Les Rencontres de la forme courte','Pluridisciplinaire Spectacle vivant','','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � LA VILLETTE','Musiques actuelles','www.jazzalavillette.com','PARIS','29/08/2019','10/09/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lectures sous l''arbre','Livre et litt�rature','http://www.lectures-sous-larbre.com/','LE CHAMBON SUR LIGNON','18/08/2019','24/08/2019','43400','45.0524632592','4.31966525163');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ CAMPUS EN CLUNISOIS','Musiques actuelles','www.jazzcampus.fr','CLUNY','17/08/2019','24/08/2019','71250','46.4303628582','4.67033276327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Musique de chambre � Giverny','Musiques classiques','https://musiqueagiverny.fr/','GIVERNY','22/08/2019','01/09/2019','27620','49.0819438915','1.53288773518');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Normandiebulle','Livre et litt�rature','www.normandiebulle.com','Darnetal','28/09/2019','29/09/2019','76160','49.4445058359','1.15546994024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Prix de court','Cin�ma et audiovisuel','https://www.festivalprixdecourt.com/accueil','POINT A PITRE','','','97110','16.2386006963','-61.5357180881');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NINKASI URBAN WEEK','Musiques actuelles','http://nuw.ninkasi.fr/','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE OUTBREAK FESTIVAL','Musiques actuelles','','BLOIS','','','41000','47.5817013938','1.30625551583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK MON FORT','Musiques actuelles','','Montfort sur Risle','','','27290','49.2945278867','0.670560456278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Br''Ain de cirque','Cirque et Arts de la rue','https://etac01.com','BOURG EN BRESSE','','','1000','46.2051520382','5.24602125501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du mot','Livre et litt�rature','http://www.festivaldumot.fr/','La Charite sur Loire','','','58400','47.1835862006','3.02885515594');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAINS NUM�RIQUES','Musiques actuelles','www.bainsnumeriques.fr','ENGHIEN LES BAINS','','','95880','48.97016048','2.30485955429');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Zones portuaires - Rencontres internationale cin�ma et villes portuaires','Cin�ma et audiovisuel','http://www.zonesportuaires-saintnazaire.com/','ST Nazaire','','','44600','47.2802857028','-2.25379927249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONTRE TEMPS','Musiques actuelles','www.contre-temps.net','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MES DE JAZZ','Musiques actuelles','https://www.lesnitsdeus.org','EUS','','','66500','42.657654474','2.44661138862');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Forum Fait son Festival','Musiques actuelles','','Calonne Ricouart','','','62470','50.4898416084','2.48534476198');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIG''HALLE','Musiques actuelles','http://ruehauteproductions.fr/pig-halle','ST ANTOINE L ABBAYE','','','38160','45.1722353559','5.21609050981');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHECK THE RHYME','Musiques actuelles','','NICE','','','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cigogne en F�te','Pluridisciplinaire Spectacle vivant','','Soubise','','','17780','45.9097262725','-1.00226108589');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ MUSETTE DES PUCES','Musiques actuelles','www.festivaldespuces.com','ST OUEN','','','93400','48.909806575','2.33257042205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Impromptus','Cirque et Arts de la rue','','ST DENIS','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ ENTRAIGUES FESTIVAL','Musiques actuelles','http://jazzentraiguesfestival.wifeo.com/','ENTRAIGUES SUR LA SORGUE','','','84320','43.9950173356','4.93295387179');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICAL''�T� (ANNEMASSE)','Musiques actuelles','www.annemasse.fr','ANNEMASSE','','','74100','46.1909730986','6.24250704322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GAR�ON LA NOTE','Musiques actuelles','www.garcon-la-note.com','AUXERRE','','','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES EMBELLIES','Musiques actuelles','www.festival-lesembellies.com','RENNES','02/05/2019','04/05/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ADO','Th��tre','https://lepreaucdn.fr/theatre/le-festival-ado/','VIRE','14/05/2019','25/05/2019','14500','48.8512498219','-0.889601911342');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des 5 continents - Franco Genevois','Cin�ma et audiovisuel','www.festival5continents.org','Ferney Voltaire','17/05/2019','26/05/2019','1210','46.2519789243','6.10826403805');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BONNE MACHINE','Musiques actuelles','www.mjcconflans.org','CONFLANS STE HONORINE','18/05/2019','18/05/2019','78700','49.0001007823','2.0990719052');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUTCH ! EXTREME METAL FESTIVAL','Musiques actuelles','','LANGRES','24/05/2019','25/05/2019','52200','47.8591544821','5.33880466821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de Talloires','Livre et litt�rature','www.talloires-lac-annecy.com','Talloires Montmin','25/05/2019','26/05/2019','74210','45.8357923857','6.23052089871');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BORDURES','Musiques actuelles','http://www.bordures.bzh','Langon','28/05/2019','01/06/2019','35660','47.733471612','-1.87194975237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARGUEST O LIVE','Musiques actuelles','https://marguestolive.wordpress.com/','Aucamville','01/06/2019','01/06/2019','82600','43.7960241357','1.22532777135');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Festival de Paris','Musiques classiques','http://lefestival.paris/','PARIS','04/06/2019','27/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUCARD DE TOURS','Musiques actuelles','www.radiobeton.com','TOURS','05/06/2019','09/06/2019','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOTES EN VERT','Musiques actuelles','https://www.notesenvert.fr/','PERIGNY','07/06/2019','09/06/2019','17180','46.1574551352','-1.08886084864');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LUCIOL IN THE SKY','Musiques actuelles','https://www.cavazik.org','Charnay les Macon','14/06/2019','15/06/2019','71850','46.3041852737','4.78693813476');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Clameur(s) - Rencontres litt�raires','Livre et litt�rature','https://clameurs.dijon.fr/','DIJON','14/06/2019','16/06/2019','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Mans fait son Cirque','Cirque et Arts de la rue','http://www.lemansfaitsoncirque.fr','LE MANS','18/06/2019','30/06/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SARABANDES','Transdisciplinaire','www.sarabandes.lapalene.fr','ROUILLAC','28/06/2019','30/06/2019','16170','45.781947293','-0.074390136916');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRANCE BLEU LIVE FESTIVAL','Musiques actuelles','https://www.francebleu.fr/musique/concerts-live/france-bleu-live-festival-aux-2-alpes-1551768521','Les Deux Alpes','17/04/2019','19/04/2019','38860','45.0159885994','6.13818619624');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HELLFEST WARM UP TOUR','Musiques actuelles','http://www.hellfest.fr/warm-up-tour-hellfest/','PARIS','21/04/2019','27/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Live in Tignes by Francofolies','Musiques actuelles','www.tignes.net','Tignes','16/04/2019','18/04/2019','73320','45.4816828491','6.93635513554');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEAUTIFUL SWAMP BLUES FESTIVAL','Musiques actuelles','www.spectacle-gtgp.calais.fr','CALAIS','26/04/2019','28/04/2019','62100','50.9502072754','1.87575566132');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres cin�matographiques de Digne','Cin�ma et audiovisuel','http://www.unautrecinema.com/','DIGNE LES BAINS','23/04/2019','26/04/2019','4000','44.0908723554','6.23590323452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film documentaire oc�anien (FIFO)','Cin�ma et audiovisuel','https://www.fifotahiti.com/','PAPEETE','02/02/2019','10/02/2019','98714','-17.552056','-149.55886');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du premier film d''Annonay','Cin�ma et audiovisuel','www.annonaypremierfilm.org','ANNONAY','08/02/2019','18/02/2019','7100','45.2460902392','4.65026947295');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESCALE D''HUMOUR','Divers Spectacle vivant','https://www.royanatlantique.fr/','ROYAN','16/02/2019','23/02/2019','17200','45.6346574238','-1.01791403375');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Da�sFabrik','Danse','http://www.dansfabrik.com','BREST','25/02/2019','02/03/2019','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DE L''ALLIGATOR','Musiques actuelles','www.nuitsdelalligator.com','PARIS','31/01/2019','23/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HIBERNAROCK','Musiques actuelles','www.hibernarock.fr','AURILLAC','02/02/2019','17/03/2019','15000','44.9245233686','2.44162453828');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAMONIX UNLIMITED FESTIVAL','Musiques actuelles','http://www.chamonix-unlimited.com','CHAMONIX MONT BLANC','02/04/2019','07/04/2019','74400','45.9309819111','6.92360096711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JOURS DE JAZZ','Musiques actuelles','www.lafertesaintaubin.com','LA FERTE ST AUBIN','28/03/2019','31/03/2019','45240','47.7119750833','1.92714666074');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Entre les lignes - Salon du livre de Bondues','Livre et litt�rature','https://www.ville-bondues.fr/salondulivre/','Bondues','23/03/2019','24/03/2019','59910','50.7096656233','3.09529083038');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Confrontation - Festival de Perpignan','Cin�ma et audiovisuel','www.inst-jeanvigo.eu','PERPIGNAN','02/04/2019','07/04/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA TERRE EST A NOUS','Musiques actuelles','http://www.nanterre.fr/','NANTERRE','13/03/2019','17/04/2019','92000','48.8960701282','2.20671346353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CEM ON FEST','Musiques actuelles','https://www.le-cem.com/concerts-evenements/','LE HAVRE','21/03/2019','23/03/2019','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Grand festival contre le racisme, l�antis�mitisme et la haine anti-LGBT','Transdisciplinaire','http://www.histoire-immigration.fr/','PARIS','16/03/2019','24/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PALMA','Transdisciplinaire','www.palmafestival.com','CAEN','03/04/2019','07/04/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAR SCENE ROCK SON','Musiques actuelles','','Carcen Ponson','13/04/2019','13/04/2019','40400','43.871874652','-0.81936363917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES HERONS MATH LE SON','Musiques actuelles','https://lesheronsmathleson.fr/','ST MATHURIN SUR LOIRE','09/03/2019','09/03/2019','49250','47.4219645317','-0.319673646947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DE LA CHANSON','Musiques actuelles','www.odc-orne.com','SEES','12/03/2019','31/03/2019','61500','48.6048296774','0.169026415284');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L�Autre Salon','Livre et litt�rature','https://www.lautrelivre.fr/','PARIS','08/03/2019','10/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT-BENOIT SWING','Musiques actuelles','www.stbenoitswing.zic.fr','ST BENOIT','','','86280','46.5486469496','0.352443691442');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film arabe de Fameck','Cin�ma et audiovisuel','www.cinemarabe.org','Fameck','','','57290','49.2987006334','6.10649265804');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�CLATS DE VOIX (MAUL�ON)','Musiques actuelles','www.festival-eclats-de-voix.fr','NUEIL LES AUBIERS','','','79250','46.9403172404','-0.596385417876');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR SON 31','Musiques actuelles','www.jazz31.hautegaronne.fr','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TUSCANIA','Musiques classiques','www.festivaltuscania.com','VIC LE COMTE','','','63270','45.6440673683','3.24047430326');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE TRAVERS','Musiques actuelles','www.abcd45.com','ORLEANS','','','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Fiesta des Suds','Musiques actuelles','http://www.dock-des-suds.org','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''HUMOUR DE CARRY LE ROUET','Divers Spectacle vivant','http://www.mairie-carrylerouet.fr','Carry le Rouet','','','13620','43.3427663021','5.15373765817');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIRTHDAY FESTIVAL','Musiques actuelles','https://fr-fr.facebook.com/birthdayfestival/','ANNECY','','','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIG BANG FESTIVAL','Musiques actuelles','www.marvellous-island.fr','La Plaine ST Denis','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres avec les musiques anciennes','Musiques classiques','http://www.musiqueancienneenpicsaintloup.com/','ST GELY DU FESC','','','34980','43.6896523672','3.80969577473');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Carnavalorock','Musiques actuelles','www.carnavalorock.com','ST BRIEUC','','','22000','48.5149806053','-2.76154552773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAMMIN JUAN FESTIVAL','Musiques actuelles','www.jammin.jazzajuan.com','JUAN LES PINS','','','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAGILIBA','Musiques actuelles','http://bagiliba.fr/','FAYENCE','','','83440','43.6103345822','6.67843279926');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOOGIE SPIRIT FESTIVAL','Pluridisciplinaire Spectacle vivant','www.bsf4.com/fr','ILLKIRCH GRAFFENSTADEN','','','67400','48.5200498962','7.73129588098');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRIMEURS DE CASTRES','Musiques actuelles','www.lesprimeursdecastres.fr','CASTRES','','','81100','43.6156511237','2.23787231587');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LOOSTIK','Pluridisciplinaire Spectacle vivant','www.loostik.eu/start','FORBACH','','','57600','49.1912650369','6.89275798526');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INTRAMUROCK FESTIVAL','Musiques actuelles','www.intramurock.com','BOULOGNE SUR MER','','','62200','50.7271332663','1.60756334802');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Fen�tres sur courts','Cin�ma et audiovisuel','www.fenetres-sur-courts.com','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INVISIBLE','Musiques actuelles','www.festivalinvisible.com','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOBLUES FESTIVAL','Musiques actuelles','www.europajazz.fr','LE MANS','','','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES MOTS DES ETOILES','Th��tre','','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VACHES FOLKS','Musiques actuelles','http://www.lesvachesfolks.fr/','DIVONNE LES BAINS','','','1220','46.3756333495','6.1158647611');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EPINAL BOUGE L''ETE','Pluridisciplinaire Spectacle vivant','http://www.epinal.fr/','EPINAL','','','88000','48.1631202656','6.47989286928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ENFANTS DU MONDE','Musiques actuelles','www.rife.asso.fr','ST MAIXENT L ECOLE','','','79400','46.4128319027','-0.210238265156');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ARTS JAILLISSANTS','Musiques classiques','www.les-arts-jaillissants.fr','Montsapey','','','73220','45.5314889919','6.36669427224');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International de Musique de Dinard','Musiques classiques','http://www.festival-music-dinard.com/','DINARD','','','35800','48.6241805945','-2.0619828606');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DIMANCHES AU BORD DE L''ORNE','Musiques actuelles','www.ville-sees.fr','SEES','','','61500','48.6048296774','0.169026415284');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival L''ACCORD�ON','Musiques actuelles','www.fetedelaccordeon.com','SAULIEU','','','21210','47.2840306915','4.23192469784');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FETES DE LA VIGNE','Pluridisciplinaire Spectacle vivant','www.fetesdelavigne.org','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE NONETTE / SCENES DE VIE','Musiques actuelles','http://www.lahautsijysuis.fr/','Nonette','','','63340','45.4846982805','3.28374300124');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film Nancy-Lorraine','Cin�ma et audiovisuel','www.fifnl.com','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CITHEM FESTIVAL','Transdisciplinaire','www.cithemfestival.fr','ALENCON','','','61000','48.4318193082','0.0915406916107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES TRETEAUX NOMADES','Th��tre','http://www.treteauxnomades.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK EN SEVRE','Musiques actuelles','www.accesrock.bandcamp.com/album/rock-en-sevre-2018','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''ILOPHONE','Musiques actuelles','http://www.ilophone.com','OUESSANT','','','29242','48.4601042319','-5.08534533544');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te de l''Humanit�','Musiques actuelles','http://fete.humanite.fr/','La Courneuve','','','93120','48.9322569546','2.39978064801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MAAD IN 93','Musiques actuelles','http://www.maad93.com','ST OUEN','','','93400','48.909806575','2.33257042205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK IN OPPOSITION','Musiques actuelles','www.rocktime.org','CARMAUX','','','81400','44.0566667256','2.16641418483');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SMMMILE - VEGAN POP FESTIVAL','Musiques actuelles','http://www.smmmilefestival.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ESPRIT DES PIERRES','Musiques classiques','https://www.ensemblebeatus.fr/','LIMOGES','','','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BRESIL EN MOUVEMENTS','Cin�ma et audiovisuel','www.bresilenmouvements.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MUSICALES DU PARC DES OISEAUX','Musiques actuelles','musicales.parcdesoiseaux.com','Villars Les Dombes','02/07/2019','12/07/2019','1330','45.9927150222','5.04507856642');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OASIS BIZZ''ART','Musiques actuelles','www.bizzartnomade.net','CHAROLS','04/07/2019','06/07/2019','26450','44.5895894991','4.95411000129');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CUIVRES EN DOMBES','Pluridisciplinaire Musique','http://www.cuivresendombes.org/','VILLARS LES DOMBES','05/07/2019','25/07/2019','1330','45.9927150222','5.04507856642');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE SON DES CUIVRES','Pluridisciplinaire Musique','www.lesondescuivres.com','MAMERS','05/07/2019','07/07/2019','72600','48.3545313197','0.370783494811');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ASTROPOLIS','Musiques actuelles','www.astropolis.org','BREST','05/07/2019','07/07/2019','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAUMON','Musiques actuelles','https://festivalsaumon.fr/','PONT SCORFF','05/07/2019','07/07/2019','56620','47.839096749','-3.41967999137');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Feufli�zhe, festival de la musique alpine','Musiques actuelles','www.feufliazhe.com','ANNECY','05/07/2019','07/07/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU COEUR DE COSNE','Pluridisciplinaire Spectacle vivant','http://www.festival-cosne.net','Cosne d Allier','07/07/2019','08/07/2019','3430','46.482962805','2.82323006329');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HARMONIC FESTIVAL','Musiques actuelles','http://harmonic-festival.com','TRIGANCE','12/07/2019','14/07/2019','83840','43.7414008338','6.43648594376');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL NATALA','Transdisciplinaire','https://www.hiero.fr/festival-natala/','COLMAR','18/07/2019','21/07/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE ROBION','Musiques actuelles','www.festivalderobion.com','ROBION','18/07/2019','27/07/2019','84440','43.8519678718','5.11008684319');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lollapalooza','Musiques actuelles','www.lollaparis.com','PARIS','20/07/2019','21/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES RANDONNEES MUSICALES DU FERRAND','Musiques classiques','','CLAVANS EN HAUT OISANS','22/07/2019','11/08/2019','38142','45.1175117933','6.16207850111');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Lectoure � voix haute','Livre et litt�rature','www.lectoure-voixhaute.fr','LECTOURE','23/07/2019','28/07/2019','32700','43.9405030561','0.650762392132');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � VILLES SUR AUZON','Musiques actuelles','https://www.jazzavillessurauzon.fr/','VILLES SUR AUZON','25/07/2019','01/08/2019','84570','44.0660605145','5.25707803023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEA FEST N SOUND','Musiques actuelles','http://www.plomodiern.fr/','Plomodiern','27/07/2019','27/07/2019','29550','48.1808861007','-4.21781568947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D�lices sonores','Musiques actuelles','http://delices-sonores.com','ST Tropez','08/08/2019','08/08/2019','83990','43.262314383','6.66343039908');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Saint-Nectaire Vall�e Verte Festival','Pluridisciplinaire Spectacle vivant','https://www.saintnectairevalleevertefestival.com/#Accueil.G','ST NECTAIRE','12/08/2019','25/08/2019','63710','45.5919139674','2.99779544156');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ROCKABILLY DE TARBES','Musiques actuelles','http://www.rockabilly-tarbes.com','TARBES','28/08/2019','01/09/2019','65000','43.2347859635','0.0660093937851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE CITY TRUCKS FESTIVAL','Musiques actuelles','http://www.thecitytrucksfestival.com','La Pommeraye','30/08/2019','01/09/2019','49620','47.3429800481','-0.869186945487');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MONTAGNE ET MUSIQUE','Transdisciplinaire','www.Fetedelamontagne.com','PALAISEAU','28/09/2019','29/09/2019','91120','48.7146765876','2.22881488083');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS EN TOUTES LETTRES','Livre et litt�rature','www.maisondelapoesieparis.com','PARIS','07/11/2019','18/11/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIENNALE MUSIQUES EN SC�NE','Pluridisciplinaire Musique','http://www.grame.fr/biennale-musiques-en-scene','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres cin�matographiques de P�zenas','Cin�ma et audiovisuel','www.lafccm.org','PEZENAS','','','34120','43.462990168','3.41651652125');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIKAMPUS','Musiques actuelles','https://fr-fr.facebook.com/asso.musikampus/','ARRAS','','','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Itin�rances d�Al�s','Cin�ma et audiovisuel','www.itinerances.org','ALES','','','30100','44.1250099126','4.08828501262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival G�o Cond� Rencontres','Divers Spectacle vivant','','NANCY','','','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Danse de tous les sens','Danse','https://www.dansedetouslessens.com/','FALAISE','','','14700','48.8957800281','-0.193401711782');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Anduze � l''Envers, festival AVeC envie!','Cirque et Arts de la rue','http://www.artsvivantsencevennes.fr/anduze-a-l-envers/','ANDUZE','','','30140','44.0507893674','3.97484165309');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du Chapiteau bleu','Cirque et Arts de la rue','http://www.lafeteduchapiteaubleu.fr','Tremblay en France','','','93290','48.9784304121','2.55468501543');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LEZ''ARTS DE LA RUE','Danse','www.lezartsdelarue.fr','AUCH','','','32000','43.6534300414','0.575190250459');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ECLATS DE VOIX A AUCH','Musiques actuelles','','AUCH','','','32000','43.6534300414','0.575190250459');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Rue est � Amiens','Cirque et Arts de la rue','http://www.larueestaamiens.com/','AMIENS','','','80000','49.9009532186','2.29007445539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Et 20 l''�t�','Cirque et Arts de la rue','https://www.et20lete.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CANALISSIMO','Pluridisciplinaire Spectacle vivant','www.canalissimo.com','Portiragnes','','','34420','43.301736727','3.3476310741');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JEUX D''EAU','Musiques classiques','http://www.rencontresmusicalesdegien.fr/','Gien','','','45500','47.71227944','2.66549550204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � GOMEN�','Musiques actuelles','www.jazzagomene.fr/','GOMENE','','','22230','48.1798510762','-2.49181773591');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DIXIE DAYS FESTIVAL','Musiques actuelles','www.dixiedayssainteadresselehavre.blogspot.fr/','STE ADRESSE','','','76310','49.5101291317','0.0779770948409');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GUITARES','Musiques actuelles','www.lesguitares.org','VILLEURBANNE','','','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL STREET PUNK INK MAS PARTY','Musiques actuelles','https://fr-fr.facebook.com/streetpunkxmasparty/','ST BRIEUC','','','22000','48.5149806053','-2.76154552773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Entrevues de Belfort','Cin�ma et audiovisuel','www.festival-entrevues.com','BELFORT','','','90000','47.6471539414','6.84541206237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE PERE NOEL EST-IL UN ROCKER ?','Musiques actuelles','http://www.leperenoelestilunrocker.com','LILLE','','','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUN ART','Transdisciplinaire','www.festivalsunart.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAZR','Musiques actuelles','www.bazr-festival.com','SETE','','','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pianos, pianos','Musiques classiques','http://www.bouffesdunord.com/fr/la-saison/festival-pianos-pianos','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES SOIREES DE SAINT MARC','Divers Spectacle vivant','https://www.lessoireesdesaintmarc.fr/','ST Marc Jaumegarde','30/08/2019','31/08/2019','13100','43.5561849254','5.52206993794');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Livre sur la place','Livre et litt�rature','http://www.lelivresurlaplace.fr/','NANCY','13/09/2019','15/09/2019','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICALARUE','Musiques actuelles','www.musicalarue.com','LUXEY','15/08/2019','17/08/2019','40430','44.2207948584','-0.515670146445');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de th��tre de Noirmoutier','Th��tre','www.trpl.fr','NOIRMOUTIER EN L ILE','11/08/2019','19/08/2019','85330','47.0086655085','-2.26243620205');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine Flamenco','Musiques actuelles','www.amorflamenco.fr','Rivesaltes','19/08/2019','24/08/2019','66600','42.7785910442','2.87687296538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZITUDES','Musiques actuelles','www.jazzitudes.com','LISIEUX','23/08/2019','29/08/2019','14100','49.1466628463','0.238274840452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BIG BANG','Transdisciplinaire','http://www.festival-bigbang.com','ST Medard En Jalles','','','33160','44.8832620816','-0.784239883546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Printemps des Arts de la Rue en Pays des Abers','Cirque et Arts de la rue','https://www.lefourneau.com/presentation-50.html','LE DRENNEC','','','29860','48.5330700302','-4.38126617487');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cam�ra des champs','Cin�ma et audiovisuel','https://www.facebook.com/Cameras.des.champs','Ville sur Yron','','','54800','49.1209779475','5.87272709272');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JEAN DE LA FONTAINE','Pluridisciplinaire Spectacle vivant','http://www.festival-jeandelafontaine.com','CHATEAU THIERRY','','','2400','49.0564070801','3.38160004843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUIT DU BLUES A CAEN','Musiques actuelles','http://www.jazzcaen.com/nuitblue.htm','CAEN','','','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU PAYS DES AMPLIS','Musiques actuelles','','BILLIAT','','','1200','46.0791346715','5.76591818163');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D''JAZZ DANS LA VILLE','Musiques actuelles','','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ros� Nuits d''�t�','Transdisciplinaire','http://www.tourismepierresdorees.com','Chasselay','','','38470','45.2605536513','5.34184461228');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RENCONTRES DES ARTS FOUS','Musiques actuelles','http://www.rencontresdesartsfous.org','FOURAS','','','17450','45.9830753407','-1.07700521801');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL GUITARE VALLEE','Pluridisciplinaire Musique','https://www.guitarevallee.fr/','RIVE DE GIER','','','42800','45.5232368969','4.61000981421');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN ROSKO','Musiques actuelles','','Roscoff','','','29680','48.7123362901','-3.989722876');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival So Film','Cin�ma et audiovisuel','http://www.sofilm-festival.fr/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Diff�rents 11 ! L''autre cin�ma espagnol - Paris','Cin�ma et audiovisuel','www.gnolas.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazzin''Cheverny','Musiques actuelles','','Cheverny','','','41700','47.4773814435','1.45430435221');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLK � LA FERME','Musiques actuelles','','BOISSY ST LEGER','','','94470','48.7470359922','2.52528049019');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'IN VOCE VERITAS','Pluridisciplinaire Musique','invoceveritas.fr/le-festival','FRANCHEVILLE','22/03/2019','24/03/2019','27160','48.7942723344','0.838965152911');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Journ�e du livre politique','Livre et litt�rature','http://www.assemblee-nationale.fr/','PARIS','16/03/2019','16/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OOOH! LE FESTIVAL','Musiques actuelles','https://oooh-lefestival.fr/','Mons en Bar�ul','01/03/2019','03/03/2019','59370','50.6432478699','3.1087677878');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Paris � Berlin � Madrid','Cin�ma et audiovisuel','https://art-action.org/site/fr/index.php','PARIS','04/03/2019','10/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAZARNAOM D''HIVER','Cirque et Arts de la rue','http://www.bazarnaom.com','CAEN','28/02/2019','02/03/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE CHORO','Musiques actuelles','http://clubduchorodeparis.free.fr/','PARIS','29/03/2019','31/03/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A VAULX JAZZ','Musiques actuelles','www.avaulxjazz.com','VAULX EN VELIN','11/03/2019','30/03/2019','69120','45.7858821061','4.92637767698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLLE NUIT DU FOLK','Musiques actuelles','www.ucps.fr','LA MAROLLE EN SOLOGNE','23/03/2019','23/03/2019','41210','47.5829832487','1.78891907492');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jeunes Gens Modernes','Pluridisciplinaire Spectacle vivant','http://www.ccn-orleans.com/','ORLEANS','31/01/2019','02/02/2019','45100','47.8828634214','1.91610357477');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin� Junior � Festival de cin�ma jeunes publics en Val-de-Marne','Cin�ma et audiovisuel','www.cinemapublic.org','CRETEIL','13/02/2019','26/02/2019','94000','48.7837401836','2.45463530415');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CUBA HOY - TERRE DE RENCONTRES','Musiques actuelles','www.yemaya.asso.fr','TOULOUSE','08/02/2019','10/02/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FUP / Festival d''humour de Paris','Divers Spectacle vivant','https://festivaldhumourdeparis.com/','PARIS','13/02/2019','26/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MINES DE JAZZ','Musiques actuelles','www.minesdejazz.fr','DECAZEVILLE','12/04/2019','11/05/2019','12300','44.567273002','2.25243853064');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Meuh Folle','Musiques actuelles','www.meuhfolle.com','ALES','05/04/2019','06/04/2019','30100','44.1250099126','4.08828501262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Escale du livre','Livre et litt�rature','http://escaledulivre.com/','BORDEAUX','05/04/2019','07/04/2019','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEIWA FESTIVAL','Musiques actuelles','www.ecorcesetames.org','TREFFIAGAT','19/04/2019','21/04/2019','29730','47.8049042408','-4.2597485124');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES ARLEQUINS','Th��tre','https://www.cholet.fr/welcome/arlequins.php','CHOLET','17/04/2019','20/04/2019','49300','47.0454402992','-0.877886189605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BAIN DE BLUES','Musiques actuelles','www.baindeblues.com','BAIN DE BRETAGNE','26/04/2019','27/04/2019','35470','47.8302948158','-1.67649507218');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DON JIGI FEST','Musiques actuelles','www.donjigifest.org','VITRE','03/05/2019','04/05/2019','35500','48.1140815063','-1.19370720718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL KLEG (EN ARWEN)','Musiques actuelles','www.en-arwen.com','Cleguerec','03/05/2019','05/05/2019','56480','48.1278326906','-3.0634223364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PEACOCK SOCIETY','Musiques actuelles','www.thepeacocksociety.fr','PARIS','05/07/2019','06/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL EN OTHE','Musiques actuelles','www.festivalenothe.org','Aix en Othe','05/07/2019','20/07/2019','10190','48.1975745771','3.73632794113');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres d''�t� de Villeneuve lez Avignon','Pluridisciplinaire Spectacle vivant','http://chartreuse.org/site/','VILLENEUVE LEZ AVIGNON','05/07/2019','23/07/2019','30400','43.9771673582','4.79466040665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRETEAUX DE NUIT','Pluridisciplinaire Spectacle vivant','http://www.treteauxdenuit.com','APT','17/07/2019','20/07/2019','84400','43.879393265','5.38921757843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ecaussyst�me','Musiques actuelles','','Gignac','26/07/2019','28/07/2019','46600','44.9899523314','1.46272754104');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Saintes','Musiques classiques','www.festivaldesaintes.org','SAINTES','12/07/2019','20/07/2019','17100','45.7422300162','-0.649623035643');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTRES RIVAGES','Musiques actuelles','www.autres-rivages.com','NIMES','15/07/2019','25/07/2019','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA ! C''EST DE LA MUSIQUE','Musiques actuelles','http://www.lacestdelamusique.com','AVIGNON','13/07/2019','17/07/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chalon dans la Rue','Cirque et Arts de la rue','http://www.chalondanslarue.com/','CHALON SUR SAONE','24/07/2019','28/07/2019','71100','46.7900288793','4.85191555008');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HAUTS DE GARONNE','Musiques actuelles','www.lerocherdepalmer.fr','CENON','03/07/2019','12/07/2019','33150','44.8548325665','-0.521018807062');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d�Avignon','Pluridisciplinaire Spectacle vivant','www.festival-avignon.com','AVIGNON','04/07/2019','23/07/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZPOTE','Musiques actuelles','www.jazzpote.com','Thionville','02/07/2019','06/07/2019','57100','49.3759716201','6.12928314217');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Pause Guitare','Musiques actuelles','www.pauseguitare.net','ALBI','02/07/2019','07/07/2019','81000','43.9258213622','2.14686328555');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�clats de Cirque','Cirque et Arts de la rue','http://www.ecoledecirquedelyon.com','LYON','29/06/2019','30/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAINT-OMER JAAZ FESTIVAL','Musiques actuelles','www.saintomerjaazfestival.fr','ST OMER','05/07/2019','09/07/2019','62500','50.7679781717','2.26334881482');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES BAROQUIALES','Musiques classiques','http://www.sospel-tourisme.com','Sospel','05/07/2019','12/07/2019','6380','43.8844607415','7.44794904955');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Live au Campo','Musiques actuelles','https://www.live-campo.com/','PERPIGNAN','19/07/2019','24/07/2019','66000','42.6965954131','2.89936953979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE FOLKLORE A PORT SUR SAONE','Musiques actuelles','http://www.festivalportsursaone.com','Port Sur Saone','29/07/2019','04/08/2019','70170','47.692500452','6.03302774909');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AVIGNON JAZZ FESTIVAL','Musiques actuelles','www.tremplinjazzavignon.fr','AVIGNON','31/07/2019','04/08/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK AU CH�TEAU','Musiques actuelles','www.rockauchateau.fr','VILLERSEXEL','03/08/2019','04/08/2019','70110','47.559065822','6.42909523129');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIENS DANS MON �LE','Musiques actuelles','www.viens-dans-mon-ile.com','L ILE D YEU','05/08/2019','09/08/2019','85350','46.7093514816','-2.34712702345');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival aux champs','Musiques actuelles','www.tuberculture.fr','Chanteix','08/08/2019','11/08/2019','19330','45.3124205901','1.6226623783');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOIRS AU VILLAGE','Musiques actuelles','','ST Calais','','','72120','47.9250578354','0.747660967628');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMNE''ZIK OPEN AIR FESTIVAL','Musiques actuelles','','BRIGNOLES','','','83170','43.3992941902','6.07740244296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES LISZTOMANIAS DE CHATEAUROUX','Musiques classiques','www.lisztomanias.fr','CHATEAUROUX','','','36000','46.8029617828','1.69399812001');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DU CIRQUE DES MUREAUX','Cirque et Arts de la rue','www.lesmureaux.fr/Festival-du-Cirque/10254/','LES MUREAUX','','','78130','48.9888041078','1.91363448925');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROANNE TABLE OUVERTE','Transdisciplinaire','http://www.roannetableouverte.com','ROANNE','','','42300','46.0449112487','4.0797045647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CIRCUS ROCK FESTIVAL','Pluridisciplinaire Spectacle vivant','','SUSVILLE','','','38350','44.9261426887','5.76206988161');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''HUMOUR DE VILLARD DE LANS','Divers Spectacle vivant','www.festivalvdl.com','VILLARD DE LANS','','','38250','45.0600847837','5.54420891959');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TAPAROLE','Musiques actuelles','www.festivaltaparole.org','MONTREUIL','','','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CELTOMANIA','Musiques actuelles','www.celtomania.fr','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LISLEDON','Musiques classiques','www.lisledonfestival.fr','Villemandeur','','','45700','47.9830187489','2.70512428321');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST HOM FRED','Musiques actuelles','https://www.facebook.com/assosino/','Roye','','','80700','49.6936899931','2.78868757676');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCKABILLY FESTIVAL','Musiques actuelles','https://tafproduction.blogspot.com/p/rockabilly-night-festival.html','ST JEAN DE VEDAS','','','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COSMIC GROOVE','Musiques actuelles','http://www.cosmicgroove.fr/','MONTPELLIER','','','34080','43.6134409138','3.86851657896');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CORSICA.DOC, Festival international du film documentaire d''Ajaccio','Cin�ma et audiovisuel','http://www.corsicadoc.com','AJACCIO','','','20167','41.9347926638','8.70132275974');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VACANCES DE MONSIEUR HAYDN','Musiques classiques','www.lesvacancesdemonsieurhaydn.com','LA ROCHE POSAY','','','86270','46.7754623113','0.80240732874');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'M�DITERRAN�O''','Musiques actuelles','www.festivalportet.fr','PORTET SUR GARONNE','','','31120','43.529395272','1.40251955724');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES EMOUVANTES','Musiques actuelles','www.tchamitchian.fr','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIG BAG FESTIVAL','Musiques actuelles','http://www.bigbagfestival.com','BAGNERES DE BIGORRE','','','65200','42.9758668859','0.132640694318');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d''Am�rique latine de Biarritz','Cin�ma et audiovisuel','https://www.festivaldebiarritz.com/','BIARRITZ','','','64200','43.4695847227','-1.55309857519');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOUR DU PAYS D''AIX - TPA','Musiques actuelles','www.aixqui.fr/','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THE ROCKIN GONE PARTY','Musiques actuelles','www.rockarocky.com','ST Rambert d Albon','','','26140','45.2891075425','4.8312134754');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de musique de Maguelone','Musiques classiques','http://www.musiqueancienneamaguelone.com/','VILLENEUVE LES MAGUELONE','04/06/2019','15/06/2019','37750','','');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIONS METAL FESTIVAL','Musiques actuelles','','Montagny','01/06/2019','01/06/2019','42840','46.032936055','4.22546405807');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES Z''ECLECTIQUES (COLLECTION ETE)','Musiques actuelles','','Chemille en Anjou','15/06/2019','16/06/2019','49092','47.3777256876','-0.609937724094');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOURNEE RICARD SA LIVE MUSIC','Musiques actuelles','www.ricardsa-livemusic.com','PARIS','09/05/2019','24/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale des Arts de la Marionnette','Divers Spectacle vivant','http://lemouffetard.com/','PARIS','03/05/2019','29/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN ARLES','Musiques actuelles','http://www.lemejan.com','ARLES','09/05/2019','18/05/2019','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA RUE DES ARTISTES','Musiques actuelles','www.laruedesartistes.fr','ST CHAMOND','14/06/2019','16/06/2019','42400','45.4698319517','4.50184989506');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres litt�raires en pays de Savoie','Livre et litt�rature','http://fondation-facim.fr/','CHAMONIX MONT BLANC','14/06/2019','16/06/2019','74400','45.9309819111','6.92360096711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CROIX-ROUSSE','Musiques actuelles','www.mediatone.net','LYON','15/06/2019','15/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DOC C�vennes - Festival international du documentaire en C�vennes','Cin�ma et audiovisuel','www.champcontrechamp.org','Lasalle','29/05/2019','01/06/2019','30460','44.0428979358','3.85182045128');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA BIBLIOTHEQUE PARLANTE','Livre et litt�rature','https://www.bnf.fr/fr','PARIS','25/05/2019','26/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RUSH','Musiques actuelles','http://rush.le106.com','ROUEN','24/05/2019','26/05/2019','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SAKIFO MUSIK FESTIVAL','Musiques actuelles','www.sakifo.com','ST PIERRE','07/06/2019','09/06/2019','97410','-21.3123242427','55.4936155164');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Bouche � oreille','Musiques actuelles','https://festivalbao.fr/','STE GEMMES SUR LOIRE','06/06/2019','09/06/2019','49130','47.4281722672','-0.575792781781');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hellfest','Musiques actuelles','www.hellfest.fr','Clisson','21/06/2019','23/06/2019','44190','47.097896998','-1.27042598067');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EUROPOPCORN','Musiques actuelles','http://www.europopcorn.fr/','MERVANS','17/05/2019','18/05/2019','71310','46.7945134241','5.18412548455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BOCKSONS FESTIVAL','Musiques actuelles','http://www.ville-valentigney.fr','Valentigney','17/05/2019','18/05/2019','25700','47.4647367767','6.82570788244');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FANFARONNADES DE TRENTEMOULT','Musiques actuelles','www.fanfaronnades.com','REZE','17/05/2019','19/05/2019','44400','47.1762338904','-1.54966399893');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUBERCAIL','Musiques actuelles','www.aubercail.fr','AUBERVILLIERS','22/05/2019','25/05/2019','93300','48.9121722626','2.38445513768');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CATALPA FESTIVAL','Musiques actuelles','http://www.lesilex.fr/','AUXERRE','28/06/2019','30/06/2019','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ECRETTEVILLE LES BAONS','Musiques actuelles','https://lestroubadoursalarue.fr/','ECRETTEVILLE LES BAONS','29/06/2019','29/06/2019','76190','49.6297955951','0.67254078576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FREE SONS A ORLIENAS','Musiques actuelles','http://freesons-orlienas.jimdo.com','ORLIENAS','28/06/2019','28/06/2019','69530','45.6609705564','4.72076320026');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIC EN CIEL','Musiques actuelles','http://www.ville-saint-priest.fr/','ST PRIEST','28/06/2019','30/06/2019','69800','45.701466556','4.94882071665');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEST''ILLE NIGHT','Musiques actuelles','','ILLE SUR TET','','','66130','42.6774223178','2.61639804475');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ N''BOUF','Musiques actuelles','www.festivaljazznbouf.wordpress.com','PEISEY NANCROIX','','','73210','45.5103194255','6.81520574462');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DE SAINT SAUVEUR','Musiques actuelles','','St  SAUVEUR EN PUISAYE','','','89520','47.6390792978','3.20437842755');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EN PLEIN ARTS','Cirque et Arts de la rue','http://www.talence.fr/','TALENCE','','','33400','44.8060817507','-0.591124592873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUEST PARK FESTIVAL','Musiques actuelles','www.ouestpark.com','LE HAVRE','','','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DES ORGUES','Musiques classiques','http://printempsdesorgues.fr/','AVRILLE','','','85440','46.4744272125','-1.49524360118');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ EN TOURAINE','Musiques actuelles','www.jazzentouraine.com','MONTLOUIS SUR LOIRE','','','37270','47.3824921367','0.841267944196');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QPN - Quinzaine Photographique Nantaise','Arts plastiques et visuels','https://www.festival-qpn.com/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BEAULIEU CLASSIC FESTIVAL','Musiques classiques','www.beaulieuclassicfestival.com','BEAULIEU SUR MER','','','6310','43.7079039397','7.33256934881');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PATRIMOINE VIVANT','Pluridisciplinaire Musique','www.patrimoinevivant.org','CHATEAU THIERRY','','','2400','49.0564070801','3.38160004843');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Arts de la Rue de Sainte Savine','Cirque et Arts de la rue','https://www.lart-deco.com/festival-des-arts-de-la-rue','Ste Savine','','','10300','48.2963408998','4.02333022061');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dehors !','Pluridisciplinaire Spectacle vivant','www.dehorsblog.wordpress.com','PORTES LES VALENCE','','','26800','44.8757833249','4.88408311324');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAND COEFF FESTIVAL','Musiques actuelles','','CAP FERRET','','','33950','44.7609440223','-1.19293697437');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL 1000 SOURCES HAUTE DORDOGNE','Musiques classiques','www.millesources.org','TULLE','','','19000','45.2731516999','1.76313875655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CUIVRES EN F�TE','Musiques actuelles','','LIMOGES','','','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES FESTIVALS','Cin�ma et audiovisuel','https://la.charente-maritime.fr/culture-patrimoine/festival-des-festivals','SURGERES','','','17700','46.1044773827','-0.753715269757');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV''HALLES','Musiques actuelles','http://www.aze.fr/','AZE','','','71550','47.0770713545','4.09241779804');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHAHUT VERT','Pluridisciplinaire Spectacle vivant','www.lechahutvert.com','Hornoy le Bourg','','','80640','49.8474299808','1.90289897698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICAL OCEAN','Musiques classiques','http://www.musicalocean.com','Lacanau','','','33680','44.983404414','-1.10810794832');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'D''JAZZ AU JARDIN','Musiques actuelles','www.mediamusic-dijon.fr','DIJON','','','21000','47.3229437965','5.03788805877');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CELT''ILONNE','Musiques actuelles','','L Ile d Olonne','','','85340','46.570163703','-1.7737502368');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE AU CHAMBERTIN','Pluridisciplinaire Musique','www.ot-gevreychambertin.fr','GEVREY CHAMBERTIN','','','21220','47.2200822946','4.98301612333');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NANTES METAL FEST','Musiques actuelles','http://s707454825.onlinehome.fr/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Inrocks','Musiques actuelles','www.lesinrocks.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL TIGHTEN UP','Musiques actuelles','http://www.festivaltightenup.fr/','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tomorrowland Winter','Musiques actuelles','https://www.tomorrowland.com/en/winter/welcome','HUEZ','13/03/2019','15/03/2019','38750','45.0969425761','6.08474810987');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COMMENT DIRE','Transdisciplinaire','https://www.facebook.com/FestivalCommentDire/','TARGON','08/03/2019','09/03/2019','33760','44.7345410468','-0.267821650496');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de BD d''Aurillac','Livre et litt�rature','https://festivalbdba.wordpress.com/','AURILLAC','09/03/2019','10/03/2019','15000','44.9245233686','2.44162453828');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DU FILM AVENTURE & DECOUVERTE','Cin�ma et audiovisuel','http://www.festival-aventure-et-decouverte.com/','VAL D ISERE','15/04/2019','18/04/2019','73400','45.7718876928','6.43266345275');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL JAZZ DU VIGAN','Musiques actuelles','www.jazzajunas.fr/','Le VIGAN','26/04/2019','27/04/2019','30120','43.9886565575','3.62522883405');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INTERSTICE','Transdisciplinaire','www.festival-interstice.net','CAEN','26/04/2019','12/05/2019','14000','49.1847936737','-0.369801713036');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS M�TIS','Musiques actuelles','www.nuitsmetis.org','MIRAMAS','28/06/2019','29/06/2019','13140','43.5842134124','5.01384348574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEE YOU IN THE PIT','Musiques actuelles','http://tafproduction.blogspot.fr/','ST JEAN DE VEDAS','25/06/2019','22/08/2019','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Essonne en Sc�ne par Les Francofolies','Musiques actuelles','http://www.essonne.fr/culture-loisirs-tourisme/essonne-en-scene-par-les-francofolies/','CHAMARANDE','28/06/2019','29/06/2019','91730','48.5167739657','2.21800082924');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SUMMER STADIUM FESTIVAL','Musiques actuelles','http://summerstadium.com','MARSEILLE','29/06/2019','29/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Nancyphonies','Musiques classiques','https://www.nancyphonies.com/','NANCY','29/06/2019','01/09/2019','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMERICAN SALOON A TARBES','Pluridisciplinaire Spectacle vivant','http://www.americansaloon.fr/','TARBES','','','65000','43.2347859635','0.0660093937851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HARMONICA AMPOIGNE','Pluridisciplinaire Musique','https://www.festival-harmonica-ampoigne.com/','Ampoigne','','','53200','47.8051436119','-0.823338302617');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MARIN MARAIS','Musiques classiques','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRIENDSTIVAL','Musiques actuelles','www.friendstival.com','PONTOISE','','','95300','49.0513737853','2.09487928948');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRAK','Pluridisciplinaire Musique','www.crakfestival.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL EN ARC','Th��tre','www.festivalenarc.fr','ARC LES GRAY','','','70100','47.4628603881','5.58172003346');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival d''Automne de Paris','Pluridisciplinaire Spectacle vivant','www.festival-automne.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLEM''FEST','Musiques actuelles','www.clementzik.com','ST CLEMENT','','','89100','48.2247837392','3.30677970985');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL AMERICA DE VINCENNES','Livre et litt�rature','www.festival-america.org','VINCENNES','','','94300','48.8473732045','2.43798650741');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CADENCES','Danse','http://www.ville-arcachon.fr/','ARCACHON','','','33120','44.6529002838','-1.17429790933');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTI''VAL DE MARNE','Musiques actuelles','www.festivaldemarne.org','IVRY SUR SEINE','','','94200','48.8128063812','2.38778023612');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANSON FRANCAISE (AIX)','Musiques actuelles','www.festival-chanson-francaise.com','AIX EN PROVENCE','','','13290','43.5360708378','5.39857444582');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ ENTRE LES DEUX TOURS','Musiques actuelles','www.jazzentrelesdeuxtours.fr','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'1001 FACETTES','Musiques actuelles','www.laboulaf.com','SOISSONS','','','2200','49.3791742979','3.32471758491');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres du cin�ma documentaire - Montreuil','Cin�ma et audiovisuel','www.peripherie.asso.fr','MONTREUIL','','','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE BERRY A DU COEUR','Musiques actuelles','www.le-berry-a-du-coeur-festival-solidaire.com','Aubigny Sur Nere','','','18700','47.4861718263','2.42121788452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RUMEURS URBAINES','Divers Spectacle vivant','www.rumeursurbaines.org','Gennevilliers','','','92230','48.9345190548','2.29384987973');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cinespana � Toulouse','Cin�ma et audiovisuel','www.cinespagnol.com','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROOTS ERGUE','Musiques actuelles','www.softr2rootsergue.com/','SAUVETERRE DE ROUERGUE','','','12800','44.2478234048','2.31504773118');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INSOLENT COLLECTION AUTOMNE','Musiques actuelles','www.festival-insolent.com','LORIENT','','','56100','47.7500486947','-3.37823200917');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CUIVRES EN NORD','Musiques actuelles','https://www.cuivresennord.com','ANOR','','','59186','49.9949415311','4.11785622281');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz en t�te','Musiques actuelles','www.jazzentete.com','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AU MOULIN','Musiques actuelles','www.mjcmoulin-olivet.org','OLIVET','','','45160','47.8546997503','1.88823884753');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LES INSOLANTES','Musiques actuelles','http://lesinsolantes.com','LA COURONNE','','','16400','45.6096373338','0.109121178707');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIRE EN SEINE','Divers Spectacle vivant','www.rireenseine.fr','ROUEN','','','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz � Saint-Germain-des-Pr�s','Musiques actuelles','www.festivaljazzsaintgermainparis.com','PARIS','16/05/2019','27/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Europa Jazz Festival','Musiques actuelles','www.europajazz.fr','LE MANS','05/03/2019','12/05/2019','72000','47.9885256718','0.200030493539');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A TRAVERS CHANTS','Musiques actuelles','www.atraverschants.org','ST SAULVE','01/03/2019','30/03/2019','59880','50.3750172475','3.56642418912');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARRAS COMEDY FESTIVAL','Divers Spectacle vivant','www.arras.fr/fr/actualites/arras-comedy-festival','ARRAS','08/03/2019','10/03/2019','62000','50.2898964997','2.76587316711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RIVIERA ELECTRO FESTIVAL (REF)','Musiques actuelles','https://ref-event.com/','JUAN LES PINS','09/06/2019','09/06/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OUTDOORMIX FESTIVAL','Musiques actuelles','http://outdoormixfestival.com','EMBRUN','07/06/2019','10/06/2019','5200','44.5804294908','6.47559298927');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PETITES FOLIES','Musiques actuelles','www.lespetitesfolies-iroise.com','LAMPAUL PLOUARZEL','30/05/2019','01/06/2019','29810','48.4502704596','-4.76463637416');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JDM FESTIVAL (LE JARDIN DU MICHEL)','Musiques actuelles','http://www.jardin-du-michel.fr/','TOUL','31/05/2019','02/06/2019','54200','48.6865463937','5.89508449535');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AMERICAN FAIR','Musiques actuelles','https://www.festival-american-fair.com','CHATEAUNEUF LES MARTIGUES','15/06/2019','16/06/2019','13220','43.3865906403','5.14753822568');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GREEN FEST','Musiques actuelles','','Sorgues','13/07/2019','13/07/2019','84700','44.0145763951','4.86740515419');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Ecrivains en bord de mer','Livre et litt�rature','http://ecrivainsenborddemer.fr/','LA BAULE','17/07/2019','21/07/2019','44500','47.2909720179','-2.3538291745');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Villages sessions, Festival de musique en Charente','Musiques actuelles','www.villagesession.com','Angouleme','16/07/2019','20/07/2019','16000','45.6472585146','0.14514490683');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Mercredis du Port','Cirque et Arts de la rue','http://lecitronjaune.com/les-mercredis-du-port/','PORT ST LOUIS DU RHONE','10/07/2019','31/07/2019','13230','43.4148193741','4.80679663589');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Big Reggae Festival','Musiques actuelles','','JUAN LES PINS','10/07/2019','10/07/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FORMAT RAISINS','Pluridisciplinaire Spectacle vivant','www.format-raisins.fr','La Charite sur Loire','10/07/2019','21/07/2019','58400','47.1835862006','3.02885515594');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Tempo Rives','Musiques actuelles','www.temporives.fr','ANGERS','11/07/2019','08/08/2019','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NO LOGO BZH','Musiques actuelles','www.nologobzh.com','ST MALO','09/08/2019','11/08/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES PLAGES DU RIRE','Divers Spectacle vivant','https://www.lesplagesdurire.com/','NICE','07/08/2019','09/08/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'KANN AL LOAR','Musiques actuelles','www.kann-al-loar.bzh','Landerneau','03/07/2019','07/07/2019','29800','48.4507528968','-4.2643453824');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA PISTE SOUS LES ETOILES','Cirque et Arts de la rue','www.larural.fr/','CREON','27/07/2019','24/08/2019','33670','44.770425384','-0.343606134456');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LEZ ARTS O SOLEIL','Musiques actuelles','http://www.lezarts-o-soleil.com','La Tour d Aigues','27/07/2019','27/07/2019','84240','43.7214835187','5.56734664063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUE EN P�RIGORD','Musiques classiques','www.musiqueenperigord.fr','ST CYPRIEN','28/07/2019','12/08/2019','24220','44.8825740011','1.02538984639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film de Lama','Cin�ma et audiovisuel','http://www.festilama.org','Lama','27/07/2019','02/08/2019','20218','42.5804431828','9.16621069911');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rues & Vous','Cirque et Arts de la rue','http://www.festivalruesetvous.net','Rions','05/07/2019','07/07/2019','33410','44.6684320932','-0.336471805521');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ART SONIC','Musiques actuelles','www.festival-artsonic.com','BRIOUZE','19/07/2019','20/07/2019','61220','48.7111600408','-0.377752655086');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLAIR DE NUIT','Musiques actuelles','www.clairdenuit.fr','STRASBOURG','26/07/2019','04/08/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film court de Villeurbanne','Cin�ma et audiovisuel','www.festcourt-villeurbanne.com','VILLEURBANNE','','','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Etoiles du documentaire','Cin�ma et audiovisuel','http://www.scam.fr/Festivaldesetoiles/Accueil','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Egaluantes','Cin�ma et audiovisuel','http://www.lesegaluantes.com/','CARENTAN LES MARAIS','','','50500','49.2963250601','-1.2559215726');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PARIS HIP HOP WINTER','Musiques actuelles','www.paris-hiphop.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AFRIQUE UNIE','Musiques actuelles','http://afri','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FUGUES SONORES','Transdisciplinaire','www.9-9bis.com/agenda/fugues-sonores','OIGNIES','','','62590','50.4644483399','2.99306842451');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du film d''�ducation','Cin�ma et audiovisuel','https://festivalfilmeduc.net/','EVREUX','','','27000','49.02015421','1.14164412464');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BOSSA NOVA','Musiques actuelles','www.festivalbossanova.com','Thais','','','94320','48.7607841928','2.38537729524');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des 3 Continents de Nantes','Cin�ma et audiovisuel','www.3continents.com','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale internationale des po�tes�','Livre et litt�rature','http://www.biennaledespoetes.fr/','CRETEIL','01/11/2019','30/11/2019','94000','48.7837401836','2.45463530415');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival BD Boum','Livre et litt�rature','www.bdboum.com','BLOIS','22/11/2019','24/11/2019','41000','47.5817013938','1.30625551583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Foul�es litt�raires','Livre et litt�rature','https://www.lesfouleeslitteraires.fr/','Lormont','22/11/2019','23/11/2019','33310','44.8765972567','-0.519774925453');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''air du temps','Musiques actuelles','www.bainsdouches-lignieres.fr','LIGNIERES','','','18160','46.7617210337','2.20479418633');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Avis de temps fort','Cirque et Arts de la rue','https://avisdetempsfort.wordpress.com/','GAVRES','','','56680','47.6951381724','-3.33453004583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Viva Patrimoine - Valence','Transdisciplinaire','www.lux-valence.com','VALENCE','','','26000','44.9229811667','4.91444013136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Chapit''Otertre','Cirque et Arts de la rue','','Meilhan sur Garonne','','','47180','44.5066850051','0.0288921045777');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU FEMININ','Transdisciplinaire','','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Sens Interdits','Th��tre','https://www.sensinterdits.org/','LYON','17/10/2019','27/10/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale de Lyon (Arts plastiques)','Arts plastiques et visuels','http://www.labiennaledelyon.com/','LYON','18/09/2019','15/01/2020','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CONCERTS AU COUCHER DE SOLEIL','Musiques classiques','www.lesconcertsaucoucherdesoleil.jimdo.com','OPPEDE','21/08/2019','25/08/2019','84580','43.8313175338','5.16982150363');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Regards crois�s','Livre et litt�rature','http://www.troisiemebureau.com/','GRENOBLE','','','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Feu aux Planches','Cirque et Arts de la rue','https://feuauxplanches.jimdo.com/','Choloy Menillot','','','54200','48.6534236231','5.80737190718');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Printemps de Paroles','Pluridisciplinaire Spectacle vivant','http://www.marneetgondoire.fr/parc-culturel-de-rentilly/festival-printemps-de-paroles-858.html','BUSSY ST MARTIN','','','77600','48.8462454012','2.67900406868');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES D''ICI ET D''AILLEURS','Cirque et Arts de la rue','www.oposito.fr','GARGES LES GONESSE','','','95140','48.9701231267','2.40534038501');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Divers et d''�t�','Cirque et Arts de la rue','https://www.diversetdete.fr/','CLERMONT DE L OISE','','','60600','49.3777168975','2.40891709196');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FEW (F�te de l''Eau � Wattwiller)','Arts plastiques et visuels','https://www.few-art.org/','Wattwiller','','','68700','47.8421539165','7.17527188812');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'l''Annuelle','Cirque et Arts de la rue','http://artistesalacampagne.fr/presentation_de_l_annuelle.html','CHAMPLIVE','','','25360','47.2883174221','6.24419443876');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV ASQUES','Musiques actuelles','','ASQUES','','','33240','44.948000841','-0.426140042682');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ART RYTHME ETHIQUE','Musiques actuelles','www.zik-hope.com','PUCEUL','','','44390','47.5188688924','-1.61966399474');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BANDAS','Musiques actuelles','www.festivaldebandas.fr','CONDOM','','','32100','43.9716968333','0.374186827134');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANSONS EN F�TE','Musiques actuelles','www.oreille-en-fete.fr','ARBOIS','','','39600','46.8941749828','5.77318253275');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIJAZZ ET MUSIQUES D''AILLEURS','Musiques actuelles','www.ville-cholet.fr','CHOLET','','','49300','47.0454402992','-0.877886189605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CE MURMURE FESTIVAL','Musiques actuelles','http://cemurmurefestival.com/','Semur En Auxois','','','21140','47.4909559267','4.34106366761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CIRQUE D''ETE A NIORT','Cirque et Arts de la rue','www.cirque-scene.fr/','NIORT','','','79000','46.328260242','-0.465353019369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SALUT A VOUS','Musiques actuelles','http://www.salutavous.fr/','Jau Dignac et Loirac','','','33590','45.4149833388','-0.96259528402');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PLEINS FEUX FESTIVAL','Musiques actuelles','www.pleins-feux-festival.com','BONNEVILLE','','','74130','46.0739003968','6.40868532191');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CANASSON','Musiques actuelles','','LA BOISSIERE DE MONTAIGU','','','85600','46.9451858636','-1.1916392484');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A RAMATUELLE','Musiques actuelles','www.jazzaramatuelle.com','RAMATUELLE','','','83350','43.2186126186','6.63754734174');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BWD 12','Musiques classiques','www.bwd12.fr','ST ETIENNE','','','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES TROLLS EN FOLIE','Musiques actuelles','https://lestrollsenfolie.wixsite.com/lestrollsenfolie','JUPILLES','','','72500','47.7918893529','0.410315352572');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLKLORIQUE DU ROUERGUE','Danse','www.festival-rouergue.com','PONT DE SALARS','','','12290','44.277717255','2.70931526263');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DERIVES','Musiques actuelles','www.derives-festival.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'POP TERRASSE','Musiques actuelles','http://pop-terrasse.fr/','ANGLET','','','64600','43.4917846595','-1.51623373371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV''ETE DE ST JEAN DE FOLLEVILLE','Musiques actuelles','www.festiv-ete-normandie.fr','ST JEAN DE FOLLEVILLE','','','76170','49.505056753','0.499483150435');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Gwo ka','Pluridisciplinaire Spectacle vivant','https://fr-fr.facebook.com/Festival-de-Gwoka-de-Sainte-Anne-291492637695533','STE ANNE','','','97180','16.2572300868','-61.3874730054');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'31 NOTES D''�T�','Musiques actuelles','www.haute-garonne.fr','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Country In Mirande','Musiques actuelles','','Mirande','','','32300','43.5186713962','0.410792361049');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rue des �toiles','Cirque et Arts de la rue','www.crabb.fr','Biscarrosse','','','40600','44.409080109','-1.1773616947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FRANCOFF','Musiques actuelles','','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre Saint-Paul-Trois-Ch�teaux','Livre et litt�rature','https://www.fetedulivrejeunesse.fr/','ST Paul Trois Chateaux','30/01/2019','03/02/2019','26130','44.3482808478','4.75744934383');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PZZLE FESTIVAL','Musiques actuelles','https://www.pzzlefestival.com','LILLE','05/04/2019','07/04/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Panorama des cin�mas du Maghreb','Cin�ma et audiovisuel','http://www.pcmmo.org/','PARIS','02/04/2019','20/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL FLAMENCO DE TOULOUSE','Musiques actuelles','www.festival-flamenco-toulouse.fr','TOULOUSE','04/04/2019','14/04/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du moyen-m�trage de Brive','Cin�ma et audiovisuel','www.festivalcinemabrive.fr','Brive la Gaillarde','02/04/2019','07/04/2019','19100','45.1435830664','1.51936836063');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENCONTRES CINEMA-NATURE','Cin�ma et audiovisuel','https://www.rencontres-cinema-nature.eu/','Dompierre sur Besbre','05/04/2019','07/04/2019','3290','46.5305895294','3.67644076991');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ IN MARS','Musiques actuelles','http://www.ville-bois-guillaume.fr/','BOIS GUILLAUME','22/03/2019','31/03/2019','76230','49.4717615714','1.12022637351');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Vice & Versa','Cirque et Arts de la rue','http://www.festival-vice-versa.com/','BOURG LES VALENCE','15/04/2019','17/04/2019','26500','44.9651094188','4.89411636376');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CONGRES TRICATEL','Musiques actuelles','http://tricatel.com/cannes2019/','CANNES','02/02/2019','03/02/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INASOUND FESTIVAL','Pluridisciplinaire Musique','http://www.inasound.fr/fr/','PARIS','20/04/2019','21/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE COSMOS RUSSE','Pluridisciplinaire Spectacle vivant','https://www.theatre-latalante.com/','PARIS','24/04/2019','27/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL BRASSENS A VAISON LA ROMAINE','Musiques actuelles','www.brassens-festivalvaisonlaromaine.eu','VAISON LA ROMAINE','27/04/2019','05/05/2019','84110','44.247701867','5.06040782309');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale du design graphique','Arts plastiques et visuels','http://www.centrenationaldugraphisme.fr/fr/cig/','CHAUMONT','01/05/2019','31/05/2019','52000','48.0980144211','5.14070044621');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'20 min de bonheur en plus','Cirque et Arts de la rue','http://www.bleu-pluriel.com','Tregueux','26/01/2019','27/01/2019','22950','48.480075769','-2.74506421541');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RADIOMEUH CIRCUS FESTIVAL','Musiques actuelles','www.circus.radiomeuh.com','LA CLUSAZ','28/03/2019','31/03/2019','74220','45.9001389505','6.46031714364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Petits et Grands','Pluridisciplinaire Spectacle vivant','www.petitsetgrands.net','NANTES','27/03/2019','31/03/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival th��tral de Coye-la-For�t','Th��tre','http://www.festivaltheatraldecoye.com/','COYE LA FORET','03/05/2019','24/05/2019','60580','49.1449874027','2.47045856809');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lire � Limoges','Livre et litt�rature','http://www.ville-limoges.fr/index.php/fr/culture/les-grands-evenements-culturels','LIMOGES','03/05/2019','05/05/2019','87280','45.8542549589','1.2487579024');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALGUES AU RYTHME','Musiques actuelles','www.alguesaurythme.com','ARRADON','07/06/2019','09/06/2019','56610','47.6335958588','-2.8220328139');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIK A PILE','Musiques actuelles','www.musikapile.fr','ST DENIS DE PILE','06/06/2019','08/06/2019','33910','44.9879017706','-0.183958862029');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A VOUS DE JOUER','Divers Spectacle vivant','https://www.lenombrildumonde.com/a-vous-de-jouer/','LYON','16/05/2019','01/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'C''EST QUOI CE CIRQUE','Cirque et Arts de la rue','http://poly-sons.com','ST AFFRIQUE','08/05/2019','08/06/2019','12400','43.9656694471','2.86653125625');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CONTOURS','Musiques actuelles','http://www.facebook.com/events/1981618308801177','CLICHY','23/05/2019','26/05/2019','92110','48.9035359139','2.30575230452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hip hop Rendez vous','Musiques actuelles','http://www.pickup-prod.com/','NANTES','01/06/2019','01/06/2019','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Derri�re les Ballots','Musiques actuelles','','Liancourt ST Pierre','15/06/2019','15/06/2019','60240','49.2330864401','1.90635269506');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES ROCHES CELTIQUES','Musiques actuelles','','ST ETIENNE','','','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ AUX CAPUCINS','Musiques actuelles','www.coulommiers.fr','COULOMMIERS','','','77120','48.8122991656','3.09124381847');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'C''EST MA TOURNEE (19)','Musiques actuelles','','CORREZE','','','19800','45.3655110046','1.87090523159');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Cin�banlieue - Paris/Saint-Denis','Cin�ma et audiovisuel','www.cinebanlieue.org','ST DENIS','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PIANO AU MUSEE WURTH','Musiques classiques','www.musee-wurth.fr/activites-evenements/piano-au-musee-wurth','ERSTEIN','','','67150','48.4213687934','7.68523629922');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du court m�trage en plein-air de Grenoble','Cin�ma et audiovisuel','https://www.cinemathequedegrenoble.fr/festival/','GRENOBLE','02/07/2019','06/07/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL EUROP�EN JEUNES TALENTS','Musiques classiques','www.jeunes-talents.org','PARIS','30/06/2019','20/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA HAUT SUR LA COLLINE','Musiques actuelles','http://www.la-haut-sur-la-colline.fr/','Saxon Sion','29/06/2019','02/07/2019','54330','48.4174218363','6.08660462033');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres-Promenades "Histoires de Passages�"','Transdisciplinaire','histoiresdepassages.com','Argentat sur Dordogne','18/07/2019','21/07/2019','19400','45.1011667634','1.93436308377');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HIBOUX ET LE CHIEN BLANC','Musiques actuelles','http://hiboux-chien-blanc.weebly.com','LA CUDE HYEMONDANS','19/07/2019','20/07/2019','25250','47.3809106703','6.63896940193');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DE ROBINSON MANDELIEU','Pluridisciplinaire Spectacle vivant','http://www.mandelieu.fr/actualites-mandelieu/manifestations-mandelieu_nuits-robinson.php','MANDELIEU LA NAPOULE','06/07/2019','25/08/2019','6210','43.5380510468','6.91808936542');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A FOIX','Musiques actuelles','www.jazzfoix.com','FOIX','23/07/2019','27/07/2019','9000','42.9658502274','1.61037495894');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Prieur� de Chirens','Musiques classiques','http://www.prieuredechirens.fr/','Chirens','20/07/2019','11/08/2019','38850','45.4166318565','5.556774983');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SURGERES BRASS FESTIVAL','Musiques actuelles','http://surgeresbrassfestival.com','SURGERES','16/07/2019','18/07/2019','17700','46.1044773827','-0.753715269757');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA CHANSON FRANCAISE DE LORMES','Musiques actuelles','http://festivaldelormes.canalblog.com','LORMES','15/07/2019','17/07/2019','58140','47.2894645643','3.82491613793');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOUILLAC EN JAZZ','Musiques actuelles','www.souillacenjazz.fr','SOUILLAC','13/07/2019','20/07/2019','46200','44.9069531608','1.46396482842');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DE L�ABBAYE DE SYLVAN�S','Musiques classiques','www.sylvanes.com','Sylvanes','14/07/2019','25/08/2019','12360','43.8251925157','2.95288652879');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FETE DU BRUIT DANS LANDERNEAU','Musiques actuelles','www.festival-fetedubruit.com','Landerneau','09/08/2019','11/08/2019','29800','48.4507528968','-4.2643453824');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International de Musique de Chambre de Salon-de-Provence','Musiques classiques','https://festival-salon.fr/fr','SALON DE PROVENCE','28/07/2019','05/08/2019','13300','43.6462885238','5.06814649576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZAPASSA','Musiques actuelles','http://www.jazzebre.com','PASSA','27/07/2019','27/07/2019','66300','42.5650078478','2.80463901382');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ETE DE VAOUR','Pluridisciplinaire Spectacle vivant','www.etedevaour.org','VAOUR','29/07/2019','11/08/2019','81140','44.070490584','1.80207559018');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des arts de la rue d''Aubigny-Les Clouzeaux','Cirque et Arts de la rue','https://www.aubigny-les-clouzeaux.fr/','Aubigny Les Clouzeaux','05/07/2019','06/07/2019','85430','46.6028241769','-1.46743549114');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SYLAK OPEN AIR','Musiques actuelles','http://www.sylakopenair.com','ST MAURICE DE GOURDANS','02/08/2019','04/08/2019','1800','45.8200191939','5.16980415584');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AU PONT DU ROCK','Musiques actuelles','www.aupontdurock.com','MALESTROIT','02/08/2019','03/08/2019','56140','47.8076863788','-2.38701072581');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DAX MOTORS N''BLUES FESTIVAL','Musiques actuelles','http://www.dmbf.fr/','DAX','05/07/2019','07/07/2019','40100','43.7006746973','-1.06014429759');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''N''STOCK','Musiques actuelles','www.rocknstock.org','PREIGNAN','05/07/2019','07/07/2019','32810','43.7210485292','0.636835530591');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival RenaissanceS','Cirque et Arts de la rue','https://www.festivalrenaissances.fr/','BAR LE DUC','05/07/2019','07/07/2019','55000','48.7642280465','5.16346492169');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ESPRIT DES VOIX','Pluridisciplinaire Musique','http://www.lespritdesvoix.fr/LEsprit_des_Voix/LEsprit_des_Voix.html','ANNECY','12/08/2019','17/08/2019','74000','45.8906432566','6.12551773598');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Z','Musiques actuelles','http://www.festival-z.com/','LE REVEST LES EAUX','15/03/2019','24/03/2019','83200','43.1824918424','5.94565392204');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AAH ! LES DEFERLANTES !','Musiques actuelles','www.train-theatre.fr','PORTES LES VALENCE','18/03/2019','23/03/2019','26800','44.8757833249','4.88408311324');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAVEURS DE PRINTEMPS','Musiques actuelles','www.faveursdeprintemps.com','TOULON','12/04/2019','14/04/2019','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEMAINE DE L''ART','Transdisciplinaire','https://www.semainedelart.com/','VERTHEUIL','12/04/2019','20/04/2019','33180','45.2541334712','-0.845409908523');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RDN Festival','Transdisciplinaire','http://reseauhiphop.bzh','R�d�n�','08/04/2019','28/04/2019','29300','47.8554814745','-3.48681564705');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHORUS DES HAUTS DE SEINE','Musiques actuelles','http://chorus.hauts-de-seine.net','NANTERRE','03/04/2019','07/04/2019','92000','48.8960701282','2.20671346353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MYTHOS','Musiques actuelles','www.festival-mythos.com','ST JACQUES DE LA LANDE','29/03/2019','07/04/2019','35136','48.0755625884','-1.72405381322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale internationale du design de Saint-�tienne','Arts plastiques et visuels','','ST ETIENNE','21/03/2019','22/04/2019','42100','45.4301235512','4.37913997076');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontre sur les Docs � Bayonne','Cin�ma et audiovisuel','http://www.lesdocks.org/','BAYONNE','13/03/2019','16/03/2019','64100','43.4922254016','-1.46607674358');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL OFF KULTUR','Musiques actuelles','www.offkultur.com','NANCY','28/03/2019','06/04/2019','54000','48.6901995499','6.17588254434');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Rue des livres','Livre et litt�rature','www.festival-ruedeslivres.org','RENNES','23/03/2019','24/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Journ�es du livre russe','Livre et litt�rature','http://journeesdulivrerusse.fr/','PARIS','16/02/2019','17/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DECI DELA FESTIVAL MULTICULTUREL DES WEPPES','Musiques actuelles','www.santes.fr','SANTES','03/03/2019','30/03/2019','59211','50.5909879066','2.96114421951');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TEMPS FORT CIRQUE[S]','Cirque et Arts de la rue','http://www.lequai-angers.eu/','ANGERS','04/04/2019','08/04/2019','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D HUMOUR DU CAP D AGDE','Divers Spectacle vivant','www.vincentribera-organisation.com','AGDE','24/04/2019','28/04/2019','34300','43.309129116','3.48447903133');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SWING A MIREPOIX','Musiques actuelles','www.swingamirepoix.com','MIREPOIX','20/04/2019','22/04/2019','9500','43.1093995072','1.86916991814');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de la BD de Perros-Guirec','Livre et litt�rature','www.bdperros.com','PERROS GUIREC','13/04/2019','14/04/2019','22700','48.8094393417','-3.46769227891');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL D''AILLEURS D''ICI','Cirque et Arts de la rue','http://dailleursdici.com','COLMAR','27/04/2019','28/04/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLICHY JAZZ','Musiques actuelles','www.ville-clichy.fr','CLICHY','08/02/2019','10/02/2019','92110','48.9035359139','2.30575230452');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Livres et Musiques','Transdisciplinaire','www.livresetmusiques.fr','DEAUVILLE','03/05/2019','05/05/2019','14800','49.3543800887','0.0744665786308');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLIZZ''ART FESTIVAL','Pluridisciplinaire Spectacle vivant','http://blizzartfestival.com/','CIRAL','02/05/2019','04/05/2019','61320','48.5071426572','-0.132794455306');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La Cabanne des Arts','Cirque et Arts de la rue','https://fr-fr.facebook.com/lacabannedesarts/','Monein','','','64360','43.2911768338','-0.562421409058');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Latinossegor','Musiques actuelles','','Soorts Hossegor','','','40150','43.6675688674','-1.41245896656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIROCK''AIL','Musiques actuelles','','PIOLENC','','','84420','44.1774697051','4.76585289056');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'XTREME FEST','Musiques actuelles','www.xtremefest.fr','ALBI','','','81000','43.9258213622','2.14686328555');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA CHANSON A TEXTE DE MONTCUQ','Musiques actuelles','','MONTCUQ','','','46800','44.335497631','1.21192283879');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'EAST SUMMER FEST','Musiques actuelles','www.eastsummerfest.fr/','DIEULOUARD','','','54380','48.8340364411','6.05567802778');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VENDREDIS DE L''ETE A BRESSUIRE','Musiques actuelles','','BRESSUIRE','','','79300','46.8545755683','-0.479375922286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Hauts de Garonne','Musiques actuelles','','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Acad�mie Bach','Musiques classiques','www.academie-bach.fr','ARQUES LA BATAILLE','','','76880','49.8821767195','1.1367858461');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA MOTTE','Musiques actuelles','www.festivaldelamotte.com','MATHA','','','17160','45.8637317103','-0.314996278369');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE D''YSSINGEAUX','Divers Spectacle vivant','http://www.festivaldurire.fr','YSSINGEAUX','','','43200','45.1393761299','4.13104937731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL SUR LIGNON','Musiques actuelles','www.festivalsurlignon.org','Fay Sur Lignon','','','43430','44.9856971989','4.21170048593');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival europ�en du film fantastique de Strasbourg','Cin�ma et audiovisuel','www.strasbourgfestival.com','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Musica','Musiques classiques','www.festival-musica.org','STRASBOURG','','','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cergy, Soit !','Cirque et Arts de la rue','www.cergysoit.fr','CERGY PONTOISE','','','95000','49.0401131567','2.05082123731');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''�t� au L.I.T.!','Pluridisciplinaire Spectacle vivant','http://www.ciegerardgerard.fr/index.php/le-theatre/le-lieu','Rivesaltes','','','66600','42.7785910442','2.87687296538');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te du livre de Hy�res','Livre et litt�rature','www.fetedulivre.hyeres.fr','HYERES','11/05/2019','12/05/2019','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Printemps des Rues','Cirque et Arts de la rue','https://www.leprintempsdesrues.com','PARIS','25/05/2019','26/05/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Y''A PAS L''FEU','Musiques actuelles','www.festival-yapaslfeu.com','CAMBRIN','24/05/2019','25/05/2019','62149','50.5116047377','2.73659179875');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�A VA BIEN SE PASSER','Musiques actuelles','www.cavabienspasser.fr','ESTRABLIN','07/06/2019','09/06/2019','38780','45.5136075933','4.95889651862');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'THIS IS MY FEST','Musiques actuelles','','MONTREUIL','07/06/2019','09/06/2019','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ARTS SCENICS','Musiques actuelles','www.artsscenics.com','Lisle sur Tarn','28/06/2019','30/06/2019','81310','43.8974980421','1.78200143861');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA 7EME VAGUE','Musiques actuelles','www.7vague.com','Bretignolles sur Mer','31/05/2019','01/06/2019','85470','46.6374826705','-1.86324200464');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Hip hop Rendez vous','Musiques actuelles','https://rendezvoushiphop.culture.gouv.fr/','MARSEILLE','25/05/2019','01/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OREADES TRANCE FESTIVAL','Musiques actuelles','https://www.facebook.com/OreadesProd/?eid=ARDEqZppp4XF5mWiGoaYVkq6DRCC3FQDTp980moXYHuL7OWzr9xfyf-FeHnYWEXjBklWHWZZkUe-60bj','PARIS','31/05/2019','01/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Horizons Arts Nature en Sancy','Transdisciplinaire','https://www.horizons-sancy.com/','MONT DORE','15/06/2019','22/09/2019','63240','45.5760999131','2.80995918175');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VIE SAUVAGE','Musiques actuelles','www.festivalviesauvage.fr','BOURG SUR GIRONDE','14/06/2019','16/06/2019','33710','45.0398664572','-0.547187308975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUAND LES FANFARES','Musiques actuelles','http://festival.mudanza.fr','SALON DE PROVENCE','08/06/2019','08/06/2019','13300','43.6462885238','5.06814649576');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'�A JAZZE FORT � FRANCHEVILLE','Musiques actuelles','https://fr-fr.facebook.com/pages/category/Nonprofit-Organization/%C3%87a-jazze-fort-%C3%A0-Francheville-1575552129383360/','FRANCHEVILLE','07/06/2019','07/06/2019','69340','45.7378794318','4.75508314041');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CREA PARC FESTIVAL','Musiques actuelles','','Clamart','21/06/2019','22/06/2019','92140','48.7964306792','2.25474652922');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Midi-minuit po�sie','Livre et litt�rature','http://maisondelapoesie-nantes.com/category/midiminuitpoesie/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MAGIE FANT ASCQ','Divers Spectacle vivant','','VILLENEUVE D ASCQ','','','59491','50.6324372393','3.1535257221');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAMB''ART''DEMENT','Cirque et Arts de la rue','http://www.ville-chambly.fr/Culture-Loisirs/Festivites/Festival-Chamb-art-dement','CHAMBLY','','','60230','49.1718103651','2.24657019692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL INTERNATIONAL DU FILM SUR LE HANDICAP','Cin�ma et audiovisuel','','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DENSITES','Musiques actuelles','www.vudunoeuf.asso.fr','Fresnes en Woevre','','','55160','49.1009729702','5.63355600212');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES COUSINS D''AMERIQUE','Musiques actuelles','www.laeta.fr/','LOUDUN','','','86200','47.0098072856','0.0990443124673');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES CRO MIGNONS','Musiques actuelles','www.lesilex.fr','AUXERRE','','','89000','47.793488225','3.58168281761');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'URBAN ART FESTIVAL','Transdisciplinaire','www.cluses.fr/','CLUSES','','','74300','46.0631088613','6.57892080059');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'En Bref !','Pluridisciplinaire Spectacle vivant','http://theatrecinemachoisy.fr/','CHOISY LE ROI','','','94600','48.764315118','2.41742518222');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CARESSEZ LE POTAGER','Pluridisciplinaire Spectacle vivant','www.caressezlepotager.net','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MUSE & PIANO','Musiques classiques','www.louvrelens.fr/activity/muse-piano','LENS','','','62300','50.4374264148','2.82105851259');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Baisers vol�s','Musiques actuelles','','ST MALO','','','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Vid�o Frames','Cin�ma et audiovisuel','www.videoformes.com','AVIGNON','','','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival B�d�cin�','Livre et litt�rature','https://www.espace110.org/bedecine','ILLZACH','','','68110','47.7734992','7.36011136903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CABARET VERT','Musiques actuelles','www.cabaretvert.com','CHARLEVILLE MEZIERES','22/08/2019','25/08/2019','8000','49.7752965803','4.71724655966');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLACK BASS FESTIVAL','Musiques actuelles','www.blackbassfestival.com','BRAUD ET ST LOUIS','30/08/2019','31/08/2019','33820','45.2544518508','-0.646204430347');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL METEO / JAZZ A MULHOUSE','Musiques actuelles','http://www.festival-meteo.fr/','MULHOUSE','27/08/2019','31/08/2019','68200','47.749163303','7.32570047509');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICALES EN COTE CHALONNAISE','Musiques classiques','https://www.musicales-cote-chalonnaise.fr/','Buxy','29/08/2019','01/09/2019','71390','46.7130408432','4.71342619185');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MELUSIK','Musiques actuelles','www.melusik.fr','LUSIGNAN','','','86600','46.4377572057','0.114217790925');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DUB LIGHTS FESTIVAL','Musiques actuelles','www.musicalriot.org','SETE','','','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon L�Inde des livres','Livre et litt�rature','www.comptoirsinde.org','PARIS','16/11/2019','17/11/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival A l''est du nouveau de Rouen','Cin�ma et audiovisuel','http://www.alest.org/fr/a-lest-nouveau/','ROUEN','','','76000','49.4413460103','1.09256784278');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ALLOTROPIQUES','Musiques actuelles','http://allotropiques.com','TOURS','','','37000','47.3986382281','0.696526376417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Felyn','Musiques actuelles','','LYON','19/06/2020','20/06/2020','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LATCHO DIVANO','Transdisciplinaire','www.latcho-divano.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de fanfares Brest et Ouessant','Musiques actuelles','','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de la BD de Chamb�ry','Livre et litt�rature','www.chamberybd.fr','CHAMBERY','04/10/2019','06/10/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTA FOUGASSE','Pluridisciplinaire Spectacle vivant','http://lezardsdu.wixsite.com/festafougasse','MURVIEL LES MONTPELLIER','','','34570','43.6095675101','3.73935842429');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ESTIVALES DU CANAL','Musiques actuelles','www.ville-vierzon.fr','VIERZON','','','18100','47.2368836077','2.0790757049');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANGEZ D''AIR','Musiques actuelles','www.changezdair.blogspot.com','ST GENIS LES OLLIERES','','','69290','45.7610551042','4.72602177608');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FLAMENCO LYON','Musiques actuelles','www.festivalflamencolyon.com','ROANNE','','','42300','46.0449112487','4.0797045647');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DAVIGNAC SUR UN PLATEAU','Pluridisciplinaire Spectacle vivant','www.davignac.com','DAVIGNAC','','','19250','45.4911899589','2.07587737304');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HIP-HOP ET DES CULTURES URBAINES DE ST-DENIS','Musiques actuelles','','ST DENIS','','','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL 1 ETE 1 QUARTIER','Pluridisciplinaire Spectacle vivant','','Colomiers','','','31770','43.611551508','1.32700218407');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BBMIX','Musiques actuelles','http://bbmix.org','BOULOGNE','','','92100','48.8365843138','2.23913599058');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Piano is not dead','Pluridisciplinaire Musique','http://letetris.fr/','LE HAVRE','','','76610','49.498452502','0.140153719153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film d''Animation - Les Nuits Magiques - B�gles','Cin�ma et audiovisuel','www.lesnuitsmagiques.fr','Begles','','','33130','44.8016009051','-0.547755768448');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BARS EN TRANS','Musiques actuelles','www.barsentrans.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des arts sonn�s','Musiques actuelles','www.festival-des-arts-sonnes.fr','ST Andre des Eaux','30/03/2019','30/03/2019','22630','48.3731003808','-2.01421441815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Play it again','Cin�ma et audiovisuel','www.festival-playitagain.com','PARIS','03/04/2019','09/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FUNAMBALS','Musiques actuelles','http://funambals2018.lacampanule.fr/','VILLEURBANNE','05/04/2019','07/04/2019','69100','45.7707704179','4.88845817426');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ANDALOU','Musiques actuelles','www.alhambra-asso.com','AVIGNON','16/03/2019','30/03/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIFOLK','Musiques actuelles','www.ronny-coutteure.fr','GRENAY','17/03/2019','29/03/2019','62160','50.4496814864','2.74513302656');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUARTIERS EN SCENE','Pluridisciplinaire Spectacle vivant','www.quartiers-en-scene.fr','RENNES','07/03/2019','16/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'1, 2, 3, MUSIQUE','Pluridisciplinaire Spectacle vivant','www.grandpiano.fr','TALENCE','12/03/2019','23/03/2019','33400','44.8060817507','-0.591124592873');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'E-WAX','Musiques actuelles','https://www.e-waxfestival.com/','Les Avanchers Valmorel','12/04/2019','14/04/2019','73260','45.4607199042','6.44362178612');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NEMOURS DE JAZZ','Musiques actuelles','www.nemours.fr','NEMOURS','13/04/2019','13/04/2019','77140','48.2596067909','2.71107993087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL LA CORDE RAIDE','Musiques actuelles','https://pontdzic.com','Pontchateau','28/02/2019','02/03/2019','44160','47.433636363','-2.09556089039');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEVENADUR','Musiques actuelles','www.sevenadur.org','RENNES','20/02/2019','17/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HEART SOUND METAL FEST','Musiques actuelles','https://www.facebook.com/events/espace-jean-marie-poirier/heart-sound-metal-fest-2019-6-avril-sucy-en-brie/258894284981001/','SUCY EN BRIE','06/04/2019','06/04/2019','94370','48.7656456593','2.5330560803');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Raccord(s)','Transdisciplinaire','https://lesediteursassocies.com/webshop/','PARIS','09/04/2019','14/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FAME - FILM & MUSIC EXPERIENCE','Transdisciplinaire','https://gaite-lyrique.net','PARIS','12/02/2019','17/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INTIME FESTIVAL','Musiques actuelles','/photosvideo-2012-2013/intime-festival.html','ST AVERTIN','31/01/2019','03/02/2019','37550','47.3571561121','0.737544440483');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLASSICOFRENZY (ex CLASSICOFOLIES)','Musiques classiques','www.classicofolies.com','CAHORS','19/02/2019','19/04/2019','46000','44.4507370916','1.44075837848');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international de cin�ma d''animation de Pontarlier','Cin�ma et audiovisuel','https://www.ccjb.fr/','PONTARLIER','26/03/2019','31/03/2019','25300','46.9119730882','6.38914602031');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PERFORMANCE D''ACTEUR','Divers Spectacle vivant','www.performancedacteur.com','CANNES','15/04/2019','22/04/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Circulations','Arts plastiques et visuels','https://www.festival-circulations.com/','PARIS','20/04/2019','30/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK N SOLEX','Musiques actuelles','www.rocknsolex.fr','RENNES','24/04/2019','28/04/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES ELECTROPICALES','Musiques actuelles','www.electropicales.com','ST DENIS','03/05/2019','04/05/2019','97400','-20.9329708192','55.446867167');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PISTEURS D''ETOILES','Cirque et Arts de la rue','www.pisteursdetoiles.com','OBERNAI','26/04/2019','04/05/2019','67210','48.4593467321','7.48006434701');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIMOUX BRASS FESTIVAL','Pluridisciplinaire Musique','www.limouxbrass.fr','LIMOUX','01/05/2019','05/05/2019','11300','43.0499293279','2.23958811407');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COSMOJAZZ FESTIVAL','Musiques actuelles','http://cosmojazzfestival.com/','CHAMONIX MONT BLANC','20/07/2019','28/07/2019','74400','45.9309819111','6.92360096711');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL MELUSINE','Musiques classiques','https://lesaulce.fr/actualites/festival/','Escolives Ste Camille','19/07/2019','21/07/2019','89290','47.7225909756','3.60249190307');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A L''AMIRAUTE','Musiques actuelles','https://www.jazzalamiraute.fr/','Pleneuf Val Andre','09/07/2019','27/08/2019','22370','48.5840054611','-2.53016887694');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GRIMALDINES','Musiques actuelles','www.les-grimaldines.com','Grimaud','16/07/2019','06/08/2019','83310','43.2820282501','6.53303224114');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Albertville Jazz Festival','Musiques actuelles','www.albertvillejazzfestival.com','ALBERTVILLE','23/07/2019','27/07/2019','73200','45.6683987277','6.40460338643');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MIDI FESTIVAL','Musiques actuelles','www.midi-festival.com','TOULON','25/07/2019','28/07/2019','83200','43.1361589728','5.93239634249');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Musique de Menton','Musiques classiques','www.festival-musique-menton.fr','MENTON','25/07/2019','13/08/2019','6500','43.7908233727','7.49365612374');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cognac Blues Passion','Musiques actuelles','www.bluespassions.com','COGNAC','04/07/2019','08/07/2019','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Days Off','Musiques actuelles','www.daysoff.fr','PARIS','04/07/2019','13/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA LONDE JAZZ FESTIVAL','Musiques actuelles','http://www.lalondejazzfestival.com','LA LONDE LES MAURES','01/08/2019','04/08/2019','83250','43.1689213414','6.24285151023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANSONS ET MOTS D''AMOU','Musiques actuelles','http://www.chansonsetmotsdamou.fr/','AMOU','01/08/2019','04/08/2019','40330','43.5938659835','-0.743678079626');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRAD''IN LIVE FESTIVAL','Musiques actuelles','www.tradinfestival.com','EMBRUN','05/07/2019','07/07/2019','5200','44.5804294908','6.47559298927');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SUR UN PLATEAU','Musiques actuelles','www.jazz-sur-un-plateau-larnas.fr','LARNAS','05/07/2019','21/07/2019','7220','44.4543456079','4.59606913482');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HELLO BIRDS FESTIVAL','Musiques actuelles','www.hellobirdsfestival.fr','ETRETAT','05/07/2019','15/09/2019','76790','49.704733151','0.205304793744');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES MURES ZICALES','Musiques actuelles','http://www.lesmureszicales.com','ST Laurent de Mure','06/07/2019','06/07/2019','69720','45.6834072587','5.06089287169');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'YZEURES''N''ROCK','Musiques actuelles','www.yzeuresnrock.com','YZEURES SUR CREUSE','02/08/2019','04/08/2019','37290','46.7902872236','0.876208149129');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LIVE IN CHARTREUSE','Musiques actuelles','http://liveinchartreuse.wixsite.com','LES ECHELLES','02/08/2019','03/08/2019','73360','45.4514870099','5.74305650824');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIBALOCHE','Musiques actuelles','www.festibaloche.com','OLARGUES','02/08/2019','04/08/2019','34390','43.540135455','2.91772316116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES KULTUR''ELLES','Musiques actuelles','musicalsol.fr/','LA REDORTE','','','11700','43.2477384944','2.64987073042');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Aventuriers','Musiques actuelles','www.festival-les-aventuriers.com','FONTENAY SOUS BOIS','','','94120','48.8511046382','2.47395409774');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL VOIX:LA','Musiques actuelles','http://festival-voixla.fr/','GRAY','','','70100','47.4320889937','5.61070658545');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OU SONT LES FEMMES','Musiques actuelles','www.le-kfe-quoi.com','FORCALQUIER','','','4300','43.9599082374','5.7882827572');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale Internationale des Arts du Cirque Provence Alpes C�te d�Azur (BIAC)','Cirque et Arts de la rue','http://www.biennale-cirque.com/fr/','MARSEILLE','11/01/2019','10/02/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival D�calages','Pluridisciplinaire Spectacle vivant','http://www.scenes-du-nord.fr/','STRASBOURG','12/01/2019','29/01/2019','67100','48.5712679849','7.76752679517');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MyFrenchFilmFestival','Cin�ma et audiovisuel','https://www.myfrenchfilmfestival.com/fr/','PARIS','18/01/2019','18/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ART N JAZZ FESTIVAL','Musiques actuelles','http://www.art-n-jazz.com','STE VERTU','','','89310','47.7398700628','3.91053461734');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Grands Concerts de Clairvaux','Musiques classiques','https://www.abbayedeclairvaux.com/','VILLE SOUS LA FERTE','','','10310','48.1358669154','4.77205760299');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ORIGINES CONTROLEES','Musiques actuelles','www.tactikollectif.org','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ARMOR DE RIRE','Divers Spectacle vivant','http://armor-de-rire.net-helium.fr/','ST BRIEUC','','','22000','48.5149806053','-2.76154552773');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUTOMNALES','Pluridisciplinaire Spectacle vivant','www.puydedome.fr','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Belles latinas','Livre et litt�rature','http://www.espaces-latinos.org','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OVERDRIVE','Musiques actuelles','','CHAULNES','','','80320','49.8169359721','2.80153906029');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BE BOP OR BE DEAD','Musiques actuelles','https://bonus-track.fr/','BELFORT','','','90000','47.6471539414','6.84541206237');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�tes le pont (�dition octobre)','Cirque et Arts de la rue','www.cnarsurlepont.fr','LA ROCHELLE','','','17000','46.1620643972','-1.17465702836');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECLECTIK ROCK','Musiques actuelles','www.les3scenes.saint-dizier.fr/festivals/festival-eclectik-rock.html','ST DIZIER','','','52100','48.6280100668','4.94923523578');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ADP FAIT SON CIRQUE','Cirque et Arts de la rue','www.atelierduplateau.org','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A GRIGNAN','Musiques actuelles','http://www.chateaux-ladrome.fr/','GRIGNAN','','','26230','44.4369175751','4.90180143483');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Baroque de Pontoise','Musiques classiques','www.festivalbaroque-pontoise.fr','PONTOISE','','','95300','49.0513737853','2.09487928948');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TANGO SWING ET BRETELLES','Musiques actuelles','www.embarcadere-montceau.fr','MONTCEAU LES MINES','','','71300','46.6762539128','4.3540960404');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE JERUSALEM','Musiques classiques','www.philharmoniedeparis.fr/fr/activite/musique-de-chambre/19023-festival-de-jerusalem','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Rendez-vous Soniques','Musiques actuelles','www.lesrendezvoussoniques.com','ST LO','','','50000','49.1099624249','-1.07755642702');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres litt�raires Impressions d''Europe','Livre et litt�rature','http://www.impressionsdeurope.com/','NANTES','','','44200','47.2316356767','-1.54831008605');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PETIT BULLETIN FESTIVAL','Musiques actuelles','http://www.petit-bulletin.fr/festival/','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du film italien de Villerupt','Cin�ma et audiovisuel','www.festival-villerupt.com','Villerupt','','','54190','49.4647155933','5.92643741054');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROUBAIX � L''ACCORD�ON','Pluridisciplinaire Musique','www.roubaixalaccordeon.fr','ROUBAIX','','','59100','50.6879774811','3.18258434623');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GROSSES GUITARES','Musiques actuelles','http://www.lesgrossesguitares.com','VAUGNERAY','','','69670','45.7318792519','4.64378667979');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LEVITATION FRANCE','Musiques actuelles','http://levitation-france.com','ANGERS','','','49000','47.476837416','-0.556125995444');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Rendez-vous musique classique','Musiques classiques','http://lesrendezvous.org/','ST BARTHELEMY','','','70270','47.7410393174','6.60409347666');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JERK OFF','Pluridisciplinaire Spectacle vivant','http://www.festivaljerkoff.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Septembre musical de l''orne','Musiques classiques','www.septembre-musical.com','ALENCON','','','61000','48.4318193082','0.0915406916107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Spectacles de Rue','Cirque et Arts de la rue','http://festivalhouldizy.fr/','Houldizy','','','8090','49.812785683','4.6695029327');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TRAVERSEES DU MARAIS','Pluridisciplinaire Spectacle vivant','https://maraiscultureplus.wordpress.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Pantiero','Musiques actuelles','www.festivalpantiero.com','CANNES','','','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BIKINI FEST','Musiques actuelles','http://www.lebikini.com','RAMONVILLE ST AGNE','','','31520','43.5441837911','1.47782372823');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES IN SEM','Musiques actuelles','www.bluesinsem.com','ST MEDARD EN JALLES','','','33160','44.8832620816','-0.784239883546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'INGENIEUSE AFRIQUE','Musiques actuelles','www.ingenieuseafrique.info','FOIX','','','9000','42.9658502274','1.61037495894');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Filets Bleus','Musiques actuelles','http://www.festivaldesfiletsbleus.fr/','Concarneau','','','29900','47.8966260003','-3.90716871821');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHAISES MUSICALES','Musiques actuelles','www.aufildesondes.com','VICQ SUR GARTEMPE','','','86260','46.726218827','0.843611918005');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PRINTEMPS DE L''AUXOIS','Musiques actuelles','http://www.ugmm.org/index.php/printemps-de-lauxois/','Vitteaux','10/05/2019','12/05/2019','21350','47.3974282764','4.54312292794');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'COSMIC TRIP','Musiques actuelles','http://www.cosmictripfestival.fr/','BOURGES','30/05/2019','02/06/2019','18000','47.0749572013','2.40417137557');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rolling Sa�ne','Musiques actuelles','www.rolling-saone.com','Gray','30/05/2019','01/06/2019','70100','47.4320889937','5.61070658545');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Oh ! Les beaux jours','Livre et litt�rature','http://ohlesbeauxjours.fr/','MARSEILLE','28/05/2019','02/06/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JEUX DE VAGUES','Musiques classiques','http://www.festival-jeux-de-vagues.com/','DINARD','30/05/2019','01/06/2019','35800','48.6241805945','-2.0619828606');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DES RIVES ET DES NOTES JAZZ A OLORON','Musiques actuelles','www.jazzoloron.com','OLORON STE MARIE','28/06/2019','07/07/2019','64400','43.1560871189','-0.587546244432');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN SINGE EN ETE','Musiques actuelles','http://www.unsingeenete.com','Mayenne','28/06/2019','29/08/2019','53100','48.3074609449','-0.614799950799');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Camping (Lyon)','Danse','https://www.cnd.fr/fr/page/33-camping','LYON','24/06/2019','28/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CAP A CASES','Musiques actuelles','www.capacases.fr','CASES DE PENE','22/06/2019','22/06/2019','66600','42.7867407426','2.77853627321');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ SOUS LES POMMIERS','Musiques actuelles','www.jazzsouslespommiers.com','COUTANCES','24/05/2019','01/06/2019','50200','49.0566189539','-1.44345003609');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE VESINET JAZZ METIS FESTIVAL','Musiques actuelles','www.vesinet.org','LE VESINET','22/05/2019','25/05/2019','78110','48.8938640591','2.13039312004');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LEVE TON VERS','Musiques actuelles','http://www.le-bijou.net','TOULOUSE','05/06/2019','07/06/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Mus�e �lectronique Festival','Musiques actuelles','http://www.leperiscope.com/','GRENOBLE','14/06/2019','15/06/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ABADIE FESTIVAL MUSIQUE','Musiques actuelles','','NICE','15/06/2019','16/06/2019','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MILLESIME FESTIVAL','Musiques actuelles','','La Reole','31/05/2019','01/06/2019','33190','44.5860904495','-0.042370404021');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Etonnants voyageurs','Transdisciplinaire','http://www.etonnants-voyageurs.com/','ST MALO','08/06/2019','10/06/2019','35400','48.6400443482','-1.98060627256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Imaginarium Festival','Musiques actuelles','www.imaginariumfestival.com','Margny les Compiegne','08/06/2019','09/06/2019','60280','49.4329968709','2.8054861143');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DEMON D''OR','Musiques actuelles','www.demondor.com','LYON','28/06/2019','29/06/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'DELTA FESTIVAL','Musiques actuelles','www.delta-festival.fr','MARSEILLE','30/06/2019','07/07/2019','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Crat�re Surfaces','Cirque et Arts de la rue','https://cratere-surfaces.com/','ALES','02/07/2019','06/07/2019','30100','44.1250099126','4.08828501262');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'AUZON LE BLUES','Musiques actuelles','http://www.auzon-le-blues-carpentras.com','CARPENTRAS','28/06/2019','28/06/2019','84200','44.0593802565','5.06134844776');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LILLE CLEF DE SOLEIL','Musiques classiques','http://www.clefdesoleil.com/','LILLE','30/06/2019','22/08/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre et de la presse jeunesse en Seine-Saint-Denis','Livre et litt�rature','http://slpjplus.fr/salon/','MONTREUIL','27/11/2019','02/12/2019','93100','48.8633175054','2.44816211857');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Manifesta','Arts plastiques et visuels','https://www.manifesta13.org/','MARSEILLE','01/06/2020','30/09/2020','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ONE MAN BAND FESTIVAL','Musiques actuelles','https://tafproduction.blogspot.com/','ST JEAN DE VEDAS','','','34430','43.5716282319','3.83221847952');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Go Ba''s','Musiques actuelles','www.festivalgobas.com','Bourbon l Archambault','24/08/2019','25/08/2019','3160','46.5836819113','3.05141625983');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Touquet Music Beach','Musiques actuelles','http://touquetmusicbeach.fr/','LE TOUQUET PARIS PLAGE','23/08/2019','24/08/2019','62520','50.5083984501','1.59923820583');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU HOUBLON','Musiques actuelles','http://www.festivalduhoublon.eu/','HAGUENAU','20/08/2019','25/08/2019','67500','48.8417047695','7.83010404968');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Caf�s litt�raires de Mont�limar','Livre et litt�rature','http://lescafeslitteraires.fr/','Montelimar','03/10/2019','06/10/2019','26200','44.5540845734','4.74869387765');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Taingy dans la rue','Cirque et Arts de la rue','www.amispatrimoinetaingy.wordpress.com','TAINGY','','','89560','47.6123914189','3.40803434271');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIC TO ROCK THE NATION','Musiques actuelles','https://loreille-dauphine.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BREIZH DISORDER','Musiques actuelles','www.massprod.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ � LA HARPE','Musiques actuelles','www.3regards.com','RENNES','','','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Diep-Haven','Arts plastiques et visuels','https://www.diephaven.org/','Dieppe','','','76200','49.9222503951','1.08681168449');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK''IN CHARGE','Musiques actuelles','','Charge','','','37530','47.431662181','1.04123088283');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CELLO FAN DE CALLIAN / PAYS DE FAYENCE','Musiques classiques','https://www.cello-fan.com/index.php','Callian','','','83440','43.6125180228','6.75708895004');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GRAVITY FESTIVAL','Musiques actuelles','','Langlade','','','30980','43.8045117845','4.25533661815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SEMAINE DE JAZZ DE CARCASSONNE','Musiques actuelles','www.carcassonne.org','CARCASSONNE','','','11000','43.2093798444','2.34398855385');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSIQUES ET CIRQUES AU PARC','Cirque et Arts de la rue','www.rosny93.fr','Rosny sous Bois','','','93110','48.8745763768','2.4863404591');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FIFRES DE GARONNE','Musiques actuelles','www.sous-fifre','ST PIERRE D AURILLAC','','','33490','44.5791928369','-0.192914222319');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ VOCAL SUNSET/SUNSIDE','Musiques actuelles','http://www.sunset-sunside.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A-NIM�','Musiques actuelles','www.a-nimes.fr','NIMES','','','30900','43.844938394','4.34806796996');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOLKLORE (CHAMBLY)','Danse','www.clec-chambly.fr','CHAMBLY','','','60230','49.1718103651','2.24657019692');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS DE LONGWY','Musiques actuelles','www.mairie-longwy.fr','LONGWY','','','54400','49.5214021779','5.76638370702');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Circonova','Cirque et Arts de la rue','www.theatre-cornouaille.fr/','QUIMPER','09/01/2019','07/02/2019','29000','47.9971425162','-4.09111944455');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DIVERSCENES','Musiques actuelles','http://diverscene.jimdo.com','CHANGE','06/01/2019','02/02/2019','53810','48.1079801148','-0.800104772023');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES CALEBASSES D''AVRIL','Musiques actuelles','www.crreasud.com','AVIGNON','27/04/2019','28/04/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHANSONS DU MONDE - ENTRE TERRE ET MER','Musiques actuelles','https://www.lephare-saintcoulomb.com/','ST Coulomb','30/04/2019','30/04/2019','35350','48.677369423','-1.92087004371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''APPEL AU BOIS NORMAND','Transdisciplinaire','lappelauboisnormand.fr/','LORE','17/05/2019','19/05/2019','61000','48.4318193082','0.0915406916107');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOFT R FESTIVAL','Musiques actuelles','https://www.softr2rootsergue.com/','SAUVETERRE DE ROUERGUE','17/05/2019','17/05/2019','12800','44.2478234048','2.31504773118');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL HOLTZI','Divers Spectacle vivant','www.festivalholtzi.fr','HOLTZHEIM','24/05/2019','26/06/2019','67810','48.5567634132','7.64425286816');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'RENDEZ VOUS HIP HOP','Musiques actuelles','','LILLE','25/05/2019','01/06/2019','59160','50.6317183168','3.04783272312');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL CHARIVARI VERTOU','Musiques actuelles','http://www.festival-charivari-vertou.fr','VERTOU','01/06/2019','02/06/2019','44120','47.1603732625','-1.47052291749');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Saint-Denis','Musiques classiques','','ST DENIS','03/06/2019','03/07/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de l''histoire de l''art de Fontainebleau','Domaines divers','http://festivaldelhistoiredelart.com/','FONTAINEBLEAU','07/06/2019','09/06/2019','77300','48.4066856508','2.68031396389');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des Mouillotins','Musiques actuelles','www.mouillotins.fr','ST Poix','07/06/2019','08/06/2019','53540','47.9663369335','-1.04358876565');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Solidays','Musiques actuelles','www.solidays.com','PARIS','21/06/2019','23/06/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES NUITS CARREES','Musiques actuelles','www.nuitscarrees.com','ANTIBES','27/06/2019','29/06/2019','6160','43.587465146','7.10635418256');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MUSICAL ETE DE SAINT DIZIER','Musiques actuelles','https://www.saint-dizier.fr','ST DIZIER','28/06/2019','30/06/2019','52100','48.6280100668','4.94923523578');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ECHOS & MERVEILLES','Musiques actuelles','http://echosetmerveilles.fr/','Bruguieres','26/04/2019','28/04/2019','31150','43.7271508931','1.40915818124');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CHANT''APPART','Musiques actuelles','http://www.chantappart.fr/','VAIRE','02/02/2019','31/03/2019','85150','46.6055340621','-1.74863672042');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Musicales Franco-Russes','Musiques classiques','','TOULOUSE','22/02/2019','16/03/2019','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du cirque de demain','Cirque et Arts de la rue','http://www.cirquededemain.paris/','PARIS','31/01/2019','03/02/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Sons d''hiver','Musiques actuelles','www.sonsdhiver.org','VITRY SUR SEINE','01/02/2019','23/02/2019','94400','48.7882828307','2.39412680533');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du livre de jeunesse Occitanie','Livre et litt�rature','http://festival-livre-jeunesse.fr','St Orens de Gameville','26/01/2019','27/01/2019','31650','43.5590787673','1.53513710649');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES RENCONTRES DE BRETAGNE EN SCENE(S)','Pluridisciplinaire Spectacle vivant','http://www.bretagneenscenes.com','CARHAIX PLOUGER','29/01/2019','31/01/2019','29270','48.2687578823','-3.56626363851');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Salon du livre jeunesse D�lires de lire','Livre et litt�rature','https://fr-fr.facebook.com/pages/category/Media/Salon-du-livre-Jeunesse-D%C3%A9lires-de-lire-939632566113688/','St Gervais la Foret','15/03/2019','17/03/2019','41350','47.5607456407','1.36138962511');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VOIRON JAZZ FESTIVAL','Musiques actuelles','www.voiron-jazz.com','VOIRON','14/03/2019','30/03/2019','38500','45.3791720843','5.58240310671');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES GIBOULEES','Cirque et Arts de la rue','http://festival-lesgiboulees.fr/','RENNES','16/03/2019','17/03/2019','35700','48.1119791219','-1.68186449144');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BANLIEUES BLEUES','Musiques actuelles','www.banlieuesbleues.org','ST DENIS','22/03/2019','19/04/2019','93200','48.9295650455','2.3592429975');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FEMMES S''EN MELENT','Musiques actuelles','www.lfsm.net','PARIS','21/03/2019','10/04/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Semaine de la po�sie','Livre et litt�rature','http://lasemainedelapoesie.assoc.univ-bpclermont.fr/','CLERMONT FERRAND','16/03/2019','23/03/2019','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'PANORAMAS','Musiques actuelles','www.festivalpanoramas.com','MORLAIX','12/04/2019','14/04/2019','29600','48.5996746612','-3.82052019963');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FOUS RIRES D''AVIGNON','Divers Spectacle vivant','http://www.leparisavignon.com','AVIGNON','02/03/2019','16/03/2019','84140','43.9352448339','4.84071572505');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'La BD est dans le pr�','Livre et litt�rature','','Fourques sur Garonne','09/03/2019','10/03/2019','47200','44.4554149219','0.154536756798');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SURFIN BIRD FESTIVAL','Musiques actuelles','www.jura-tourism.com/offre/fiche/surfin-bird-festival/320006631','LONS LE SAUNIER','','','39000','46.6744796278','5.55733212947');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MENNECY METAL FEST','Musiques actuelles','www.mennecy.fr/Mennecy-Metal-Fest-930#.W2RizdUzYdU','MENNECY','','','91540','48.5586242184','2.43753236038');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LA RUE DU BIZEOU','Musiques actuelles','https://www.tourismelandes.com','ARBOUCAVE','','','40320','43.6059261585','-0.44174435794');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'UN BOL D''AIRS','Musiques actuelles','www.boldairs.net','PUYGOUZON','','','81990','43.8884573228','2.16955643928');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CULTURE AU QUAI','Transdisciplinaire','www.cultureauquai.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TOULOUSE LES ORGUES','Musiques classiques','www.toulouse-les-orgues.org','TOULOUSE','','','31300','43.5963814303','1.43167293364');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Des cirques Indisciplin�s','Cirque et Arts de la rue','www.theatre-arles.com','ARLES','','','13123','43.5468692378','4.66215642574');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES AUTOMNALES DU PUY DE DOME','Pluridisciplinaire Spectacle vivant','www.festival.puy-de-dome.fr','CLERMONT FERRAND','','','63100','45.7856492991','3.11554542903');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HYPNOTIK','Musiques actuelles','www.elektrosystem.org','CHASSIEU','','','69680','45.7376148968','4.96172914286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival International du Film Ind�pendant de Bordeaux','Cin�ma et audiovisuel','www.fifib.com','BORDEAUX','','','33000','44.8572445351','-0.57369678116');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OSE(Z) LA DANSE (ex HIP HOP & CO)','Danse','www.m3q.centres-sociaux.fr/?page_id=738','POITIERS','','','86000','46.5839207726','0.359947653003');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'GUITARE ACOUSTIQUE A NICE','Musiques actuelles','http://www.espacemagnan.com/','NICE','','','6100','43.7119992661','7.23826889465');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'HEURES MUSICALES DE BINIC','Musiques classiques','www.musicalesdebinic.blogspot.com','BINIC','','','22520','48.6255350477','-2.8413899481');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ABERS BLUES','Musiques actuelles','www.hot-club-jazz-iroise.fr/','BRELES','','','29810','48.4709032483','-4.68640383039');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'OREILLES EN POINTE','Musiques actuelles','www.oreillesenpointe.com','FIRMINY','','','42700','45.3788146093','4.28879456136');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Instants Vid�o','Transdisciplinaire','www.instantsvideo.com','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'QUINTE ARTISTIQUE FESTIVAL','Musiques actuelles','www.casinodebeaulieu.com','BEAULIEU SUR MER','','','6310','43.7079039397','7.33256934881');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'A L''EST DES DUNES - Jazz � Sables d''or','Musiques actuelles','www.alestdesdunes.com','FREHEL','','','22240','48.6348223959','-2.36202603588');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'TERRE DE COULEURS','Musiques actuelles','www.terredecouleurs.asso.fr','DAUMAZAN SUR ARIZE','','','9350','43.1445751789','1.30386151934');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Lunallena Festival','Musiques actuelles','https://www.lunallena-festival.com','BANDOL','','','83150','43.1473544522','5.74789358398');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du Conte de Baden','Divers Spectacle vivant','','Baden','','','56870','47.614993273','-2.90383771516');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LA ROUTE DU SIRQUE','Cirque et Arts de la rue','http://www.sirquenexon.com','NEXON','','','87800','45.6749732411','1.18567878559');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES FETES LATINO MEXICAINES','Musiques actuelles','www.barcelonnette.com','BARCELONNETTE','','','4400','44.3785614205','6.65215089713');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ DE LUNEL','Musiques actuelles','www.ville-lunel.fr','LUNEL','','','34400','43.6820481757','4.13566472301');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIJAZZ','Musiques actuelles','www.houlgate-tourisme.fr/','HOULGATE','','','14510','49.2974071263','-0.0677562139709');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES DENTELLES ELECTRONIQUES','Musiques actuelles','','Caudry','','','59540','50.1187185043','3.4192394153');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BALEAPOP','Transdisciplinaire','www.baleapop.com','ST JEAN DE LUZ','','','64500','43.3947886873','-1.63099021573');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CLASSIQUE SUR LE ROC','Musiques classiques','www.guesclin.com/index.php/fr','ST Coulomb','','','35350','48.677369423','-1.92087004371');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES MUSIQUES SACREES DU MONDE DE SAINT-ANTOINE DE FICALBA','Pluridisciplinaire Musique','http://sacree-saint-antoine.fr/','St -Antoine de Ficalba','','','47340','44.1868209409','0.516380488388');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'BLUES EN CHENIN','Musiques actuelles','www.bluesenchenin.com','ST LAMBERT DU LATTAY','','','49750','47.3002199454','-0.640314853216');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MON BABY BLUES FESTIVAL','Musiques actuelles','www.monbabybluesfestival.com','MONTBELIARD','','','25200','47.5155169816','6.79148147353');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ART SCENE FESTIVAL','Musiques actuelles','www.lartscenefestival.com','ATTIGNAT','','','1340','46.2861802203','5.1795233845');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES COTEAUX BORDELAIS','Pluridisciplinaire Spectacle vivant','http://www.coteaux-bordelais.fr/','Fargues ST Hilaire','','','33370','44.819815277','-0.439992385579');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Biennale de Lyon (Danse)','Danse','www.biennaledeladanse.com','LYON','','','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DU RIRE FERNAND RAYNAUD','Divers Spectacle vivant','www.ville-saint-germain.com','ST Germain des Fosses','','','3260','46.1959477042','3.42997700831');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LAFI BALA','Musiques actuelles','www.lafibala.com','CHAMBERY','28/06/2019','30/06/2019','73000','45.583182552','5.90903392417');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Les Carnets','Livre et litt�rature','https://festivalcarnets.com/','LA ROQUE D ANTHERON','29/06/2019','30/06/2019','13640','43.7176409517','5.30157633559');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Fnac Live','Musiques actuelles','www.fnaclive.com','PARIS','03/07/2019','05/07/2019','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JALLES HOUSE ROCK','Musiques actuelles','http://www.jalleshouserock.fr/','ST MEDARD EN JALLES','04/07/2019','06/07/2019','33160','44.8832620816','-0.784239883546');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JAZZ A MONTONVILLERS','Musiques actuelles','https://fr-fr.facebook.com/festivaldejazzdemontonvillers/','MONTONVILLERS','05/07/2019','07/07/2019','80260','49.9964398506','2.29571982025');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Les Affranchis','Cirque et Arts de la rue','www.lecarroi.org','LA FLECHE','05/07/2019','07/07/2019','72200','47.6893893794','-0.0646954849714');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Le Son Continu','Musiques actuelles','http://www.lesoncontinu.fr/','Lourouer ST Laurent','11/07/2019','14/07/2019','36400','46.6194091093','2.01790100072');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dub Camp Festival','Musiques actuelles','www.dubcampfestival.com','Joue Sur Erdre','11/07/2019','14/07/2019','44440','47.5092437538','-1.42943494948');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'F�te des Brodeuses','Pluridisciplinaire Spectacle vivant','www.fetedesbrodeuses.com','Pont l Abbe','12/07/2019','14/07/2019','29120','47.8654885488','-4.21962319652');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Cin�-rencontres de Prades','Cin�ma et audiovisuel','http://www.cine-rencontres.org/','Prades','15/07/2019','23/07/2019','66500','42.6145021996','2.43074849066');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ETE A PAU','Musiques actuelles','www.leteapau.com','PAU','17/07/2019','03/08/2019','64000','43.3200189773','-0.350337918181');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'MORTY JAZZ FESTIVAL','Musiques actuelles','https://mortyjazz.wordpress.com/','MORTEFONTAINE','18/07/2019','20/07/2019','2600','49.3362768244','3.08053479514');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS DU SUD A VENCE','Musiques actuelles','www.nuitsdusud.com','VENCE','19/07/2019','02/08/2019','6140','43.7384640641','7.10194436087');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Voix vives de M�diterran�e S�te','Livre et litt�rature','http://www.voixvivesmediterranee.com','SETE','19/07/2019','27/07/2019','34200','43.3917705831','3.64705148296');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE MUSIQUE DE CHAMBRE DE SAINT PAUL DE VENCE','Musiques classiques','http://www.festivalsaintpauldevence.com','ST PAUL DE VENCE','20/07/2019','31/07/2019','6570','43.6950190291','7.12116951865');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FOIRE AUX VINS D''ALSACE DE COLMAR','Musiques actuelles','www.foire-colmar.com','COLMAR','26/07/2019','04/08/2019','68000','48.1099405789','7.38468690323');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Jazz en libert�','Musiques actuelles','https://www.andernos-jazz-festival.fr/','ANDERNOS LES BAINS','26/07/2019','28/07/2019','33510','44.754520164','-1.08109347037');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE LACOSTE','Pluridisciplinaire Spectacle vivant','http://www.festivaldelacoste.com','Lacoste','26/07/2019','11/08/2019','84480','43.8257421042','5.26592271907');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LE CHANT DE L EUCALYPTUS','Musiques actuelles','http://www.chant-eucalyptus.com','PLOUHINEC','27/07/2019','27/07/2019','56680','47.6929109171','-3.23358959435');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du P�rigord Noir','Musiques classiques','www.festivalmusiqueperigordnoir.com','MONTIGNAC','28/07/2019','11/10/2019','24290','45.0627368774','1.15422675286');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'VISIONS','Musiques actuelles','www.festivalvisions.com','Plougonvelin','02/08/2019','04/08/2019','29217','48.3513274854','-4.71760872698');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'ROCK EN STOCK','Musiques actuelles','www.rockenstock.org','ETAPLES','02/08/2019','04/08/2019','62630','50.5235816785','1.65199985504');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SOIREES DU CASTELLET','Musiques classiques','https://www.lessoireesducastellet.fr/','Le Castellet','03/08/2019','19/08/2019','83330','43.227667489','5.76560108367');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Rencontres Chaland','Livre et litt�rature','www.rencontres.yveschaland.com','NERAC','05/10/2019','06/10/2019','47600','44.1321004332','0.343358299602');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival Points de vue','Arts plastiques et visuels','https://pointsdevuefest.org/','BAYONNE','16/10/2019','20/10/2019','64100','43.4922254016','-1.46607674358');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival du polar de Cognac','Transdisciplinaire','https://www.festival-polar-cognac.fr/','COGNAC','18/10/2019','20/10/2019','16100','45.6962326551','-0.33507941815');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival international du cirque de Grenoble','Cirque et Arts de la rue','https://www.gcproductions.fr/','GRENOBLE','14/11/2019','24/11/2019','38000','45.1821215167','5.72133051752');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival des fims et des livres voyageurs','Transdisciplinaire','www.quaisdudepart.fr','LYON','21/11/2019','24/11/2019','69001','45.7699284397','4.82922464978');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Festival de Danse - Cannes C�te d�Azur France','Danse','https://www.festivaldedanse-cannes.com/','CANNES','29/11/2019','15/12/2019','6400','43.5526202843','7.00427592728');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DES STARS DE LA MAGIE ET DES ETOILES DU CIRQUE','Cirque et Arts de la rue','www.mental-arts-magie.blogspot.fr','FAY AUX LOGES','','','45450','47.9326007256','2.16025043268');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'L''ArtJoyette','Pluridisciplinaire Spectacle vivant','','ST Varent','','','79330','46.8927639643','-0.223311639048');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL DE L''ANCHE','Musiques actuelles','www.festivaldelanche.com','HYERES','','','83400','43.1018713534','6.18898508469');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CRESCENT JAZZ FESTIVAL','Musiques actuelles','www.lecrescent.net','MACON','','','71000','46.3205511756','4.81842529639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIVAL ALL STARS AU NEW MORNING','Musiques actuelles','www.newmorning.com','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NUITS COULEURS','Musiques actuelles','www.nuitscouleurs.com','GIGNAC','','','34150','43.6393937216','3.57384628868');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'FESTIV'' EN MARCHE','Musiques actuelles','http://www.festiv-en-marche.com','MOUHET','','','36170','46.3892514963','1.44265053138');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'CORBEIL-ESSONNES JAZZ FESTIVAL','Musiques actuelles','www.corbeilessonnesjazzfestival.com','CORBEIL ESSONNES','','','91100','48.6034809251','2.46934079002');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'JOUTES MUSICALES DE PRINTEMPS','Musiques actuelles','www.le-chantier.com','CORRENS','','','83570','43.4864185627','6.06826217623');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'LES VENDANGES DE L''HUMOUR','Divers Spectacle vivant','http://www.lesvendanges-de-lhumour.com','MACON','','','71000','46.3205511756','4.81842529639');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Des �les et des livres','Livre et litt�rature','https://festival-interbibly.com/','REIMS','','','51100','49.2514906066','4.0402302322');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'NOBORDER','Musiques actuelles','www.festivalnoborder.com','BREST','','','29200','48.4004997828','-4.5027907853');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'SWINGIN PARIS','Danse','https://www.swingin-paris.com/','PARIS','','','75001','48.8626304852','2.33629344655');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'REEVOX','Musiques actuelles','http://gmem.org','MARSEILLE','','','13001','43.2999009436','5.38227869795');
INSERT INTO TableFestival (nomF,typeF,lien,ville,dateD,dateF,postal,longitude,latitude) VALUES (
'Dr�les de No�ls','Cirque et Arts de la rue','http://www.droles-de-noels.fr/','ARLES','','','13123','43.5468692378','4.66215642574');
