drop table AvisHebergement;
drop table AvisFestival;
drop table ReservationLogement;
drop table FestivalReservation;
drop table Reservation;
drop table LogementDispo;
drop table Disponibilite;
drop table Logement;
drop table Photo;
drop table ParcResidentiel;
drop table Residence;
drop table VillageVacances;
drop table Camping;
drop table Hotel;
drop table Hebergement;
drop table Festival;
drop table Organisateur;
drop table Utilisateur;
drop table Hebergeur;
drop table Personne;


create table Personne (
    numPersonne varchar(128),
    nom varchar(50) not null,
    prenom varchar(30) not null,
    telephone varchar(15),
    mail varchar(30) not null,
    constraint pk_Personne primary key (numPersonne)
);

create table Organisateur (
    numOrganisateur integer,
    numPersonne varchar(128) not null,
    constraint pk_Organisateur primary key (numOrganisateur),
    constraint fk_Organisateur foreign key (numPersonne) references Personne (numPersonne)
);

create table Utilisateur (
    numUtilisateur integer,
    numPersonne varchar(128) not null,
    constraint pk_Utilisateur primary key (numUtilisateur),
    constraint fk_Utilisateur foreign key (numPersonne) references Personne (numPersonne)
);

create table Hebergeur (
    numHebergeur integer,
    numPersonne varchar(128) not null,
    constraint pk_Hebergeur primary key (numHebergeur),
    constraint fk_Hebergeur foreign key (numPersonne) references Personne (numPersonne)
);

create table Festival (
    numFestival integer,
    nomF varchar(100),
    noteMoyenne integer check(noteMoyenne >= 0 and noteMoyenne <= 10),
    dateDeb date,
    dateFin date,
    typeF varchar(50),
    siteWeb varchar(250),
    codePostal varchar(5),
    ville varchar(40),
    pays varchar(15),
    capaciteCateg0 integer check(capaciteCateg0 >= 0),
    capaciteCateg1 integer check(capaciteCateg1 >= 0),
    capaciteCateg2 integer check(capaciteCateg2 >= 0),
    tarifCateg0 number check(tarifCateg0 >= 0),
    tarifCateg1 number check(tarifCateg1 >= 0),
    tarifCateg2 number check(tarifCateg2 >= 0),
    etat varchar(10) check(etat in ('Disponible','Annule')),
    longitude number,
    latitude number,
    numOrganisateur integer,
    constraint pk_Festival primary key (numFestival),
    constraint fk_Festival foreign key (numOrganisateur) references Organisateur (numOrganisateur)
);


-- typeF varchar(50) check(typeF in ('Art Plastique','Audio Visuel','Cirque','Theatre','Danse','Pluridisciplinaire spectale vivant','Musique Classique','Pluridisciplinaire musique','Musique Actuelles','Litterature','Transdisciplinaire','Divers spectales vivant')),
-- 'Danse','Pluridisciplinaire spectale vivant','Musique Classique','Pluridisciplinaire musique','Musique Actuelles','Litterature','Transdisciplinaire','Divers spectales vivant'))

create table Hebergement (
    numHebergement integer,
    nomHebergement varchar(70) not null,
    noteMoyenne integer check (noteMoyenne >= 0 and noteMoyenne <= 10),
    classement integer check (classement >= 0 and classement <= 5),
    telephone varchar(15),
    siteWeb varchar(250),
    nom_rue varchar(100),
    codePostal varchar(8) not null,
    ville varchar(40) not null,
    pays varchar(15) not null,
    description varchar(60),
    longitude number,
    latitude number,
    numHebergeur integer not null,
    constraint pk_Hebergement primary key (numHebergement),
    constraint fk_Hebergement foreign key (numHebergeur) references Hebergeur (numHebergeur)
);

create table Hotel (
    numHotel integer,
    tarifEnfant number check(tarifEnfant >= 0) not null,
    tarifAdulte number check(tarifAdulte >= 0) not null,
    numHebergement integer not null,
    constraint pk_Hotel primary key (numHotel),
    constraint fk_Hotel foreign key (numHebergement) references Hebergement (numHebergement)
);

create table Camping (
    numCamping integer,
    nbEmplacement integer check(nbEmplacement >= 0) not null,
    tarif number check(tarif >= 0) not null,
    numHebergement integer not null,
    constraint pk_Camping primary key (numCamping),
    constraint fk_Camping foreign key (numHebergement) references Hebergement (numHebergement)
);

create table VillageVacances (
    numVillage integer,
    tarifEnfant number check(tarifEnfant >= 0) not null,
    tarifAdulte number check(tarifAdulte >= 0) not null,
    type_sejour varchar(10) check(type_sejour in('Location','Pension','L/P')),
    nbUniteHabitation integer check(nbUniteHabitation >= 0) not null,
    numHebergement integer not null,
    constraint pk_VillageVacances primary key (numVillage),
    constraint fk_VillageVacances foreign key (numHebergement) references Hebergement (numHebergement)
);

create table Residence (
    numResidence integer,
    nbHabitation integer check(nbHabitation >= 0) not null,
    tarif number check(tarif >= 0) not null,
    numHebergement integer not null,
    constraint pk_Residence primary key (numResidence),
    constraint fk_Residence foreign key (numHebergement) references Hebergement (numHebergement)
);

create table ParcResidentiel (
    numParcResidentiel integer,
    nbEmplacement integer check(nbEmplacement >= 0) not null,
    tarif number check(tarif >= 0) not null,
    numHebergement integer not null,
    constraint pk_ParcResidentiel primary key (numParcResidentiel),
    constraint fk_ParcResidentiel foreign key (numHebergement) references Hebergement (numHebergement)
);

create table Photo (
    numPhoto integer,
    chemin varchar(150),
    numHebergement integer not null,
    constraint pk_Photo primary key (numPhoto),
    constraint fk_Photo foreign key (numHebergement) references Hebergement (numHebergement)
);

create table Logement (
    numLogement integer,
    description varchar(150),
    type_chambre varchar(10) check(type_chambre in ('Simple','Double','Familiale','Dortoir','Autre')),
    nbEnfant integer check(nbEnfant >= 0),
    nbAdulte integer check(nbAdulte >= 0),
    capaciteMax integer check(capaciteMax >= 0),
    prixLogement number,
    numHebergement integer not null,
    constraint pk_Logement primary key (numLogement),
    constraint fk_Logement foreign key (numHebergement) references Hebergement (numHebergement)
);
  
create table Disponibilite (
    numDispo integer,
    dateDispo date,
    constraint pk_Disponibilite primary key (numDispo)
);
  
create table LogementDispo (
    numLogement integer,
    numDispo integer,
    constraint pk_LogementDispo primary key (numLogement,numDispo),
    constraint fk_LogementDispo foreign key (numLogement) references Logement (numLogement),
    constraint fk_LogementDispo_date foreign key (numDispo) references Disponibilite (numDispo)
);
  
create table Reservation (
    numReservation integer,
    dateReservation date not null,
    dateValidation date,
    etatReservation varchar(10) check(etatReservation in ('En cours','Valide','Annule')),
    montanTotal number check(montanTotal >= 0),
    nbEnfant integer check(nbEnfant >= 0),
    nbAdulte integer check(nbAdulte >= 0),
    numUtilisateur integer not null,
    constraint pk_Reservation primary key (numReservation),
    constraint fk_Reservation foreign key (numUtilisateur) references Utilisateur (numUtilisateur)
);

create table AvisFestival (
    numAvisF integer,
    texte_avis varchar(250) not null,
    note integer check(note >= 0 and note <= 10),
    numFestival integer not null,
    numReservation integer not null,
    constraint pk_AvisFestival primary key (numAvisF),
    constraint fk_AvisResF foreign key (numReservation) references Reservation (numReservation),
    constraint fk_AvisFestival_F foreign key (numFestival) references Festival (numFestival)
);

create table AvisHebergement (
    numAvisH integer,
    texte_avis varchar(250) not null,
    note integer check(note >= 0 and note <= 10),
    numHebergement integer not null,
    numReservation integer not null,
    constraint pk_AvisHebergement primary key (numAvisH),
    constraint fk_AvisResH foreign key (numReservation) references Reservation (numReservation),
    constraint fk_AvisHebergement foreign key (numHebergement) references Hebergement (numHebergement)
);

create table FestivalReservation (
    numFestival integer,
    numReservation integer,
    nbCategorie0 integer check(nbCategorie0 >= 0),
    nbCategorie1 integer check(nbCategorie1 >= 0),
    nbCategorie2 integer check(nbCategorie2 >= 0),
    datePlaceRes date,
    constraint pk_FestivalReservation primary key (numFestival,numReservation,datePlaceRes),
    constraint fk_FestivalReservation foreign key (numFestival) references Festival (numFestival),
    constraint fk_FestivalReservation_Res foreign key (numReservation) references Reservation (numReservation)
);

create table ReservationLogement (
    numLogement integer,
    numReservation integer,
    dateDebut date not null,
    dateFin date not null,
    constraint pk_ReservationLogement primary key (numLogement,numReservation,dateDebut),
    constraint fk_ReservationLogement foreign key (numLogement) references Logement (numLogement),
    constraint fk_ReservationLogement_Res foreign key (numReservation) references Reservation (numReservation)
);



