insert into Personne values ('11','Dupon','Mbappe','0708102500','id1@gmail.com');
insert into Organisateur values (seq_organisateur.nextval,'11');

insert into Personne values ('12','Bar','Poto','0708103200','id2@gmail.com');
insert into Organisateur values (seq_organisateur.nextval,'12');

insert into Personne values ('13','Tetrel','Loana','0708103300','id3@gmail.com');
insert into Hebergeur values (seq_hebergeur.nextval,'13');

insert into Personne values ('14','Test','Faux','0708103400','id40,@gmail.com');
insert into Hebergeur values (seq_hebergeur.nextval,'14');

insert into Personne values ('15','Test','Juste','0708103500','id5@gmail.com');
insert into Utilisateur values (seq_utilisateur.nextval,'15');

insert into Festival values (seq_festival.nextval,'CirqueCampus',0,TO_DATE('2020/01/01', 'yyyy/mm/dd'),TO_DATE('2020/01/30', 'yyyy/mm/dd'),'Cirque','wwww.google.com','06200','Nice','France',10,15,20,20,30,40,'Disponible',0,0,1);
insert into Festival values (seq_festival.nextval,'TheatreCampus',0,TO_DATE('2020/02/01', 'yyyy/mm/dd'),TO_DATE('2020/02/26', 'yyyy/mm/dd'),'Theatre','wwww.theatreuga.com','38000','Grenoble','France',10,15,20,20,30,40,'Disponible',0,0,1);
insert into Festival values (seq_festival.nextval,'Mini Film',0,TO_DATE('2020/03/01', 'yyyy/mm/dd'),TO_DATE('2020/03/30', 'yyyy/mm/dd'),'Audio Visuel','wwww.facelook.com','08000','Valence','France',5,10,20,20,30,40,'Disponible',0,0,2);
insert into Festival values (seq_festival.nextval,'Danse Party',0,TO_DATE('2020/04/01', 'yyyy/mm/dd'),TO_DATE('2020/04/30', 'yyyy/mm/dd'),'Danse','wwww.danselook.com','70000','Tourcoin','France',5,10,20,20,30,40,'Disponible',0,0,2);

insert into Hebergement values (seq_hebergement.nextval,'CampingParadis',0,1,'0408103400','wwww.CampingParadis.com','nuls','05000','Marseille','France','Description',0,0,2);
insert into Hebergement values (seq_hebergement.nextval,'BandB',0,3,'0408103418','wwww.BandB.com','salsa','48000','Montpelier','France','Description',0,0,1);

insert into Hebergement values (seq_hebergement.nextval,'VillageUga',0,0,'0408103401','wwww.VillageUga.com','chavant','38600','Gieres','France','Description',0,0,2);
insert into Hebergement values (seq_hebergement.nextval,'ResidenceCannet',0,3,'0408103410','wwww.Cannet.com','salsa','45000','Lille','France','Description',0,0,1);

insert into Hebergement values (seq_hebergement.nextval,'Parc Croisette',0,1,'0408103402','wwww.Croisette.com','nuls','17000','Cannes','France','Description',0,0,2);
insert into Hebergement values (seq_hebergement.nextval,'Hilton',0,3,'0408103414','wwww.hilton.com','princesse grace','06000','Monaco','Monaco','Description',0,0,1);

insert into Hotel values (seq_hotel.nextval,10,30,2);
insert into Hotel values (seq_hotel.nextval,15,45,6);

insert into Camping values (seq_camping.nextval,25,30,1);
insert into VillageVacances values (seq_village.nextval,8,16,'Location',36,3);

insert into Residence values (seq_residence.nextval,20,100,4);
insert into ParcResidentiel values (seq_parcresidentiel.nextval,38,60,5);

insert into Photo values (seq_photo.nextval,'C/Album/BandB',2);
insert into Photo values (seq_photo.nextval,'C/Album/CampingParadis',1);
insert into Photo values (seq_photo.nextval,'C/Album/VillageUga',3);
insert into Photo values (seq_photo.nextval,'C/Album/Parc Croisette',5);
insert into Photo values (seq_photo.nextval,'C/Album/Hilton',6);

insert into Logement values (seq_logement.nextval,'Chambre Simple, vue sur la mer magnifique','Simple',1,1,1,100,2);
insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Simple',1,1,1,100,2);
insert into Logement values (seq_logement.nextval,'Chambre Double, vue magnifique sur la mer','Double',2,2,2,100,2);
insert into Logement values (seq_logement.nextval,'Chambre Familiale, vue sur la mer et les montagnes','Familiale',3,3,6,100,2);

insert into Logement values (seq_logement.nextval,'Chambre Simple, vue sur la Croisette','Simple',1,1,1,100,6);
insert into Logement values (seq_logement.nextval,'Chambre Simple, vue sur le parking','Simple',1,1,1,100,6);
insert into Logement values (seq_logement.nextval,'Chambre Double, vue sur la mer','Double',2,2,2,100,6);
insert into Logement values (seq_logement.nextval,'Chambre Familiale, vue sur la mer et les montagnes','Familiale',3,3,6,100,6);

insert into Logement values (seq_logement.nextval,'Dortoire Village de Valence','Dortoir',8,8,8,202,3);
insert into Logement values (seq_logement.nextval,'Chambre Village de Valence','Autre',3,2,5,202,3);

insert into Logement values (seq_logement.nextval,'Emplacement Camping','Autre',4,4,4,202,1);
insert into Logement values (seq_logement.nextval,'Emplacement Camping','Autre',5,5,5,202,1);

insert into Logement values (seq_logement.nextval,'Residence Croisette','Autre',3,3,6,202,4);
insert into Logement values (seq_logement.nextval,'Residence Croisette','Autre',2,2,4,202,4);

insert into Logement values (seq_logement.nextval,'Parc Residentiel','Autre',2,2,4,202,5);
insert into Logement values (seq_logement.nextval,'Parc Residentiel','Autre',3,2,5,202,5);

insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/01', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/02', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/03', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/04', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/05', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/06', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/07', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/08', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/09', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/10', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/11', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/12', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/13', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/14', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/15', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/16', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/17', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/18', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/19', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/20', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/21', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/22', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/23', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/24', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/25', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/26', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/27', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/28', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/29', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/30', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/01/31', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/01', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/02', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/03', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/04', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/05', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/06', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/07', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/08', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/09', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/10', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/11', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/12', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/13', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/14', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/15', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/16', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/17', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/18', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/19', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/20', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/21', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/22', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/23', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/24', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/25', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/26', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/27', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/28', 'yyyy/mm/dd'));
insert into Disponibilite values (seq_dispo.nextval,TO_DATE('2020/02/29', 'yyyy/mm/dd'));

insert into LogementDispo values(1,1);
insert into LogementDispo values(1,2);
insert into LogementDispo values(1,3);
insert into LogementDispo values(1,4);
insert into LogementDispo values(1,5);
insert into LogementDispo values(1,6);
insert into LogementDispo values(1,7);
insert into LogementDispo values(1,8);
insert into LogementDispo values(1,9);
insert into LogementDispo values(1,10);
insert into LogementDispo values(1,11);
insert into LogementDispo values(1,12);
insert into LogementDispo values(1,13);
insert into LogementDispo values(1,14);
insert into LogementDispo values(1,15);
insert into LogementDispo values(1,16);
insert into LogementDispo values(1,17);
insert into LogementDispo values(1,18);
insert into LogementDispo values(1,19);
insert into LogementDispo values(1,20);
insert into LogementDispo values(1,21);
insert into LogementDispo values(1,22);
insert into LogementDispo values(1,23);
insert into LogementDispo values(1,24);
insert into LogementDispo values(1,25);
insert into LogementDispo values(1,26);
insert into LogementDispo values(1,27);

insert into LogementDispo values(2,1);
insert into LogementDispo values(2,2);
insert into LogementDispo values(2,3);
insert into LogementDispo values(2,4);
insert into LogementDispo values(2,5);
insert into LogementDispo values(2,6);
insert into LogementDispo values(2,7);
insert into LogementDispo values(2,8);
insert into LogementDispo values(2,9);
insert into LogementDispo values(2,10);
insert into LogementDispo values(2,11);
insert into LogementDispo values(2,12);
insert into LogementDispo values(2,13);
insert into LogementDispo values(2,14);
insert into LogementDispo values(2,15);
insert into LogementDispo values(2,16);
insert into LogementDispo values(2,17);
insert into LogementDispo values(2,18);
insert into LogementDispo values(2,19);
insert into LogementDispo values(2,20);

insert into LogementDispo values(3,1);
insert into LogementDispo values(3,2);
insert into LogementDispo values(3,3);
insert into LogementDispo values(3,4);
insert into LogementDispo values(3,5);
insert into LogementDispo values(3,6);
insert into LogementDispo values(4,7);
insert into LogementDispo values(4,8);
insert into LogementDispo values(4,9);
insert into LogementDispo values(4,10);
insert into LogementDispo values(4,11);
insert into LogementDispo values(5,12);
insert into LogementDispo values(5,13);
insert into LogementDispo values(5,14);
insert into LogementDispo values(5,25);
insert into LogementDispo values(5,36);
insert into LogementDispo values(6,47);
insert into LogementDispo values(6,38);
insert into LogementDispo values(6,39);
insert into LogementDispo values(6,30);

insert into Reservation values (seq_reservation.nextval,TO_DATE('2020/01/21 14:25:32' , 'yyyy/mm/dd hh24:mi:ss'),null,'En cours',100,2,2,1);

insert into AvisFestival values (seq_avisfestival.nextval,'Donde esta la playa',3,3,2);

insert into AvisHebergement values (seq_avishebergement.nextval,'Donde esta la playa',3,4,2);

insert into FestivalReservation values (3,2,0,5,2,TO_DATE('2020/01/22', 'yyyy/mm/dd'));
insert into FestivalReservation values (3,2,0,5,2,TO_DATE('2020/01/23', 'yyyy/mm/dd'));
insert into FestivalReservation values (3,2,0,5,2,TO_DATE('2020/01/24', 'yyyy/mm/dd'));

insert into ReservationLogement values (13,2,TO_DATE('2020/01/22', 'yyyy/mm/dd'),TO_DATE('2020/01/24', 'yyyy/mm/dd'));

-- insert into ReservationLogement values (15175,9,TO_DATE('2020/01/30', 'yyyy/mm/dd'),TO_DATE('2020/01/30', 'yyyy/mm/dd'));

-- insert into Festival values (seq_festival.nextval,'TheatreCampus',0,TO_DATE('2020/02/01', 'yyyy/mm/dd'),TO_DATE('2020/02/26', 'yyyy/mm/dd'),'Theatre','wwww.theatreuga.com','38000','Grenoble','France',10,15,20,20,30,40,'Disponible',0,0,1);

-- Update Festival set capaciteCateg0 = 5 , capaciteCateg1 = 5, capaciteCateg2 = 5 where nomF = 'TheatreCampus';