Drop SEQUENCE seq_reservation;
Drop SEQUENCE seq_dispo;
Drop SEQUENCE seq_logement;
Drop SEQUENCE seq_photo;
Drop SEQUENCE seq_avishebergement;
Drop SEQUENCE seq_avisfestival;
Drop SEQUENCE seq_parcresidentiel;
Drop SEQUENCE seq_residence;
Drop SEQUENCE seq_village;
Drop SEQUENCE seq_camping;
Drop SEQUENCE seq_hotel;
Drop SEQUENCE seq_hebergement;
Drop SEQUENCE seq_festival;
Drop SEQUENCE seq_utilisateur;
Drop SEQUENCE seq_hebergeur;
Drop SEQUENCE seq_organisateur;
Drop SEQUENCE seq_personne;

CREATE SEQUENCE seq_personne
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_organisateur
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_hebergeur
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_utilisateur
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_festival
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;


CREATE SEQUENCE seq_hebergement
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_hotel
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_camping
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_village
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_residence
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_parcresidentiel
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_avisfestival
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;
    
CREATE SEQUENCE seq_avishebergement
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_photo
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_logement
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_dispo
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;

CREATE SEQUENCE seq_reservation;
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    CACHE 100;