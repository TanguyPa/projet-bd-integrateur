import {Component, Input, OnInit} from '@angular/core';
import {DateFormatPipe} from "../../../utils/date-format.pipe";
import {HebergementService} from "../../../services/hebergement.service";
import {ReservationService} from "../../../services/reservation.service";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-panier-ele-logement',
  templateUrl: './panier-ele-logement.component.html',
  styleUrls: ['./panier-ele-logement.component.scss'],
  providers:[DateFormatPipe]
})
export class PanierEleLogementComponent implements OnInit {
  @Input() data;
  @Input() numR;
  l;
  constructor(private serviceLogement: HebergementService, private datePipe: DateFormatPipe, private reservationService: ReservationService, private serviceData: DataService) { }

  ngOnInit() {
    if(this.data && this.data.numHebergement) {
      this.serviceLogement.getHebergementById(this.data.numHebergement).subscribe(r => {
        console.log('r', r);
        this.l = r['hebergement'];
      });
    }
  }
  transformDate(date){
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }
  supPanier() {
    this.reservationService.deleteLogeDePanier( this.numR, this.data.numLogement, this.datePipe.transform(this.data.dateDeb), this.datePipe.transform(this.data.dateFin)).subscribe(() => {
      this.serviceData.refreshPanier();
    });
  }
}
