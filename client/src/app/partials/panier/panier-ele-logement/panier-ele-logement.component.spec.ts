import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanierEleLogementComponent } from './panier-ele-logement.component';

describe('PanierEleLogementComponent', () => {
  let component: PanierEleLogementComponent;
  let fixture: ComponentFixture<PanierEleLogementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanierEleLogementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanierEleLogementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
