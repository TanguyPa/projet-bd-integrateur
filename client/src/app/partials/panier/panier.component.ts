import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SidebarService} from "../../services/sidebar.service";
import {ReservationService} from "../../services/reservation.service";
import {MatDialog} from "@angular/material";
import {DialogMsgComponent} from "../../pages/dialog-msg/dialog-msg.component";
import {Observable} from "rxjs/index";
import {FestivalService} from "../../services/festival.service";
import {HebergementService} from "../../services/hebergement.service";
import {DataService} from "../../services/data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {
  @Output() public hide = new EventEmitter();
  msg;
  panierInfo;
  festivals = [];
  logements = [];
  constructor( private router: Router, private sidenav: SidebarService, private serviceFestival: FestivalService, private serviceLogement: HebergementService,
               private serviceReservation: ReservationService, public dialog: MatDialog, public dataService: DataService) {
    this.dataService.setPanierRefreshCallback(() => {
      this.loadPanier();
    })
  }
  ngOnInit() {
    this.loadPanier();
  }
  loadPanier() {
    this.serviceReservation.getPanier(localStorage.getItem('persId')).subscribe(res => {
      if(res['panier']){
        if(res['panier'][0]) {
          // console.log('res',res);
          this.panierInfo = res['panier'][0];
          // console.log('type de montantTotal',
          // typeof this.panierInfo.montantTotal,this.panierInfo.montantTotal );
          this.festivals = res['panier'][0]['resFestival'];
          this.logements = res['panier'][0]['resLogement'];
        }
      }
    });
  }

  getFestival(id): Observable<any> {
    return this.serviceFestival.getFestivalById(id);
  }
  sendMessageToParent() {
    this.hide.emit()
  }
  commander() {
    if (this.festivals != undefined && this.logements != undefined && (this.festivals.length === 0) || (this.logements.length === 0)) {
      this.openMsgDialog(false, 'Chaque reservation doit contenir au moins un festival et un logement! ');
    } else  {
      this.serviceReservation.checkExpiration(localStorage.getItem('userId')).subscribe(
        data => {
          this.router.navigateByUrl('validPanier/' + this.panierInfo.numReservation);
        },
        error => this.openMsgDialog(true, error.error.message)
      );
    }
  }
  openMsgDialog(error, msg): void {
    const dialogRef = this.dialog.open(DialogMsgComponent, {
      width: '400px',
      data: {
        error: error,
        msg: msg
      }
    });
  }
}
