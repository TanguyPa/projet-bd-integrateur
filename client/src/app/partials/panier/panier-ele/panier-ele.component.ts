import {Component, Input, OnInit} from '@angular/core';
import {FestivalService} from "../../../services/festival.service";
import {HebergementService} from "../../../services/hebergement.service";
import {DateFormatPipe} from "../../../utils/date-format.pipe";
import {ReservationService} from "../../../services/reservation.service";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-panier-ele',
  templateUrl: './panier-ele.component.html',
  styleUrls: ['./panier-ele.component.scss'],
  providers:[DateFormatPipe]
})
export class PanierEleComponent implements OnInit {
  @Input() data;
  @Input() numR;
  f;
  constructor(private serviceFestival: FestivalService, private datePipe: DateFormatPipe, private reservationService: ReservationService, private dataService: DataService) { }

  ngOnInit() {
    if(this.data && this.data.numFestival) {
      this.serviceFestival.getFestivalById(this.data.numFestival).subscribe(r => {
        this.f = r['festival'];
        console.log(this.f);
      });
    }
  }
  transformDate(date){
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }

  supPanier() {
    this.reservationService.deleteFestDePanier(this.f.id, this.numR, this.datePipe.transform(this.data.dateRes)).subscribe(() => {
      this.dataService.refreshPanier();
    });
  }
}
