import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResHebergementsComponent } from './res-hebergements.component';

describe('ResHebergementsComponent', () => {
  let component: ResHebergementsComponent;
  let fixture: ComponentFixture<ResHebergementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResHebergementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResHebergementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
