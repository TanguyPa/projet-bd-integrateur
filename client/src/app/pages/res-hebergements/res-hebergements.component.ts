import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DateFormatPipe} from "../../utils/date-format.pipe";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {HebergementService} from "../../services/hebergement.service";

@Component({
  selector: 'app-res-hebergements',
  templateUrl: './res-hebergements.component.html',
  styleUrls: ['./res-hebergements.component.scss'],
  providers:[DateFormatPipe]

})
export class ResHebergementsComponent implements OnInit {
  id;
  hebergement;
  form: FormGroup;
  minDate = new Date(Date());
  maxDate = new Date(2021, 0, 1);
  dateRes = {
    dateDebut: Date(),
    dateFin: Date()
  };
  @Output() public hide = new EventEmitter();

  constructor(private router: Router, private route: ActivatedRoute, private serviceHebergement: HebergementService, fb: FormBuilder, private datePipe: DateFormatPipe) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.form = fb.group({
      date: [{begin: this.minDate, end: this.minDate}]
    });
  }

  ngOnInit() {
    this.serviceHebergement.getHebergementById(this.id).subscribe(r => {
      this.hebergement = r['hebergement'];
    });
    this.form.controls['date'].valueChanges.subscribe(value => {
      this.dateRes.dateDebut = this.datePipe.transform(value? value.begin: null);
      this.dateRes.dateFin = this.datePipe.transform(value? value.end: null);

      console.log('value', this.dateRes.dateDebut, this.dateRes.dateFin);
    });
  }

}
