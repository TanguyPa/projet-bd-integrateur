import {Component, Input, OnInit} from '@angular/core';
import {HebergementService} from "../../../services/hebergement.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DateFormatPipe} from "../../../utils/date-format.pipe";
@Component({
  selector: 'app-list-logements',
  templateUrl: './list-logements.component.html',
  styleUrls: ['./list-logements.component.scss']
})
export class ListLogementsComponent implements OnInit {
  @Input() data;
  logements;
  form: FormGroup;
  minDate = new Date(Date());
  maxDate = new Date(2021, 0, 1);
  dateRes = {
    dateDebut: Date(),
    dateFin: Date()
  };
  constructor(private serviceHebergement: HebergementService, fb: FormBuilder, private datePipe: DateFormatPipe) {
    this.form = fb.group({
      date: [{begin: this.minDate, end: this.minDate}]
    });
  }

  ngOnInit() {
    this.serviceHebergement.getLogements(this.data).subscribe(r => {
      this.logements = this.getLogementList(r['Logements']);
    });
    this.form.controls['date'].valueChanges.subscribe(value => {
      this.dateRes.dateDebut = this.datePipe.transform(value? value.begin: null);
      this.dateRes.dateFin = this.datePipe.transform(value? value.end: null);
      console.log('value', this.dateRes.dateDebut, this.dateRes.dateFin);
    });
  }
  onChangeDate() {

  }
  getLogementList = (logementList) => {
    const logements = logementList.map(l => JSON.stringify({...l, numLogement: 1}));
    let res = {};
    logements.forEach((x, i) => { res[x] = { ...logementList[i], count: ((res[x] && res[x].count) || 0)+1 }; });
    return Object.keys(res).map(e => res[e]);
  }
 }
