import { Component, OnInit } from '@angular/core';
import {ReservationService} from "../../services/reservation.service";

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  reservations = [{
    id: 123123,
    date: '2020-01-27',
    montantTotal: 200,
    festivals: [
    {
      "id": 1,
      "nom": "CirqueCampus",
      "dateDebut": "2020-01-01",
      "dateFin": "2020-01-30",
      "siteWeb": "wwww.google.com",
      "typeF": "Cirque",
      "ville": "Nice",
      "pays": "France",
      "capaciteCategorie1": 15,
      "capaciteCategorie2": 20,
      "capaciteCategorie0": 10,
      "noteMoyenne": 0,
      "tarifCategorie1": 30,
      "tarifCategorie2": 40,
      "tarifCategorie0": 20,
      "etat": "Disponible",
      "longitude": 0,
      "latitude": 0,
      "numOrganisateur": 1
    },
    {
      "id": 2,
      "nom": "TheatreCampus",
      "dateDebut": "2020-02-01",
      "dateFin": "2020-02-26",
      "siteWeb": "wwww.theatreuga.com",
      "typeF": "Theatre",
      "ville": "Grenoble",
      "pays": "France",
      "capaciteCategorie1": 15,
      "capaciteCategorie2": 20,
      "capaciteCategorie0": 10,
      "noteMoyenne": 0,
      "tarifCategorie1": 30,
      "tarifCategorie2": 40,
      "tarifCategorie0": 20,
      "etat": "Disponible",
      "longitude": 0,
      "latitude": 0,
      "numOrganisateur": 1
    },
    {
      "id": 3,
      "nom": "Mini Film",
      "dateDebut": "2020-03-01",
      "dateFin": "2020-03-30",
      "siteWeb": "wwww.facelook.com",
      "typeF": "Audio Visuel",
      "ville": "Valence",
      "pays": "France",
      "capaciteCategorie1": 10,
      "capaciteCategorie2": 20,
      "capaciteCategorie0": 5,
      "noteMoyenne": 0,
      "tarifCategorie1": 30,
      "tarifCategorie2": 40,
      "tarifCategorie0": 20,
      "etat": "Disponible",
      "longitude": 0,
      "latitude": 0,
      "numOrganisateur": 2
    },
    {
      "id": 4,
      "nom": "Danse Party",
      "dateDebut": "2020-04-01",
      "dateFin": "2020-04-30",
      "siteWeb": "wwww.danselook.com",
      "typeF": "Danse",
      "ville": "Tourcoin",
      "pays": "France",
      "capaciteCategorie1": 10,
      "capaciteCategorie2": 20,
      "capaciteCategorie0": 5,
      "noteMoyenne": 0,
      "tarifCategorie1": 30,
      "tarifCategorie2": 40,
      "tarifCategorie0": 20,
      "etat": "Disponible",
      "longitude": 0,
      "latitude": 0,
      "numOrganisateur": 2
    },
    {
      "id": 0,
      "nom": "CirqueCampus",
      "dateDebut": "2020-01-01",
      "dateFin": "2020-01-30",
      "siteWeb": "wwww.google.com",
      "typeF": "Cirque",
      "ville": "Nice",
      "pays": "France",
      "capaciteCategorie1": 15,
      "capaciteCategorie2": 20,
      "capaciteCategorie0": 10,
      "noteMoyenne": 10,
      "tarifCategorie1": 30,
      "tarifCategorie2": 40,
      "tarifCategorie0": 20,
      "etat": "Disponible",
      "longitude": 0,
      "latitude": 0,
      "numOrganisateur": 1
    }
  ],
  logements: []}];
  resAnnulees = [];
  resFutur = [];
  resPassees = [];
  commentable:boolean = false;
  constructor(private serviceRes: ReservationService) {
    this.resPassees = this.reservations;
  }

  ngOnInit() {
/*    this.serviceRes.getAllRes().subscribe(r => {
      this.reservations = r[''];
    });
    this.serviceRes.getResFutur().subscribe(r => {
      this.resFutur = r[''];
    });
    this.serviceRes.getResAnnulees().subscribe(r => {
      this.resAnnulees = r[''];
    });
    this.serviceRes.getResPassees().subscribe(r => {
      this.resPassees = r[''];
    });*/
  }


}
