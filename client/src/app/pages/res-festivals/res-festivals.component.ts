import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FestivalService} from "../../services/festival.service";
import {DateFormatPipe} from "../../utils/date-format.pipe";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ReservationService} from "../../services/reservation.service";
import {DialogMsgComponent} from "../dialog-msg/dialog-msg.component";
import {MatDialog} from "@angular/material";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-res-festivals',
  templateUrl: './res-festivals.component.html',
  styleUrls: ['./res-festivals.component.scss'],
  providers:[DateFormatPipe]
})
export class ResFestivalsComponent implements OnInit {
  id;
  festival;
  form: FormGroup;
  nbCategorie0;
  nbCategorie1;
  nbCategorie2;
  minDate = new Date(Date());
  maxDate = new Date(2021, 0, 1);
  dateDebut;
  dateFin;
  constructor(private router: Router, private route: ActivatedRoute, private serviceReservation: ReservationService,
              private serviceFestival: FestivalService, fb: FormBuilder, private datePipe: DateFormatPipe, public dialog: MatDialog, private dataService: DataService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.form = fb.group({
      date: [{begin: this.minDate, end: this.minDate}]
    });
  }
  ngOnInit() {
    this.serviceFestival.getFestivalById(this.id).subscribe(r => {
      this.festival = r['festival'];
    });
    this.form.controls['date'].valueChanges.subscribe(value => {
      this.dateDebut = this.datePipe.transform(value? value.begin: null);
      this.dateFin = this.datePipe.transform(value? value.end: null);
      console.log('value', this.dateDebut, this.dateFin);
    });
  }
  transformDate(date){
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }
  ajouterPanier(){
    console.log('numU', 1);
    console.log('numF', this.id);
    console.log('nbC0', this.nbCategorie0);
    console.log('nbC1', this.nbCategorie1);
    console.log('nbC2', this.nbCategorie2);
    console.log('dateDebut', this.dateDebut);
    console.log('dateFin', this.dateFin);
    this.serviceReservation.addFestival(localStorage.getItem('userId'), this.id, this.nbCategorie0?this.nbCategorie0: 0, this.nbCategorie1? this.nbCategorie1: 0, this.nbCategorie2? this.nbCategorie2: 0, this.dateDebut? this.dateDebut : this.datePipe.transform(this.minDate),this.dateFin? this.dateFin: this.datePipe.transform(this.minDate)).subscribe(
      data => {
        this.dataService.refreshPanier();
        this.openMsgDialog(false,'Vous avez ajouté le festival avec succès! Vous pouvez choisir un logement autour! ');
      },
      error => {

        if (error){
          if (error && error.error && error.message) {
            this.openMsgDialog(true, error.error.message);
          } else {
            this.openMsgDialog(true, 'Contraintes non-respectées!');
          }
        }
      }

    );

  }
  openMsgDialog(error, msg): void {
    const dialogRef = this.dialog.open(DialogMsgComponent, {
      width: '400px',
      data: {
        error: error,
        msg: msg
      }
    });
    if (!error) {
      dialogRef.afterClosed().subscribe(result => {
        this.router.navigateByUrl('hebergements?ville='+this.festival.ville);
      });
    }

  }
  onChangeCategorie0(value){
    this.nbCategorie0 = value;
    console.log('nbC0', this.nbCategorie0);
  }
  onChangeCategorie1(value){
    this.nbCategorie1 = value;
  }
  onChangeCategorie2(value){
    this.nbCategorie2 = value;
    console.log('value', value);

    console.log('nbC0', this.nbCategorie2);
  }
}
