import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResFestivalsComponent } from './res-festivals.component';

describe('ResFestivalsComponent', () => {
  let component: ResFestivalsComponent;
  let fixture: ComponentFixture<ResFestivalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResFestivalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResFestivalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
