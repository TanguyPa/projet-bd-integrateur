import {Component, OnInit} from '@angular/core';
import {FestivalService} from "../../services/festival.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {GeoDataService} from "../../services/geo-data.service";
import {DateFormatPipe} from "../../utils/date-format.pipe";

@Component({
  selector: 'app-list-festival',
  templateUrl: './list-festival.component.html',
  styleUrls: ['./list-festival.component.scss'],
  providers:[DateFormatPipe]
})
export class ListFestivalComponent implements OnInit {
  numPage = 0;
  taillePage = 20;
  festivals;
  nomControl = new FormControl();
  villeControl = new FormControl();
  filteredOptionsVille;
  filteredOptionsNom;
  inlineRange;
  ville = '';
  nom = '';
  form: FormGroup;
  dateDebut;
  dateFin;
  categorie = '';
  categories = [];
  minDate = new Date(Date());
  maxDate = new Date(2021, 0, 1);
  constructor(private serviceFestival: FestivalService, private serviceVille: GeoDataService, fb: FormBuilder, private datePipe: DateFormatPipe) {
    this.form = fb.group({
      date: [{begin: this.minDate, end: this.minDate}]
    });

  }


  ngOnInit() {
    this.serviceFestival.getDomaines().subscribe(r => {
      this.categories = r['domaines'];
    });
    this.form.controls['date'].valueChanges.subscribe(value => {

      this.dateDebut = this.datePipe.transform(value? value.begin: null);
      this.dateFin = this.datePipe.transform(value? value.end: null);

      console.log('value', this.dateDebut, this.dateFin);
      this.refresh();
    });
    this.villeControl.valueChanges
      .subscribe({
        next: (value) => {
          this.serviceVille.getVilles(value).subscribe((r) => {
            this.filteredOptionsVille = r;
            this.refresh();
          });
        }
      });
    this.nomControl.valueChanges
      .subscribe({
        next: (value) => {
          this.serviceFestival.getFestivals(this.taillePage, this.numPage, value).subscribe((r) => {
            this.filteredOptionsNom = r['festivals'];
            this.refresh();
          });
        }
      });
    this.refresh();
  }

  refresh() {
    this.serviceFestival.getFestivals(this.taillePage, this.numPage, this.nom, this.ville, this.categorie, this.dateDebut, this.dateFin).subscribe(r => {
      this.festivals = r['festivals'];
    });
  }
  onChangeVille(ville) {
    this.ville = ville;
  }
  onChangeNom(nom) {
    this.nom = nom;
  }
  onChangeCategorie(categorie) {
    this.categorie = categorie;
    this.refresh();
  }
}
