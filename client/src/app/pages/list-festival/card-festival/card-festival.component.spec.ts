import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardFestivalComponent } from './card-festival.component';

describe('CardFestivalComponent', () => {
  let component: CardFestivalComponent;
  let fixture: ComponentFixture<CardFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
