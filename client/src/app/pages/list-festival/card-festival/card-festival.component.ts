import {Component, Input, OnInit} from '@angular/core';
import {GenerateAvatarService} from "../../../services/generate-avatar.service";

@ Component({
  selector: 'app-card-festival',
  templateUrl: './card-festival.component.html',
  styleUrls: ['./card-festival.component.scss']
})
export class CardFestivalComponent implements OnInit {
  @Input() data;
  img;
  imageToShow: any;
  isImageLoading;
  constructor(private serviceImgGenerator: GenerateAvatarService) { }

  ngOnInit() {
    this.isImageLoading = true;
    this.serviceImgGenerator.getAvatar(this.data.nom).subscribe(r => {
      this.createImageFromBlob(r);
      this.isImageLoading = false;
    }, error => {
      this.isImageLoading = false;
      console.log(error);
    });
  }

  getImg(img){
    let b64Response = btoa(this.img);
    let outputImg = document.createElement('img');
    outputImg.src = 'data:image/png;base64,'+b64Response;
    this.img = outputImg;
  }

  transformDate(date){
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
