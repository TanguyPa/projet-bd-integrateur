import {Component, OnInit, ViewChild} from '@angular/core';
import {DateFormatPipe} from "../../utils/date-format.pipe";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {MatPaginator} from "@angular/material";
import {DialogMsgComponent} from "../dialog-msg/dialog-msg.component";
import {HebergementService} from "../../services/hebergement.service";
import {GeoDataService} from "../../services/geo-data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list-hebergement',
  templateUrl: './list-hebergement.component.html',
  styleUrls: ['./list-hebergement.component.scss'],
  providers:[DateFormatPipe]

})
export class ListHebergementComponent implements OnInit {
  latitude = -28.68352;
  longitude = -147.20785;
  hebergements;
  ville;
  map = false;
  minDate = new Date(Date());
  maxDate = new Date(2021, 0, 1);
  form: FormGroup;
  villeControl = new FormControl();
  filteredOptionsVille;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  constructor(private router: Router, fb: FormBuilder, private datePipe: DateFormatPipe, private serviceHebergement: HebergementService, private serviceVille: GeoDataService) {
    this.form = fb.group({
      date: [{begin: this.minDate, end: this.minDate}]
    });
    this.ville = this.router.getCurrentNavigation()
      .extractedUrl.queryParams ? this.router.getCurrentNavigation().extractedUrl.queryParams.ville : '';
  }

  ngOnInit() {
    this.refresh();
    this.villeControl.valueChanges
      .subscribe({
        next: (value) => {
          this.serviceVille.getVilles(value).subscribe((r) => {
            this.filteredOptionsVille = r;
            this.refresh();
          });
        }
      });
  }

  refresh() {
    this.serviceHebergement.getHebergements(this.ville? this.ville: '').subscribe(r => {
      this.hebergements = r['hebergements'].map(h => ({...h, opacity: 0.5}));

      if (this.hebergements.length > 0) {
        this.latitude = +this.hebergements[0].latitude;
        this.longitude = +this.hebergements[0].longitude;
        this.hebergements[0].opacity = 1;
      }
    });
  }
  onChangeVille(ville) {
    this.ville = ville;
  }
  handleClickMarker = (h) => {
    console.log('event', h);
    this.hebergements.forEach(h => h.opacity = 0.5);
    h.opacity = 1;
    this.hebergements[this.hebergements.indexOf(h)] = this.hebergements[0];
    this.hebergements[0] = h;
  }

}
