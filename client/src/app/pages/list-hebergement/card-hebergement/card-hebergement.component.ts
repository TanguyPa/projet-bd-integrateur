import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-hebergement',
  templateUrl: './card-hebergement.component.html',
  styleUrls: ['./card-hebergement.component.scss']
})
export class CardHebergementComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
