import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardHebergementComponent } from './card-hebergement.component';

describe('CardHebergementComponent', () => {
  let component: CardHebergementComponent;
  let fixture: ComponentFixture<CardHebergementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardHebergementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardHebergementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
