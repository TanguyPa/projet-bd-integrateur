import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
export interface DialogErrorData {
  error: boolean,
  msg: string;
}
@Component({
  selector: 'app-dialog-msg',
  templateUrl: './dialog-msg.component.html',
  styleUrls: ['./dialog-msg.component.scss']
})
export class DialogMsgComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private data: DialogErrorData,
              private dialogRef: MatDialogRef<DialogMsgComponent>) {
    this.dialogRef.updateSize('400vw','400vw');
  }

  ngOnInit() {
  }
  onConfirmClick(): void {
    this.dialogRef.close(true);
  }
}
