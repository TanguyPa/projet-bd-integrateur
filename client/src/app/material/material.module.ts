import {
  MatButtonModule, MatCheckboxModule, MatExpansionModule, MatGridListModule, MatSidenavModule, MatListModule,
  MatDialogModule,
  MatToolbarModule, MatStepperModule, MatPaginatorModule, MatMenuModule, MatSelectModule, MatInputModule,
  MatBadgeModule, MatNativeDateModule,
  MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatTabsModule, MatProgressSpinnerModule,
  MatAutocompleteModule, MatSnackBarModule, MatDatepickerModule, MatSlideToggleModule
} from '@angular/material';

import {NgModule} from '@angular/core';

@NgModule({
  imports: [MatProgressSpinnerModule, MatButtonModule, MatBadgeModule, MatDialogModule, MatSnackBarModule, MatDatepickerModule,
    MatCheckboxModule, MatExpansionModule, MatSidenavModule, MatToolbarModule, MatMenuModule, MatAutocompleteModule, MatNativeDateModule,
    MatGridListModule, MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatTabsModule, MatStepperModule, MatPaginatorModule,
    MatSelectModule, MatInputModule, MatSlideToggleModule],
  exports: [MatProgressSpinnerModule, MatButtonModule, MatNativeDateModule, MatCheckboxModule, MatExpansionModule, MatGridListModule, MatSidenavModule, MatSnackBarModule,
    MatListModule, MatMenuModule, MatSelectModule, MatAutocompleteModule, MatInputModule, MatBadgeModule, MatDialogModule, MatDatepickerModule, MatSlideToggleModule,
    MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatToolbarModule, MatTabsModule, MatStepperModule, MatPaginatorModule]
})
export class MaterialModule {

}
