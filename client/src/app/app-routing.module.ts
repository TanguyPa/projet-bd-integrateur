import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccueilComponent} from "./pages/accueil/accueil.component";
import {ListFestivalComponent} from "./pages/list-festival/list-festival.component";
import {ListHebergementComponent} from "./pages/list-hebergement/list-hebergement.component";
import {ResFestivalsComponent} from "./pages/res-festivals/res-festivals.component";
import {ReservationComponent} from "./pages/reservation/reservation.component";
import {ValidationPanierComponent} from "./pages/validation-panier/validation-panier.component";
import {ResHebergementsComponent} from "./pages/res-hebergements/res-hebergements.component";

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'festivals', component: ListFestivalComponent },
  { path: 'hebergements', component: ListHebergementComponent },
  { path: 'festivals/:id', component: ResFestivalsComponent },
  { path: 'hebergements/:id', component: ResHebergementsComponent },
  { path: 'reservation', component: ReservationComponent },
  { path: 'validPanier/:id', component: ValidationPanierComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
