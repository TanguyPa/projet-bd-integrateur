import {Component, ViewChild} from '@angular/core';
import {SidebarService} from "./services/sidebar.service";
import {MatSidenav} from "@angular/material";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
  @ViewChild('sidenav',{static: false}) public sidenav: MatSidenav;
  constructor(private sidenavService: SidebarService){
    this.sidenavService.setSidenav(this.sidenav);
  }
  toggle(){
    this.sidenav.toggle();
  }
  showPanier(){
    this.sidenav.open();
  }
  hidePanier(){
    this.sidenav.close();
  }
}
