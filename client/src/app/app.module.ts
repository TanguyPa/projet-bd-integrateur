import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import {MegaMenuModule} from 'primeng/megamenu';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import { PanierComponent } from './partials/panier/panier.component';
import { ListFestivalComponent } from './pages/list-festival/list-festival.component';
import { ListHebergementComponent } from './pages/list-hebergement/list-hebergement.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from "./material/material.module";
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {CardFestivalComponent} from "./pages/list-festival/card-festival/card-festival.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SidebarModule} from 'primeng/sidebar';
import { ResFestivalsComponent } from './pages/res-festivals/res-festivals.component';
import {environment} from "../environments/environment";
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
import { CardHebergementComponent } from './pages/list-hebergement/card-hebergement/card-hebergement.component';
import { ResHebergementsComponent } from './pages/res-hebergements/res-hebergements.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ReservationComponent } from './pages/reservation/reservation.component';
import { OrganisateurComponent } from './pages/organisateur/organisateur.component';
import { HebergeurComponent } from './pages/hebergeur/hebergeur.component';
import { DateFormatPipe } from './utils/date-format.pipe';
import { DialogMsgComponent } from './pages/dialog-msg/dialog-msg.component';
import { DialogAvisComponent } from './pages/dialog-avis/dialog-avis.component';
import { PackageReservationComponent } from './pages/reservation/package-reservation/package-reservation.component';
import { PanierEleComponent } from './partials/panier/panier-ele/panier-ele.component';
import { PanierEleLogementComponent } from './partials/panier/panier-ele-logement/panier-ele-logement.component';
import { ValidationPanierComponent } from './pages/validation-panier/validation-panier.component';
import { LogementComponent } from './pages/res-hebergements/logement/logement.component';
import { ListLogementsComponent } from './pages/res-hebergements/list-logements/list-logements.component';

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      scopes: [
        'public_profile',
        'email',
        'user_likes',
        'user_friends'
      ],
      customParameters: {
        'auth_type': 'reauthenticate'
      },
      provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID
    },
    firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    firebase.auth.GithubAuthProvider.PROVIDER_ID,
    {
      requireDisplayName: false,
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID
    },
    firebase.auth.PhoneAuthProvider.PROVIDER_ID,
    firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  ],
  tosUrl: '<your-tos-link>',
  privacyPolicyUrl: '<your-privacyPolicyUrl-link>',
  credentialHelper: firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
};

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    HeaderComponent,
    FooterComponent,
    PanierComponent,
    ListFestivalComponent,
    ListHebergementComponent,
    CardFestivalComponent,
    ResFestivalsComponent,
    CardHebergementComponent,
    ResHebergementsComponent,
    ReservationComponent,
    OrganisateurComponent,
    HebergeurComponent,
    DateFormatPipe,
    DialogMsgComponent,
    DialogAvisComponent,
    PackageReservationComponent,
    PanierEleComponent,
    PanierEleLogementComponent,
    ValidationPanierComponent,
    LogementComponent,
    ListLogementsComponent,
  ],
  imports: [
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MegaMenuModule,
    SidebarModule,
    MaterialModule,
    BrowserModule,
    SatDatepickerModule,
    SatNativeDateModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsApiKey
      /* apiKey is required, unless you are a
      premium customer, in which case you can
      use clientId
      */
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    DialogMsgComponent,
    DialogAvisComponent
  ],
  entryComponents: [
    DialogMsgComponent,
    DialogAvisComponent
  ],
})
export class AppModule { }
