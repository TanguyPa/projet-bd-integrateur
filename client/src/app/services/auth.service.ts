import { Injectable } from '@angular/core';
// import {auth, User} from 'firebase';
import {environment} from '../../environments/environment';
import {FirebaseAuth, FirebaseFirestore} from '@angular/fire';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/index";

const URL = environment.api + '/authentification';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private fireStore: FirebaseFirestore;

  constructor(private afAuth: AngularFireAuth, private http: HttpClient) {
    // Initialize Cloud Firestore through Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: environment.firebase.apiKey,
        authDomain: environment.firebase.authDomain,
        projectId:  environment.firebase.projectId
      });
    }
    this.fireStore = firebase.firestore();
  }

  authentificationServer(numUser: string, nom: string, prenom: string, tel: string, mail: string): Observable<any> {
    let params = new HttpParams();
    params = params.set('numP', numUser);
    params = params.set('nom', nom);
    params = params.set('prenom', prenom);
    params = params.set('telephone', tel);
    params = params.set('mail', mail);
    console.log(params);
    return this.http.post(URL, '',{params: params});
  }

}
