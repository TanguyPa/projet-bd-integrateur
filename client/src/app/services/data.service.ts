import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private panierRefreshCallback = () => { };

  constructor() { }

  setPanierRefreshCallback (callBack) {
    this.panierRefreshCallback = callBack;
  }

  refreshPanier() {
    this.panierRefreshCallback();
  }
}
