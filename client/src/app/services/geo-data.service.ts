import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/index";

const URL_VILLE = 'https://geo.api.gouv.fr/communes';

@Injectable({
  providedIn: 'root'
})

export class GeoDataService {
  constructor(private http: HttpClient) {
  }

  getVilles(query): Observable<any> {
    let params = new HttpParams();
    params = params.set('fields', 'nom');
    params = params.set('nom', query);
    return this.http.get(URL_VILLE, {params: params});
  }
}
