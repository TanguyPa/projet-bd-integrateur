import { TestBed } from '@angular/core/testing';

import { GenerateAvatarService } from './generate-avatar.service';

describe('GenerateAvatarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenerateAvatarService = TestBed.get(GenerateAvatarService);
    expect(service).toBeTruthy();
  });
});
